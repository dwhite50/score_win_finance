Unit I_FCommon;

{$DEFINE indatabase}
{$DEFINE isnoattend}
{$DEFINE isfinance}

Interface
Uses fDbase;
function TestFFlags(const Flag: word): boolean;
procedure SetFFlags(const Flag: word; const on: boolean);
procedure CheckFtinfo(var Ftinfo: Ftinfo_rec; const DBSize: integer);
Function CustomFinanceEdit(Const Scrno, Fld: Integer; Var Field: OpenString;
  Const DoEdit: boolean; Var Display_Rec: Boolean;
  Const Func: char; Sender: TObject): boolean;
function GetTTInd(const tt: String):byte;
function CalcUSamountForOfs(const showit: boolean): real;
{$I tdrank.inc}
{$I I_TFVars.inc}
{$I acblrank.dcl}
{$I ffflag.inc}

Var
  total_changed: boolean;

Implementation

Uses Windows, Messages, STSTRS, SysUtils, Dbase, DB33, CSDef, Util1, Util2, YesNoBoxU, PVIO,
VListBox, History,groups,FormEdit;

{$I STSTRS.dcl}
{$I Fkeys.inc}
{$I tfixdb.inc}
{$I I_TFinan.inc}
{$I showpts.inc}

End.

