Unit formVoucher;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ovcbase, ovcdlg, ovcclkdg, StdCtrls, Mask, Buttons,
  ToolWin, ExtCtrls, ActnMan, ActnCtrls, Menus, ActnList, ovcef, ovcsf,
  ovcrvcbx, ovcpb, ovcpf, ovcnf, ovceditf, ovcedpop, ovcedcal, ovcbordr,
  ovcbcalc, ovcbcldr, XPStyleActnCtrls, Grids, ImgList, ResizeKit, LMDCustomComponent,
  LMDWndProcComponent, LMDFormShape, LMDShapeHint,
  LMDHint, LMDCustomScrollBox, LMDListBox, LMDCustomControl, LMDCustomPanel,
  LMDCustomBevelPanel, LMDBaseEdit, LMDCustomEdit, LMDCustomMaskEdit, LMDCustomExtCombo,
  LMDCustomListComboBox, LMDListComboBox, ovccmbx, LMDHeaderListComboBox,
  LMDCustomComboBox, LMDComboBox, STSTRS, LMDIniCtrl, LMDFormA,
  LMDStorBase, LMDStorXMLVault, LMDStorBinVault, LMDCustomHint,
  LMDCustomShapeHint, LMDMessageHint, LibData, LMDCalendarEdit,
  LMDCustomButton, LMDButton, fDbase, Dbase, DB33, LibUtil, Clipbrd, CSDef,
  Util1, Util2, YesNoBoxU, PVIO, VListBox, History, LMDCustomParentPanel,
  LMDCustomGroupBox, LMDGroupBox, LibFinance, LMDSimplePanel, I_FCommon,hh;

Type
  TfrmVoucher = Class(TForm)
    GBMenu: TGroupBox;
    Shape23: TShape;
    ButNext: TButton;
    ButPrev: TButton;
    ButFind: TButton;
    ButTop: TButton;
    ButLast: TButton;
    ButEdit: TButton;
    ButAdd: TButton;
    ButCopy: TButton;
    ButDelete: TButton;
    ButQuit: TButton;
    ButtonF7Key: TButton;
    ButtonF6Key: TButton;
    ButtonF8Key: TButton;
    ButtonF2Key: TButton;
    StaticTextF3Key: TStaticText;
    StaticTextF4Key: TStaticText;
    StaticTextF5Key: TStaticText;
    StaticTextF2Key: TStaticText;
    ButtonF1Key: TButton;
    StaticTextF1Key: TStaticText;
    ActionManager1: TActionManager;
    ButNextAction: TAction;
    ButPrevAction: TAction;
    ButFindAction: TAction;
    ButTopAction: TAction;
    ButLastAction: TAction;
    ButEditAction: TAction;
    ButAddAction: TAction;
    ButCopyAction: TAction;
    ButDeleteAction: TAction;
    ButQuitAction: TAction;
    F4Key: TAction;
    F3Key: TAction;
    F5Key: TAction;
    F2Key: TAction;
    F7Key: TAction;
    F1Key: TAction;
    F8Key: TAction;
    F6Key: TAction;
    CtrlCKey: TAction;
    ButPlus: TButton;
    StaticText1: TStaticText;
    PlusKey: TAction;
    ButMinus: TButton;
    StaticText2: TStaticText;
    MinusKey: TAction;
    StatusBar1: TStatusBar;
    ResizeKit1: TResizeKit;
    OvcController1: TOvcController;
    LMDHint1: TLMDHint;
    ActionManager2: TActionManager;
    ButPlayerInfoDone: TAction;
    StaticText3: TStaticText;
    GBTournWorkSheet: TGroupBox;
    SpeedButton1: TSpeedButton;
    ListTables: TAction;
    PopupMenu1: TPopupMenu;
    TournamentInformation1: TMenuItem;
    Worksheet1: TMenuItem;
    Invoice1: TMenuItem;
    Voucher1: TMenuItem;
    Ballancesheet1: TMenuItem;
    Officefinancialpart11: TMenuItem;
    Officefinancialpart2: TMenuItem;
    Quit1: TMenuItem;
    GBTDSessions: TGroupBox;
    Label12: TLabel;
    OPFDICSessions: TOvcPictureField;
    Label13: TLabel;
    OPFStaffSessions: TOvcPictureField;
    GBTransAmount: TGroupBox;
    OPFTransAmount: TOvcPictureField;
    GBHotel: TGroupBox;
    Label7: TLabel;
    OPFHotelDays: TOvcPictureField;
    Label8: TLabel;
    OPFHotelAmount: TOvcPictureField;
    GBPerDiem: TGroupBox;
    Label9: TLabel;
    OPFPerDiemDays: TOvcPictureField;
    GBAdvanceAmount: TGroupBox;
    Label14: TLabel;
    OPFAdvanceAmount: TOvcPictureField;
    GBCPTRAmount: TGroupBox;
    Label11: TLabel;
    OPFCPTRAmount: TOvcPictureField;
    GBOther: TGroupBox;
    Label17: TLabel;
    OPFOtherAmount: TOvcPictureField;
    LMDGroupBox5: TLMDGroupBox;
    Label24: TLabel;
    Label25: TLabel;
    OSFSanctionNo: TOvcSimpleField;
    OSFTournament: TOvcSimpleField;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label10: TLabel;
    Label23: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    OSFName: TOvcSimpleField;
    OSFTDNumber: TOvcSimpleField;
    OSFTDRank: TOvcSimpleField;
    OSFEmpStatus: TOvcSimpleField;
    OSFSalaried: TOvcSimpleField;
    OSFDates: TOvcSimpleField;
    OSFPlayerNo: TOvcSimpleField;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    Exe_Info: TAction;
    LMDSimplePanel1: TLMDSimplePanel;
    Label15: TLabel;
    ONFItemType: TOvcNumericField;
    Label16: TLabel;
    ONFSubItemType: TOvcNumericField;
    Label18: TLabel;
    OSFDescription: TOvcSimpleField;
    Label19: TLabel;
    OPFItems: TOvcPictureField;
    Label20: TLabel;
    OPFItemRate: TOvcPictureField;
    Label21: TLabel;
    OPFTotal: TOvcPictureField;
    Label22: TLabel;
    OSFPaidBy: TOvcSimpleField;
    LabelUseExchangeRate: TLabel;
    OSFUseExRate: TOvcSimpleField;
    LabelExchangeAmount: TLabel;
    OPFExchangeAmount: TOvcPictureField;
    LMDTournWSDone: TLMDButton;
    LabelSubItemType: TLabel;
    LabelItemType: TLabel;
    LabelPaidBy: TLabel;
    Procedure ButQuitActionExecute(Sender: TObject);
    Procedure FormActivate(Sender: TObject);
    Procedure ListTablesExecute(Sender: TObject);
    Procedure pFillFields;
    Procedure pNextRecordf;
    Procedure pPrevRecordf;
    Procedure ButNextActionExecute(Sender: TObject);
    Procedure ButPrevActionExecute(Sender: TObject);
    Procedure ButLastActionExecute(Sender: TObject);
    Procedure ButTopActionExecute(Sender: TObject);
    Procedure ButFindActionExecute(Sender: TObject);
    Procedure PVoucherInfo(bEnable: Boolean);
    Procedure ButEditActionExecute(Sender: TObject);
    Procedure ButPlayerInfoDoneExecute(Sender: TObject);
    Procedure ButAddActionExecute(Sender: TObject);
    Procedure ButCopyActionExecute(Sender: TObject);
    Procedure ButDeleteActionExecute(Sender: TObject);
    Procedure pUpdateStatusBar;
    Procedure pDateExit(Sender: TObject);
    Procedure MinusKeyExecute(Sender: TObject);
    Procedure F1KeyExecute(Sender: TObject);
    Procedure pGetVoucherData;
    Procedure pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure PlusKeyExecute(Sender: TObject);
    Procedure F2KeyExecute(Sender: TObject);
    Procedure pPuMenuItemSelected(Sender: TObject);
    Procedure pAbortChanges;
    Procedure F8KeyExecute(Sender: TObject);
    Procedure F7KeyExecute(Sender: TObject);
    Procedure F6KeyExecute(Sender: TObject);
    Procedure pWhichScreen(oSender: TObject);
    Procedure OSFDatesExit(Sender: TObject); // Open frmOffFinP2
    Function fShowItemType(Const Item: byte): String;
    Function fShowSubItemType(Const Item, SubItem: byte): String;
    Function fValidVoucherSubItem(Const Item, SubItem: byte): boolean;
    Function fValidVoucherItem(Const Item: byte): boolean;
    Procedure FormClose(Sender: TObject; Var Action: TCloseAction);
    Function fShowPaidBy(Const Paid: String): String;
    Procedure pClearFields;
    Procedure pCopyRecData;
    Procedure pCheckEditFields;
    Procedure pSaveChanges;
    Procedure ONFItemTypeExit(Sender: TObject);
    Procedure pButtOn(bTurnOn: Boolean);
    Procedure ONFSubItemTypeEnter(Sender: TObject);
    Function fGetTDind(Const rank: String): byte;
    Function fVEditStat(Const vtype, subtype: byte; Const mask: word): boolean;
    Procedure ONFSubItemTypeExit(Sender: TObject);
    Procedure pUseExRate;
    Procedure OPFItemsExit(Sender: TObject);
    Procedure OPFTotalExit(Sender: TObject);
    Procedure OSFPaidByExit(Sender: TObject);
    Procedure OSFUseExRateExit(Sender: TObject);
    Procedure pFillStaticFields;
    Procedure pBlankVoucherRecord;
    Procedure ONFItemTypeEnter(Sender: TObject);
    Procedure OSFDescriptionEnter(Sender: TObject);
    Procedure OSFDescriptionExit(Sender: TObject);
    Procedure OPFItemsEnter(Sender: TObject);
    Procedure OPFItemRateEnter(Sender: TObject);
    Procedure OPFItemRateExit(Sender: TObject);
    Procedure OPFTotalEnter(Sender: TObject);
    Procedure OSFPaidByEnter(Sender: TObject);
    Procedure OSFUseExRateEnter(Sender: TObject);
    Procedure OPFExchangeAmountEnter(Sender: TObject);
    Procedure OPFExchangeAmountExit(Sender: TObject);
    Procedure Action1Execute(Sender: TObject);
    Procedure Action2Execute(Sender: TObject);
    Procedure pSetWinState;
    Procedure ResizeKit1ExitResize(Sender: TObject; XScale, YScale: Double);
    Procedure Action3Execute(Sender: TObject);
    Procedure Action4Execute(Sender: TObject);
    //    Procedure pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
    //    Procedure pONFieldAvail(bAvail: Boolean; oOSField: TOvcNumericField);
    //    Procedure pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
    //    Procedure pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
    //    Procedure pOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
    //    Procedure pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
    //    Procedure pLMDHCBFieldAvail(bAvail: Boolean; oOSField: TLMDHeaderListComboBox);
    Function FormHelp(Command: Word; Data: Integer; Var CallHelp: Boolean): Boolean;
    Procedure Exe_InfoExecute(Sender: TObject);
    Procedure pResetTabStops;
  private
    { Private declarations }
  public
    { Public declarations }
    bKeyIsUnique: Boolean;
  End;

Var
  frmVoucher: TfrmVoucher;
  bEscKeyUsed: Boolean;
  { Integer variable used dynamically to store the tables record count }
  iVoucherRecs: Integer;
  { Integer variable used to store the records for this FHASH Invoice Link number }
  iVoucherCount: Integer;
  ksVTempKey: keystr;
  liRecnoVO_1, liRecnoVO_2, liRecnoVO_3: longint;
  ksSaveKey: keystr;
  liCurrRec: LongInt;

Const
  iLenArray: Integer = MaxFilno;

Implementation

Uses formTblsCurrOpen, formFTInfo, tfprint, formWorkSheet, formInvoice;

{$I STSTRS.dcl}

{$R *.dfm}

Procedure TfrmVoucher.ButQuitActionExecute(Sender: TObject);
Begin
  { The user wants to Quit, change the Public Variable:  bFTQuitUsed to True }
  frmWorkSheet.bFTQuitUsed := True;
  Close;
End;

Procedure TfrmVoucher.FormActivate(Sender: TObject);
Begin
  frmWorkSheet.bWinStNorm := (frmWorkSheet.WindowState = wsNormal);
  If frmWorkSheet.bWinStNorm Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  frmWorkSheet.bWinStNorm := (WindowState = wsNormal);
  pSetPanelWidth(frmVoucher, StatusBar1);
  { TopFilNo: What is the topmost database we are using in this application. }
  TopFilNo := Ftinfo_no;
  fFilno := Finvitem_no;
  bEscKeyUsed := False;
  // -------------------------------------------------------------------------
  Application.HintHidePause := 200000;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  { Flush_DF & Flush_IF: Must be set to True for so that everytime a change is
    made to a "Record", dbase re-writes the table header and in Playern.dat,
    updates dbLastImpDate entry. Set them to False when you Delete a record,
    then immediately set them back to True }
  { This must be done as a standard whenever a table is opened for modifications }
  Flush_DF := True;
  Flush_IF := True;
  { Store the Selected Invoice Key to the Voucher Temp KeyString Variable }
  ksVTempKey := ksFInvoiceKey;

  //liRecnoVO := fRecno[fFilno];
  { Load the appropriate data }
  pGetVoucherData;
  { This variable Scrno, is used in some of the older code procedures to determine which
    calculations need to be performed based on the screen we are in.
    ----------------------------------------------------------
    Screen 1 = Tournament Information    - FTInfo.dat
    Screen 2 = Tournament Work Sheet     - FWkSheet.dat
    Screen 3 = Tournament Invoice        - FInvoice.dat
    Screen 4 = Voucher Items             - FInvItem.dat
    Screen 5 = Tournament Balance Sheet  - FBalance.dat
    Screen 6 = Office financial part 1   - FOFs.dat
    Screen 7 = Office financial part 2   - FOFs.dat }
  Scrno := 4;
  //==========================================================
  If CSDef.CFG.Tournament Then StatusBar1.Panels[0].Text := 'Tournament'
  Else StatusBar1.Panels[0].Text := 'Club';
  // ----------------------------------------------------------------------------------------------------------
  { Show the path to the table in use and the table name in StatusBar1. }
  StatusBar1.Panels[1].Text := UpperCase(Prepend + 'FInvItem.DAT');
  { Store the number of records to iVoucherRecs }
  iVoucherRecs := db33.UsedRecs(fDatF[fFilNo]^);
  { Add the record count to StatusBar1's display }
  StatusBar1.Panels[1].Text := StatusBar1.Panels[1].Text + ' - Contains ' +
    Trim(IntToStr(iVoucherRecs)) + ' Records.';
End;

Procedure TfrmVoucher.ListTablesExecute(Sender: TObject);
Begin
  frmTblsCurrOpen := TfrmTblsCurrOpen.Create(Self);
  frmTblsCurrOpen.ShowModal;
  frmTblsCurrOpen.Free;
End;

Procedure TfrmVoucher.pFillFields;
Begin
  With FINVITEM Do Begin
    // -----------------------------------------------------------------------------------------
    ONFItemType.Text := ITEM_TYPE;
    LabelItemType.Caption := fShowItemType(Ival(item_type));
    ONFSubItemType.Text := SUB_ITEM_TYPE;
    LabelSubItemType.Caption := fShowSubItemType(Ival(ONFItemType.Text), Ival(ONFSubItemType.Text));
    OSFDescription.Text := Trim(DESCRIPTION);
    OPFItems.Text := ITEMS;
    OPFItemRate.Text := ITEM_RATE;
    OPFTotal.Text := TOTAL;
    OSFPaidBy.Text := PAID_BY;
    LabelPaidBy.Caption := fShowPaidBy(paid_by);
    // -----------------------------------------------------------------------------------------
    { If this is a Canadian Tournament, denoted by FTInfo.USE_EX_RATE being Yes }
    OSFUseExRate.Text := USE_EX_RATE;
    OPFExchangeAmount.Text := EX_RATE_AMT;
  End;
  pUseExRate;
  // -----------------------------------------------------------------------------------------
End;

Procedure TfrmVoucher.pNextRecordf;
Begin
  fNext_Record;
  liRecnoVO := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmVoucher.ButNextActionExecute(Sender: TObject);
Begin
  pNextRecordf;
End;

Procedure TfrmVoucher.pPrevRecordf;
Begin
  { Move the record pointer in the table to the previous record and load the data in the screen fields }
  fPrev_Record;
  liRecnoVO := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmVoucher.ButPrevActionExecute(Sender: TObject);
Begin
  pPrevRecordf;
End;

Procedure TfrmVoucher.ButLastActionExecute(Sender: TObject);
Begin
  fLast_Record;
  liRecnoVO := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmVoucher.ButTopActionExecute(Sender: TObject);
Begin
  fTop_Record;
  liRecnoVO := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmVoucher.ButFindActionExecute(Sender: TObject);
Begin
  fFind_Record(fGetKey(Finvoice_no, 2), fGetKey(fFilno, 1), '', MC, 0, True);
  liRecnoVO := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmVoucher.PVoucherInfo(bEnable: Boolean);
Begin
  FreeHintWin;
  { Set the state of ActionManager1 for the GBMenu }
  If bEnable Then ActionManager1.State := asSuspended Else ActionManager1.State := asNormal;
  { Set the state of ActionManager2 for the form GBMenu }
  If bEnable Then ActionManager2.State := asNormal Else ActionManager2.State := asSuspended;
  // ----------------------------------------------------------------------
  If bEnable Then GBMenu.Hide Else GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := Not bEnable;
  // ----------------------------------------------------------------------
  GBTournWorkSheet.Enabled := bEnable;
  // ----------------------------------------------------------------------
  LMDTournWSDone.Enabled := bEnable;
  LMDTournWSDone.Visible := bEnable;
  // ----------------------------------------------------------------------
End;

Procedure TfrmVoucher.ButEditActionExecute(Sender: TObject);
Begin
  liCurrRec := frecno[fFilno];
  { In module Dbase call the procedure fPreEditSaveKeys to save the KEYS in all tables we are
    editing so the application will know if a KEY Field has been changed and Indexes need to be
    regenerated }
  fPreEditSaveKeys;
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 2;
  sDataMode := 'E';
  { Turn the Info Data Area ON }
  PVoucherInfo(True);
  { Put the cursor in the first field }
  ONFItemType.SetFocus;
End;

Procedure TfrmVoucher.ButPlayerInfoDoneExecute(Sender: TObject);
Begin
  pSaveChanges;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  pFillStaticFields;
  { Turn the Info Data Area OFF }
  PVoucherInfo(False);
  pUpdateStatusBar;
End;

Procedure TfrmVoucher.ButAddActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 1;
  sDataMode := 'A';
  fInitRecord(fFilno);
  { Clear the Edit Fields of any data }
  pFillFields;
  { Turn the Info Data Area ON }
  PVoucherInfo(True);
  { Put the cursor in the first field }
  ONFItemType.SetFocus;
End;

Procedure TfrmVoucher.ButCopyActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 3;
  sDataMode := 'C';
  fAdd_Init;
  pFillFields;
  { Turn the Info Data Area ON }
  PVoucherInfo(True);
  { Put the cursor in the first field }
  ONFItemType.SetFocus;
End;

Procedure TfrmVoucher.ButDeleteActionExecute(Sender: TObject);
Begin
  fDelete_Record(True, self.HelpContext);
  // ------------------------------------------------------------
  pUpdateStatusBar;
  // ------------------------------------------------------------
  If iVoucherRecs > 0 Then Begin
    { Move the record pointer in the table to the next available record and load the data in the screen
      fields }
    pNextRecordf;
    If ButNext.Enabled Then ButNext.SetFocus Else ButQuit.SetFocus;
  End
  Else Begin
    liRecnoVO := 0;
    pClearFields;
    // FillFields;
    pButtOn(False);
    ButAdd.SetFocus;
  End;
End;

Procedure TfrmVoucher.pUpdateStatusBar;
Begin
  iVoucherRecs := db33.UsedRecs(fDatF[fFilNo]^);
  StatusBar1.Panels[1].Text := UpperCase(dbase.Prepend + 'FINVITEM.DAT') + ' - Contains ' +
    Trim(IntToStr(iVoucherRecs)) + ' Records.';
  StatusBar1.Panels[2].Text := IntToStr(iVoucherCount) + ' vouchers';
End;

Procedure TfrmVoucher.pDateExit(Sender: TObject);
Var
  sDateText: String;
Begin
  { Replace all blanks with zeros in the date }
  (Sender As TOvcDateEdit).Text := fFillDateBlanks((Sender As TOvcDateEdit).Text);
  { Must take out the slashes before passing the date to ChkDate Function }
  sDateText := fStripDateField((Sender As TOvcDateEdit).Text);
  If Not (ChkDate(sDateText)) Then Begin
    MessageDlg('Event date is invalid.', mtError, [mbOK], 0);
    (Sender As TOvcDateEdit).SetFocus;
  End;
End;

Procedure TfrmVoucher.MinusKeyExecute(Sender: TObject);
Begin
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := 2;
  frmWorkSheet.bFTQuitUsed := False;
  Close;
End;

Procedure TfrmVoucher.F1KeyExecute(Sender: TObject);
Begin
  keybd_event(VK_F1, MapVirtualKey(VK_F1, 0), 0, 0);
End;

Procedure TfrmVoucher.pGetVoucherData;
Begin
  pFillStaticFields;
  If liRecnoVO > 0 Then
    { Fill the form fields with the Data }
    pFillFields
  Else Begin
    fTop_Record;
    If fStatus_OK(fFilno) Then Begin
      liRecnoVO := fDbase.fRecno[fFilno];
    End
    Else Begin
      liRecnoVO := 0;
      pButtOn(False);
      fInitRecord(fFilno);
      ButAdd.SetFocus;
    End;
    { Fill the form fields with the Data }
    pFillFields;
  End;
End;

Procedure TfrmVoucher.pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Var
  wKey: Word;
Begin
  OK := True;
  wKey := Key;
  Case key Of
    { 107 is the Plus Key was pressed on the numeric keypad }
    107: If GBMenu.Enabled Then PlusKeyExecute(Sender);
    { 109 is the Minus Key was pressed on the numeric keypad }
    109: If GBMenu.Enabled Then MinusKeyExecute(Sender);
    { Return key was pressed }
    VK_RETURN: If Not GBMenu.Enabled Then Begin
        key := 0;
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
      End;
    { ESCape key was pressed, Abort the record or record changes... }
    VK_ESCAPE: If Not GBMenu.Enabled Then Begin
        bEscKeyUsed := True;
        pAbortChanges;
        If Not ok Then Begin
          { frmWorkSheet.iFormTag must be set to the forms number that user selected }
          frmWorkSheet.iFormTag := 0;
          frmWorkSheet.bFTQuitUsed := False;
          Close;
        End;
      End;
    { F9 key was pressed, save the record or the changes... }
    VK_F9: If Not GBMenu.Enabled Then pSaveChanges;
  Else
    If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
      If (Key = VK_UP) Then Begin
        ButPrev.Click;
        ButPrev.SetFocus;
      End
      Else Begin
        ButNext.Click;
        ButNext.SetFocus;
      End;
      Key := 0;
    End;
  End;
End;

Procedure TfrmVoucher.pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
Var
  wKey: Word;
Begin
  { Do not trap for the Return Key here, it causes the cursor to move two fields at a time }
  wKey := Key;
  Case key Of
    { ESCape key was pressed, Abort the record or record changes... }
    VK_ESCAPE: If Not GBMenu.Enabled Then Begin
        bEscKeyUsed := True;
        pAbortChanges;
        If Not ok Then Begin
          { frmWorkSheet.iFormTag must be set to the forms number that user selected }
          frmWorkSheet.iFormTag := 0;
          frmWorkSheet.bFTQuitUsed := False;
          Close;
        End;
      End;
    { F9 key was pressed, save the record or the changes... }
    VK_F9: If Not GBMenu.Enabled Then pSaveChanges;
  Else
    If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
      If (Key = VK_UP) Then Begin
        ButPrev.Click;
        ButPrev.SetFocus;
      End
      Else Begin
        ButNext.Click;
        ButNext.SetFocus;
      End;
      Key := 0;
    End;
  End;
End;

Procedure TfrmVoucher.PlusKeyExecute(Sender: TObject);
Begin
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := 4; { 4 = Balance Sheet, frmBalance }
  frmWorkSheet.bFTQuitUsed := False;
  Close;
End;

Procedure TfrmVoucher.F2KeyExecute(Sender: TObject);
Var
  iForLoop: Integer;
Begin
  GBMenu.Enabled := False;
  For iForLoop := (PopupMenu1.Items.Count - 1) Downto 0 Do
    PopupMenu1.Items.Items[iForLoop].Enabled := True;
  PopupMenu1.Items.Items[(Scrno - 1)].Enabled := False;
  PopupMenu1.Popup(350, 500);
  GBMenu.Enabled := True;
End;

Procedure TfrmVoucher.pPuMenuItemSelected(Sender: TObject);
Begin
  { Variable iFormTag possible values and their meanings:
    0 = Tournament Information, frmTournInfo
    1 = Tournament Worksheet, frmWorkSheet
    2 = Invoice, frmInvoice
    3 = Voucher, frmVoucher
    4 = Balance Sheet, frmBalanceSheet
    5 = Office Financial Part 1, frmOffFinP1
    6 = Office Financial Part 2, frmOffFinP2 }
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := (Sender As TMenuItem).Tag;
  If ((frmWorkSheet.iFormTag >= 0) And (frmWorkSheet.iFormTag <= 6)) Then Begin
    { Store this records key to the Public Variable:  frmWorkSheet.ksFTInfoKey }
    If ksFTInfoKey = '' Then ksFTInfoKey := GetKey(FTINFO_no, 1);
    Close;
  End;
End;

Procedure TfrmVoucher.pWhichScreen(oSender: TObject);
Begin
  frmWorkSheet.sCaption := (oSender As TMenuItem).Caption;
  frmWorkSheet.sPopupItemName := frmWorkSheet.sCaption;
  MessageDlg('You selected item ' + IntToStr(frmWorkSheet.iformTag) +
    ', which is ' + frmWorkSheet.sPopupItemName, mtInformation, [mbOK], 0);
End;

Procedure TfrmVoucher.pAbortChanges;
Begin
  FreeHintWin;
  pResetTabStops;
  { Turn the Info Data Area OFF }
  PVoucherInfo(False);
  If iDataMode = 1 Then fTop_Record
  Else fGetARec(fFilNo);
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  pGetVoucherData;
End;

Procedure TfrmVoucher.F8KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (1) to Print to Screen }
  PrintReport(1);
End;

Procedure TfrmVoucher.F7KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (2) to Print Reports }
  PrintReport(2);
End;

Procedure TfrmVoucher.F6KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (3) to Print Reports to an ASCII file }
  PrintReport(3);
End;

Procedure TfrmVoucher.OSFDatesExit(Sender: TObject);
Begin
  If Trim(OSFDates.Text) = '' Then Begin
    MessageDlg('Date must not be blank!', mtError, [mbOK], 0);
    OSFDates.SetFocus;
  End;
End;

Function TfrmVoucher.fShowItemType(Const Item: byte): String;
Begin
  If ((Item > 0) And (Item <= 7)) Then
    fShowItemType := Pad(Copy(VoucherTypes[Item], 3, 14), 14)
  Else fShowItemType := '';
End;

Function TfrmVoucher.fShowSubItemType(Const Item, SubItem: byte): String;
Begin
  If fValidVoucherSubItem(Item, SubItem) Then
    fShowSubItemType := Pad(Copy(VoucherSubTypes[Item, SubItem], 3, 20), 20)
  Else fShowSubItemType := '';
End;

Function TfrmVoucher.fValidVoucherSubItem(Const Item, SubItem: byte): boolean;
Begin
  fValidVoucherSubItem := fValidVoucherItem(Item) And (SubItem > 0) And (SubItem <=
    Vsubtypes[Item]);
  if (Item = 1) and (SubItem = 3) then fValidVoucherSubItem := false;
End;

Function TfrmVoucher.fValidVoucherItem(Const Item: byte): boolean;
Begin
  fValidVoucherItem := (Item > 0) And (Item <= 7);
End;

Procedure TfrmVoucher.FormClose(Sender: TObject; Var Action: TCloseAction);
Begin
  If frmWorkSheet.iFormTag = (Scrno - 1) Then frmWorkSheet.iFormTag := 1;
  liRecnoVO := 0;
  If frmWorkSheet.bFTQuitUsed Then Begin
    { Close any open tables }
    fdBase.fCloseFiles;
    dBase.CloseFiles;
    Application.Terminate;
  End;
End;

Function TfrmVoucher.fShowPaidBy(Const Paid: String): String;
Var
  j: byte;
Begin
  For j := 1 To MaxPaidBy Do If Paid = Copy(Paidby[j], 1, 1) Then Begin
      fShowPaidBy := Pad(Copy(Paidby[j], 3, 7), 7);
      exit;
    End;
  fShowPaidBy := '';
End;

Procedure TfrmVoucher.pClearFields;
Begin
  ONFItemType.Text := '0';
  LabelItemType.Caption := '';
  ONFSubItemType.Text := '0';
  LabelSubItemType.Caption := '';
  OSFDescription.Text := '                    ';
  OPFItems.Text := '0';
  OPFItemRate.Text := '0';
  OPFTotal.Text := '0';
  OSFPaidBy.Text := ' ';
  LabelPaidBy.Caption := '';
  OSFUseExRate.Text := 'N';
  //FINVITEM.USE_EX_RATE := OSFUseExRate.Text;
  OPFExchangeAmount.Text := '0';
End;

Procedure TfrmVoucher.pCopyRecData;
Begin
  { Used when Adding or Copying a Record }
  With FINVITEM Do Begin
    FHASH := FINVOICE.FHASH;
    ITEM_TYPE := LeftPad(Trim(ONFItemType.Text),2);
    SUB_ITEM_TYPE := LeftPad(Trim(ONFSubItemType.Text),2);
    DESCRIPTION := pad(OSFDescription.Text, 20);
    ITEMS := LeftPad(Trim(OPFItems.Text),7);
    ITEM_RATE := LeftPad(Trim(OPFItemRate.Text),8);
    TOTAL := LeftPad(Trim(OPFTotal.Text),8);
    PAID_BY := Pad(OSFPaidBy.Text,1);
    USE_EX_RATE := Pad(OSFUseExRate.Text,1);
    EX_RATE_AMT := LeftPad(Trim(OPFExchangeAmount.Text),8);
  End;
End;

Procedure TfrmVoucher.pCheckEditFields;
Begin
  { Used when Editing a Record }
  // ---------------------------------------------------------------------
  With FINVITEM Do Begin
    ITEM_TYPE := LeftPad(Trim(ONFItemType.Text),2);
    SUB_ITEM_TYPE := LeftPad(Trim(ONFSubItemType.Text),2);
    DESCRIPTION := pad(OSFDescription.Text, 20);
    ITEMS := LeftPad(Trim(OPFItems.Text),7);
    ITEM_RATE := LeftPad(Trim(OPFItemRate.Text),8);
    TOTAL := LeftPad(Trim(OPFTotal.Text),8);
    PAID_BY := Pad(OSFPaidBy.Text,1);
    USE_EX_RATE := Pad(OSFUseExRate.Text,1);
    EX_RATE_AMT := LeftPad(Trim(OPFExchangeAmount.Text),8);
  End;
End;

Procedure TfrmVoucher.pSaveChanges;
Var
  ksFindKey: KeyStr;
Begin
  { Data Mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  Case iDataMode Of
    1, 3: Begin // Add or Copy Modes
        pCopyRecData;
        fdbase.fAdd_Record;
        liRecnoVO := fDbase.fRecno[fFilno];
        pButtOn(True);
        Inc(iVoucherCount);
        pUpdateStatusBar;
        FixTotals(iDataMode);
      End;
    2: Begin // Edit Mode
        pCheckEditFields;
        bKeyIsUnique := fPostEditFixKeys;
        If Not bKeyIsUnique Then Begin
          If (MessageDlg('This Voucher already exists in this database.', mtError,
            [mbOK], 0) = mrOK) Then Begin
            ONFItemType.SetFocus;
            Exit;
          End
          Else pAbortChanges;
        End
        Else Begin
          //liRecnoVO_1 := fDbase.fRecno[fFilno];
          fdbase.fPutARec(fFilNo);
          //liRecnoVO_2 := fDbase.fRecno[fFilno];
          ksSaveKey := fgetkey(fFilNo, 1);
          FixTotals(iDataMode);
          //liRecnoVO_3 := fDbase.fRecno[fFilno];
          // ----------------------------------------------------------------------------------
          { After editing, record pointer in moved to the last record, which causes an error
            Added the followings commands to correct the problem. }
          ClearKey(fIdxKey[fFilno, 1]^);
          ksFindKey := ksSaveKey;
          SearchKey(fIdxKey[fFilNo, 1]^, fRecno[fFilNo], ksFindKey);
          fRecno[fFilno] := liRecnoVO;
          fGetARec(fFilno);
          // ----------------------------------------------------------------------------------
        End;
      End;
  End;
  pFillFields;
  iDataMode := 0;
  sDataMode := null;
  pResetTabStops;
  { Turn the Player Info Data Area ON }
  PVoucherInfo(False);
End;

Procedure TfrmVoucher.pButtOn(bTurnOn: Boolean);
Begin
  // ------------------------------------------------------------
  { Buttons }
  ButCopy.Enabled := bTurnOn;
  ButCopyAction.Enabled := bTurnOn;
  ButDelete.Enabled := bTurnOn;
  ButDeleteAction.Enabled := bTurnOn;
  ButEdit.Enabled := bTurnOn;
  ButEditAction.Enabled := bTurnOn;
  ButFind.Enabled := bTurnOn;
  ButFindAction.Enabled := bTurnOn;
  ButLast.Enabled := bTurnOn;
  ButLastAction.Enabled := bTurnOn;
  ButNext.Enabled := bTurnOn;
  ButNextAction.Enabled := bTurnOn;
  ButPrev.Enabled := bTurnOn;
  ButPrevAction.Enabled := bTurnOn;
  ButTop.Enabled := bTurnOn;
  ButTopAction.Enabled := bTurnOn;
  // ------------------------------------------------------------
  { Function Keys }
  F3Key.Enabled := bTurnOn;
  F4Key.Enabled := bTurnOn;
  F5Key.Enabled := bTurnOn;
  ButtonF6Key.Enabled := bTurnOn;
  F6Key.Enabled := bTurnOn;
  ButtonF7Key.Enabled := bTurnOn;
  F7Key.Enabled := bTurnOn;
  ButtonF8Key.Enabled := bTurnOn;
  F8Key.Enabled := bTurnOn;
  // ------------------------------------------------------------
End;

Function TfrmVoucher.fGetTDind(Const rank: String): byte;
Var
  j: byte;
Begin
  For j := 1 To MaxtdRank Do If rank = Copy(TD_rank_text[j], 1, 3)
    Then Begin
      Result := j;
      exit;
    End;
  Result := 0;
End;

Function TfrmVoucher.fVEditStat(Const vtype, subtype: byte; Const mask: word): boolean;
Begin
  Result := (VoucherEditStat[vtype, subtype] And mask) <> 0;
End;

Procedure TfrmVoucher.pUseExRate;
Begin
  If UpperCase(Trim(OSFUseExRate.Text)) = 'Y' Then Begin
    OPFExchangeAmount.TabStop := True;
    OfsItemtypes := (MaxOfsItemtypes - 2);
    OPFExchangeAmount.Options := [efoCaretToEnd, efoRightAlign];
  End
  Else Begin
    OPFExchangeAmount.TabStop := False;
    OfsItemtypes := MaxOfsItemtypes;
    OPFExchangeAmount.Options := [efoCaretToEnd, efoReadOnly, efoRightAlign];
  End;
  OPFExchangeAmount.Color := clWindow;
  Refresh;
End;

Procedure TfrmVoucher.ONFSubItemTypeEnter(Sender: TObject);
Var
  sHints, sThisHint: String;
  iItemsLoop, iItemType: Integer;
  cRetLF: String;
Begin
  cRetLF := #13 + #10;
  sHints := '';
  iItemType := Ival(ONFItemType.Text);

  For iItemsLoop := 1 To MaxSubtypes Do Begin
    sThisHint := VoucherSubTypes[iItemType, iItemsLoop];
    If Trim(sThisHint) <> '' Then Begin
      If Trim(sHints) <> '' Then
        sHints := sHints + cRetLF + VoucherSubTypes[iItemType, iItemsLoop]
      Else
        sHints := VoucherSubTypes[iItemType, iItemsLoop];
    End;
  End;
  ONFSubItemType.Hint := sHints;
  // ---------------------------------------------------------------------------
  bDoEdit := True;
  FINVITEM.SUB_ITEM_TYPE := LeftPad(Trim(ONFSubItemType.Text),2);
  bDoEdit := CustomFinanceEdit(Scrno, 4, FINVITEM.SUB_ITEM_TYPE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'N');
End;

Procedure TfrmVoucher.ONFSubItemTypeExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (fValidVoucherSubItem(Ival(ONFItemType.Text), Ival(ONFSubItemType.Text))) Then Begin
        ErrBox('Invalid sub item type', mc, 0);
        ONFSubItemType.Text := '';
        ONFSubItemType.SetFocus;
        LabelSubItemType.Caption := ''
      End
      Else Begin
        FINVITEM.SUB_ITEM_TYPE := LeftPad(Trim(ONFSubItemType.Text),2);
        bDoEdit := CustomFinanceEdit(Scrno, 4, FINVITEM.SUB_ITEM_TYPE, False, bDisplay_Rec, sDataMode,
          Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'N');
      End;
    End;
  End;
End;

Procedure TfrmVoucher.pFillStaticFields;
Begin
  With FTINFO Do Begin
    OSFSanctionNo.Text := SANCTION_NO;
    OSFTournament.Text := TOURN_NAME;
  End;
  With FINVOICE Do Begin
    OSFTDNumber.Text := TD_NO;
    OSFTDRank.Text := TD_RANK;
    OSFEmpStatus.Text := EMPL_STAT;
    OSFDates.Text := DATES;
    OSFName.Text := NAME;
    OSFPlayerNo.Text := PLAYER_NO;
    OPFDicSessions.Text := Dic_Sessions;
    OPFStaffSessions.Text := Staff_Sessions;
    OPFTransAmount.Text := TRANS_AMT;
    OPFHotelDays.Text := HOTEL_DAYS;
    OPFHotelAmount.Text := HOTEL_AMT;
    OPFPerDiemDays.Text := PER_DIEM_DAYS;
    OPFAdvanceAmount.Text := Advance_amt;
    OPFCPTRAmount.Text := Cptr_amt;
    OPFOtherAmount.Text := Other_amt;
  End;
  { Only Visible if this is a Canadian Tournament, denoted by FTInfo.USE_EX_RATE being Yes }
  pUseExRate;
End;

Procedure TfrmVoucher.pBlankVoucherRecord;
Begin
  With FINVITEM Do Begin
    DESCRIPTION := '                    '; // String[20]; {C  0}
    EX_RATE_AMT := '    0.00'; // STRING[8]; {N  2}
    ITEMSx := '  0.00'; // STRING[6]; {N  1}
    ITEM_RATE := '   0.000'; // STRING[8]; {N  3}
    ITEM_TYPE := ' 0'; // STRING[2]; {N  0}
    PAID_BY := ' '; // String[1]; {C  0}
    SUB_ITEM_TYPE := ' 0'; // STRING[2]; {N  0}
    TOTAL := '    0.00'; // STRING[8]; {N  2}
    USE_EX_RATE := ' '; // String[1]; {C  0}
    ITEMS := '   0.00'; // STRING[7]; {N  2}
  End;
End;

Procedure TfrmVoucher.ONFItemTypeEnter(Sender: TObject);
Begin
  pResetTabStops;
  bDoEdit := True;
  FINVITEM.ITEM_TYPE := LeftPad(Trim(ONFItemType.Text),2);
  bDoEdit := CustomFinanceEdit(Scrno, 2, FINVITEM.ITEM_TYPE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'N');
End;

Procedure TfrmVoucher.ONFItemTypeExit(Sender: TObject);
Var
  osText: Openstring;
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (fValidVoucherItem(Ival(ONFItemType.Text))) Then Begin
        ErrBox('Invalid voucher item type', mc, 0);
        ONFItemType.Text := '';
        ONFItemType.SetFocus;
        LabelItemType.Caption := '';
      End
      Else Begin
        FINVITEM.ITEM_TYPE := LeftPad(Trim(ONFItemType.Text),2);
        bDoEdit := CustomFinanceEdit(Scrno, 2, FINVITEM.ITEM_TYPE, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'N');
      End;
    End;
  End;
End;

Procedure TfrmVoucher.OSFDescriptionEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FINVITEM.DESCRIPTION := Pad(OSFDescription.Text,20);
  bDoEdit := CustomFinanceEdit(Scrno, 6, FINVITEM.DESCRIPTION, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmVoucher.OSFDescriptionExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      FINVITEM.DESCRIPTION := Pad(OSFDescription.Text,20);
      bDoEdit := CustomFinanceEdit(Scrno, 6, FINVITEM.DESCRIPTION, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmVoucher.OPFItemsEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FINVITEM.ITEMS := LeftPad(Trim(OPFItems.Text),7);
  bDoEdit := CustomFinanceEdit(Scrno, 7, FINVITEM.ITEMS, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmVoucher.OPFItemsExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      FINVITEM.ITEMS := LeftPad(Trim(OPFItems.Text),7);
      bDoEdit := CustomFinanceEdit(Scrno, 7, FINVITEM.ITEMS, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmVoucher.OPFItemRateEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FINVITEM.ITEM_RATE := LeftPad(Trim(OPFItemRate.Text),8);
  bDoEdit := CustomFinanceEdit(Scrno, 8, FINVITEM.ITEM_RATE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmVoucher.OPFItemRateExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      FINVITEM.ITEM_RATE := LeftPad(Trim(OPFItemRate.Text),8);
      bDoEdit := CustomFinanceEdit(Scrno, 8, FINVITEM.ITEM_RATE, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmVoucher.OPFTotalEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FINVITEM.TOTAL := LeftPad(Trim(OPFTotal.Text),8);
  bDoEdit := CustomFinanceEdit(Scrno, 9, FINVITEM.TOTAL, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmVoucher.OPFTotalExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      FINVITEM.TOTAL := LeftPad(Trim(OPFTOTAL.Text),8);
      bDoEdit := CustomFinanceEdit(Scrno, 9, FINVITEM.TOTAL, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmVoucher.OSFPaidByEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FINVITEM.PAID_BY := Pad(OSFPaidBy.Text,1);
  bDoEdit := CustomFinanceEdit(Scrno, 10, FINVITEM.PAID_BY, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmVoucher.OSFPaidByExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((Trim(OSFPaidBy.Text) = 'S') Or (Trim(OSFPaidBy.Text) = 'A')) Then Begin
        ErrBox('Paid By must be (A)cbl or (S)ponser', mc, 0);
        OSFPaidBy.Text := 'S';
        OSFPaidBy.SetFocus;
      End
      Else Begin
        FINVITEM.PAID_BY := Pad(OSFPaidBy.Text,1);
        bDoEdit := CustomFinanceEdit(Scrno, 10, FINVITEM.PAID_BY, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmVoucher.OSFUseExRateEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FINVITEM.USE_EX_RATE := Pad(OSFUseExRate.Text,1);
  bDoEdit := CustomFinanceEdit(Scrno, 12, FINVITEM.USE_EX_RATE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmVoucher.OSFUseExRateExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((Trim(OSFUseExRate.Text) = 'N') Or (Trim(OSFUseExRate.Text) = 'Y')) Then Begin
        ErrBox('Apply exchange rate must be (Y)es or (N)o', mc, 0);
        //If FTInfo.USE_EX_RATE = 'Y' Then OSFUseExRate.Text := 'Y' Else OSFUseExRate.Text := 'N';
        OSFUseExRate.SetFocus;
      End
      Else Begin
        FINVITEM.USE_EX_RATE := Pad(OSFUseExRate.Text,1);
        bDoEdit := CustomFinanceEdit(Scrno, 12, FINVITEM.USE_EX_RATE, False, bDisplay_Rec, sDataMode,
          Sender);
        OPFExchangeAmount.Text := FINVITEM.EX_RATE_AMT;
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'S');
        If Trim(OSFUseExRate.Text) = 'Y' Then Begin
          OPFExchangeAmount.TabStop := True;
          OPFExchangeAmount.SetFocus;
        End;
      End;
    End;
  End;
End;

Procedure TfrmVoucher.OPFExchangeAmountEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FINVITEM.EX_RATE_AMT := LeftPad(Trim(OPFExchangeAmount.Text),8);
  bDoEdit := CustomFinanceEdit(Scrno, 13, FINVITEM.EX_RATE_AMT, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmVoucher.OPFExchangeAmountExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      FINVITEM.EX_RATE_AMT := LeftPad(Trim(OPFExchangeAmount.Text),8);
      bDoEdit := CustomFinanceEdit(Scrno, 13, FINVITEM.EX_RATE_AMT, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmVoucher.Action1Execute(Sender: TObject);
Begin
  pSetWinState;
End;

Procedure TfrmVoucher.Action2Execute(Sender: TObject);
Begin
  pSetWinState;
End;

Procedure TfrmVoucher.pSetWinState;
Begin
  If WindowState = wsMaximized Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  frmWorkSheet.bWinStNorm := (WindowState = wsNormal);
  If frmWorkSheet.bWinStNorm Then frmWorkSheet.WindowState := wsNormal
  Else frmWorkSheet.WindowState := wsMaximized;
  pSetPanelWidth(frmVoucher, StatusBar1);
End;

Procedure TfrmVoucher.ResizeKit1ExitResize(Sender: TObject; XScale,
  YScale: Double);
Begin
  pSetPanelWidth(frmVoucher, StatusBar1);
End;

Procedure TfrmVoucher.Action3Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmVoucher.Action4Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

//Procedure TfrmVoucher.pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
//Begin
//  If UpperCase(sType) = 'S' Then pOSFieldAvail(bDoedit, (oSender As TOvcSimpleField))
//  Else If UpperCase(sType) = 'P' Then pOPFieldAvail(bDoedit, (oSender As TOvcPictureField))
//  Else If UpperCase(sType) = 'C' Then pOCBFieldAvail(bDoedit, (oSender As TOvcComboBox))
//  Else If UpperCase(sType) = 'D' Then pODEFieldAvail(bDoedit, (oSender As TOvcDateEdit))
//  Else If UpperCase(sType) = 'H' Then pLMDHCBFieldAvail(bDoedit, (oSender As TLMDHeaderListComboBox))
//  Else If UpperCase(sType) = 'N' Then pONFieldAvail(bDoedit, (oSender As TOvcNumericField))
//  Else MessageDlg('No Procedure created for that type!', mtError, [mbOK], 0);
//  // -------------------------------------------------------------------------------------------------------
//  If bDisplay_Rec Then pFillFields;
//  // -------------------------------------------------------------------------------------------------------
//  If bWasOnEnter Then Begin
//    If UpperCase(sType) = 'S' Then Begin
//      If Not (oSender As TOvcSimpleField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'P') Then Begin
//      If Not (oSender As TOvcPictureField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'D') Then Begin
//      If Not (oSender As TOvcDateEdit).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'H') Then Begin
//      If Not (oSender As TLMDHeaderListComboBox).TabStop Then
//        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'C') Then Begin
//      If Not (oSender As TOvcComboBox).TabStop Then
//        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'N') Then Begin
//      If Not (oSender As TOvcNumericField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End;
//  End;
//End;
//
//Procedure TfrmVoucher.pONFieldAvail(bAvail: Boolean; oOSField: TOvcNumericField);
//Var
//  sFieldName: String;
//Begin
//  sFieldName := oOSField.Name;
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmVoucher.pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
//Var
//  sFieldName: String;
//Begin
//  sFieldName := oOSField.Name;
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoCaretToEnd, efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [efoCaretToEnd];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmVoucher.pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
//Var
//  sFieldName: String;
//Begin
//  sFieldName := oOSField.Name;
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoCaretToEnd, efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [efoCaretToEnd];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmVoucher.pOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
//Var
//  sFieldName: String;
//Begin
//  sFieldName := oOSField.Name;
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmVoucher.pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
//Var
//  sFieldName: String;
//Begin
//  sFieldName := oOSField.Name;
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.ReadOnly := True;
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.ReadOnly := False;
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmVoucher.pLMDHCBFieldAvail(bAvail: Boolean; oOSField: TLMDHeaderListComboBox);
//Var
//  sFieldName: String;
//Begin
//  sFieldName := oOSField.Name;
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.ReadOnly := True;
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.ReadOnly := False;
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;

Function TfrmVoucher.FormHelp(Command: Word; Data: Integer; Var CallHelp: Boolean): Boolean;
Begin
  if data < 500000 then
    HtmlHelp(GetDesktopWindow,DefChmFile,HH_HELP_CONTEXT,Data);
//  If data < 500000 Then WinHelp(Application.Handle, 'ACBLSCORE.HLP', Help_Context, Data);
End;

Procedure TfrmVoucher.Exe_InfoExecute(Sender: TObject);
Var
  sCD: String;
Begin
  sCD := fGetFileDate(Application.ExeName);
  MessageDlg('Name: ' + Application.ExeName + '  Date:' + sCD, mtInformation, [mbOK], 0);
End;

Procedure TfrmVoucher.pResetTabStops;
Begin
  { Reset ALL editable fields tabstop to true so fields will be available for next action: Add/Edit/Copy }
  ONFItemType.TabStop := True;
  ONFSubItemType.TabStop := True;
  OSFDescription.TabStop := True;
  OPFItems.TabStop := True;
  OPFItemRate.TabStop := True;
  OPFTotal.TabStop := True;
  OSFPaidBy.TabStop := True;
  OSFUseExRate.TabStop := True;
  OPFExchangeAmount.TabStop := True;
End;

End.

