Unit formOFSP1;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ovcbase, ovcdlg, ovcclkdg, StdCtrls, Mask, Buttons,
  ToolWin, ExtCtrls, ActnMan, ActnCtrls, Menus, ActnList, ovcef, ovcsf,
  ovcrvcbx, ovcpb, ovcpf, ovcnf, ovceditf, ovcedpop, ovcedcal, ovcbordr,
  ovcbcalc, ovcbcldr, XPStyleActnCtrls, Grids, ImgList, ResizeKit, LMDCustomComponent,
  LMDWndProcComponent, LMDFormShape, LMDShapeHint,
  LMDHint, LMDCustomScrollBox, LMDListBox, LMDCustomControl, LMDCustomPanel,
  LMDCustomBevelPanel, LMDBaseEdit, LMDCustomEdit, LMDCustomMaskEdit, LMDCustomExtCombo,
  LMDCustomListComboBox, LMDListComboBox, ovccmbx, LMDHeaderListComboBox,
  LMDCustomComboBox, LMDComboBox, STSTRS, LMDIniCtrl, LMDFormA,
  LMDStorBase, LMDStorXMLVault, LMDStorBinVault, LMDCustomHint,
  LMDCustomShapeHint, LMDMessageHint, LibData, LMDCalendarEdit,
  LMDCustomButton, LMDButton, fDbase, Dbase, DB33, LibUtil, Clipbrd, CSDef,
  Util1, Util2, YesNoBoxU, PVIO, VListBox, History, LMDCustomParentPanel,
  LMDCustomGroupBox, LMDGroupBox, LibFinance, LMDMouseBaseCtrl,
  LMDMousePositioner, I_FCommon,hh;

Type
  TfrmOFSP1 = Class(TForm)
    GBMenu: TGroupBox;
    Shape23: TShape;
    ButNext: TButton;
    ButPrev: TButton;
    ButFind: TButton;
    ButTop: TButton;
    ButLast: TButton;
    ButEdit: TButton;
    ButAdd: TButton;
    ButCopy: TButton;
    ButDelete: TButton;
    ButQuit: TButton;
    ButtonF7Key: TButton;
    ButtonF6Key: TButton;
    ButtonF8Key: TButton;
    ButtonF2Key: TButton;
    StaticTextF3Key: TStaticText;
    StaticTextF4Key: TStaticText;
    StaticTextF5Key: TStaticText;
    StaticTextF2Key: TStaticText;
    ButtonF1Key: TButton;
    StaticTextF1Key: TStaticText;
    ActionManager1: TActionManager;
    ButNextAction: TAction;
    ButPrevAction: TAction;
    ButFindAction: TAction;
    ButTopAction: TAction;
    ButLastAction: TAction;
    ButEditAction: TAction;
    ButAddAction: TAction;
    ButCopyAction: TAction;
    ButDeleteAction: TAction;
    ButQuitAction: TAction;
    F4Key: TAction;
    F3Key: TAction;
    F5Key: TAction;
    F2Key: TAction;
    F7Key: TAction;
    F1Key: TAction;
    F8Key: TAction;
    F6Key: TAction;
    CtrlCKey: TAction;
    ButPlus: TButton;
    StaticText1: TStaticText;
    PlusKey: TAction;
    ButMinus: TButton;
    StaticText2: TStaticText;
    MinusKey: TAction;
    StatusBar1: TStatusBar;
    ResizeKit1: TResizeKit;
    OvcController1: TOvcController;
    LMDHint1: TLMDHint;
    ActionManager2: TActionManager;
    ButPlayerInfoDone: TAction;
    StaticText3: TStaticText;
    GBTournWorkSheet: TGroupBox;
    LMDTournWSDone: TLMDButton;
    SpeedButton1: TSpeedButton;
    ListTables: TAction;
    PopupMenu1: TPopupMenu;
    TournamentInformation1: TMenuItem;
    Worksheet1: TMenuItem;
    Invoice1: TMenuItem;
    Voucher1: TMenuItem;
    Ballancesheet1: TMenuItem;
    Officefinancialpart11: TMenuItem;
    Officefinancialpart2: TMenuItem;
    Quit1: TMenuItem;
    LMDGroupBox1: TLMDGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    OSFDescription: TOvcSimpleField;
    LabelItemType: TLabel;
    LMDGroupBox2: TLMDGroupBox;
    Label2: TLabel;
    Label27: TLabel;
    OPFSalaried: TOvcPictureField;
    OPFItems: TOvcPictureField;
    Label4: TLabel;
    Label5: TLabel;
    OPFItemRate: TOvcPictureField;
    Label6: TLabel;
    OPFAmount: TOvcPictureField;
    Label9: TLabel;
    Label10: TLabel;
    OPFFullTime: TOvcPictureField;
    Label11: TLabel;
    Label12: TLabel;
    OPFPartTime: TOvcPictureField;
    Label13: TLabel;
    Label14: TLabel;
    OPFExtendedPartTime: TOvcPictureField;
    Label15: TLabel;
    Label16: TLabel;
    OPFSectionalSurcharge: TOvcPictureField;
    Label17: TLabel;
    Label18: TLabel;
    OPFSanctionFeeTotal: TOvcPictureField;
    Label20: TLabel;
    OPFSupplyTotal: TOvcPictureField;
    Label21: TLabel;
    Label22: TLabel;
    OPFACBLHandRecords: TOvcPictureField;
    Label25: TLabel;
    Label26: TLabel;
    OPFTransPrepaid: TOvcPictureField;
    LMDGroupBox5: TLMDGroupBox;
    Label3: TLabel;
    OSFSanctionNo: TOvcSimpleField;
    Label1: TLabel;
    OSFTournament: TOvcSimpleField;
    OSFSanctionNumberO: TOvcSimpleField;
    Label28: TLabel;
    OPFTotalTables: TOvcPictureField;
    Label29: TLabel;
    OPFSanctionFreeTables: TOvcPictureField;
    Label30: TLabel;
    OPFSanctionTables: TOvcPictureField;
    OPFSanctionFee: TOvcPictureField;
    Label31: TLabel;
    Label32: TLabel;
    OPFSupplyTables: TOvcPictureField;
    Label19: TLabel;
    OPFSupplyRate: TOvcPictureField;
    Label23: TLabel;
    Label24: TLabel;
    OPFNonMembersTotal: TOvcPictureField;
    OPFNonMembers: TOvcPictureField;
    Label33: TLabel;
    OPFNonMembersRate: TOvcPictureField;
    Label34: TLabel;
    Label35: TLabel;
    OPFMembFees: TOvcPictureField;
    Label36: TLabel;
    Label37: TLabel;
    OPFOthers: TOvcPictureField;
    Label38: TLabel;
    Label39: TLabel;
    OvcPictureField1: TOvcPictureField;
    OPFCandianMemberFees: TOvcPictureField;
    Action1: TAction;
    Action2: TAction;
    ONFItemType: TOvcNumericField;
    Action3: TAction;
    Action4: TAction;
    Procedure ButQuitActionExecute(Sender: TObject);
    Procedure FormActivate(Sender: TObject);
    Procedure ListTablesExecute(Sender: TObject);
    Procedure pFillFields;
    Procedure pNextRecordf;
    Procedure pPrevRecordf;
    Procedure ButNextActionExecute(Sender: TObject);
    Procedure ButPrevActionExecute(Sender: TObject);
    Procedure ButLastActionExecute(Sender: TObject);
    Procedure ButTopActionExecute(Sender: TObject);
    Procedure ButFindActionExecute(Sender: TObject);
    Procedure pWorkSheetInfo(bEnable: Boolean);
    Procedure ButEditActionExecute(Sender: TObject);
    Procedure ButPlayerInfoDoneExecute(Sender: TObject);
    Procedure ButAddActionExecute(Sender: TObject);
    Procedure ButCopyActionExecute(Sender: TObject);
    Procedure ButDeleteActionExecute(Sender: TObject);
    Procedure pUpdateStatusBar;
    Procedure pDateExit(Sender: TObject);
    Procedure MinusKeyExecute(Sender: TObject);
    Procedure F1KeyExecute(Sender: TObject);
    Procedure pGetOFSP1Data;
    Procedure pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure PlusKeyExecute(Sender: TObject);
    Procedure F2KeyExecute(Sender: TObject);
    Procedure pPuMenuItemSelected(Sender: TObject);
    Procedure pAbortChanges;
    Procedure F8KeyExecute(Sender: TObject);
    Procedure F7KeyExecute(Sender: TObject);
    Procedure F6KeyExecute(Sender: TObject);
    Procedure pWhichScreen(oSender: TObject); // Open frmOffFinP2
    Function fShowItemType(Const Item: byte): String;
    Function fShowSubItemType(Const Item, SubItem: byte): String;
    Function fValidVoucherSubItem(Const Item, SubItem: byte): boolean;
    Function fValidVoucherItem(Const Item: byte): boolean;
    Procedure FormClose(Sender: TObject; Var Action: TCloseAction);
    Function fShowOFSP1Item(Const Item: String): String;
    Procedure pSaveChanges;
    Procedure pCheckEditFields;
    Procedure pCopyRecData;
    Procedure pClearFields;
    Procedure OSFDescriptionExit(Sender: TObject);
    Function fValidOfsDescription: boolean;
    Procedure ONFItemTypeExit(Sender: TObject);
    Procedure pButtOn(bTurnOn: Boolean);
    Procedure pFieldCalcs;
    Procedure ONFItemTypeEnter(Sender: TObject);
    Function fOfsStat(Const vtype, mask: byte): boolean;
    Procedure pSanctionFree;
    Procedure pItemDescript(bAvail: Boolean);
    Procedure PComputerHands;
    Procedure pOther;
    Procedure OPFItemsExit(Sender: TObject);
    Procedure pItems(bAvail: Boolean);
    Procedure OPFItemRateExit(Sender: TObject);
    Procedure pItemRate(bAvail: Boolean);
    Procedure pAmount(bAvail: Boolean);
    Procedure OPFAmountExit(Sender: TObject);
    Procedure Action1Execute(Sender: TObject);
    Procedure Action2Execute(Sender: TObject);
    Procedure pSetWinState;
    Procedure ResizeKit1ExitResize(Sender: TObject; XScale, YScale: Double);
    Procedure pFillStaticFields;
    Procedure OSFDescriptionEnter(Sender: TObject);
    Procedure OPFItemsEnter(Sender: TObject);
    Procedure OPFItemRateEnter(Sender: TObject);
    Procedure OPFAmountEnter(Sender: TObject);
    Procedure Action3Execute(Sender: TObject);
    Procedure Action4Execute(Sender: TObject);
    Function FormHelp(Command: Word; Data: Integer; Var CallHelp: Boolean): Boolean;
    //    Procedure pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
    //    Procedure pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
    //    Procedure pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
    //    Procedure pOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
    //    Procedure pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
    //    Procedure pLMDHCBFieldAvail(bAvail: Boolean; oOSField: TLMDHeaderListComboBox);
    //    Procedure pONFieldAvail(bAvail: Boolean; oOSField: TOvcNumericField);
    Procedure pResetTabStops;
  private
    { Private declarations }
  public
    { Public declarations }
    { Variable to store the length of the table array }
    bKeyIsUnique: Boolean;
  End;

Var
  frmOFSP1: TfrmOFSP1;
  bEscKeyUsed: Boolean;
  bIs: Boolean;
  { Integer variable used dynamically to store the tables record count }
  iOFSP1Recs: Integer;
  { Integer variable used to store the records for this sanction number }
  iOFSP1sCount: Integer;
  ksThisTempKey: keystr;

Const
  iLenArray: Integer = MaxFilno;

Implementation

Uses formTblsCurrOpen, tfprint, formWorkSheet;

{$I STSTRS.dcl}

{$R *.dfm}

Procedure TfrmOFSP1.ButQuitActionExecute(Sender: TObject);
Begin
  { The user wants to Quit, change the Public Variable:  bFTQuitUsed to True }
  frmWorkSheet.bFTQuitUsed := True;
  Close;
End;

Procedure TfrmOFSP1.FormActivate(Sender: TObject);
Begin
  frmWorkSheet.bWinStNorm := (frmWorkSheet.WindowState = wsNormal);
  If frmWorkSheet.bWinStNorm Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  frmWorkSheet.bWinStNorm := (WindowState = wsNormal);
  pSetPanelWidth(frmOFSP1, StatusBar1);
  { TopFilNo: What is the topmost database we are using in this application. }
  TopFilNo := Ftinfo_no;
  fFilno := FOfs_no;
  bEscKeyUsed := False;
  // -------------------------------------------------------------------------
  Application.HintHidePause := 200000;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := Null;
  { Flush_DF & Flush_IF: Must be set to True for so that everytime a change is
    made to a "Record", dbase re-writes the table header and in Playern.dat,
    updates dbLastImpDate entry. Set them to False when you Delete a record,
    then immediately set them back to True }
  { This must be done as a standard whenever a table is opened for modifications }
  Flush_DF := True;
  Flush_IF := True;
  //==========================================================
  { Store the FTInfo Key variable to This Temp Key }
  ksThisTempKey := ksFTInfoKey;
  { Load the appropriate data }
  pGetOFSP1Data;
  { This variable Scrno, is used in some of the older code procedures to determine which
    calculations need to be performed based on the screen we are in.
    ----------------------------------------------------------
    Screen 1 = Tournament Information    - FTInfo.dat
    Screen 2 = Tournament Work Sheet     - FWkSheet.dat
    Screen 3 = Tournament Invoice        - FInvoice.dat
    Screen 4 = Voucher Items             - FInvItem.dat
    Screen 5 = Tournament Balance Sheet  - FBalance.dat
    Screen 6 = Office financial part 1   - FOFs.dat
    Screen 7 = Office financial part 2   - FOFs.dat }
  Scrno := 6;
  //==========================================================
  If CSDef.CFG.Tournament Then StatusBar1.Panels[0].Text := 'Tournament'
  Else StatusBar1.Panels[0].Text := 'Club';
  // ----------------------------------------------------------------------------------------------------------
  { Show the path to the table in use and the table name in StatusBar1. }
  StatusBar1.Panels[1].Text := UpperCase(Prepend + 'FOfs.DAT');
  { Store the number of records to IOFSP1Recs }
  IOFSP1Recs := db33.UsedRecs(fDatF[FOfs_no]^);
  { Add the record count to StatusBar1's display }
  StatusBar1.Panels[1].Text := StatusBar1.Panels[1].Text + ' - Contains ' +
    Trim(IntToStr(IOFSP1Recs)) + ' Records.';
End;

Procedure TfrmOFSP1.ListTablesExecute(Sender: TObject);
Begin
  frmTblsCurrOpen := TfrmTblsCurrOpen.Create(Self);
  frmTblsCurrOpen.ShowModal;
  frmTblsCurrOpen.Free;
End;

Procedure TfrmOFSP1.pFillFields;
Begin
  // -------------------------------------------------
  With FOFS Do Begin
    ONFItemType.Text := ITEM_TYPE;
    LabelItemType.Caption := fShowOFSP1Item(item_type);
    OSFDescription.Text := Trim(DESCRIPTION);
    OPFItems.Text := ITEMS;
    OPFItemRate.Text := ITEM_RATE;
    OPFAmount.Text := AMOUNT;
    OSFSanctionNumberO.Text := SANCTION_NO;
  End;
  // -------------------------------------------------
  pFillStaticFields;
End;

Procedure TfrmOFSP1.pFillStaticFields;
Var
  bShowCandian: Boolean;
Begin
  With FTINFO Do Begin
    OSFSanctionNo.Text := SANCTION_NO;
    OSFTournament.Text := TOURN_NAME;
    OPFSalaried.Text := SALARIED_FEES;
    OPFFullTime.Text := FULL_TIME_FEES;
    OPFPartTime.Text := PART_TIME_FEES;
    OPFExtendedPartTime.Text := Extended_fees;
    OPFSanctionFeeTotal.Text := SANCTION_FEE_TOTAL;
    OPFTotalTables.Text := TOTAL_TABLES;
    OPFSanctionFreeTables.Text := SANC_FREE_TABLES;
    OPFSanctionTables.Text := SANCTION_TABLES;
    OPFSanctionFee.Text := SANCTION_FEE;
    OPFSanctionFeeTotal.Text := SANCTION_FEE_TOTAL;
    OPFSupplyTables.Text := SUPPLY_TABLES;
    OPFSupplyRate.Text := SUPPLY_RATE;
    OPFSupplyTotal.Text := SUPPLY_TOTAL;
    OPFACBLHandRecords.Text := HAND_RECORD_TOTAL;
    OPFTransPrepaid.Text := TRANS_PREPAID;
    OPFNonMembers.Text := NON_MEMBERS_ACBL;
    OPFNonMembersRate.Text := NON_MEMB_ACBL_RATE;
    OPFNonMembersTotal.Text := NON_MEM_TOTAL_ACBL;
    OPFMembFees.Text := Memb_fees;
    OPFOthers.Text := ACBL_OTHER;
    // -----------------------------------------------------------------------------
    { Only Visible if this is a Canadian Tournament, denoted by Can_Memb_fees > 0 }
    OPFCandianMemberFees.Text := Can_Memb_fees;
    bShowCandian := (StrToFloat(OPFCandianMemberFees.GetStrippedEditString) > 0.00);
    OPFCandianMemberFees.Visible := bShowCandian;
    OPFCandianMemberFees.Enabled := bShowCandian;
    OPFCandianMemberFees.TabStop := bShowCandian;
    If UpperCase(Trim(EXCHANGE_RATE)) = 'Y' Then OfsItemtypes := (MaxOfsItemtypes - 2)
    Else OfsItemtypes := MaxOfsItemtypes;
  End;
End;

Procedure TfrmOFSP1.pNextRecordf;
Begin
  { Move the record pointer in the table to the next available record and load the data in the screen fields }
  fNext_Record;
  liRecnoOF := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmOFSP1.ButNextActionExecute(Sender: TObject);
Begin
  pNextRecordf;
End;

Procedure TfrmOFSP1.pPrevRecordf;
Begin
  { Move the record pointer in the table to the previous record and load the data in the screen fields }
  fPrev_Record;
  liRecnoOF := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmOFSP1.ButPrevActionExecute(Sender: TObject);
Begin
  pPrevRecordf;
End;

Procedure TfrmOFSP1.ButLastActionExecute(Sender: TObject);
Begin
  fLast_Record;
  liRecnoOF := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmOFSP1.ButTopActionExecute(Sender: TObject);
Begin
  fTop_Record;
  liRecnoOF := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmOFSP1.ButFindActionExecute(Sender: TObject);
Begin
  fFind_Record(fGetKey(Ftinfo_no, 1), fGetKey(fFilno, 1), '', MC, 0, True);
  liRecnoOF := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmOFSP1.pWorkSheetInfo(bEnable: Boolean);
Begin
  FreeHintWin;
  { Set the state of ActionManager1 for the players GBMenu }
  If bEnable Then ActionManager1.State := asSuspended
  Else ActionManager1.State := asNormal;
  { Set the state of ActionManager2 for the form players GBMenu }
  If bEnable Then ActionManager2.State := asNormal
  Else ActionManager2.State := asSuspended;
  // ----------------------------------------------------------------------
  If bEnable Then GBMenu.Hide Else GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := Not bEnable;
  // ----------------------------------------------------------------------
  GBTournWorkSheet.Enabled := bEnable;
  // ----------------------------------------------------------------------
  LMDTournWSDone.Enabled := bEnable;
  LMDTournWSDone.Visible := bEnable;
  // ----------------------------------------------------------------------
End;

Procedure TfrmOFSP1.ButEditActionExecute(Sender: TObject);
Begin
  { In module Dbase call the procedure fPreEditSaveKeys to save the KEYS in all tables we are
    editing so the application will know if a KEY Field has been changed and Indexes need to be
    regenerated }
  fPreEditSaveKeys;
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 2;
  sDataMode := 'E';
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(True);
  { Put the cursor in the first field }
  ONFItemType.SetFocus;
End;

Procedure TfrmOFSP1.ButPlayerInfoDoneExecute(Sender: TObject);
Begin
  pSaveChanges;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  pFillStaticFields;
  { Turn the Player Info Data Area OFF }
  pWorkSheetInfo(False);
  pUpdateStatusBar;
End;

Procedure TfrmOFSP1.ButAddActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 1;
  sDataMode := 'A';
  { Clear the Edit Fields of any data }
  fInitRecord(fFilno);
  pFillFields;
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(True);
  { Put the cursor in the first field }
  ONFItemType.SetFocus;
End;

Procedure TfrmOFSP1.ButCopyActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 3;
  sDataMode := 'C';
  fAdd_Init;
  pFillFields;
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(True);
  { Put the cursor in the first field }
  ONFItemType.SetFocus;
End;

Procedure TfrmOFSP1.ButDeleteActionExecute(Sender: TObject);
Begin
  fDelete_Record(True, self.HelpContext);
  pUpdateStatusBar;
  If IOFSP1Recs > 0 Then Begin
    { Move the record pointer in the table to the next available record and load the
      data in the screen fields }
    pNextRecordf;
    If ButNext.Enabled Then ButNext.SetFocus Else ButQuit.SetFocus;
  End
  Else Begin
    liRecnoOF := 0;
    pClearFields;
    pFillFields;
    pButtOn(False);
    ButAdd.SetFocus;
  End;
End;

Procedure TfrmOFSP1.pUpdateStatusBar;
Begin
  IOFSP1Recs := db33.UsedRecs(fDatF[FOfs_no]^);
  StatusBar1.Panels[1].Text := UpperCase(dbase.Prepend + 'FOfs.DAT') + ' - Contains ' +
    Trim(IntToStr(IOFSP1Recs)) + ' Records.';
  StatusBar1.Panels[2].Text := IntToStr(iOFSP1sCount) + ' items';
End;

Procedure TfrmOFSP1.pDateExit(Sender: TObject);
Var
  sDateText: String;
Begin
  { Replace all blanks with zeros in the date }
  (Sender As TOvcDateEdit).Text := fFillDateBlanks((Sender As TOvcDateEdit).Text);
  { Must take out the slashes before passing the date to ChkDate Function }
  sDateText := fStripDateField((Sender As TOvcDateEdit).Text);
  If Not (ChkDate(sDateText)) Then Begin
    MessageDlg('Event date is invalid.', mtError, [mbOK], 0);
    (Sender As TOvcDateEdit).SetFocus;
  End;
End;

Procedure TfrmOFSP1.MinusKeyExecute(Sender: TObject);
Begin
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := 4; { 4 = Balance Sheet, frmBalanceSheet }
  frmWorkSheet.bFTQuitUsed := False;
  Close;
End;

Procedure TfrmOFSP1.F1KeyExecute(Sender: TObject);
Begin
  //pRunExtProg(frmOFSP1, CFG.LoadPath + 'WUtility', 'MANUAL\CONTENTS');
  keybd_event(VK_F1, MapVirtualKey(VK_F1, 0), 0, 0);
End;

Procedure TfrmOFSP1.pGetOFSP1Data;
Begin
  If liRecnoOF > 0 Then
    { Fill the form fields with the Data }
    pFillFields
  Else Begin
    { Go to the first record }
    fTop_Record;
    If fStatus_OK(fFilno) Then Begin
      liRecnoOF := fDbase.fRecno[fFilno];
    End
    Else Begin
      liRecnoOF := 0;
      fInitRecord(fFilno);
      pButtOn(False);
      ButAdd.SetFocus;
    End;
    { Fill the form fields with the Data }
    pFillFields;
  End;
End;

Procedure TfrmOFSP1.pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Var
  wKey: Word;
Begin
  wKey := Key;
  Case key Of
    { 107 is the Plus Key was pressed on the numeric keypad }
    107: If GBMenu.Enabled Then PlusKeyExecute(Sender);
    { 109 is the Minus Key was pressed on the numeric keypad }
    109: If GBMenu.Enabled Then MinusKeyExecute(Sender);
    { Return key was pressed }
    VK_RETURN: If Not GBMenu.Enabled Then Begin
        key := 0;
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
      End;
    { ESCape key was pressed, Abort the record or record changes... }
    VK_ESCAPE: If Not GBMenu.Enabled Then Begin
        bEscKeyUsed := True;
        pAbortChanges;
        If Not ok Then Begin
          { frmWorkSheet.iFormTag must be set to the forms number that user selected }
          frmWorkSheet.iFormTag := 0;
          frmWorkSheet.bFTQuitUsed := False;
          Close;
        End;
        //Else Close;
      End;
    { F9 key was pressed, save the record or the changes... }
    VK_F9: If Not GBMenu.Enabled Then pSaveChanges;
  Else
    If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
      If (Key = VK_UP) Then Begin
        ButPrev.Click;
        ButPrev.SetFocus;
      End
      Else Begin
        ButNext.Click;
        ButNext.SetFocus;
      End;
      Key := 0;
    End;
  End;
End;

Procedure TfrmOFSP1.pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
Var
  wKey: Word;
Begin
  { Do not trap for the Return Key here, it causes the cursor to move two fields at a time }
  wKey := Key;
  Case key Of
    { ESCape key was pressed, Abort the record or record changes... }
    VK_ESCAPE: If Not GBMenu.Enabled Then Begin
        bEscKeyUsed := True;
        pAbortChanges;
        If Not ok Then Begin
          { frmWorkSheet.iFormTag must be set to the forms number that user selected }
          frmWorkSheet.iFormTag := 0;
          frmWorkSheet.bFTQuitUsed := False;
          Close;
        End;
        //Else Close;
      End;
    { F9 key was pressed, save the record or the changes... }
    VK_F9: If Not GBMenu.Enabled Then pSaveChanges;
  Else
    If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
      If (Key = VK_UP) Then Begin
        ButPrev.Click;
        ButPrev.SetFocus;
      End
      Else Begin
        ButNext.Click;
        ButNext.SetFocus;
      End;
      Key := 0;
    End;
  End;
End;

Procedure TfrmOFSP1.PlusKeyExecute(Sender: TObject);
Begin
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := 6; { 6 = Office Financial Part 2, frmOffFinP2 }
  frmWorkSheet.bFTQuitUsed := False;
  Close;
End;

Procedure TfrmOFSP1.F2KeyExecute(Sender: TObject);
Var
  iForLoop: Integer;
Begin
  GBMenu.Enabled := False;
  For iForLoop := (PopupMenu1.Items.Count - 1) Downto 0 Do
    PopupMenu1.Items.Items[iForLoop].Enabled := True;
  PopupMenu1.Items.Items[(Scrno - 1)].Enabled := False;
  PopupMenu1.Popup(350, 500);
  GBMenu.Enabled := True;
End;

Procedure TfrmOFSP1.pPuMenuItemSelected(Sender: TObject);
Begin
  { Variable iFormTag possible values and their meanings:
    0 = Tournament Information, frmTournInfo
    1 = Tournament Worksheet, frmWorkSheet
    2 = Invoice, frmInvoice
    3 = Voucher, frmVoucher
    4 = Balance Sheet, frmBalanceSheet
    5 = Office Financial Part 1, frmOffFinP1
    6 = Office Financial Part 2, frmOffFinP2 }
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := (Sender As TMenuItem).Tag;
  If ((frmWorkSheet.iFormTag >= 0) And (frmWorkSheet.iFormTag <= 6)) Then Begin
    { Store this records key to the Public Variable:  frmWorkSheet.ksFTInfoKey }
    If ksFTInfoKey = '' Then ksFTInfoKey := GetKey(FTINFO_no, 1);
    { Store the Invoice Record FHash Key to the Public Variable:  frmWorkSheet.ksFInvoiceKey
      If the Voucher Screen was selected }
    If frmWorkSheet.iFormTag = 3 Then frmWorkSheet.pGetInvoiceKey;
    Close;
  End;
End;

Procedure TfrmOFSP1.pWhichScreen(oSender: TObject);
Begin
  frmWorkSheet.sCaption := (oSender As TMenuItem).Caption;
  frmWorkSheet.sPopupItemName := frmWorkSheet.sCaption;
  MessageDlg('You selected item ' + IntToStr(frmWorkSheet.iformTag) +
    ', which is ' + frmWorkSheet.sPopupItemName, mtInformation, [mbOK], 0);
End;

Procedure TfrmOFSP1.pAbortChanges;
Begin
  FreeHintWin;
  pResetTabStops;
  { Turn the Player Info Data Area OFF }
  pWorkSheetInfo(False);
  { Fill the form fields with the Data }
  If iDataMode = 1 Then fTop_Record
  Else fGetARec(fFilNo);
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := Null;
  pFillFields;
End;

Procedure TfrmOFSP1.F8KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (1) to Print to Screen }
  PrintReport(1);
End;

Procedure TfrmOFSP1.F7KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (2) to Print Reports }
  PrintReport(2);
End;

Procedure TfrmOFSP1.F6KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (3) to Print Reports to an ASCII file }
  PrintReport(3);
End;

Function TfrmOFSP1.fShowItemType(Const Item: byte): String;
Begin
  If ((Item > 0) And (Item <= 7)) Then
    fShowItemType := Pad(Copy(VoucherTypes[Item], 3, 14), 14)
  Else fShowItemType := '';
End;

Function TfrmOFSP1.fShowSubItemType(Const Item, SubItem: byte): String;
Begin
  If fValidVoucherSubItem(Item, SubItem) Then
    fShowSubItemType := Pad(Copy(VoucherSubTypes[Item, SubItem], 3, 20), 20)
  Else fShowSubItemType := '';
End;

Function TfrmOFSP1.fValidVoucherSubItem(Const Item, SubItem: byte): boolean;
Begin
  fValidVoucherSubItem := fValidVoucherItem(Item) And (SubItem > 0) And (SubItem <=
    Vsubtypes[Item]);
End;

Function TfrmOFSP1.fValidVoucherItem(Const Item: byte): boolean;
Begin
  fValidVoucherItem := (Item > 0) And (Item <= 7);
End;

Procedure TfrmOFSP1.FormClose(Sender: TObject; Var Action: TCloseAction);
Begin
  If frmWorkSheet.iFormTag = (Scrno - 1) Then frmWorkSheet.iFormTag := 1;
  liRecnoOF := 0;
  If frmWorkSheet.bFTQuitUsed Then Begin
    { Close any open tables }
    fdBase.fCloseFiles;
    dBase.CloseFiles;
    Application.Terminate;
  End;
End;

Function TfrmOFSP1.fShowOFSP1Item(Const Item: String): String;
Var
  i: byte;
Begin
  i := Ival(Item);
  If (i > 0) And (i <= MaxOfsItemtypes) Then
    fShowOFSP1Item := Pad(Copy(OfsItemHelp[Ival(Item)], 4, 20), 20)
  Else fShowOFSP1Item := '';
End;

Procedure TfrmOFSP1.pClearFields;
Begin
  OSFSanctionNo.Text := FTINFO.SANCTION_NO;
  // -------------------------------------------------
  ONFItemType.Text := '';
  LabelItemType.Caption := '';
  OSFDescription.Text := '';
  OPFItems.Text := '0';
  OPFItemRate.Text := '0';
  OPFAmount.Text := '0';
End;

Procedure TfrmOFSP1.pCopyRecData;
Begin
  { Used when Adding or Copying a Record }
  With FOFS Do Begin
    SANCTION_NO := Pad(OSFSanctionNo.Text,10);
    // -------------------------------------------------
    ITEM_TYPE := LeftPad(Trim(ONFItemType.Text),2);
    DESCRIPTION := Pad(OSFDescription.Text,25);
    ITEMS := LeftPad(Trim(OPFItems.GetStrippedEditString),6);
    ITEM_RATE := LeftPad(Trim(OPFItemRate.GetStrippedEditString),8);
    AMOUNT := LeftPad(Trim(OPFAmount.GetStrippedEditString),9);
  End;
End;

Procedure TfrmOFSP1.pCheckEditFields;
Begin
  { Used when Editing a Record }
  // ---------------------------------------------------------------------
  With FOFS Do Begin
    ITEM_TYPE := LeftPad(Trim(ONFItemType.Text),2);
    DESCRIPTION := Pad(OSFDescription.Text,25);
    ITEMS := LeftPad(Trim(OPFItems.GetStrippedEditString),6);
    ITEM_RATE := LeftPad(Trim(OPFItemRate.GetStrippedEditString),8);
    AMOUNT := LeftPad(Trim(OPFAmount.GetStrippedEditString),9);
  End;
End;

Procedure TfrmOFSP1.pSaveChanges;
Begin
  { Data Mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  Case iDataMode Of
    1, 3: Begin // Add or Copy Modes
        pCopyRecData;
        fdbase.fAdd_Record;
        liRecnoOF := fDbase.fRecno[fFilno];
        pButtOn(True);
        Inc(iOFSP1sCount);
        pUpdateStatusBar;
        FixTotals(iDataMode);
      End;
    2: Begin // Edit Mode
        pCheckEditFields;
        bKeyIsUnique := fPostEditFixKeys;
        If Not bKeyIsUnique Then Begin
          If (MessageDlg('This Office Financial Sheet Item already exists in this database.',
            mtError, [mbOK], 0) = mrOK) Then Begin
            ONFItemType.SetFocus;
            Exit;
          End
          Else pAbortChanges;
        End
        Else Begin
          {If fValidateFields Then}
          fdbase.fPutARec(fFilNo);
          FixTotals(iDataMode);
        End;
      End;
  End;
  pFillFields;
  iDataMode := 0;
  sDataMode := null;
  pResetTabStops;
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(False);
End;

Procedure TfrmOFSP1.OSFDescriptionExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (ValidOfsDescription(ONFItemType.Text, OSFDescription.Text)) Then Begin
        ErrBox('Invalid Player Number', mc, 0);
        OSFDescription.Text := '';
        OSFDescription.SetFocus;
      End
      Else Begin
        FOFS.DESCRIPTION := Pad(OSFDescription.Text,25);
        bDoEdit := CustomFinanceEdit(Scrno, 3, FOFS.DESCRIPTION, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Function TfrmOFSP1.fValidOfsDescription: boolean;
Var
  pnum: String[7];
Begin
  Result := true;
  If Not (Ival(ONFItemType.Text) In [4, 11]) Then exit;
  If (Pos(OSFDescription.Text[2], '0123456789') > 0) Then Begin
    pnum := OSFDescription.Text;
    If Not Player_check(pnum, pnACBL) Then Result := false;
  End;
End;

Procedure TfrmOFSP1.ONFItemTypeExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (ValidOfsItem(StrToInt(ONFItemType.Text))) Then Begin
        ErrBox('Invalid Item Type', mc, 0);
        ONFItemType.Text := '';
        ONFItemType.SetFocus;
      End
      Else Begin
        { Popup boxes for the appropriate Item Description }
        //IF (Trim(FOFS.ITEM_TYPE) <> Trim(ONFItemType.Text)) Then pFieldCalcs;
        FOFS.ITEM_TYPE := LeftPad(Trim(ONFItemType.Text),2);
        bDoEdit := CustomFinanceEdit(Scrno, 1, FOFS.ITEM_TYPE, False, bDisplay_Rec, sDataMode, Sender);
        // ---------------------------------------------------------------------------
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'N');
        // ---------------------------------------------------------------------------
        If Ival(ONFItemType.Text) = 0 Then
          LabelItemType.Caption := ''
        Else
          LabelItemType.Caption := fShowOFSP1Item(FOfs.ITEM_TYPE);
      End;
    End;
  End;
End;

Procedure TfrmOFSP1.pButtOn(bTurnOn: Boolean);
Begin
  // ------------------------------------------------------------
  { Buttons }
  ButCopy.Enabled := bTurnOn;
  ButCopyAction.Enabled := bTurnOn;
  ButDelete.Enabled := bTurnOn;
  ButDeleteAction.Enabled := bTurnOn;
  ButEdit.Enabled := bTurnOn;
  ButEditAction.Enabled := bTurnOn;
  ButFind.Enabled := bTurnOn;
  ButFindAction.Enabled := bTurnOn;
  ButLast.Enabled := bTurnOn;
  ButLastAction.Enabled := bTurnOn;
  ButNext.Enabled := bTurnOn;
  ButNextAction.Enabled := bTurnOn;
  ButPrev.Enabled := bTurnOn;
  ButPrevAction.Enabled := bTurnOn;
  ButTop.Enabled := bTurnOn;
  ButTopAction.Enabled := bTurnOn;
  // ------------------------------------------------------------
  { Function Keys }
  F3Key.Enabled := bTurnOn;
  F4Key.Enabled := bTurnOn;
  F5Key.Enabled := bTurnOn;
  ButtonF6Key.Enabled := bTurnOn;
  F6Key.Enabled := bTurnOn;
  ButtonF7Key.Enabled := bTurnOn;
  F7Key.Enabled := bTurnOn;
  ButtonF8Key.Enabled := bTurnOn;
  F8Key.Enabled := bTurnOn;
  // ------------------------------------------------------------
End;

Procedure TfrmOFSP1.pFieldCalcs;
Begin
  Case Ival(ONFItemType.Text) Of
    1: pSanctionFree; {Sanction free tables}
    3: PComputerHands; {ACBL computer hands}
    5: pOther; {Other for ACBL (Charity, etc)}
  Else Begin
      OSFDescription.Text := '';
      pItemDescript(False);
    End;
  End;
End;

Procedure TfrmOFSP1.pSanctionFree;
Begin
  { Data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  If iDataMode = 2 Then SFreeid := Ival(OSFDescription.Text[1]);
  If TestFFlags(ffCharitySanc) And (SFreeid > 5) Then Dec(SFreeid, 3);
  If iDatamode <> 0 Then Begin { If editing, adding or copying }
    If TestFFlags(ffCharitySanc) Then SFreeid := PickChoice(
        NSfreeEntries, SFreeid, 'sanction free reason ', @NSFreeText, MC, false, 0)
    Else SFreeid := PickChoice(SfreeEntries, SFreeid,
        'sanction free reason ', @SFreeText, MC, false, 0);
  End;
  If SFreeid < 1 Then
    pItemDescript(False)
  Else Begin
    pItemDescript(True);
    If TestFFlags(ffCharitySanc) And (SFreeid > 2) Then Inc(SFreeid, 3);
    If SFreeid < SFreeEntries Then Begin
      If iDatamode <> 0 Then Begin { If editing, adding or copying }
        OSFDescription.Text := Pad(SFreeText[SFreeid], 25);
      End;
      pItemDescript(False);
    End
    Else pItemDescript(True);
  End;
End;

Procedure TfrmOFSP1.PComputerHands;
Begin
  HRid := PickChoice(HRentries, HRid, 'hand record size ', @HRText, MC, false, 0);
  If HRid < 1 Then Begin
    pItemDescript(False)
  End
  Else Begin
    If iDatamode <> 0 Then Begin { If editing, adding or copying }
      If HRid = HRentries Then Begin
        If Empty(OSFDescription.Text) Then OSFDescription.Text := Pad('Packages of', 25);
      End
      Else OSFDescription.Text := Pad('Packages of ' + HRText[HRid], 25);
      total_changed := Valu(OPFItemRate.Text) <> HRPrices[HRid];
      If HRid < HRentries Then OPFItemRate.Text := Fstr(HRPrices[HRid], 8, 2);
      If total_changed Then
        OPFAmount.Text := Fstr(Valu(OPFItems.Text) * Valu(OPFItemRate.Text), 9, 2);
    End;
  End;
  If HRid < HRentries Then pItemDescript(False) Else pItemDescript(True);
End;

Procedure TfrmOFSP1.pOther;
Begin
  If iDataMode = 2 Then Otherid := Ival(OSFDescription.Text[1]);
  If Otherid < 1 Then Otherid := SOtherentries;
  If iDatamode <> 0 Then Begin { If editing, adding or copying }
    Otherid := PickChoice(Otherid, 1, 'other for ACBL ', @SOtherText, MC, false, 0);
  End;
  If Otherid < 1 Then Begin
    pItemDescript(False);
  End
  Else Begin
    If Otherid < SOtherentries Then Begin
      If iDatamode <> 0 Then Begin { If editing, adding or copying }
        OSFDescription.Text := Pad(SOtherText[Otherid], 25);
      End;
      pItemDescript(False);
    End;
  End;
End;

Procedure TfrmOFSP1.pItemDescript(bAvail: Boolean);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Item Description Field }
    OSFDescription.Options := [efoCaretToEnd, efoReadOnly];
    OSFDescription.TabStop := False;
  End
  Else Begin
    { Allow editing of the Item Description Field }
    OSFDescription.Options := [efoCaretToEnd];
    OSFDescription.TabStop := True;
    OSFDescription.Color := clWindow;
  End;
End;

Procedure TfrmOFSP1.ONFItemTypeEnter(Sender: TObject);
//Var
//  sHints, cRetLF: String;
Begin
  //  cRetLF := #13 + #10;
  //  sHints := ' 1  Sanction free tables';
  //  sHints := sHints + cRetLF + ' 2  Computer/printer rental';
  //  sHints := sHints + cRetLF + ' 3  ACBL computer hands';
  //  sHints := sHints + cRetLF + ' 4  ACBL membership fees (US)';
  //  sHints := sHints + cRetLF + ' 5  Other for ACBL (Charity, etc)';
  //  sHints := sHints + cRetLF + ' 6  Non-mem sanction fee';
  //  sHints := sHints + cRetLF + ' 7  Other expenses paid by ACBL';
  //  sHints := sHints + cRetLF + ' 8  Scrip to ACBL';
  //  sHints := sHints + cRetLF + ' 9  US Check';
  //  If UpperCase(FTInfo.USE_EX_RATE) = 'Y' Then Begin
  //    sHints := sHints + cRetLF + '10  CAN Check';
  //    sHints := sHints + cRetLF + '11  ACBL membership fees (CAN)';
  //  End;
  //  ONFItemType.Hint := sHints;
    // ---------------------------------------------------------------------------
  pResetTabStops;
  bDoEdit := True;
  FOFS.ITEM_TYPE := LeftPad(Trim(ONFItemType.Text),2);
  bDoEdit := CustomFinanceEdit(Scrno, 1, FOFS.ITEM_TYPE, True, bDisplay_Rec, sDataMode, Sender);
  // ---------------------------------------------------------------------------
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'N');
End;

Function TfrmOFSP1.fOfsStat(Const vtype, mask: byte): boolean;
Begin
  fOfsStat := (OfsEditStat[vtype] And mask) <> 0;
End;

Procedure TfrmOFSP1.OPFItemsExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Trim(FOFS.ITEMS) <> Trim(OPFItems.Text) Then Begin
        If fOfsStat(Ival(ONFItemType.Text), ofsNoItems) Then Begin {units}
          OPFItems.Text := Fstr(0, 6, 1);
        End
        Else Begin
          FOFS.ITEMS := LeftPad(Trim(OPFItems.Text),6);
          bDoEdit := CustomFinanceEdit(Scrno, 4, FOFS.ITEMS, False, bDisplay_Rec, sDataMode, Sender);
          If bDisplay_Rec Then pFillFields;
          plOnFieldLastStep(False, Sender, 'P');
        End;
      End;
    End;
  End;
End;

Procedure TfrmOFSP1.pItems(bAvail: Boolean);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    OPFItems.Options := [efoCaretToEnd, efoReadOnly, efoRightAlign];
    OPFItems.TabStop := False;
  End
  Else Begin
    { Allow editing of the Field }
    OPFItems.Options := [efoCaretToEnd, efoRightAlign];
    OPFItems.TabStop := True;
    OPFItems.Color := clWindow;
  End;
End;

Procedure TfrmOFSP1.OPFItemRateExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Trim(FOFS.ITEM_RATE) <> Trim(OPFItemRate.Text) Then Begin
        FOFS.ITEM_RATE := LeftPad(Trim(OPFItemRate.Text),8);
        bDoEdit := CustomFinanceEdit(Scrno, 5, FOFS.ITEM_RATE, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'P');
      End;
    End;
  End;
End;

Procedure TfrmOFSP1.pItemRate(bAvail: Boolean);
Begin
  If Not bAvail Then Begin {rate}
    { Disallow editing of the Field }
    OPFItemRate.Options := [efoCaretToEnd, efoReadOnly, efoRightAlign];
    OPFItemRate.TabStop := False;
  End
  Else Begin
    { Allow editing of the Field }
    OPFItemRate.Options := [efoCaretToEnd, efoRightAlign];
    OPFItemRate.TabStop := True;
    OPFItemRate.Color := clWindow;
  End;
End;

Procedure TfrmOFSP1.pAmount(bAvail: Boolean);
Begin
  If Not bAvail Then Begin {Amount}
    { Disallow editing of the Field }
    OPFAmount.Options := [efoCaretToEnd, efoReadOnly, efoRightAlign];
    OPFAmount.TabStop := False;
  End
  Else Begin
    { Allow editing of the Field }
    OPFAmount.Options := [efoCaretToEnd, efoRightAlign];
    OPFAmount.TabStop := True;
    OPFAmount.Color := clWindow;
  End;
End;

Procedure TfrmOFSP1.OPFAmountExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      FOFS.AMOUNT := LeftPad(Trim(OPFAmount.Text),9);
      bDoEdit := CustomFinanceEdit(Scrno, 6, FOFS.AMOUNT, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmOFSP1.Action1Execute(Sender: TObject);
Begin
  pSetWinState;
End;

Procedure TfrmOFSP1.Action2Execute(Sender: TObject);
Begin
  pSetWinState;
End;

Procedure TfrmOFSP1.pSetWinState;
Begin
  If WindowState = wsMaximized Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  frmWorkSheet.bWinStNorm := (WindowState = wsNormal);
  If frmWorkSheet.bWinStNorm Then frmWorkSheet.WindowState := wsNormal
  Else frmWorkSheet.WindowState := wsMaximized;
  pSetPanelWidth(frmOFSP1, StatusBar1);
End;

Procedure TfrmOFSP1.ResizeKit1ExitResize(Sender: TObject; XScale, YScale: Double);
Begin
  pSetPanelWidth(frmOFSP1, StatusBar1);
End;

Procedure TfrmOFSP1.OSFDescriptionEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FOFS.DESCRIPTION := Pad(OSFDescription.Text,25);
  bDoEdit := CustomFinanceEdit(Scrno, 3, FOFS.DESCRIPTION, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmOFSP1.OPFItemsEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FOFS.ITEMS := LeftPad(Trim(OPFItems.Text),6);
  bDoEdit := CustomFinanceEdit(Scrno, 4, FOFS.ITEMS, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmOFSP1.OPFItemRateEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FOFS.ITEM_RATE := LeftPad(Trim(OPFItemRate.Text),8);
  bDoEdit := CustomFinanceEdit(Scrno, 5, FOFS.ITEM_RATE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmOFSP1.OPFAmountEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FOFS.AMOUNT := LeftPad(Trim(OPFAmount.Text),9);
  bDoEdit := CustomFinanceEdit(Scrno, 6, FOFS.AMOUNT, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmOFSP1.Action3Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmOFSP1.Action4Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Function TfrmOFSP1.FormHelp(Command: Word; Data: Integer; Var CallHelp: Boolean): Boolean;
Begin
  if data < 500000 then
    HtmlHelp(GetDesktopWindow,DefChmFile,HH_HELP_CONTEXT,Data);
//  If data < 500000 Then WinHelp(Application.Handle, 'ACBLSCORE.HLP', Help_Context, Data);
End;

//Procedure TfrmOFSP1.pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
//Begin
//  If UpperCase(sType) = 'S' Then pOSFieldAvail(bDoedit, (oSender As TOvcSimpleField))
//  Else If UpperCase(sType) = 'P' Then pOPFieldAvail(bDoedit, (oSender As TOvcPictureField))
//  Else If UpperCase(sType) = 'C' Then pOCBFieldAvail(bDoedit, (oSender As TOvcComboBox))
//  Else If UpperCase(sType) = 'D' Then pODEFieldAvail(bDoedit, (oSender As TOvcDateEdit))
//  Else If UpperCase(sType) = 'H' Then pLMDHCBFieldAvail(bDoedit, (oSender As TLMDHeaderListComboBox))
//  Else If UpperCase(sType) = 'N' Then pONFieldAvail(bDoedit, (oSender As TOvcNumericField))
//  Else MessageDlg('No Procedure created for that type!', mtError, [mbOK], 0);
//  // -------------------------------------------------------------------------------------------------------
//  If bDisplay_Rec Then pFillFields;
//  // -------------------------------------------------------------------------------------------------------
//  If bWasOnEnter Then Begin
//    If UpperCase(sType) = 'S' Then Begin
//      If Not (oSender As TOvcSimpleField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'P') Then Begin
//      If Not (oSender As TOvcPictureField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'D') Then Begin
//      If Not (oSender As TOvcDateEdit).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'H') Then Begin
//      If Not (oSender As TLMDHeaderListComboBox).TabStop Then
//        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'C') Then Begin
//      If Not (oSender As TOvcComboBox).TabStop Then
//        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'N') Then Begin
//      If Not (oSender As TOvcNumericField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End;
//  End;
//End;
//
//Procedure TfrmOFSP1.pONFieldAvail(bAvail: Boolean; oOSField: TOvcNumericField);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmOFSP1.pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoCaretToEnd, efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [efoCaretToEnd];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmOFSP1.pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoCaretToEnd, efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [efoCaretToEnd];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmOFSP1.pOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmOFSP1.pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.ReadOnly := True;
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.ReadOnly := False;
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmOFSP1.pLMDHCBFieldAvail(bAvail: Boolean; oOSField: TLMDHeaderListComboBox);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.ReadOnly := True;
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.ReadOnly := False;
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;

Procedure TfrmOFSP1.pResetTabStops;
Begin
  { Reset ALL editable fields tabstop to true so fields will be available for next action: Add/Edit/Copy }
  ONFItemType.TabStop := True;
  OSFDescription.TabStop := True;
  OPFItems.TabStop := True;
  OPFItemRate.TabStop := True;
  OPFAmount.TabStop := True;
  OSFSanctionNumberO.TabStop := True;
End;

End.

