Unit LibFinance;

Interface

Uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, ComCtrls,
  db33, csdef, formedit, util1, util2, history, mtables, {groups, }fdBase, dBase, STSTRS, YesNoBoxU,
  {PVIO,} VListBox, ovcef, ovcsf, ovcrvcbx, ovcpb, ovcpf, ovcnf, ovceditf, ovcedpop, ovcedcal, ovcbordr,
  ovcbcalc, ovcbcldr, ovcbase, ovcdlg, ovcclkdg, ovccmbx, LMDHeaderListComboBox, formWorkSheet;

Procedure FinancePostEditProc(Const Fno: byte);
Function ShowBalanceItem(Const Item: String): _Str20;
Function ShowOfsItem(Const Item: String): _Str20;
Procedure FixTotals(iFunc: Integer);
Function CalcSponsorPreNet: real;
Function ValidOfsItem(Const item: byte): boolean;
Function ValidBalItem(Const item: byte): boolean;
Function ValidOfsDescription(Const sItemType, sDescription: String): boolean;
Function GetLink(Const Nummer: Integer): KeyStr;
Procedure FixInvoice;
Procedure pReadFees;
Procedure pWriteFees;
Procedure CalcAll;
Procedure pSetPanelWidth(Sender: TObject; StatusBar: TStatusBar);
Procedure plOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
Procedure plONFieldAvail(bAvail: Boolean; oOSField: TOvcNumericField);
Procedure plOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
Procedure plOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
Procedure plOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
Procedure plODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
Procedure plLMDHCBFieldAvail(bAvail: Boolean; oOSField: TLMDHeaderListComboBox);

Implementation

Uses I_FCommon,
     groups;

{$I STSTRS.dcl}
(*{$I tfinlib.inc}*)
Function GetLink(Const Nummer: Integer): KeyStr;
Var
  TKey: KeyStr;
Begin
  TKey := '';
  Case Nummer Of
    8: Begin
        TKey := Copy(FTINFO.SANCTION_NO, 1, 10);
        TKey := upper(TKey);
      End;
    2: Begin
        TKey := Copy(FTINFO.SANCTION_NO, 1, 10);
        TKey := upper(TKey);
      End;
    3: Begin
        TKey := Copy(FTINFO.SANCTION_NO, 1, 10);
        TKey := upper(TKey);
      End;
    9: Begin
        TKey := Copy(FTINFO.SANCTION_NO, 1, 10);
        TKey := upper(TKey);
      End;
    7: Begin
        TKey := Copy(FINVOICE.FHASH, 1, 3);
      End;
    5: Begin
        TKey := Copy(TOURN.SANCTION_NO, 1, 10);
        TKey := upper(TKey);
      End;
  Else TKey := ''
  End;
  GetLink := TKey;
End;

Function ValidOfsItem(Const item: byte): boolean;
Begin
  ValidOfsItem := (item <= OfsItemTypes) And (item > 0);
End;

Function ValidBalItem(Const item: byte): boolean;
Begin
  ValidBalItem := (item <= BalItemTypes) And (item > 0);
End;

Function CalcSponsorPreNet: real;
Begin
  With Ftinfo Do CalcSponsorPreNet := Valu(Net) + Valu(Additions_total)
    - Valu(Expenses_total) - Valu(Deductions_total) - Valu(Exchange_acbl_total)
      - Valu(Exchange_td_total);
End;

Procedure CalcBalanceTotals(Const Update: boolean);
Begin
  With Ftinfo Do Begin
    Sponsor_Net := Fstr(CalcSponsorPreNet + Valu(Sponsor_Check), 10, 2);
    Money_total := Fstr(Valu(Checks_total) + Valu(Cash_total) + Valu(other_total), 10, 2);
  End;
  If Not Update Then exit;
  fPutARec(Ftinfo_no);
End;

Procedure CalcOfstotals(Const Update: boolean);
var
        MaxSFees : real;
        SancFee : real;
        SancTab : real;
Begin
  With Ftinfo Do Begin
    Sanction_tables := Fstr(Valu(Total_tables) - Valu(Sanc_free_tables), 8, 1);
    Sanction_fee_total := Fstr(Valu(Sanction_tables) * Valu(Sanction_fee), 9, 2);
    if TestFFlags(ffCalcSanction) then begin
      SancFee := Valu(Sanction_fee);
      SancTab := Valu(Sanction_tables);
      if SancTab < 500 then MaxSFees := SancFee * SancTab
      else begin
        MaxSFees := SancFee * 500;
        if SancTab < 1000 then
          MaxSFees := MaxSFees + (SancFee-0.27) * (SancTab-500)
        else begin
          MaxSFees := MaxSFees + (SancFee-0.27) * 500;
          if SancTab < 2000 then
            MaxSFees := MaxSFees + (SancFee-0.62) * (SancTab-1000)
          else begin
            MaxSFees := MaxSFees + (SancFee-0.62) * 1000;
            if SancTab < 4000 then
              MaxSFees := MaxSFees + (SancFee-1.09) * (SancTab-2000)
            else begin
              MaxSFees := MaxSFees + (SancFee-1.09) * 2000;
              MaxSFees := MaxSFees + (SancFee-1.40) * (SancTab-4000);
            end;
          end;
        end;
      end;
      Sanction_fee_total := Fstr(MaxSFees,9,2);
      {if Valu(Sanction_tables) > 4000 then MaxSFees := Valu(Sanction_fee) * 100000
      else if Valu(Sanction_tables) > 2000 then MaxSFees := (Valu(Sanction_fee)-0.25) * 4000
      else if Valu(Sanction_tables) > 1000 then MaxSFees := (Valu(Sanction_fee)-0.25) * 2000
      else if Valu(Sanction_tables) > 500 then MaxSFees := (Valu(Sanction_fee)-0.25) * 1000
      else MaxSFees := (Valu(Sanction_fee)-0.25) * 500;
      if Valu(Sanction_fee_total) > MaxSFees then Sanction_fee_total := Fstr(MaxSFees,9,2);}
    end;
    Non_mem_total_acbl := Fstr(Valu(Non_members_acbl) * Valu(Non_memb_acbl_rate), 9, 2);
    Acbl_collected := Fstr(Valu(Salaried_fees) + Valu(Part_time_fees)
      + Valu(Full_time_fees) + Valu(Surcharge)
      + Valu(Sanction_fee_total) + Valu(Supply_total)
      + Valu(Hand_record_total) + Valu(Trans_prepaid) + Valu(Acbl_other)
      + Valu(Non_mem_total_acbl) + Valu(memb_fees) + Valu(Extended_fees), 10, 2);
    Acbl_paid_out := Fstr(Valu(trans_dic) + Valu(Comp_rental)
      + Valu(Acbl_other_paid), 9, 2);
    Acbl_due := Fstr(Valu(Acbl_collected) - Valu(Acbl_paid_out), 10, 2);
    Us_due_acbl := Fstr(Valu(Acbl_due) - Valu(scrip_to_acbl), 10, 2);
    If Use_ex_rate = 'Y' Then Begin
      Exchange_Acbl_total := Fstr(Valu(ACBL_due) * Valu(Exchange_rate) / 100, 9, 2);
      zCanadian_due_acbl := Fstr((Valu(Us_due_acbl) - Valu(Us_check))
        * (Valu(Exchange_rate) + 100) / 100, 10, 2);
    End;
  End;
  If Not Update Then exit;
  fPutARec(Ftinfo_no);
End;

Procedure CalcTotalExpenses(Const Update: boolean);
Begin
  With Ftinfo Do
    Expenses_total := Fstr(Valu(Director_fees) + Valu(Hotel_total)
      + Valu(Per_diem_total) + Valu(Sanction_fee_total) + Valu(supply_total)
      + Valu(surcharge) + Valu(Trans_total_inv) + Valu(Hand_record_total)
      + Valu(Acbl_other) + Valu(non_mem_total_acbl), 10, 2);
  If Not Update Then exit;
  fPutARec(Ftinfo_no);
End;

Procedure CalcAll;
Begin
  ok := true;
  If Not fStatus_Ok(Ftinfo_no) Then exit;
  CalcTotalExpenses(False);
  CalcOfstotals(false);
  CalcBalanceTotals(false);
  fPutARec(Ftinfo_no);
End;

Procedure FixVoucher(Var InfoSave: TFtinfoSave);
Var
  HashKey: KeyStr;
  tempkey: Keystr;
  I: TFinvoiceSave;
  tempr: real;

  Procedure FixTotSalaries;
  Begin
    With FInvoice, FinvItem, InfoSave Do If Salaried = 'Y' Then
        oldsalaried := oldsalaried + Valu(Total)
      Else Case Empl_stat[1] Of
          'F': oldfulltime := oldfulltime + Valu(Total);
          'P': oldparttime := oldparttime + Valu(Total);
          'E': oldextended := oldextended + Valu(Total);
        End;
  End;

Begin
  If Not fStatus_Ok(fFilno) Then FillChar(I, SizeOf(I), 0)
  Else Begin
    HashKey := fGetKey(Finvoice_no, 2);
    tempkey := HashKey;
    FillChar(I, SizeOf(I), 0);
    SearchKey(fIdxKey[FinvItem_no, 1]^, fRecno[FinvItem_no], tempkey);
    While ok And CompareKey(tempkey, HashKey) Do With Finvitem, I Do Begin
        fGetARec(FinvItem_no);
        oldexchangetotal := oldexchangetotal + Valu(Ex_rate_amt);
        Case Ival(Item_type) Of
          1: Case Ival(sub_item_type) Of {sessions}
              1: Begin {DIC sessions}
                  olddicsess := olddicsess + Valu(Items);
                  oldstafftotal := oldstafftotal + Valu(Total);
                  FixTotSalaries;
                End;
            Else If Paid_by = 'S' Then Begin
              oldstaffsess := oldstaffsess + Valu(Items);
              oldstafftotal := oldstafftotal + Valu(Total);
              If Ival(sub_item_type) In [2, 7] Then FixTotSalaries;
            End;
            End;
          2: Begin {transportation}
              Case Paid_by[1] Of
                'S': oldtransamti := oldtransamti + Valu(Total);
                'A': Begin
                    tempr := CalcUSamountForOfs(false);
                    oldtransamtd := oldtransamtd + tempr;
                    oldexchangetotal := oldexchangetotal + Valu(Total) - tempr;
                  End;
              End;
              If Ival(sub_item_type) = 3 Then {pre paid}
                With InfoSave Do transprepaid := transprepaid + Valu(Total);
            End;
          3: olddiemdays := olddiemdays + Valu(Items); {per diem}
          4: Begin {Hotel}
              oldhotelamt := oldhotelamt + Valu(Total);
              oldhoteldays := oldhoteldays + Valu(Items);
            End;
          5: Begin {Computer/printer}
              With InfoSave Do comprental := comprental + Valu(Total);
              oldcptramt := oldcptramt + Valu(Total);
            End;
          6: oldadvanceamt := oldadvanceamt + Valu(Total); {Advance}
          7: Begin {Other}
              oldotheramt := oldotheramt + Valu(Total);
              If paid_by = 'A' Then Begin
                tempr := CalcUSamountForOfs(false);
                oldotheracblamt := oldotheracblamt + tempr;
                InfoSave.otheracblamt := InfoSave.otheracblamt + tempr;
                oldexchangetotal := oldexchangetotal + Valu(Total) - tempr;
              End;
            End;
        End;
        NextKey(fIdxKey[FinvItem_no, 1]^, fRecno[FinvItem_no], tempkey);
      End;
  End;
  If fStatus_Ok(Ftinfo_no) Then With Finvoice, I Do Begin
      Dic_sessions := Fstr(olddicsess, 5, 2);
      Staff_sessions := Fstr(oldstaffsess, 5, 2);
      Staff_td_total := Fstr(oldstafftotal, 8, 2);
      Trans_amt := Fstr(oldtransamti, 8, 2);
      Trans_amtd := Fstr(oldtransamtd, 8, 2);
      Per_diem_days := Fstr(olddiemdays, 4, 1);
      Hotel_amt := Fstr(oldhotelamt, 8, 2);
      Hotel_days := Fstr(oldhoteldays, 2, 0);
      Exchange_total := Fstr(oldexchangetotal, 8, 2);
      Advance_amt := Fstr(oldadvanceamt, 7, 2);
      Cptr_amt := Fstr(oldcptramt, 6, 2);
      Other_amt := Fstr(oldotheramt, 7, 2);
      Other_Acbl_amt := Fstr(oldotheracblamt, 7, 2);
      fPutARec(Finvoice_no);
    End;
End;

Procedure FixInvoice;
Var
  olditemrecno: longint;
  oldinvrecno: longint;
  HashKey: KeyStr;
  InvKey: KeyStr;
  tempInvkey: Keystr;
  tempHashkey: Keystr;
  oldInvkey: keystr;
  oldHashkey: keystr;
  I: TFinvoiceSave;
  InfoSave: TFtinfoSave;
Begin
  olditemrecno := fRecno[FinvItem_no];
  oldhashkey := fGetKey(FinvItem_no, 1);
  oldinvrecno := fRecno[Finvoice_no];
  oldinvkey := fGetKey(Finvoice_no, 1);
  InvKey := fGetKey(Ftinfo_no, 1);
  tempinvkey := InvKey;
  FillChar(I, SizeOf(I), 0);
  FillChar(InfoSave, SizeOf(InfoSave), 0);
  SearchKey(fIdxKey[Finvoice_no, 1]^, fRecno[Finvoice_no], tempinvkey);
  While ok And CompareKey(tempinvkey, InvKey) Do With Finvoice, I Do Begin
      fGetARec(Finvoice_no);
      FixVoucher(InfoSave);
      oldstafftotal := oldstafftotal + Valu(Staff_td_total);
      oldtransamti := oldtransamti + Valu(Trans_amt);
      oldtransamtd := oldtransamtd + Valu(Trans_amtd);
      olddiemdays := olddiemdays + Valu(Per_diem_days);
      oldhotelamt := oldhotelamt + Valu(Hotel_amt);
      oldhoteldaystotal := oldhoteldaystotal + Valu(Hotel_days);
      oldexchangetotal := oldexchangetotal + Valu(Exchange_total);
      oldtdsessions := oldtdsessions + Valu(Dic_sessions) + Valu(Staff_sessions);
      NextKey(fIdxKey[Finvoice_no, 1]^, fRecno[Finvoice_no], tempinvkey);
    End;
  fRecno[Finvoice_no] := oldinvrecno;
  fFindMulti(Finvoice_no, fRecno[Finvoice_no], oldinvKey);
  If ok Then fGetARec(Finvoice_no);
  fRecno[FinvItem_no] := olditemrecno;
  fFindMulti(FinvItem_no, fRecno[FinvItem_no], oldhashKey);
  If ok Then fGetARec(FinvItem_no);
  With Ftinfo, I, InfoSave Do Begin
    Hotel_total := Fstr(oldhotelamt, 8, 2);
    Hotel_days_total := Fstr(oldhoteldaystotal, 3, 0);
    Per_diem_total_days := Fstr(olddiemdays, 6, 1);
    Per_diem_total := Fstr(olddiemdays * Valu(per_diem_rate), 9, 2);
    Trans_total_inv := Fstr(oldtransamti, 8, 2);
    Trans_dic := Fstr(oldtransamtd, 8, 2);
    Salaried_fees := Fstr(oldsalaried, 9, 2);
    Full_time_fees := Fstr(oldfulltime, 9, 2);
    Part_time_fees := Fstr(oldparttime, 9, 2);
    Extended_fees := Fstr(oldextended, 9, 2);
    Director_fees := Fstr(oldstafftotal, 9, 2);
    Exchange_td_total := Fstr(oldexchangetotal, 9, 2);
    Comp_rental_i := Fstr(comprental, 6, 2);
    comp_rental := Fstr(comprental + Valu(Comp_rental_o), 6, 2);
    Dir_sessions := Fstr(oldtdsessions, 6, 2);
    Trans_prepaid := Fstr(transprepaid, 8, 2);
    Acbl_other_paid_i := Fstr(otheracblamt, 7, 2);
  End;
  CalcTotalExpenses(true);
  CalcOfsTotals(true);
End;

Procedure FixReceipts;
Var
  oldrecno: longint;
  SancKey: KeyStr;
  tempkey: Keystr;
  oldkey: keystr;
  I: TFtinfoSave;
  fOldFilno: word;
  sancfee : real;
  ttind : byte;
Begin
  fOldFilno := fFilno;
  fFilno := Fwksheet_no;
  If Not fStatus_Ok(fFilno) Then FillChar(I, SizeOf(I), 0)
  Else Begin
    oldrecno := fRecno[fFilno];
    oldkey := fGetKey(fFilno, 1);
    SancKey := fGetKey(Ftinfo_no, 1);
    tempkey := SancKey;
    FillChar(I, SizeOf(I), 0);
    SearchKey(fIdxKey[fFilno, 1]^, fRecno[fFilno], tempkey);
    While ok And CompareKey(tempkey, SancKey) Do With Fwksheet, I Do Begin
        fGetARec(fFilno);
        oldtables := oldtables + Valu(tables);
        oldentry_total := oldentry_total + Valu(Entry_Total);
        oldFill_in := oldFill_in + Valu(fill_in);
        oldnon_mem := oldnon_mem + Valu(non_memb);
        NextKey(fIdxKey[fFilno, 1]^, fRecno[fFilNo], tempkey);
      End;
    fRecno[fFilNo] := oldrecno;
    fFindMulti(fFilNo, fRecno[fFilNo], oldKey);
    If ok Then fGetARec(fFilNo);
  End;
  If fStatus_Ok(Ftinfo_no) Then With Ftinfo, I Do Begin
      Total_Tables := Fstr(oldtables, 8, 1);
      If TestFFlags(ffCalcSurcharge) Then Begin
        Surcharge := Fstr(oldtables * InSurcharge, 6, 2);
        If Valu(Surcharge) > SecSurcharge Then
          Surcharge := Fstr(SecSurcharge, 6, 2);
      End;
      {if TestFFlags(ffCalcSanction) then begin
        ttind := GetTTInd(Tourn_Type);
        if oldTables > 4000 then SancFee := SancFees[ttind]-100
        else if oldtables > 2000 then SancFee := SancFees[ttind]-75
        else if Oldtables > 1000 then SancFee := SancFees[ttind]-50
        else if oldtables > 500 then sancfee := SancFees[ttind]-25
        else sancfee := SancFees[ttind];
        Sanction_fee := Fstr(sancfee / 100,6,2);
      end;}
      If Valu(Supply_rate) > 0 Then Supply_tables := Total_tables
      Else Supply_Tables := Fstr(0, 8, 1);
      Supply_total := Fstr(Round(Valu(Supply_tables) * Valu(Supply_rate) * 20) / 20, 9, 2);
      Entry_total := Fstr(oldentry_total, 10, 2);
      Non_mem := Fstr(oldnon_mem, 9, 2);
      Gross := Fstr(oldentry_total + oldnon_mem, 10, 2);
      Fill_in := Fstr(oldFill_in, 9, 2);
      Net := Fstr(oldentry_total + oldnon_mem - oldFill_in, 10, 2);
      fPutARec(Ftinfo_no);
    End;
  fFilNo := fOldFilno;
End;

Procedure FixBalance;
Var
  oldrecno: longint;
  SancKey: KeyStr;
  tempkey: Keystr;
  oldkey: keystr;
  I: TBalanceSave;
  fOldFilno: word;
Begin
  fOldFilno := fFilNo;
  fFilNo := FBalance_no;
  If Not fStatus_Ok(fFilNo) Then FillChar(I, SizeOf(I), 0)
  Else Begin
    oldrecno := fRecno[fFilNo];
    oldkey := fGetKey(fFilNo, 1);
    SancKey := fGetKey(Ftinfo_no, 1) + 'B';
    tempkey := SancKey;
    FillChar(I, SizeOf(I), 0);
    SearchKey(fIdxKey[fFilNo, 1]^, fRecno[fFilNo], tempkey);
    While ok And CompareKey(tempkey, SancKey) Do With FBalance, I Do Begin
        fGetARec(fFilNo);
        Case Ival(Item_type) Of
          1: olddeductions := olddeductions + Valu(Amount);
          2: oldadditions := oldadditions + Valu(Amount);
          3: oldchecks := oldchecks + Valu(Amount);
          4: oldcash := oldcash + Valu(Amount);
          5: oldother := oldother + Valu(Amount);
          6: oldspcheck := oldspcheck + Valu(Amount);
        End;
        NextKey(fIdxKey[fFilNo, 1]^, fRecno[fFilNo], tempkey);
      End;
    fRecno[fFilNo] := oldrecno;
    fFindMulti(fFilNo, fRecno[fFilNo], oldKey);
    If ok Then fGetARec(fFilNo);
  End;
  If fStatus_Ok(Ftinfo_no) Then With Ftinfo, I Do Begin
      CalcOfstotals(false);
      Deductions_total := Fstr(olddeductions, 9, 2);
      Additions_total := Fstr(oldadditions, 9, 2);
      Checks_total := Fstr(oldchecks, 9, 2);
      Cash_total := Fstr(oldcash, 9, 2);
      Other_total := Fstr(oldother, 9, 2);
      If (oldspcheck = 0) And (is_spon_check = 'Y') Then Begin
        If use_ex_rate = 'N' Then Sponsor_check := us_due_acbl
        Else Sponsor_check := Fstr(Valu(Can_memb_fees) + Valu(zCanadian_due_acbl), 10, 2);
      End
      Else Sponsor_check := Fstr(oldspcheck, 10, 2);
      CalcBalanceTotals(true);
    End;
  fFilNo := fOldFilno;
End;

Procedure FixOfs;
Var
  oldrecno: longint;
  SancKey: KeyStr;
  tempkey: Keystr;
  oldkey: keystr;
  I: TOfsSave;
  fOldFilno: word;
Begin
  fOldFilno := fFilNo;
  fFilNo := FOfs_no;
  If Not fStatus_Ok(fFilNo) Then FillChar(I, SizeOf(I), 0)
  Else Begin
    oldrecno := fRecno[fFilNo];
    oldkey := fGetKey(fFilNo, 1);
    SancKey := fGetKey(Ftinfo_no, 1) + 'O';
    tempkey := SancKey;
    FillChar(I, SizeOf(I), 0);
    SearchKey(fIdxKey[fFilNo, 1]^, fRecno[fFilNo], tempkey);
    While ok And CompareKey(tempkey, SancKey) Do With FOfs, I Do Begin
        fGetARec(fFilNo);
        Case Ival(Item_type) Of
          1: sancfreetables := sancfreetables + Valu(Items);
          2: comprentaltotal := comprentaltotal + Valu(Amount);
          3: handrecordtotal := handrecordtotal + Valu(Amount);
          4: memfeestotal := memfeestotal + Valu(Amount);
          5: acblother := acblother + Valu(Amount);
          6: nonmemtotal := nonmemtotal + Valu(Items);
          7: acblotherpaid := acblotherpaid + Valu(Amount);
          8: scriptoacbl := scriptoacbl + Valu(Amount);
          9: uscheck := uscheck + Valu(Amount);
          10: cancheck := cancheck + Valu(Amount);
          11: canmemfeestotal := canmemfeestotal + Valu(Amount);
        End;
        NextKey(fIdxKey[fFilNo, 1]^, fRecno[fFilNo], tempkey);
      End;
    fRecno[fFilNo] := oldrecno;
    fFindMulti(fFilNo, fRecno[fFilNo], oldKey);
    If ok Then fGetARec(fFilNo);
  End;
  If fStatus_Ok(Ftinfo_no) Then With Ftinfo, I Do Begin
      Sanc_free_tables := Fstr(sancfreetables, 7, 1);
      Hand_record_total := Fstr(handrecordtotal, 8, 2);
      comp_rental_o := Fstr(comprentaltotal, 6, 2);
      comp_rental := Fstr(comprentaltotal + Valu(comp_rental_i), 6, 2);
      Acbl_other := Fstr(acblother, 8, 2);
      Acbl_other_paid := Fstr(acblotherpaid + Valu(Acbl_other_paid_i), 8, 2);
      Scrip_to_acbl := Fstr(scriptoacbl, 8, 2);
      Us_check := Fstr(uscheck, 10, 2);
      Can_check := Fstr(cancheck, 10, 2);
      Non_members_acbl := Fstr(nonmemtotal, 4, 0);
      Memb_fees := Fstr(memfeestotal, 7, 2);
      Can_Memb_fees := Fstr(canmemfeestotal, 7, 2);
      CalcOfsTotals(true);
    End;
  fFilNo := fOldFilno;
  FixInvoice;
End;

Procedure FixTotals(iFunc: Integer);
Begin
  { iFunc is iDataMode passed: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  Case fFilno Of
    // -----------------------------------------------
    Ftinfo_no: If iFunc = 2 Then Begin
        FixReceipts;
        FixInvoice;
        FixOfs;
        FixBalance;
      End;
    // -----------------------------------------------
    Fwksheet_no: Begin
        FixReceipts;
        FixInvoice;
        FixOfs;
        FixBalance;
      End;
    // -----------------------------------------------
    Finvitem_no, Finvoice_no: Begin
        FixInvoice;
        FixOfs;
        FixBalance;
      End;
    // -----------------------------------------------
    FBalance_no: FixBalance;
    // -----------------------------------------------
    FOfs_no: Begin
        FixOfs;
        FixBalance;
      End;
  End;
End;

Procedure FinancePostEditProc(Const Fno: byte);
Var
  j: byte;
Begin
  If Fno = 1 Then With Ftinfo Do Begin
    SetFFlags(ffCharitySanc,(Pos('99',Tourn_Dates) = 0)
      and (Pos('2000',Tourn_Dates) = 0));
    for j := 1 to TTypes do if Tourn_type = Copy(TournTypes[j],1,2) then begin
      SetFFlags(ffCalcSurcharge,(Surcharges[j] = 2) and (TD_no > '100')
        and (Td_no < '999'));
      SetFFlags(ffCalcSanction,(Surcharges[j] = 3));
      exit;
    end;
    SetFFlags(ffCalcSurcharge,false);
  End;
End;

Function ShowOfsItem(Const Item: String): _Str20;
Var
  i: byte;
Begin
  i := Ival(Item);
  If (i > 0) And (i <= MaxOfsItemtypes) Then
    ShowOfsItem := Pad(Copy(OfsItemHelp[Ival(Item)], 4, 20), 20)
  Else ShowOfsItem := CharStr(' ', 20);
End;

Function ShowBalanceItem(Const Item: String): _Str20;
Var
  i: byte;
Begin
  i := Ival(Item);
  If (i > 0) And (i <= BalItemtypes) Then
    ShowBalanceItem := Pad(Copy(BalItemHelp[Ival(Item)], 3, 20), 20)
  Else ShowBalanceItem := CharStr(' ', 20);
End;

Procedure pReadFees;
Var
  F: text;
  Line: String[80];
  Ttype: String[2];
  T: byte;
Begin
  Assign(F, CFG.Loadpath + TournFeeName);
  {$I-}ReSet(F); {$I+}
  If IOResult <> 0 Then exit;
  ReadLn(F, RateYear);
  For T := 1 To MaxTdrank Do ReadLn(F, Tdrates[T]);
  {$IFDEF tsup}
  ReadLn(F, RegSup);
  ReadLn(F, SecSup);
  {$ELSE}
  ReadLn(F, Line);
  ReadLn(F, Line);
  {$ENDIF}
  {ReadLn(F,PerDiem);}
  ReadLn(F, Line);
  ReadLn(F, SecSurcharge);
  While Not Eof(F) Do Begin
    ReadLn(F, Line);
    If Length(Line) > 5 Then Begin
      Ttype := Pad(ExtractWords(1, Line, ','), 2);
      If Not Empty(Ttype) Then For T := 1 To Ttypes Do
          If Ttype = Copy(TournTypes[T], 1, 2) Then Begin
            SancFees[T] := Ival(ExtractWords(2, Line, ','));
            Surcharges[T] := Ival(ExtractWords(3, Line, ','));
          End;
    End;
  End;
  Close(F);
End;

Procedure pWriteFees;
Var
  F: text;
  Line: String[80];
  Ttype: String[2];
  T: byte;
Begin
  Assign(F, CFG.Loadpath + TournFeeName);
  {$I-}ReWrite(F); {$I+}
  If IOResult <> 0 Then exit;
  WriteLn(F, RateYear);
  For T := 1 To MaxTdrank Do WriteLn(F, Fstr(Tdrates[T], 6, 2));
  {$IFDEF tsup}
  WriteLn(F, Fstr(RegSup, 6, 2));
  WriteLn(F, Fstr(SecSup, 6, 2));
  {$ELSE}
  WriteLn(F, Fstr(0, 6, 2));
  WriteLn(F, Fstr(0, 6, 2));
  {$ENDIF}
  {WriteLn(F,Fstr(PerDiem,6,2));}
  WriteLn(F, Fstr(0, 6, 2));
  WriteLn(F, Fstr(SecSurcharge, 6, 2));
  For T := 1 To Ttypes Do
    WriteLn(F, Copy(TournTypes[T], 1, 2), ',', SancFees[T], ',', Surcharges[T]);
  Close(F);
End;

Procedure pSetPanelWidth(Sender: TObject; StatusBar: TStatusBar);
Begin
  StatusBar.Panels[0].Width := (((Sender As TForm).ClientWidth * 15) Div 100);
  StatusBar.Panels[1].Width := (((Sender As TForm).ClientWidth * 70) Div 100);
  StatusBar.Panels[2].Width := (((Sender As TForm).ClientWidth * 15) Div 100);
End;

Function ValidOfsDescription(Const sItemType, sDescription: String): boolean;
Var
  pnum: String[7];
Begin
  ValidOfsDescription := true;
  If Not (Ival(sItemtype) In [4, 11]) Then exit;
  If (Pos(sDescription[2], '0123456789') > 0) Then Begin
    pnum := sDescription;
    If Not Player_check(pnum, pnACBL) Then ValidOfsDescription := false;
  End;
End;

Procedure plOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
Begin
exit;
  If UpperCase(sType) = 'S' Then plOSFieldAvail(bDoedit, (oSender As TOvcSimpleField))
  Else If UpperCase(sType) = 'P' Then plOPFieldAvail(bDoedit, (oSender As TOvcPictureField))
  Else If UpperCase(sType) = 'C' Then plOCBFieldAvail(bDoedit, (oSender As TOvcComboBox))
  Else If UpperCase(sType) = 'D' Then plODEFieldAvail(bDoedit, (oSender As TOvcDateEdit))
  Else If UpperCase(sType) = 'H' Then plLMDHCBFieldAvail(bDoedit, (oSender As TLMDHeaderListComboBox))
  Else If UpperCase(sType) = 'N' Then plONFieldAvail(bDoedit, (oSender As TOvcNumericField))
  Else MessageDlg('No Procedure created for that type!', mtError, [mbOK], 0);
  // -------------------------------------------------------------------------------------------------------
  If bWasOnEnter Then Begin
    If UpperCase(sType) = 'S' Then Begin
      If Not (oSender As TOvcSimpleField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'P') Then Begin
      If Not (oSender As TOvcPictureField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'D') Then Begin
      If Not (oSender As TOvcDateEdit).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'H') Then Begin
      If Not (oSender As TLMDHeaderListComboBox).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'C') Then Begin
      If Not (oSender As TOvcComboBox).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'N') Then Begin
      If Not (oSender As TOvcNumericField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End;
  End;
End;

Procedure plONFieldAvail(bAvail: Boolean; oOSField: TOvcNumericField);
Var
  sFieldName: String;
Begin
  sFieldName := oOSField.Name;
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.Options := [efoReadOnly];
    oOSField.TabStop := False;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.Options := [];
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
End;

Procedure plOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
Var
  sFieldName: String;
Begin
  sFieldName := oOSField.Name;
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.Options := [efoCaretToEnd, efoReadOnly];
    oOSField.TabStop := False;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.Options := [efoCaretToEnd];
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
End;

Procedure plOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
Var
  sFieldName: String;
Begin
  sFieldName := oOSField.Name;
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.Options := [efoCaretToEnd, efoReadOnly];
    oOSField.TabStop := False;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.Options := [efoCaretToEnd];
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
End;

Procedure plOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
Var
  sFieldName: String;
Begin
  sFieldName := oOSField.Name;
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.TabStop := False;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
End;

Procedure plODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
Var
  sFieldName: String;
Begin
  sFieldName := oOSField.Name;
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.ReadOnly := True;
    oOSField.TabStop := False;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.ReadOnly := False;
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
End;

Procedure plLMDHCBFieldAvail(bAvail: Boolean; oOSField: TLMDHeaderListComboBox);
Var
  sFieldName: String;
Begin
  sFieldName := oOSField.Name;
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.ReadOnly := True;
    oOSField.TabStop := False;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.ReadOnly := False;
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
End;

End.

