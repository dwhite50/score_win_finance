Unit formOFSP2;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ovcbase, ovcdlg, ovcclkdg, StdCtrls, Mask, Buttons,
  ToolWin, ExtCtrls, ActnMan, ActnCtrls, Menus, ActnList, ovcef, ovcsf,
  ovcrvcbx, ovcpb, ovcpf, ovcnf, ovceditf, ovcedpop, ovcedcal, ovcbordr,
  ovcbcalc, ovcbcldr, XPStyleActnCtrls, Grids, ImgList, ResizeKit, LMDCustomComponent,
  LMDWndProcComponent, LMDFormShape, LMDShapeHint,
  LMDHint, LMDCustomScrollBox, LMDListBox, LMDCustomControl, LMDCustomPanel,
  LMDCustomBevelPanel, LMDBaseEdit, LMDCustomEdit, LMDCustomMaskEdit, LMDCustomExtCombo,
  LMDCustomListComboBox, LMDListComboBox, ovccmbx, LMDHeaderListComboBox,
  LMDCustomComboBox, LMDComboBox, STSTRS, LMDIniCtrl, LMDFormA,
  LMDStorBase, LMDStorXMLVault, LMDStorBinVault, LMDCustomHint,
  LMDCustomShapeHint, LMDMessageHint, LibData, LMDCalendarEdit,
  LMDCustomButton, LMDButton, fDbase, Dbase, DB33, LibUtil, Clipbrd, CSDef,
  Util1, Util2, YesNoBoxU, PVIO, VListBox, History, LMDCustomParentPanel,
  LMDCustomGroupBox, LMDGroupBox, LibFinance, I_FCommon,hh;

Type
  TfrmOFSP2 = Class(TForm)
    GBMenu: TGroupBox;
    Shape23: TShape;
    ButNext: TButton;
    ButPrev: TButton;
    ButFind: TButton;
    ButTop: TButton;
    ButLast: TButton;
    ButEdit: TButton;
    ButAdd: TButton;
    ButCopy: TButton;
    ButDelete: TButton;
    ButQuit: TButton;
    ButtonF7Key: TButton;
    ButtonF6Key: TButton;
    ButtonF8Key: TButton;
    ButtonF2Key: TButton;
    StaticTextF3Key: TStaticText;
    StaticTextF4Key: TStaticText;
    StaticTextF5Key: TStaticText;
    StaticTextF2Key: TStaticText;
    ButtonF1Key: TButton;
    StaticTextF1Key: TStaticText;
    ActionManager1: TActionManager;
    ButNextAction: TAction;
    ButPrevAction: TAction;
    ButFindAction: TAction;
    ButTopAction: TAction;
    ButLastAction: TAction;
    ButEditAction: TAction;
    ButAddAction: TAction;
    ButCopyAction: TAction;
    ButDeleteAction: TAction;
    ButQuitAction: TAction;
    F4Key: TAction;
    F3Key: TAction;
    F5Key: TAction;
    F2Key: TAction;
    F7Key: TAction;
    F1Key: TAction;
    F8Key: TAction;
    F6Key: TAction;
    CtrlCKey: TAction;
    ButPlus: TButton;
    StaticText1: TStaticText;
    PlusKey: TAction;
    ButMinus: TButton;
    StaticText2: TStaticText;
    MinusKey: TAction;
    StatusBar1: TStatusBar;
    ResizeKit1: TResizeKit;
    OvcController1: TOvcController;
    LMDHint1: TLMDHint;
    ActionManager2: TActionManager;
    ButPlayerInfoDone: TAction;
    StaticText3: TStaticText;
    GBTournWorkSheet: TGroupBox;
    LMDTournWSDone: TLMDButton;
    SpeedButton1: TSpeedButton;
    ListTables: TAction;
    PopupMenu1: TPopupMenu;
    TournamentInformation1: TMenuItem;
    Worksheet1: TMenuItem;
    Invoice1: TMenuItem;
    Voucher1: TMenuItem;
    Ballancesheet1: TMenuItem;
    Officefinancialpart11: TMenuItem;
    Officefinancialpart2: TMenuItem;
    Quit1: TMenuItem;
    LMDGroupBox1: TLMDGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    OSFDescription: TOvcSimpleField;
    LabelItemType: TLabel;
    LMDGroupBox2: TLMDGroupBox;
    Label2: TLabel;
    Label27: TLabel;
    OPFDirectorsTrans: TOvcPictureField;
    OPFItems: TOvcPictureField;
    Label4: TLabel;
    Label5: TLabel;
    OPFItemRate: TOvcPictureField;
    Label6: TLabel;
    OPFAmount: TOvcPictureField;
    Label9: TLabel;
    Label10: TLabel;
    OPFCompRental: TOvcPictureField;
    Label11: TLabel;
    Label12: TLabel;
    OPFACBLOtherPaid: TOvcPictureField;
    Label13: TLabel;
    Label14: TLabel;
    OPFACBLPaidOut: TOvcPictureField;
    Label15: TLabel;
    Label16: TLabel;
    OPFACBLDue: TOvcPictureField;
    Label17: TLabel;
    Label18: TLabel;
    OPFScripToACBL: TOvcPictureField;
    Label20: TLabel;
    OPFUSDueACBL: TOvcPictureField;
    Label21: TLabel;
    Label22: TLabel;
    OPFUSCheck: TOvcPictureField;
    Label25: TLabel;
    Label26: TLabel;
    OPFCanDueACBL: TOvcPictureField;
    LMDGroupBox5: TLMDGroupBox;
    Label3: TLabel;
    OSFSanctionNo: TOvcSimpleField;
    Label1: TLabel;
    OSFTournament: TOvcSimpleField;
    OSFSanctionNumberO: TOvcSimpleField;
    Label28: TLabel;
    Label32: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    OPFCanCheck: TOvcPictureField;
    Label33: TLabel;
    OPFExchangeRate: TOvcPictureField;
    Label38: TLabel;
    Label39: TLabel;
    OPFACBLCollected: TOvcPictureField;
    OPFCandianDollarsDueToAcbl: TOvcPictureField;
    Label19: TLabel;
    Label29: TLabel;
    OPFCandianMemberFees: TOvcPictureField;
    Label30: TLabel;
    LMDMessageHint1: TLMDMessageHint;
    Action1: TAction;
    Action2: TAction;
    ONFItemType: TOvcNumericField;
    Action3: TAction;
    Action4: TAction;
    Action5: TAction;
    Procedure ButQuitActionExecute(Sender: TObject);
    Procedure FormActivate(Sender: TObject);
    Procedure ListTablesExecute(Sender: TObject);
    Procedure pFillFields;
    Procedure pFillStaticFields;
    Procedure pNextRecordf;
    Procedure pPrevRecordf;
    Procedure ButNextActionExecute(Sender: TObject);
    Procedure ButPrevActionExecute(Sender: TObject);
    Procedure ButLastActionExecute(Sender: TObject);
    Procedure ButTopActionExecute(Sender: TObject);
    Procedure ButFindActionExecute(Sender: TObject);
    Procedure pWorkSheetInfo(bEnable: Boolean);
    Procedure ButEditActionExecute(Sender: TObject);
    Procedure ButPlayerInfoDoneExecute(Sender: TObject);
    Procedure ButAddActionExecute(Sender: TObject);
    Procedure ButCopyActionExecute(Sender: TObject);
    Procedure ButDeleteActionExecute(Sender: TObject);
    Procedure pUpdateStatusBar;
    Procedure pDateExit(Sender: TObject);
    Procedure MinusKeyExecute(Sender: TObject);
    Procedure F1KeyExecute(Sender: TObject);
    Procedure pGetOFSP2Data;
    Procedure pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure PlusKeyExecute(Sender: TObject);
    Procedure F2KeyExecute(Sender: TObject);
    Procedure pPuMenuItemSelected(Sender: TObject);
    Procedure pAbortChanges;
    Procedure F8KeyExecute(Sender: TObject);
    Procedure F7KeyExecute(Sender: TObject);
    Procedure F6KeyExecute(Sender: TObject);
    Procedure pWhichScreen(oSender: TObject); // Open frmOffFinP2
    Function fShowItemType(Const Item: byte): String;
    Function fShowSubItemType(Const Item, SubItem: byte): String;
    Function fValidVoucherSubItem(Const Item, SubItem: byte): boolean;
    Function fValidVoucherItem(Const Item: byte): boolean;
    Procedure FormClose(Sender: TObject; Var Action: TCloseAction);
    Function fShowOFSP2Item(Const Item: String): String;
    Procedure pSaveChanges;
    Procedure pCheckEditFields;
    Procedure pCopyRecData;
    Procedure pClearFields;
    Procedure pButtOn(bTurnOn: Boolean);
    Procedure ONFItemTypeEnter(Sender: TObject);
    Procedure OPFItemsExit(Sender: TObject);
    Procedure OPFItemRateExit(Sender: TObject);
    //    Procedure pAmountCalc;
    Procedure OPFAmountExit(Sender: TObject);
    Function fOfsStat(Const vtype, mask: byte): boolean;
    Procedure ONFItemTypeExit(Sender: TObject);
    Procedure OSFDescriptionExit(Sender: TObject);
    Procedure Action1Execute(Sender: TObject);
    Procedure Action2Execute(Sender: TObject);
    Procedure ResizeKit1ExitResize(Sender: TObject; XScale, YScale: Double);
    Procedure pSetWinState;
    Procedure OSFDescriptionEnter(Sender: TObject);
    Procedure OPFItemsEnter(Sender: TObject);
    Procedure OPFItemRateEnter(Sender: TObject);
    Procedure OPFAmountEnter(Sender: TObject);
    Procedure Action4Execute(Sender: TObject);
    Procedure Action5Execute(Sender: TObject);
    Function FormHelp(Command: Word; Data: Integer; Var CallHelp: Boolean): Boolean;
    //    Procedure pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
    //    Procedure pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
    //    Procedure pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
    //    Procedure pOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
    //    Procedure pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
    //    Procedure pLMDHCBFieldAvail(bAvail: Boolean; oOSField: TLMDHeaderListComboBox);
    //    Procedure pONFieldAvail(bAvail: Boolean; oOSField: TOvcNumericField);
    Procedure pResetTabStops;
  private
    { Private declarations }
  public
    { Public declarations }
    { Variable to store the length of the table array }
    bKeyIsUnique: Boolean;
  End;

Var
  frmOFSP2: TfrmOFSP2;
  bEscKeyUsed: Boolean;
  bIs: Boolean;
  { Integer variable used dynamically to store the tables record count }
  iOFSP2Recs: Integer;
  { Integer variable used to store the records for this sanction number }
  iOFSP2sCount: Integer;
  ksThisTempKey: keystr;

Const
  iLenArray: Integer = MaxFilno;

Implementation

Uses formTblsCurrOpen, tfprint, formWorkSheet;

{$I STSTRS.dcl}

{$R *.dfm}

Procedure TfrmOFSP2.ButQuitActionExecute(Sender: TObject);
Begin
  { The user wants to Quit, change the Public Variable:  bFTQuitUsed to True }
  frmWorkSheet.bFTQuitUsed := True;
  Close;
End;

Procedure TfrmOFSP2.FormActivate(Sender: TObject);
Begin
  frmWorkSheet.bWinStNorm := (frmWorkSheet.WindowState = wsNormal);
  If frmWorkSheet.bWinStNorm Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  frmWorkSheet.bWinStNorm := (WindowState = wsNormal);
  pSetPanelWidth(frmOFSP2, StatusBar1);
  { TopFilNo: What is the topmost database we are using in this application. }
  TopFilNo := Ftinfo_no;
  fFilno := FOfs_no;
  bEscKeyUsed := False;
  // -------------------------------------------------------------------------
  Application.HintHidePause := 200000;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := Null;
  { Flush_DF & Flush_IF: Must be set to True for so that everytime a change is
    made to a "Record", dbase re-writes the table header and in Playern.dat,
    updates dbLastImpDate entry. Set them to False when you Delete a record,
    then immediately set them back to True }
  { This must be done as a standard whenever a table is opened for modifications }
  Flush_DF := True;
  Flush_IF := True;
  //==========================================================
  { Store the FTInfo Key variable to This Temp Key }
  ksThisTempKey := ksFTInfoKey;
  { Load the appropriate worksheet data }
  pGetOFSP2Data;
  { This variable Scrno, is used in some of the older code procedures to determine which
    calculations need to be performed based on the screen we are in.
    ----------------------------------------------------------
    Screen 1 = Tournament Information    - FTInfo.dat
    Screen 2 = Tournament Work Sheet     - FWkSheet.dat
    Screen 3 = Tournament Invoice        - FInvoice.dat
    Screen 4 = Voucher Items             - FInvItem.dat
    Screen 5 = Tournament Balance Sheet  - FBalance.dat
    Screen 6 = Office financial part 1   - FOFs.dat
    Screen 7 = Office financial part 2   - FOFs.dat }
  Scrno := 6;
  //==========================================================
  If CSDef.CFG.Tournament Then StatusBar1.Panels[0].Text := 'Tournament'
  Else StatusBar1.Panels[0].Text := 'Club';
  // ----------------------------------------------------------------------------------------------------------
  { Show the path to the table in use and the table name in StatusBar1. }
  StatusBar1.Panels[1].Text := UpperCase(Prepend + 'FOfs.DAT');
  { Store the number of records to IOFSP2Recs }
  IOFSP2Recs := db33.UsedRecs(fDatF[FOfs_no]^);
  { Add the record count to StatusBar1's display }
  StatusBar1.Panels[1].Text := StatusBar1.Panels[1].Text + ' - Contains ' +
    Trim(IntToStr(IOFSP2Recs)) + ' Records.';
End;

Procedure TfrmOFSP2.ListTablesExecute(Sender: TObject);
Begin
  frmTblsCurrOpen := TfrmTblsCurrOpen.Create(Self);
  frmTblsCurrOpen.ShowModal;
  frmTblsCurrOpen.Free;
End;

Procedure TfrmOFSP2.pFillFields;
Begin
  With FOFS Do Begin
    ONFItemType.Text := ITEM_TYPE;
    LabelItemType.Caption := fShowOFSP2Item(item_type);
    OSFDescription.Text := Trim(DESCRIPTION);
    OPFItems.Text := ITEMS;
    OPFItemRate.Text := ITEM_RATE;
    OPFAmount.Text := AMOUNT;
    OSFSanctionNumberO.Text := SANCTION_NO;
  End;
  // -------------------------------------------------
  pFillStaticFields;
End;

Procedure TfrmOFSP2.pFillStaticFields;
Var
  bShowCandian: Boolean;
Begin
  With FTINFO Do Begin
    OSFSanctionNo.Text := SANCTION_NO;
    OSFTournament.Text := TOURN_NAME;
    // -------------------------------------------------
    OPFACBLCollected.Text := ACBL_COLLECTED;
    OPFDirectorsTrans.Text := TRANS_DIC;
    OPFCompRental.Text := COMP_RENTAL;
    OPFACBLOtherPaid.Text := ACBL_OTHER_PAID;
    OPFACBLPaidOut.Text := ACBL_PAID_OUT;
    OPFACBLDue.Text := ACBL_DUE;
    OPFScripToACBL.Text := SCRIP_TO_ACBL;
    OPFUSDueACBL.Text := US_DUE_ACBL;
    OPFUSCheck.Text := US_CHECK;
    OPFCandianDollarsDueToAcbl.Text := Fstr(Valu(us_due_acbl) - Valu(Us_check), 10, 2);
    OPFCandianMemberFees.Text := Can_Memb_fees;
    OPFExchangeRate.Text := Fstr(Valu(exchange_rate) / 100 + 1, 5, 4);
    OPFCanDueACBL.Text := Fstr(Valu(zcanadian_due_acbl) + Valu(can_memb_fees), 10, 2);
    OPFCanCheck.Text := CAN_CHECK;
    // -----------------------------------------------------------------------------------------
    { Only Visible if this is a Canadian Tournament, denoted by USE_EX_RATE being Yes }
    Label23.Visible := (UpperCase(USE_EX_RATE) = 'Y');
    Label24.Visible := (UpperCase(USE_EX_RATE) = 'Y');
    Label25.Visible := (UpperCase(USE_EX_RATE) = 'Y');
    Label26.Visible := (UpperCase(USE_EX_RATE) = 'Y');
    Label29.Visible := (UpperCase(USE_EX_RATE) = 'Y');
    Label30.Visible := (UpperCase(USE_EX_RATE) = 'Y');
    Label33.Visible := (UpperCase(USE_EX_RATE) = 'Y');
    OPFCandianDollarsDueToAcbl.Visible := (UpperCase(USE_EX_RATE) = 'Y');
    OPFExchangeRate.Visible := (UpperCase(USE_EX_RATE) = 'Y');
    OPFCandianMemberFees.Visible := (UpperCase(USE_EX_RATE) = 'Y');
    OPFCanDueACBL.Visible := (UpperCase(USE_EX_RATE) = 'Y');
    OPFCanCheck.Visible := (UpperCase(USE_EX_RATE) = 'Y');
    If UpperCase(Trim(EXCHANGE_RATE)) = 'Y' Then OfsItemtypes := (MaxOfsItemtypes - 2)
    Else OfsItemtypes := MaxOfsItemtypes;
  End;
  // -----------------------------------------------------------------------------------------
End;

Procedure TfrmOFSP2.pNextRecordf;
Begin
  { Move the record pointer in the table to the next available record and load the data in the screen fields }
  fNext_Record;
  liRecnoOF := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmOFSP2.ButNextActionExecute(Sender: TObject);
Begin
  pNextRecordf;
End;

Procedure TfrmOFSP2.pPrevRecordf;
Begin
  { Move the record pointer in the table to the previous record and load the data in the screen fields }
  fPrev_Record;
  liRecnoOF := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmOFSP2.ButPrevActionExecute(Sender: TObject);
Begin
  pPrevRecordf;
End;

Procedure TfrmOFSP2.ButLastActionExecute(Sender: TObject);
Begin
  fLast_Record;
  liRecnoOF := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmOFSP2.ButTopActionExecute(Sender: TObject);
Begin
  fTop_Record;
  liRecnoOF := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmOFSP2.ButFindActionExecute(Sender: TObject);
Begin
  fFind_Record(fGetKey(Ftinfo_no, 1), fGetKey(fFilno, 1), '', MC, 0, True);
  liRecnoOF := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmOFSP2.pWorkSheetInfo(bEnable: Boolean);
Begin
  FreeHintWin;
  { Set the state of ActionManager1 for the players GBMenu }
  If bEnable Then ActionManager1.State := asSuspended Else
    ActionManager1.State := asNormal;
  { Set the state of ActionManager2 for the form players GBMenu }
  If bEnable Then ActionManager2.State := asNormal Else
    ActionManager2.State := asSuspended;
  // ----------------------------------------------------------------------
  If bEnable Then GBMenu.Hide Else GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := Not bEnable;
  // ----------------------------------------------------------------------
  GBTournWorkSheet.Enabled := bEnable;
  // ----------------------------------------------------------------------
  LMDTournWSDone.Enabled := bEnable;
  LMDTournWSDone.Visible := bEnable;
  // ----------------------------------------------------------------------
End;

Procedure TfrmOFSP2.ButEditActionExecute(Sender: TObject);
Begin
  { In module Dbase call the procedure fPreEditSaveKeys to save the KEYS in all tables we are
    editing so the application will know if a KEY Field has been changed and Indexes need to be
    regenerated }
  fPreEditSaveKeys;
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 2;
  sDataMode := 'E';
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(True);
  { Put the cursor in the first field }
  ONFItemType.SetFocus;
End;

Procedure TfrmOFSP2.ButPlayerInfoDoneExecute(Sender: TObject);
Begin
  pSaveChanges;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  pFillStaticFields;
  { Turn the Player Info Data Area OFF }
  pWorkSheetInfo(False);
  pUpdateStatusBar;
End;

Procedure TfrmOFSP2.ButAddActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 1;
  sDataMode := 'A';
  { Clear the Edit Fields of any data }
  fInitRecord(fFilno);
  pFillFields;
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(True);
  { Put the cursor in the first field }
  ONFItemType.SetFocus;
End;

Procedure TfrmOFSP2.ButCopyActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 3;
  sDataMode := 'C';
  fAdd_Init;
  pFillFields;
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(True);
  { Put the cursor in the first field }
  ONFItemType.SetFocus;
End;

Procedure TfrmOFSP2.ButDeleteActionExecute(Sender: TObject);
Begin
  fDelete_Record(True, self.HelpContext);
  pUpdateStatusBar;
  If IOFSP2Recs > 0 Then Begin
    { Move the record pointer in the table to the next available record and load the
      data in the screen fields }
    pNextRecordf;
    If ButNext.Enabled Then ButNext.SetFocus Else ButQuit.SetFocus;
  End
  Else Begin
    liRecnoOF := 0;
    pClearFields;
    pFillFields;
    pButtOn(False);
    ButAdd.SetFocus;
  End;
End;

Procedure TfrmOFSP2.pUpdateStatusBar;
Begin
  IOFSP2Recs := db33.UsedRecs(fDatF[FOfs_no]^);
  StatusBar1.Panels[1].Text := UpperCase(dbase.Prepend + 'FOfs.DAT') + ' - Contains ' +
    Trim(IntToStr(IOFSP2Recs)) + ' Records.';
  StatusBar1.Panels[2].Text := IntToStr(iOFSP2sCount) + ' items';
End;

Procedure TfrmOFSP2.pDateExit(Sender: TObject);
Var
  sDateText: String;
Begin
  { Replace all blanks with zeros in the date }
  (Sender As TOvcDateEdit).Text := fFillDateBlanks((Sender As TOvcDateEdit).Text);
  { Must take out the slashes before passing the date to ChkDate Function }
  sDateText := fStripDateField((Sender As TOvcDateEdit).Text);
  If Not (ChkDate(sDateText)) Then Begin
    MessageDlg('Event date is invalid.', mtError, [mbOK], 0);
    (Sender As TOvcDateEdit).SetFocus;
  End;
End;

Procedure TfrmOFSP2.MinusKeyExecute(Sender: TObject);
Begin
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := 5; { 5 = Office Financial Part 1, frmOffFinP1 }
  frmWorkSheet.bFTQuitUsed := False;
  Close;
End;

Procedure TfrmOFSP2.F1KeyExecute(Sender: TObject);
Begin
  //pRunExtProg(frmOFSP2, CFG.LoadPath + 'WUtility', 'MANUAL\CONTENTS');
  keybd_event(VK_F1, MapVirtualKey(VK_F1, 0), 0, 0);
End;

Procedure TfrmOFSP2.pGetOFSP2Data;
Begin
  If liRecnoOF > 0 Then
    { Fill the form fields with the Data }
    pFillFields
  Else Begin
    { Go to the first record }
    fTop_Record;
    If fStatus_OK(fFilno) Then Begin
      liRecnoOF := fDbase.fRecno[fFilno];
    End
    Else Begin
      liRecnoOF := 0;
      fInitRecord(fFilno);
      pButtOn(False);
      ButAdd.SetFocus;
    End;
    { Fill the form fields with the Data }
    pFillFields;
  End;
End;

Procedure TfrmOFSP2.pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Var
  wKey: Word;
Begin
  wKey := Key;
  Case key Of
    { 107 is the Plus Key was pressed on the numeric keypad }
    107: If GBMenu.Enabled Then PlusKeyExecute(Sender);
    { 109 is the Minus Key was pressed on the numeric keypad }
    109: If GBMenu.Enabled Then MinusKeyExecute(Sender);
    { Return key was pressed }
    VK_RETURN: If Not GBMenu.Enabled Then Begin
        key := 0;
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
      End;
    { ESCape key was pressed, Abort the record or record changes... }
    VK_ESCAPE: If Not GBMenu.Enabled Then Begin
        bEscKeyUsed := True;
        pAbortChanges;
        If Not ok Then Begin
          { frmWorkSheet.iFormTag must be set to the forms number that user selected }
          frmWorkSheet.iFormTag := 0;
          frmWorkSheet.bFTQuitUsed := False;
          Close;
        End;
        //Else Close;
      End;
    { F9 key was pressed, save the record or the changes... }
    VK_F9: If Not GBMenu.Enabled Then pSaveChanges;
  Else
    If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
      If (Key = VK_UP) Then Begin
        ButPrev.Click;
        ButPrev.SetFocus;
      End
      Else Begin
        ButNext.Click;
        ButNext.SetFocus;
      End;
      Key := 0;
    End;
  End;
End;

Procedure TfrmOFSP2.pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
Var
  wKey: Word;
Begin
  { Do not trap for the Return Key here, it causes the cursor to move two fields at a time }
  wKey := Key;
  Case key Of
    { ESCape key was pressed, Abort the record or record changes... }
    VK_ESCAPE: If Not GBMenu.Enabled Then Begin
        bEscKeyUsed := True;
        pAbortChanges;
        If Not ok Then Begin
          { frmWorkSheet.iFormTag must be set to the forms number that user selected }
          frmWorkSheet.iFormTag := 0;
          frmWorkSheet.bFTQuitUsed := False;
          Close;
        End;
        //Else Close;
      End;
    { F9 key was pressed, save the record or the changes... }
    VK_F9: If Not GBMenu.Enabled Then pSaveChanges;
  Else
    If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
      If (Key = VK_UP) Then Begin
        ButPrev.Click;
        ButPrev.SetFocus;
      End
      Else Begin
        ButNext.Click;
        ButNext.SetFocus;
      End;
      Key := 0;
    End;
  End;
End;

Procedure TfrmOFSP2.PlusKeyExecute(Sender: TObject);
Begin
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := 0; { 0 = Tournament Information, frmTournInfo }
  frmWorkSheet.bFTQuitUsed := False;
  Close;
End;

Procedure TfrmOFSP2.F2KeyExecute(Sender: TObject);
Var
  iForLoop: Integer;
Begin
  GBMenu.Enabled := False;
  For iForLoop := (PopupMenu1.Items.Count - 1) Downto 0 Do
    PopupMenu1.Items.Items[iForLoop].Enabled := True;
  PopupMenu1.Items.Items[(Scrno - 1)].Enabled := False;
  PopupMenu1.Popup(350, 500);
  GBMenu.Enabled := True;
End;

Procedure TfrmOFSP2.pPuMenuItemSelected(Sender: TObject);
Begin
  { Variable iFormTag possible values and their meanings:
    0 = Tournament Information, frmTournInfo
    1 = Tournament Worksheet, frmWorkSheet
    2 = Invoice, frmInvoice
    3 = Voucher, frmVoucher
    4 = Balance Sheet, frmBalanceSheet
    5 = Office Financial Part 1, frmOffFinP1
    6 = Office Financial Part 2, frmOffFinP2 }
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := (Sender As TMenuItem).Tag;
  If ((frmWorkSheet.iFormTag >= 0) And (frmWorkSheet.iFormTag <= 6)) Then Begin
    { Store this records key to the Public Variable:  frmWorkSheet.ksFTInfoKey }
    If ksFTInfoKey = '' Then ksFTInfoKey := GetKey(FTINFO_no, 1);
    { Store the Invoice Record FHash Key to the Public Variable:  frmWorkSheet.ksFInvoiceKey
      If the Voucher Screen was selected }
    If frmWorkSheet.iFormTag = 3 Then frmWorkSheet.pGetInvoiceKey;
    Close;
  End;
End;

Procedure TfrmOFSP2.pWhichScreen(oSender: TObject);
Begin
  frmWorkSheet.sCaption := (oSender As TMenuItem).Caption;
  frmWorkSheet.sPopupItemName := frmWorkSheet.sCaption;
  MessageDlg('You selected item ' + IntToStr(frmWorkSheet.iformTag) +
    ', which is ' + frmWorkSheet.sPopupItemName, mtInformation, [mbOK], 0);
End;

Procedure TfrmOFSP2.pAbortChanges;
Begin
  FreeHintWin;
  pResetTabStops;
  { Turn the Player Info Data Area OFF }
  pWorkSheetInfo(False);
  { Fill the form fields with the Data }
  If iDataMode = 1 Then fTop_Record
  Else fGetARec(fFilNo);
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := Null;
  pFillFields;
End;

Procedure TfrmOFSP2.F8KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (1) to Print to Screen }
  PrintReport(1);
End;

Procedure TfrmOFSP2.F7KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (2) to Print Reports }
  PrintReport(2);
End;

Procedure TfrmOFSP2.F6KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (3) to Print Reports to an ASCII file }
  PrintReport(3);
End;

Function TfrmOFSP2.fShowItemType(Const Item: byte): String;
Begin
  If ((Item > 0) And (Item <= 7)) Then
    fShowItemType := Pad(Copy(VoucherTypes[Item], 3, 14), 14)
  Else fShowItemType := '';
End;

Function TfrmOFSP2.fShowSubItemType(Const Item, SubItem: byte): String;
Begin
  If fValidVoucherSubItem(Item, SubItem) Then
    fShowSubItemType := Pad(Copy(VoucherSubTypes[Item, SubItem], 3, 20), 20)
  Else fShowSubItemType := '';
End;

Function TfrmOFSP2.fValidVoucherSubItem(Const Item, SubItem: byte): boolean;
Begin
  fValidVoucherSubItem := fValidVoucherItem(Item) And (SubItem > 0) And (SubItem <=
    Vsubtypes[Item]);
End;

Function TfrmOFSP2.fValidVoucherItem(Const Item: byte): boolean;
Begin
  fValidVoucherItem := (Item > 0) And (Item <= 7);
End;

Procedure TfrmOFSP2.FormClose(Sender: TObject; Var Action: TCloseAction);
Begin
  If frmWorkSheet.iFormTag = (Scrno - 1) Then frmWorkSheet.iFormTag := 1;
  liRecnoOF := 0;
  If frmWorkSheet.bFTQuitUsed Then Begin
    { Close any open tables }
    fdBase.fCloseFiles;
    dBase.CloseFiles;
    Application.Terminate;
  End;
End;

Function TfrmOFSP2.fShowOFSP2Item(Const Item: String): String;
Var
  i: byte;
Begin
  i := Ival(Item);
  If (i > 0) And (i <= MaxOfsItemtypes) Then
    fShowOFSP2Item := Pad(Copy(OfsItemHelp[Ival(Item)], 4, 20), 20)
  Else fShowOFSP2Item := '';
End;

Procedure TfrmOFSP2.pClearFields;
Begin
  OSFSanctionNo.Text := FTINFO.SANCTION_NO;
  // -------------------------------------------------
  ONFItemType.Text := '';
  LabelItemType.Caption := '';
  OSFDescription.Text := '';
  OPFItems.Text := '0';
  OPFItemRate.Text := '0';
  OPFAmount.Text := '0';
End;

Procedure TfrmOFSP2.pCopyRecData;
Begin
  { Used when Adding or Copying a Record }
  With FOFS Do Begin
    SANCTION_NO := Pad(OSFSanctionNo.Text,10);
    // -------------------------------------------------
    ITEM_TYPE := LeftPad(Trim(ONFItemType.Text),2);
    DESCRIPTION := Pad(OSFDescription.Text,25);
    ITEMS := LeftPad(Trim(OPFItems.GetStrippedEditString),6);
    ITEM_RATE := LeftPad(Trim(OPFItemRate.GetStrippedEditString),8);
    AMOUNT := LeftPad(Trim(OPFAmount.GetStrippedEditString),9);
  End;
End;

Procedure TfrmOFSP2.pCheckEditFields;
Begin
  { Used when Editing a Record }
  // ---------------------------------------------------------------------
  With FOFS Do Begin
    ITEM_TYPE := LeftPad(Trim(ONFItemType.Text),2);
    DESCRIPTION := Pad(OSFDescription.Text,25);
    ITEMS := LeftPad(Trim(OPFItems.GetStrippedEditString),6);
    ITEM_RATE := LeftPad(Trim(OPFItemRate.GetStrippedEditString),8);
    AMOUNT := LeftPad(Trim(OPFAmount.GetStrippedEditString),9);
  End;
End;

Procedure TfrmOFSP2.pSaveChanges;
Begin
  { Data Mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  Case iDataMode Of
    1, 3: Begin // Add or Copy Modes
        pCopyRecData;
        fdbase.fAdd_Record;
        liRecnoOF := fDbase.fRecno[fFilno];
        pButtOn(True);
        Inc(iOFSP2sCount);
        pUpdateStatusBar;
        FixTotals(iDataMode);
      End;
    2: Begin // Edit Mode
        pCheckEditFields;
        bKeyIsUnique := fPostEditFixKeys;
        If Not bKeyIsUnique Then Begin
          If (MessageDlg('This Office Financial Sheet Item already exists in this database.',
            mtError, [mbOK], 0) = mrOK) Then Begin
            ONFItemType.SetFocus;
            Exit;
          End
          Else pAbortChanges;
        End
        Else Begin
          {If fValidateFields Then}
          fdbase.fPutARec(fFilNo);
          FixTotals(iDataMode);
        End;
      End;
  End;
  pFillFields;
  iDataMode := 0;
  sDataMode := null;
  pResetTabStops;
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(False);
End;

Procedure TfrmOFSP2.pButtOn(bTurnOn: Boolean);
Begin
  // ------------------------------------------------------------
  { Buttons }
  ButCopy.Enabled := bTurnOn;
  ButCopyAction.Enabled := bTurnOn;
  ButDelete.Enabled := bTurnOn;
  ButDeleteAction.Enabled := bTurnOn;
  ButEdit.Enabled := bTurnOn;
  ButEditAction.Enabled := bTurnOn;
  ButFind.Enabled := bTurnOn;
  ButFindAction.Enabled := bTurnOn;
  ButLast.Enabled := bTurnOn;
  ButLastAction.Enabled := bTurnOn;
  ButNext.Enabled := bTurnOn;
  ButNextAction.Enabled := bTurnOn;
  ButPrev.Enabled := bTurnOn;
  ButPrevAction.Enabled := bTurnOn;
  ButTop.Enabled := bTurnOn;
  ButTopAction.Enabled := bTurnOn;
  // ------------------------------------------------------------
  { Function Keys }
  F3Key.Enabled := bTurnOn;
  F4Key.Enabled := bTurnOn;
  F5Key.Enabled := bTurnOn;
  ButtonF6Key.Enabled := bTurnOn;
  F6Key.Enabled := bTurnOn;
  ButtonF7Key.Enabled := bTurnOn;
  F7Key.Enabled := bTurnOn;
  ButtonF8Key.Enabled := bTurnOn;
  F8Key.Enabled := bTurnOn;
  // ------------------------------------------------------------
End;

Procedure TfrmOFSP2.ONFItemTypeEnter(Sender: TObject);
//Var
//  sHints, cRetLF: String;
Begin
  //  cRetLF := #13 + #10;
  //  sHints := ' 1  Sanction free tables';
  //  sHints := sHints + cRetLF + ' 2  Computer/printer rental';
  //  sHints := sHints + cRetLF + ' 3  ACBL computer hands';
  //  sHints := sHints + cRetLF + ' 4  ACBL membership fees (US)';
  //  sHints := sHints + cRetLF + ' 5  Other for ACBL (Charity, etc)';
  //  sHints := sHints + cRetLF + ' 6  Non-mem sanction fee';
  //  sHints := sHints + cRetLF + ' 7  Other expenses paid by ACBL';
  //  sHints := sHints + cRetLF + ' 8  Scrip to ACBL';
  //  sHints := sHints + cRetLF + ' 9  US Check';
  //  If UpperCase(FTInfo.USE_EX_RATE) = 'Y' Then Begin
  //    sHints := sHints + cRetLF + '10  CAN Check';
  //    sHints := sHints + cRetLF + '11  ACBL membership fees (CAN)';
  //  End;
  //  ONFItemType.Hint := sHints;
    // ---------------------------------------------------------------------------
  pResetTabStops;
  bDoEdit := True;
  FOFS.ITEM_TYPE := LeftPad(Trim(ONFItemType.Text),2);
  bDoEdit := CustomFinanceEdit(Scrno, 1, FOFS.ITEM_TYPE, True, bDisplay_Rec, sDataMode, Sender);
  // ---------------------------------------------------------------------------
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'N');
End;

Function TfrmOFSP2.fOfsStat(Const vtype, mask: byte): boolean;
Begin
  fOfsStat := (OfsEditStat[vtype] And mask) <> 0;
End;

Procedure TfrmOFSP2.OPFItemsExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Trim(FOFS.ITEMS) <> Trim(OPFItems.Text) Then Begin
        If fOfsStat(Ival(ONFItemType.Text), ofsNoItems) Then Begin {units}
          OPFItems.Text := Fstr(0, 6, 1);
        End
        Else Begin
          FOFS.ITEMS := LeftPad(Trim(OPFItems.Text),6);
          bDoEdit := CustomFinanceEdit(Scrno, 4, FOFS.ITEMS, False, bDisplay_Rec, sDataMode, Sender);
          If bDisplay_Rec Then pFillFields;
          plOnFieldLastStep(False, Sender, 'P');
        End;
      End;
    End;
  End;
End;

Procedure TfrmOFSP2.OPFItemRateExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Trim(FOFS.ITEM_RATE) <> Trim(OPFItemRate.Text) Then Begin
        FOFS.ITEM_RATE := LeftPad(Trim(OPFItemRate.Text),8);
        bDoEdit := CustomFinanceEdit(Scrno, 5, FOFS.ITEM_RATE, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'P');
      End;
    End;
  End;
End;

//Procedure TfrmOFSP2.pAmountCalc;
//Begin
//  If fOfsStat(Ival(ONFItemType.Text), ofsNoAmount) Then Begin {Amount}
//    If Not (Ival(ONFItemType.Text) In [2, 3, 6, 8]) Then
//      OPFAmount.Text := Fstr(0, 9, 2)
//    Else Begin
//      If Valu(OPFItemRate.Text) = 0 Then OPFItemRate.Text := Fstr(0.5, 8, 2);
//      OPFAmount.Text := Fstr(Valu(OPFItems.Text) * Valu(OPFItemRate.Text), 9, 2);
//    End;
//  End;
//End;

Procedure TfrmOFSP2.OPFAmountExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      FOFS.AMOUNT := LeftPad(Trim(OPFAmount.Text),9);
      bDoEdit := CustomFinanceEdit(Scrno, 6, FOFS.AMOUNT, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmOFSP2.ONFItemTypeExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (ValidOfsItem(StrToInt(ONFItemType.Text))) Then Begin
        ErrBox('Invalid Item Type', mc, 0);
        ONFItemType.Text := '';
        ONFItemType.SetFocus;
      End
      Else Begin
        { Popup boxes for the appropriate Item Description }
        // If (Trim(FOFS.ITEM_TYPE) <> Trim(ONFItemType.Text)) Then pFieldCalcs;
        FOFS.ITEM_TYPE := LeftPad(Trim(ONFItemType.Text),2);
        bDoEdit := CustomFinanceEdit(Scrno, 1, FOFS.ITEM_TYPE, False, bDisplay_Rec, sDataMode, Sender);
        // ---------------------------------------------------------------------------
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'N');
        // ---------------------------------------------------------------------------
        If Ival(ONFItemType.Text) = 0 Then
          LabelItemType.Caption := ''
        Else
          LabelItemType.Caption := fShowOFSP2Item(FOfs.ITEM_TYPE);
      End;
    End;
  End;
End;

Procedure TfrmOFSP2.OSFDescriptionExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      FOFS.DESCRIPTION := Pad(OSFDescription.Text,25);
      bDoEdit := CustomFinanceEdit(Scrno, 3, FOFS.DESCRIPTION, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmOFSP2.Action1Execute(Sender: TObject);
Begin
  pSetWinState;
End;

Procedure TfrmOFSP2.Action2Execute(Sender: TObject);
Begin
  pSetWinState;
End;

Procedure TfrmOFSP2.ResizeKit1ExitResize(Sender: TObject; XScale, YScale: Double);
Begin
  pSetPanelWidth(frmOFSP2, StatusBar1);
End;

Procedure TfrmOFSP2.pSetWinState;
Begin
  If WindowState = wsMaximized Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  frmWorkSheet.bWinStNorm := (WindowState = wsNormal);
  If frmWorkSheet.bWinStNorm Then frmWorkSheet.WindowState := wsNormal
  Else frmWorkSheet.WindowState := wsMaximized;
  pSetPanelWidth(frmOFSP2, StatusBar1);
End;

Procedure TfrmOFSP2.OSFDescriptionEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FOFS.DESCRIPTION := Pad(OSFDescription.Text,25);
  bDoEdit := CustomFinanceEdit(Scrno, 3, FOFS.DESCRIPTION, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmOFSP2.OPFItemsEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FOFS.ITEMS := LeftPad(Trim(OPFItems.Text),6);
  bDoEdit := CustomFinanceEdit(Scrno, 4, FOFS.ITEMS, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmOFSP2.OPFItemRateEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FOFS.ITEM_RATE := LeftPad(Trim(OPFItemRate.Text),8);
  bDoEdit := CustomFinanceEdit(Scrno, 5, FOFS.ITEM_RATE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmOFSP2.OPFAmountEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FOFS.AMOUNT := LeftPad(Trim(OPFAmount.Text),9);
  bDoEdit := CustomFinanceEdit(Scrno, 6, FOFS.AMOUNT, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmOFSP2.Action4Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmOFSP2.Action5Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Function TfrmOFSP2.FormHelp(Command: Word; Data: Integer; Var CallHelp: Boolean): Boolean;
Begin
  if data < 500000 then
    HtmlHelp(GetDesktopWindow,DefChmFile,HH_HELP_CONTEXT,Data);
//  If data < 500000 Then WinHelp(Application.Handle, 'ACBLSCORE.HLP', Help_Context, Data);
End;

//Procedure TfrmOFSP2.plOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
//Begin
//  If UpperCase(sType) = 'S' Then pOSFieldAvail(bDoedit, (oSender As TOvcSimpleField))
//  Else If UpperCase(sType) = 'P' Then pOPFieldAvail(bDoedit, (oSender As TOvcPictureField))
//  Else If UpperCase(sType) = 'C' Then pOCBFieldAvail(bDoedit, (oSender As TOvcComboBox))
//  Else If UpperCase(sType) = 'D' Then pODEFieldAvail(bDoedit, (oSender As TOvcDateEdit))
//  Else If UpperCase(sType) = 'H' Then pLMDHCBFieldAvail(bDoedit, (oSender As TLMDHeaderListComboBox))
//  Else If UpperCase(sType) = 'N' Then pONFieldAvail(bDoedit, (oSender As TOvcNumericField))
//  Else MessageDlg('No Procedure created for that type!', mtError, [mbOK], 0);
//  // -------------------------------------------------------------------------------------------------------
//  If bDisplay_Rec Then pFillFields;
//  // -------------------------------------------------------------------------------------------------------
//  If bWasOnEnter Then Begin
//    If UpperCase(sType) = 'S' Then Begin
//      If Not (oSender As TOvcSimpleField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'P') Then Begin
//      If Not (oSender As TOvcPictureField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'D') Then Begin
//      If Not (oSender As TOvcDateEdit).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'H') Then Begin
//      If Not (oSender As TLMDHeaderListComboBox).TabStop Then
//        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'C') Then Begin
//      If Not (oSender As TOvcComboBox).TabStop Then
//        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'N') Then Begin
//      If Not (oSender As TOvcNumericField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End;
//  End;
//End;
//
//Procedure TfrmOFSP2.pONFieldAvail(bAvail: Boolean; oOSField: TOvcNumericField);
//Var
//  sFieldName: String;
//Begin
//  sFieldName := oOSField.Name;
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmOFSP2.pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
//Var
//  sFieldName: String;
//Begin
//  sFieldName := oOSField.Name;
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoCaretToEnd, efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [efoCaretToEnd];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmOFSP2.pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
//Var
//  sFieldName: String;
//Begin
//  sFieldName := oOSField.Name;
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoCaretToEnd, efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [efoCaretToEnd];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmOFSP2.pOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
//Var
//  sFieldName: String;
//Begin
//  sFieldName := oOSField.Name;
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmOFSP2.pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
//Var
//  sFieldName: String;
//Begin
//  sFieldName := oOSField.Name;
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.ReadOnly := True;
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.ReadOnly := False;
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmOFSP2.pLMDHCBFieldAvail(bAvail: Boolean; oOSField: TLMDHeaderListComboBox);
//Var
//  sFieldName: String;
//Begin
//  sFieldName := oOSField.Name;
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.ReadOnly := True;
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.ReadOnly := False;
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;

Procedure TfrmOFSP2.pResetTabStops;
Begin
  { Reset ALL editable fields tabstop to true so fields will be available for next action: Add/Edit/Copy }
  ONFItemType.TabStop := True;
  OSFDescription.TabStop := True;
  OPFItems.TabStop := True;
  OPFItemRate.TabStop := True;
  OPFAmount.TabStop := True;
  OSFSanctionNumberO.TabStop := True;
End;

End.

