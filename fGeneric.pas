Unit fGeneric;

Interface

Procedure pDiffScreen(iPassedKey: Integer);

Implementation

Uses formWorkSheet;


Procedure pDiffScreen(iPassedKey: Integer);
Begin
  { Variable iPassedKey possible values and their meanings:
    0 = Tournament Information, frmTournInfo
    1 = Tournament Worksheet, frmFinance
    2 = Invoice, frmInvoice
    3 = Voucher, frmVoucher
    4 = Balance Sheet, frmBalanceSheet
    5 = Office Financial Part 1, frmOffFinP1
    6 = Office Financial Part 2, frmOffFinP2 }
  Case iPassedKey Of
    0: Begin { Open frmPTInfo (Tournament Information) }
        ksFTInfoKey := '';
        frmWorkSheet.pOpenFTInfo;
      End;
    1: ; { 1 = Tournament Worksheet, frmWorkSheet }
    2: ; { 2 = Invoice, frmInvoice }
    3: ; { 3 = Voucher, frmVoucher }
    4: ; { 4 = Balance Sheet, frmBalanceSheet }
    5: ; { 5 = Office Financial Part 1, frmOffFinP1 }
    6: ; { 6 = Office Financial Part 2, frmOffFinP2 }
  End;
End;

End.
