Unit formTblsCurrOpen;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ResizeKit, LMDGroupBox, StdCtrls,hh,csdef;

Type
  TfrmTblsCurrOpen = Class(TForm)
    ResizeKit1: TResizeKit;
    Memo1: TMemo;
    Procedure FormActivate(Sender: TObject);
    Procedure FormKeyDown(Sender: TObject; Var Key: Word;
      Shift: TShiftState);
    Procedure FormKeyUp(Sender: TObject; Var Key: Word;
      Shift: TShiftState);
    Procedure Memo1KeyDown(Sender: TObject; Var Key: Word;
      Shift: TShiftState);
    Procedure Memo1KeyUp(Sender: TObject; Var Key: Word;
      Shift: TShiftState);
    function FormHelp(Command: Word; Data: Integer;
      var CallHelp: Boolean): Boolean;
  private
    { Private declarations }
  public
    { Public declarations }
  End;

Var
  frmTblsCurrOpen: TfrmTblsCurrOpen;
  iLines: Integer;

Implementation

Uses fDbase, Dbase, formWorkSheet;

{$R *.dfm}

Procedure TfrmTblsCurrOpen.FormActivate(Sender: TObject);
Var
  iForLoop: Integer;
  sToAdd: String;
Begin
  { Display a list of all open tables. }
  ResizeKit1.Enabled := False;
  { Clear any items in the Memo }
  Memo1.Lines.Clear;
  Memo1.Lines.Add('[ Tables Currently Opened ]');
  Memo1.Alignment := taLeftJustify;
  Memo1.Lines.Add('');
  Memo1.Lines.Add('* Filno Array Tables');
  { Fill the Memo with the names of the open Finance tables }
  { 3 = Tourn, 4 = TournEv }
  For iForLoop := 3 To 4 Do Begin
    If dbase.DBFileOpen[iForLoop] Then Begin
      Memo1.Lines.Add(dbase.DBNames[iForLoop, 0] + '  -  ' + IntToStr(iForLoop));
    End;
  End;
  { Fill the Memo with the names of the open Finance tables }
  Memo1.Lines.Add('');
  Memo1.Lines.Add('* FFilno Array Tables');
  iLenArray := fMaxFilno;
  For iForLoop := 1 To iLenArray Do Begin
    If fdbase.fDBFileOpen[iForLoop] Then Begin
      Memo1.Lines.Add(fdbase.fDBNames[iForLoop, 0] + '  -  ' + IntToStr(iForLoop));
    End;
  End;
  iLenArray := MaxFilno;
  iLines := Memo1.Lines.Count;
  ResizeKit1.Enabled := True;
  frmTblsCurrOpen.Height := iLines * 19;
End;

Procedure TfrmTblsCurrOpen.FormKeyDown(Sender: TObject; Var Key: Word;
  Shift: TShiftState);
Begin
  If Key = VK_ESCAPE Then Close;
End;

Procedure TfrmTblsCurrOpen.FormKeyUp(Sender: TObject; Var Key: Word;
  Shift: TShiftState);
Begin
  If Key = VK_ESCAPE Then Close;
End;

Procedure TfrmTblsCurrOpen.Memo1KeyDown(Sender: TObject; Var Key: Word;
  Shift: TShiftState);
Begin
  If Key = VK_ESCAPE Then Close;
End;

Procedure TfrmTblsCurrOpen.Memo1KeyUp(Sender: TObject; Var Key: Word;
  Shift: TShiftState);
Begin
  If Key = VK_ESCAPE Then Close;
End;

function TfrmTblsCurrOpen.FormHelp(Command: Word; Data: Integer; var CallHelp: Boolean): Boolean;
begin
  if data < 500000 then
    HtmlHelp(GetDesktopWindow,DefChmFile,HH_HELP_CONTEXT,Data);
//  if data < 500000 then WinHelp(Application.Handle,'ACBLSCORE.HLP',Help_Context,Data);
end;

End.
