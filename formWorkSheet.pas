Unit formWorkSheet;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ovcbase, ovcdlg, ovcclkdg, StdCtrls, Mask, Buttons,
  ToolWin, ExtCtrls, ActnMan, ActnCtrls, Menus, ActnList, ovcef, ovcsf,
  ovcrvcbx, ovcpb, ovcpf, ovcnf, ovceditf, ovcedpop, ovcedcal, ovcbordr,
  ovcbcalc, ovcbcldr, XPStyleActnCtrls, Grids, ImgList, ResizeKit, LMDCustomComponent,
  LMDWndProcComponent, LMDFormShape, LMDShapeHint,
  LMDHint, LMDCustomScrollBox, LMDListBox, LMDCustomControl, LMDCustomPanel,
  LMDCustomBevelPanel, LMDBaseEdit, LMDCustomEdit, LMDCustomMaskEdit, LMDCustomExtCombo,
  LMDCustomListComboBox, LMDListComboBox, ovccmbx, LMDHeaderListComboBox,
  LMDCustomComboBox, LMDComboBox, STSTRS, LMDIniCtrl, LMDFormA,
  LMDStorBase, LMDStorXMLVault, LMDStorBinVault, LMDCustomHint,
  LMDCustomShapeHint, LMDMessageHint, LibData, LMDCalendarEdit,
  LMDCustomButton, LMDButton, fDbase, Dbase, DB33, LibUtil, Clipbrd, CSDef,
  Util1, Util2, YesNoBoxU, PVIO, VListBox, History, LMDControl,
  LMDDrawEdge, LMDCustomParentPanel, LMDCustomGroupBox, LMDGroupBox, JvHint,
  hh,hh_funcs;

{ Initialize all variables for finances located in the file fVariables.inc}
{$I fVariables.inc}

Type
  TfrmWorkSheet = Class(TForm)
    GBMenu: TGroupBox;
    Shape23: TShape;
    ButNext: TButton;
    ButPrev: TButton;
    ButFind: TButton;
    ButTop: TButton;
    ButLast: TButton;
    ButEdit: TButton;
    ButAdd: TButton;
    ButCopy: TButton;
    ButDelete: TButton;
    ButQuit: TButton;
    ButtonF7Key: TButton;
    ButtonF6Key: TButton;
    ButtonF8Key: TButton;
    ButtonF2Key: TButton;
    StaticTextF3Key: TStaticText;
    StaticTextF4Key: TStaticText;
    StaticTextF5Key: TStaticText;
    StaticTextF2Key: TStaticText;
    ButtonF1Key: TButton;
    StaticTextF1Key: TStaticText;
    ActionManager1: TActionManager;
    ButNextAction: TAction;
    ButPrevAction: TAction;
    ButFindAction: TAction;
    ButTopAction: TAction;
    ButLastAction: TAction;
    ButEditAction: TAction;
    ButAddAction: TAction;
    ButCopyAction: TAction;
    ButDeleteAction: TAction;
    ButQuitAction: TAction;
    F4Key: TAction;
    F3Key: TAction;
    F5Key: TAction;
    F2Key: TAction;
    F7Key: TAction;
    F1Key: TAction;
    F8Key: TAction;
    F6Key: TAction;
    CtrlCKey: TAction;
    ButPlus: TButton;
    StaticText1: TStaticText;
    PlusKey: TAction;
    ButMinus: TButton;
    StaticText2: TStaticText;
    MinusKey: TAction;
    StatusBar1: TStatusBar;
    OvcController1: TOvcController;
    LMDHint1: TLMDHint;
    ActionManager2: TActionManager;
    ButPlayerInfoDone: TAction;
    StaticText3: TStaticText;
    GBTournWorkSheet: TGroupBox;
    Shape1: TShape;
    Label3: TLabel;
    Label4: TLabel;
    Label10: TLabel;
    OSFSanctionNo: TOvcSimpleField;
    OSFEventCode: TOvcSimpleField;
    OSFEventName: TOvcSimpleField;
    LMDTournWSDone: TLMDButton;
    SpeedButton1: TSpeedButton;
    ListTables: TAction;
    Label1: TLabel;
    OSFTournament: TOvcSimpleField;
    Label2: TLabel;
    ODEEventDate: TOvcDateEdit;
    LabelDOWText: TLabel;
    Label5: TLabel;
    OPFTime: TOvcPictureField;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    OPFTables: TOvcPictureField;
    OPFEntryFee: TOvcPictureField;
    OPFEntryTotal: TOvcPictureField;
    OPFNonMember: TOvcPictureField;
    OPFFillIn: TOvcPictureField;
    Label17: TLabel;
    OSFComment: TOvcSimpleField;
    Label19: TLabel;
    OSFNewPage: TOvcSimpleField;
    PopupMenu1: TPopupMenu;
    TournamentInformation1: TMenuItem;
    Worksheet1: TMenuItem;
    Invoice1: TMenuItem;
    Voucher1: TMenuItem;
    Ballancesheet1: TMenuItem;
    Officefinancialpart11: TMenuItem;
    Officefinancialpart2: TMenuItem;
    Quit1: TMenuItem;
    OSFSanctionNumberWS: TOvcSimpleField;
    LMDGroupBox1: TLMDGroupBox;
    Label15: TLabel;
    OPFTotalTables: TOvcPictureField;
    Label16: TLabel;
    OPFTotalOfEntry: TOvcPictureField;
    Label18: TLabel;
    OPFPlusNonMembers: TOvcPictureField;
    Label20: TLabel;
    OPFGrossEntries: TOvcPictureField;
    Label21: TLabel;
    OPFLessFillins: TOvcPictureField;
    Label22: TLabel;
    OPFNetReceipts: TOvcPictureField;
    Shape2: TShape;
    ONFSessions: TOvcNumericField;
    ONFEntries: TOvcNumericField;
    LabelLinePageControl: TLabel;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    Procedure ButQuitActionExecute(Sender: TObject);
    Procedure FormClose(Sender: TObject; Var Action: TCloseAction);
    Procedure FormActivate(Sender: TObject);
    Function fCheckNeededFiles: Boolean;
    Function fFinancePrepend(Const Fno: Integer): String;
    Procedure pOpenNeededFiles;
    Procedure ListTablesExecute(Sender: TObject);
    Procedure pFillFields;
    Procedure pNextRecordf;
    Procedure pPrevRecordf;
    Procedure ButNextActionExecute(Sender: TObject);
    Procedure ButPrevActionExecute(Sender: TObject);
    Procedure ButLastActionExecute(Sender: TObject);
    Procedure ButTopActionExecute(Sender: TObject);
    Procedure ButFindActionExecute(Sender: TObject);
    Procedure pWorkSheetInfo(bEnable: Boolean);
    Procedure ButEditActionExecute(Sender: TObject);
    Procedure ButPlayerInfoDoneExecute(Sender: TObject);
    Procedure ButAddActionExecute(Sender: TObject);
    Procedure ButCopyActionExecute(Sender: TObject);
    Procedure ButDeleteActionExecute(Sender: TObject);
    Procedure pUpdateStatusBar;
    Procedure OSFEventCodeExit(Sender: TObject);
    //Function fFound(Const Fno, Kno: Integer; ChkKey: KeyStr): Boolean;
    Procedure pDateExit(Sender: TObject);
    Procedure MinusKeyExecute(Sender: TObject);
    Procedure pOpenFTInfo;
    Procedure F1KeyExecute(Sender: TObject);
    Procedure pGetWorkSheetData;
    Procedure pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure PlusKeyExecute(Sender: TObject);
    Procedure OPFTimeExit(Sender: TObject);
    Procedure F2KeyExecute(Sender: TObject);
    Procedure pPuMenuItemSelected(Sender: TObject);
    Procedure OSFNewPageExit(Sender: TObject);
    Procedure pAbortChanges;
    Procedure F8KeyExecute(Sender: TObject);
    Procedure F7KeyExecute(Sender: TObject);
    Procedure F6KeyExecute(Sender: TObject);
    Procedure pWhichScreen(oSender: TObject);
    Procedure pOpenInvoice; // Open frmInvoice
    Procedure pOpenVoucher; // Open frmVoucher
    Procedure pOpenBalanceSheet; // Open frmBalanceSheet
    Procedure pOpenOffFinP1; // Open frmOffFinP1
    Procedure pOpenOffFinP2;
    Procedure pSaveChanges;
    Procedure pCopyRecData;
    Procedure pCheckEditFields;
    Procedure pCountWorkSheets;
    Procedure pClearFields;
    Function fValidateFields: Boolean;
    Procedure pGetInvoiceKey;
    Procedure pDateFieldKeyPress(Sender: TObject; Var Key: Char);
    Procedure OPFEntryFeeExit(Sender: TObject);
    Procedure pButtOn(bTurnOn: Boolean);
    Procedure pClear_Rec(Const Fno: Integer);
    Procedure pFillDescript;
    Procedure ONFEntriesExit(Sender: TObject);
    Procedure FullScreenExecute(Sender: TObject);
    Procedure Action1Execute(Sender: TObject);
    Procedure Action2Execute(Sender: TObject);
    Procedure ResizeKit1ExitResize(Sender: TObject; XScale, YScale: Double);
    Procedure pSetWinState;
    //    Procedure pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
    //    Procedure pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
    //    Procedure pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
    //    Procedure pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
    //    Procedure pONFieldAvail(bAvail: Boolean; oOSField: TOvcNumericField);
    Procedure OSFEventCodeEnter(Sender: TObject);
    Procedure ODEEventDateEnter(Sender: TObject);
    Procedure OPFTimeEnter(Sender: TObject);
    Procedure OSFEventNameEnter(Sender: TObject);
    Procedure OSFEventNameExit(Sender: TObject);
    Procedure ONFSessionsEnter(Sender: TObject);
    Procedure ONFSessionsExit(Sender: TObject);
    Procedure OPFTablesEnter(Sender: TObject);
    Procedure OPFTablesExit(Sender: TObject);
    Procedure ONFEntriesEnter(Sender: TObject);
    Procedure OPFEntryFeeEnter(Sender: TObject);
    Procedure OPFEntryTotalEnter(Sender: TObject);
    Procedure OPFEntryTotalExit(Sender: TObject);
    Procedure OPFNonMemberEnter(Sender: TObject);
    Procedure OPFNonMemberExit(Sender: TObject);
    Procedure OPFFillInEnter(Sender: TObject);
    Procedure OPFFillInExit(Sender: TObject);
    Procedure OSFCommentEnter(Sender: TObject);
    Procedure OSFCommentExit(Sender: TObject);
    Procedure OSFNewPageEnter(Sender: TObject);
    Procedure Action3Execute(Sender: TObject);
    Procedure Action4Execute(Sender: TObject);
    Function FormHelp(Command: Word; Data: Integer; Var CallHelp: Boolean): Boolean;
    Procedure pResetTabStops;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    { Integer variable used dynamically to store the tables record count }
    iReccount, iWorkSheetsCount: Integer;
    iFTIReccount: Integer;
    { ksFTInfoKey:  KeyStr for the currently active FTInfo Record, blank if none
     ksFInvoiceKey:  KeyStr for the currently active FInvoice Record, blank if none }
    iMinusKey, iPlusKey: Integer;
    sPopupItemName: String;
    bFTQuitUsed: Boolean;
    iFormTag: Integer;
    sCaption: String;
    bKeyIsUnique: Boolean;
    slVoucherDescript: TStringList;
    bWinStNorm: Boolean;
  End;

Var
  frmWorkSheet: TfrmWorkSheet;
  mHHelp: THookHelpSystem;
  GetPrePend: String;
  ksFirstKey: KeyStr;
  ksTRecNoKey, ksTTempKey: keystr;
  ksERecNoKey, ksETempKey: keystr;
  ksARecNoKey, ksATempKey: keystr;
  ksITempKey: keystr;
  ksWSTempKey: keystr;
  bTournDir: Boolean;
  iLenArr: Integer;

Const
  iLenArray: Integer = MaxFilno;
  ksFTInfoKey: KeyStr = '';
  ksFInvoiceKey: KeyStr = '';
  ksVTempKey: KeyStr = '';
  bStart: Boolean = False;

Const
  { iDataMode Integer Variable
    Set the current data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode: Integer = 0;
  { sDataMode is the Char representation of iDataMode }
  sDataMode: Char = Null;
  bDoEdit: Boolean = True;
  bDisplay_Rec: Boolean = False;
  bEscKeyUsed: Boolean = False;

Implementation

Uses formTblsCurrOpen, formFTInfo, tfprint, formInvoice, formVoucher, formBalance, formOFSP1, formOFSP2,
  LibFinance, I_FCommon;

{$I STSTRS.dcl}

{$R *.dfm}

Procedure TfrmWorkSheet.ButQuitActionExecute(Sender: TObject);
Begin
  Close;
End;

Procedure TfrmWorkSheet.FormClose(Sender: TObject; Var Action: TCloseAction);
Begin
  { Close any open tables }
  fdBase.fCloseFiles;
  dBase.CloseFiles;
End;

Procedure TfrmWorkSheet.FormActivate(Sender: TObject);
Begin
  If bStart Then exit;
  bStart := True;
  bWinStNorm := True;
  bWinStNorm := (WindowState = wsNormal);
  pSetPanelWidth(frmWorkSheet, StatusBar1);
  { Must be set to true for Win32 applications. Used in Dbase and Fdbase }
  fDBUseLinks := True;
  fUseDBLinkages := True;
  { TopFilNo: What is the topmost database we are using in this application. }
  TopFilNo := Ftinfo_no;
  bEscKeyUsed := False;
  OfsItemtypes := MaxOfsItemtypes;
  { Procedure to check and see if the Tables were closed correctly }
  TestBadDbase;
  {
    liRecnoFT  = LongInt Record Number for Table 1 FTInfo
    liRecnoWS  = LongInt Record Number for Table 2 FWkSheet
    liRecnoINV = LongInt Record Number for Table 3 FInvoice
    liRecnoVO  = LongInt Record Number for Table 4 FInvItem (Vouchers)
    liRecnoBAL = LongInt Record Number for Table 5 FBalance
    liRecnoOF  = LongInt Record Number for Table 6 FOfs (Office Financial Sheet)
  }
  liRecnoFT := 0;
  liRecnoWS := 0;
  liRecnoINV := 0;
  liRecnoVO := 0;
  liRecnoBAL := 0;
  liRecnoOF := 0;
  // -------------------------------------------------------------------------
  { Variable iFormTag possible values and their meanings:
    0 = Tournament Information, frmTournInfo
    1 = Tournament Worksheet, frmWorkSheet
    2 = Invoice, frmInvoice
    3 = Voucher, frmVoucher
    4 = Balance Sheet, frmBalanceSheet
    5 = Office Financial Part 1, frmOffFinP1
    6 = Office Financial Part 2, frmOffFinP2 }
  iFormTag := 0;
  // -------------------------------------------------------------------------
  Application.HintHidePause := 200000;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  { Flush_DF & Flush_IF: Must be set to True for so that everytime a change is
    made to a "Record", dbase re-writes the table header and in Playern.dat,
    updates dbLastImpDate entry. Set them to False when you Delete a record,
    then immediately set them back to True }
  { This must be done as a standard whenever a table is opened for modifications }
  Flush_DF := True;
  Flush_IF := True;
  //==========================================================
  CSDef.ReadCFG;
  GetPrePend := fFinancePrePend(TDInfo_no);
  Prepend := FixPath(CFG.DBPath);
  { Read the current fees from the file }
  pReadFees;
  { If the Tournament Directory exists on the current drive }
  bTournDir := DirectoryExists(PrePend);
  If bTournDir Then Begin
    If fCheckNeededFiles Then Begin
      pOpenNeededFiles;
      fFilno := Ftinfo_no;
      Filno := 0;
      // -------------------------------------------------------------------------
      pFillDescript;
      //==========================================================
      iFTIReccount := db33.UsedRecs(fDatF[Ftinfo_no]^);
      bFTQuitUsed := False;
      ksFInvoiceKey := '';
      { Open FTInfo if there is more than record in FTInfo.Dat }
      If ((iFTIReccount <> 1) And (ksFTInfoKey = '')) Then Begin
        pOpenFTInfo; { Open FTInfo if there is more than record in FTInfo.Dat }
        liRecnoFT := fDbase.fRecno[FTInfo_No];
      End
      Else Begin
        // ------------------------------------------
        { Go to the first record in FTInfo }
        fTop_Record;
        liRecnoFT := fDbase.fRecno[FTInfo_No];
        { Store the Key }
        ksFTInfoKey := fGetKey(FTInfo_No, 1);
        ksThisTempKey := ksFTInfoKey;
        // ------------------------------------------
        iFormTag := 1; { Set iFormTag to 1 so we know that frmWorkSheet is what should be open }
        fFilno := Fwksheet_no;
        { Load the appropriate worksheet data }
        pGetWorkSheetData;
      End;
      { This variable Scrno, is used in some of the older code procedures to determine which
        calculations need to be performed based on the screen we are in.
        ----------------------------------------------------------
        Screen 1 = Tournament Information    - FTInfo.dat
        Screen 2 = Tournament Work Sheet     - FWkSheet.dat
        Screen 3 = Tournament Invoice        - FInvoice.dat
        Screen 4 = Voucher Items             - FInvItem.dat
        Screen 5 = Tournament Balance Sheet  - FBalance.dat
        Screen 6 = Office financial part 1   - FOFs.dat
        Screen 7 = Office financial part 2   - FOFs.dat  }
      Scrno := 2;
      //==========================================================
      { These variables are declared in CSDef.pas }
      IniName := GetCurrentDir + '\' + DefIniName;
      AppName := 'Finance';
      //==========================================================
      CSDef.ReadCFG;
      If CSDef.CFG.Tournament Then StatusBar1.Panels[0].Text := 'Tournament'
      Else StatusBar1.Panels[0].Text := 'Club';
      // ----------------------------------------------------------------------------------------------------------
      { Show the path to the table in use and the table name in StatusBar1. }
      StatusBar1.Panels[1].Text := UpperCase(Prepend + 'FWKSHEET.DAT');
      { Store the number of records to iReccount }
      iReccount := db33.UsedRecs(fDatF[Fwksheet_no]^);
      { Add the record count to StatusBar1's display }
      StatusBar1.Panels[1].Text := StatusBar1.Panels[1].Text + ' - Contains ' +
        Trim(IntToStr(iReccount)) + ' Records.';
    End
    Else Begin
      Application.Terminate;
    End;
  End
  Else Begin
    MessageDlg('The directory ' + Prepend + ' does not exist.' + #13 + #10 +
      'Cannot continue...', mtError, [mbOK], 0);
    Application.Terminate;
  End;
End;

Function TfrmWorkSheet.fCheckNeededFiles: Boolean;
Var
  b1, b2: Boolean;
Begin
  b1 := (Not FileExists(Prepend + DBNames[Tourn_no, 0]));
  b2 := (Not FileExists(Prepend + DBNames[Tournev_no, 0]));
  If (Not FileExists(Prepend + DBNames[Tourn_no, 0]))
    Or (Not FileExists(Prepend + DBNames[Tournev_no, 0])) Then Begin
    ErrBox('Tournament needs to be set up before financial reports can be used.', MC, 0);
    Result := False;
  End
  Else Result := True;
End;

Function TfrmWorkSheet.fFinancePrepend(Const Fno: Integer): String;
Begin
  Case Fno Of
    TDInfo_no: Result := FixPath(CFG.PLPath);
  Else Result := FixPath(CFG.DBPath);
  End;
End;

Procedure TfrmWorkSheet.pOpenNeededFiles;
Begin
  { Ftinfo_no = 1;
    Fwksheet_no = 2;
    Finvoice_no = 3;
    Tourn_no = 4;
    TournEv_no = 5;
    TDInfo_no = 6;
    Finvitem_no = 7;
    FBalance_no = 8;
    FOfs_no = 9; }
  fopenfiles;
  OpenDBFile(Tourn_no);
  OpenDBFile(Tournev_no);
End;

Procedure TfrmWorkSheet.ListTablesExecute(Sender: TObject);
Begin
  frmTblsCurrOpen := TfrmTblsCurrOpen.Create(Self);
  frmTblsCurrOpen.ShowModal;
  frmTblsCurrOpen.Free;
End;

Procedure TfrmWorkSheet.pFillFields;
Begin
  OSFSanctionNo.Text := FTINFO.SANCTION_NO;
  OSFTournament.Text := FTINFO.TOURN_NAME;
  // -------------------------------------------------
  With FWKSHEET Do Begin
    OSFEventCode.Text := Trim(EVENT_CODE);
    ODEEventDate.Text := LibData.fFormatDateString(EVENT_DATE);
    If Length(ODEEventDate.Text) > 9 Then
      LabelDOWText.Caption := FormatDateTime('DDDD', StrToDate(ODEEventDate.Text))
    Else
      LabelDOWText.Caption := '';
    OPFTime.Text := TIME;
    OSFSanctionNumberWS.Text := SANCTION_NO;
    OSFEventName.Text := Trim(EVENT_NAME);
    ONFSessions.Text := SESSIONS;
    OPFTables.Text := TABLES;
    ONFEntries.Text := ENTRIES;
    OPFEntryFee.Text := ENTRY_FEE;
    OPFEntryTotal.Text := ENTRY_TOTAL;
    OPFNonMember.Text := NON_MEMB;
    OPFFillIn.Text := FILL_IN;
    OSFComment.Text := Trim(COMMENT);
    OSFNewPage.Text := new_page;
  End;
  If OSFNewPage.Text = 'P' Then
    LabelLinePageControl.Caption := 'New Page'
  Else If Pos(OSFNewPage.Text, '0123456789') > 0 Then
    LabelLinePageControl.Caption := OSFNewPage.Text + ' blank lines'
  Else
    LabelLinePageControl.Caption := '';
  // -------------------------------------------------
  With FTINFO Do Begin
    OPFTotalTables.Text := TOTAL_TABLES;
    OPFTotalOfEntry.Text := ENTRY_TOTAL;
    OPFPlusNonMembers.Text := NON_MEM;
    OPFGrossEntries.Text := GROSS;
    OPFLessFillins.Text := FILL_IN;
    OPFNetReceipts.Text := NET;
  End;
  { Disable these fields if the Event Code Entry is NonEvent
    otherwise Enable them }
  ONFSessions.Enabled := Not (OSFEventCode.Text = NonEvent);
  OPFTables.Enabled := Not (OSFEventCode.Text = NonEvent);
  ONFEntries.Enabled := Not (OSFEventCode.Text = NonEvent);
  OPFEntryFee.Enabled := Not (OSFEventCode.Text = NonEvent);
  OPFNonMember.Enabled := Not (OSFEventCode.Text = NonEvent);
  OPFFillIn.Enabled := Not (OSFEventCode.Text = NonEvent);
End;

Procedure TfrmWorkSheet.pNextRecordf;
Begin
  { Move the record pointer in the table to the next available record and load the data in the screen fields }
  fNext_Record;
  liRecnoWS := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmWorkSheet.ButNextActionExecute(Sender: TObject);
Begin
  pNextRecordf;
End;

Procedure TfrmWorkSheet.pPrevRecordf;
Begin
  { Move the record pointer in the table to the previous record and load the data in the screen fields }
  fPrev_Record;
  liRecnoWS := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmWorkSheet.ButPrevActionExecute(Sender: TObject);
Begin
  pPrevRecordf;
End;

Procedure TfrmWorkSheet.ButLastActionExecute(Sender: TObject);
Begin
  fLast_Record;
  liRecnoWS := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmWorkSheet.ButTopActionExecute(Sender: TObject);
Begin
  fTop_Record;
  liRecnoWS := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmWorkSheet.ButFindActionExecute(Sender: TObject);
Begin
  fFind_Record(fGetKey(Ftinfo_no, 1), fGetKey(fFilno, 1), '', MC, 0, True);
  liRecnoWS := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmWorkSheet.pWorkSheetInfo(bEnable: Boolean);
Begin
  FreeHintWin;
  { Set the state of ActionManager1 for the players GBMenu }
  If bEnable Then ActionManager1.State := asSuspended Else
    ActionManager1.State := asNormal;
  { Set the state of ActionManager2 for the form players GBMenu }
  If bEnable Then ActionManager2.State := asNormal Else
    ActionManager2.State := asSuspended;
  // ----------------------------------------------------------------------
  If bEnable Then GBMenu.Hide Else GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := Not bEnable;
  GBMenu.ShowHint := Not bEnable;
  // ----------------------------------------------------------------------
  GBTournWorkSheet.Enabled := bEnable;
  GBTournWorkSheet.ShowHint := Not bEnable;
  // ----------------------------------------------------------------------
  LMDTournWSDone.Enabled := bEnable;
  LMDTournWSDone.Visible := bEnable;
  // ----------------------------------------------------------------------
End;

Procedure TfrmWorkSheet.ButEditActionExecute(Sender: TObject);
Begin
  { In module Dbase call the procedure fPreEditSaveKeys to save the KEYS in all tables we are
    editing so the application will know if a KEY Field has been changed and Indexes need to be
    regenerated }
  fPreEditSaveKeys;
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 2;
  sDataMode := 'E';
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(True);
  { Put the cursor in the first field }
  OSFEventCode.SetFocus;
End;

Procedure TfrmWorkSheet.ButPlayerInfoDoneExecute(Sender: TObject);
Begin
  pSaveChanges;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  { Turn the Player Info Data Area OFF }
  pWorkSheetInfo(False);
  pUpdateStatusBar;
End;

Procedure TfrmWorkSheet.ButAddActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 1;
  sDataMode := 'A';
  { Clear the Edit Fields of any data }
  //pClearFields;
  finitRecord(fFilno);
  pFillFields;
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(True);
  { Put the cursor in the first field }
  OSFEventCode.SetFocus;
End;

Procedure TfrmWorkSheet.ButCopyActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 3;
  sDataMode := 'C';
  fAdd_Init;
  pFillFields;
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(True);
  { Put the cursor in the first field }
  OSFEventCode.SetFocus;
End;

Procedure TfrmWorkSheet.ButDeleteActionExecute(Sender: TObject);
Begin
  fDelete_Record(True, self.HelpContext);
  // ------------------------------------------------------------
  pUpdateStatusBar;
  // ------------------------------------------------------------
  iReccount := db33.UsedRecs(fDatF[Fwksheet_no]^);
  If iReccount > 0 Then Begin
    { Move the record pointer in the table to the next available record and load the
      data in the screen fields }
    pNextRecordf;
    // ------------------------------------------------------------
    If ButNext.Enabled Then ButNext.SetFocus Else ButQuit.SetFocus;
  End
  Else Begin
    liRecnoWS := 0;
    pClearFields;
    pButtOn(False);
    ButAdd.SetFocus;
  End;
End;

Procedure TfrmWorkSheet.pUpdateStatusBar;
Begin
  iReccount := db33.UsedRecs(fDatF[Fwksheet_no]^);
  StatusBar1.Panels[1].Text := UpperCase(dbase.Prepend + 'FWKSHEET.DAT') + ' - Contains ' +
    Trim(IntToStr(iReccount)) + ' Records.';
  StatusBar1.Panels[2].Text := IntToStr(iWorkSheetsCount) + ' work sheets';
End;

//Function TfrmWorkSheet.fFound(Const Fno, Kno: Integer; ChkKey: KeyStr): Boolean;
//Var
//  bIsFound: Boolean;
//Begin
//  OK := True;
//  If (KeyLen[Fno, Kno] > 0) Then Begin
//    FindKey(IdxKey[Fno, Kno]^, Recno[Fno], ChkKey);
//    If OK Then GetRec(DatF[Fno]^, Recno[Fno], TOURNEV);
//    fFound := OK;
//  End
//  Else fFound := False;
//  OK := True; (* RESET OK for SUBSEQUENT File ALIGNMENT *)
//End;

Procedure TfrmWorkSheet.MinusKeyExecute(Sender: TObject);
Begin
  { Open frmFTInfo to view FTInfo.Dat (Tournament Information) }
  pOpenFTInfo;
End;

Procedure TfrmWorkSheet.pOpenFTInfo;
Begin
//  frmWorkSheet.Hide;
//  ActionManager1.State := asSuspended;
  // ==========================================
  frmFTInfo := TfrmFTInfo.Create(Self);
  frmFTInfo.ShowModal;
  frmFTInfo.Free;
  // ==========================================
  frmWorkSheet.Show;
//  ActionManager1.State := asNormal;
  If Not bFTQuitUsed Then Begin
    Scrno := 2;
    fFilno := Fwksheet_no;
    pGetWorkSheetData;
  End;
End;

Procedure TfrmWorkSheet.F1KeyExecute(Sender: TObject);
Begin
  //pRunExtProg(frmWorkSheet, CFG.LoadPath + 'WUtility', 'MANUAL\CONTENTS');
  keybd_event(VK_F1, MapVirtualKey(VK_F1, 0), 0, 0);
End;

Procedure TfrmWorkSheet.pGetWorkSheetData;
Begin
  { ksFTInfoKey contains the KeyStr if a FTInfo record has been selected, it is blank if not }
  If Trim(ksFTInfoKey) = '' Then Begin
    { Go to the first record in FTInfo }
    ClearKey(fIdxKey[FTINFO_no, 1]^);
    { Find the first available record }
    SearchKey(fIdxKey[FTINFO_no, 1]^, fRecno[FTINFO_no], ksFirstKey);
    { Store the table fields in the record variable }
    fGetARec(FTINFO_no);
    liRecnoWS := fDbase.fRecno[FTINFO_no];
    { Store the records KeyStr }
    ksTRecNoKey := fGetKey(FTINFO_no, 1);
    ksFTInfoKey := ksTRecNoKey;
    { Store the selected FTInfo KeyStr to the Temp KeyStr Variable }
    ksTTempKey := ksTRecNoKey;
  End
  Else Begin
    { Store the selected FTInfo KeyStr to the Temp KeyStr Variable }
    ksTRecNoKey := ksFTInfoKey;
    { Store the selected FTInfo KeyStr to the Temp KeyStr Variable }
    ksTTempKey := ksTRecNoKey;
  End;
  { Open the selected form }
  If (iFormTag <> 1) Then Begin
    { The user has selected another form, so it needs to opened }
    Case iFormTag Of
      0: pOpenFTInfo; // 0 = Tournament Information, frmTournInfo
      2: pOpenInvoice; // 2 = Invoice, frmInvoice
      3: pOpenVoucher; // 3 = Voucher, frmVoucher
      4: pOpenBalanceSheet; // 4 = Balance Sheet, frmBalanceSheet
      5: pOpenOffFinP1; // 5 = Office Financial Part 1, frmOffFinP1
      6: pOpenOffFinP2; // 6 = Office Financial Part 2, frmOffFinP2
    Else
      iFormTag := 1;
    End;
  End;
  If Not bFTQuitUsed Then Begin
    Scrno := 2;
    // --------------------------------------------------------
    If bWinStNorm Then WindowState := wsNormal
    Else WindowState := wsMaximized;
    bWinStNorm := (WindowState = wsNormal);
    pSetPanelWidth(frmWorkSheet, StatusBar1);
    // --------------------------------------------------------
    If liRecnoWS > 0 Then
      { Fill the form fields with the WorkSheet Data }
      pFillFields
    Else Begin
      { Count the WorkSheet records for this Sanction Number }
      pCountWorkSheets;
      fTop_Record;
      { Go to the first record in Fwksheet }
      If fStatus_OK(fFilno) Then Begin
        liRecnoWS := fDbase.fRecno[Fwksheet_no];
        { Fill the form fields with the Data }
        pFillFields;
      End
      Else Begin
        liRecnoWS := 0;
        pButtOn(False);
        ButAdd.SetFocus;
      End;
    End;
  End;
End;

Procedure TfrmWorkSheet.pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Var
  wKey: Word;
Begin
  wKey := Key;
  Case key Of
    { 107 is the Plus Key was pressed on the numeric keypad }
    107: If GBMenu.Enabled Then PlusKeyExecute(Sender);
    { 109 is the Minus Key was pressed on the numeric keypad }
    109: If GBMenu.Enabled Then MinusKeyExecute(Sender);
    { Return key was pressed }
    VK_RETURN: If Not GBMenu.Enabled Then Begin
        key := 0;
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
      End;
    { ESCape key was pressed, Abort the record or record changes... }
    VK_ESCAPE: If Not GBMenu.Enabled Then Begin
        bEscKeyUsed := True;
        pAbortChanges;
        { If there are no child records in Fwksheet for this Sanction Number,
          we are in add mode and the Esc key has been pressed.
          Close this form and return to the display of the FTInfo record. }
        If Not ok Then pOpenFTInfo;
      End
      Else close;
    { F9 key was pressed, save the record or the changes... }
    VK_F9: If Not GBMenu.Enabled Then pSaveChanges;
  Else
    If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
      If (Key = VK_UP) Then Begin
        ButPrev.Click;
        ButPrev.SetFocus;
      End
      Else Begin
        ButNext.Click;
        ButNext.SetFocus;
      End;
      Key := 0;
    End;
  End;
End;

Procedure TfrmWorkSheet.pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
Var
  wKey: Word;
Begin
  { Do not trap for the Return Key here, it causes the cursor to move two fields at a time }
  wKey := Key;
  Case key Of
    { ESCape key was pressed, Abort the record or record changes... }
    VK_ESCAPE: If Not GBMenu.Enabled Then Begin
        bEscKeyUsed := True;
        pAbortChanges;
        { If there are no child records in Fwksheet for this Sanction Number,
          we are in add mode and the Esc key has been pressed.
          Close this form and return to the display of the FTInfo record. }
        If Not ok Then pOpenFTInfo;
      End;
    { F9 key was pressed, save the record or the changes... }
    VK_F9: If Not GBMenu.Enabled Then pSaveChanges;
  Else
    If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
      If (Key = VK_UP) Then Begin
        ButPrev.Click;
        ButPrev.SetFocus;
      End
      Else Begin
        ButNext.Click;
        ButNext.SetFocus;
      End;
      Key := 0;
    End;
  End;
End;

Procedure TfrmWorkSheet.PlusKeyExecute(Sender: TObject);
Begin
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := 2; { 2 = Invoice, frmInvoice }
  frmWorkSheet.bFTQuitUsed := False;
  pOpenInvoice; // Open frmInvoice
End;

Procedure TfrmWorkSheet.F2KeyExecute(Sender: TObject);
Var
  iForLoop: Integer;
Begin
  GBMenu.Enabled := False;
  For iForLoop := (PopupMenu1.Items.Count - 1) Downto 0 Do
    PopupMenu1.Items.Items[iForLoop].Enabled := True;
  PopupMenu1.Items.Items[1].Enabled := False;
  PopupMenu1.Popup(350, 500);
  GBMenu.Enabled := True;
End;

Procedure TfrmWorkSheet.pPuMenuItemSelected(Sender: TObject);
Begin
  If ksFTInfoKey = '' Then Begin
    ksFTInfoKey := GetKey(FTINFO_no, 1);
    ksThisTempKey := ksFTInfoKey;
  End;
  { Variable iFormTag possible values and their meanings:
    0 = Tournament Information, frmTournInfo
    1 = Tournament Worksheet, frmWorkSheet
    2 = Invoice, frmInvoice
    3 = Voucher, frmVoucher
    4 = Balance Sheet, frmBalanceSheet
    5 = Office Financial Part 1, frmOffFinP1
    6 = Office Financial Part 2, frmOffFinP2 }
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  iFormTag := (Sender As TMenuItem).Tag;
  Case iFormTag Of
    0: pOpenFTInfo; // 0 = Tournament Information, frmTournInfo
    //1: pWhichScreen(Sender); // 1 = Tournament Worksheet, frmWorkSheet
    2: pOpenInvoice; // 2 = Invoice, frmInvoice
    3: Begin
        { Store the Invoice Record FHash Key to the Public Variable:  frmWorkSheet.ksFInvoiceKey
          If the Voucher Screen was selected }
        pGetInvoiceKey;
        pOpenVoucher; // 3 = Voucher, frmVoucher
      End;
    4: pOpenBalanceSheet; // 4 = Balance Sheet, frmBalanceSheet
    5: pOpenOffFinP1; // 5 = Office Financial Part 1, frmOffFinP1
    6: pOpenOffFinP2; // 6 = Office Financial Part 2, frmOffFinP2
  Else
    ActionManager1.State := asNormal;
    iFormTag := 1;
  End;
End;

Procedure TfrmWorkSheet.pWhichScreen(oSender: TObject);
Begin
  sCaption := (oSender As TMenuItem).Caption;
  frmWorkSheet.sPopupItemName := sCaption;
  MessageDlg('You selected item ' + IntToStr(frmWorkSheet.iformTag) +
    ', which is ' + frmWorkSheet.sPopupItemName, mtInformation, [mbOK], 0);
End;

Procedure TfrmWorkSheet.pAbortChanges;
Begin
  FreeHintWin;
  pResetTabStops;
  { Turn the Player Info Data Area OFF }
  pWorkSheetInfo(False);
  { Fill the form fields with the WorkSheet Data }
  If iDataMode = 1 Then fTop_Record
  Else fGetARec(fFilNo);
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  pFillFields;
  { If there are no child records in Fwksheet for this Sanction Number,
    we are in add mode and the Esc key has been pressed.
    Close this form and return to the display of the FTInfo record. }
  If iWorkSheetsCount = 0 Then pOpenFTInfo;
End;

Procedure TfrmWorkSheet.F8KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (1) to Print to Screen }
  PrintReport(1);
End;

Procedure TfrmWorkSheet.F7KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (2) to Print Reports }
  PrintReport(2);
End;

Procedure TfrmWorkSheet.F6KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (3) to Print Reports to an ASCII file }
  PrintReport(3);
End;

Procedure TfrmWorkSheet.pOpenInvoice; // Open frmInvoice
Begin
  ActionManager1.State := asSuspended;
  frmWorkSheet.Hide;
  // ==========================================
  frmInvoice := TfrmInvoice.Create(Self);
  frmInvoice.ShowModal;
  frmInvoice.Free;
  // ==========================================
  frmWorkSheet.Show;
  ActionManager1.State := asNormal;
  If Not bFTQuitUsed Then Begin
    Scrno := 2;
    fFilno := Fwksheet_no;
    pGetWorkSheetData;
  End;
End;

Procedure TfrmWorkSheet.pOpenVoucher; // Open frmVoucher
Begin
  If ksFInvoiceKey <> '' Then Begin
    ActionManager1.State := asSuspended;
    frmWorkSheet.Hide;
    // ==========================================
    frmVoucher := TfrmVoucher.Create(Self);
    frmVoucher.ShowModal;
    frmVoucher.Free;
    // ==========================================
    frmWorkSheet.Show;
    ActionManager1.State := asNormal;
    If Not bFTQuitUsed Then Begin
      Scrno := 2;
      fFilno := Fwksheet_no;
      pGetWorkSheetData;
    End;
  End
  Else Begin
    MessageDlg('    There are no "Invoice Records" for this "Sanction Number".' + #13 + #10 + '' +
      #13 + #10 + 'Add the invoice record before attempting to add the invoice items!', mtError,
      [mbOK], 0);
    fFilno := Fwksheet_no;
    { Set iFormTag to WorkSheet (1) }
    iFormTag := 0;
    pGetWorkSheetData;
  End;
End;

Procedure TfrmWorkSheet.pOpenBalanceSheet; // Open frmBalanceSheet
Begin
  ActionManager1.State := asSuspended;
  frmWorkSheet.Hide;
  // ==========================================
  frmBalance := TfrmBalance.Create(Self);
  frmBalance.ShowModal;
  frmBalance.Free;
  // ==========================================
  frmWorkSheet.Show;
  ActionManager1.State := asNormal;
  If Not bFTQuitUsed Then Begin
    Scrno := 2;
    fFilno := Fwksheet_no;
    pGetWorkSheetData;
  End;
End;

Procedure TfrmWorkSheet.pOpenOffFinP1; // Open frmOffFinP1
Begin
  ActionManager1.State := asSuspended;
  frmWorkSheet.Hide;
  // ==========================================
  frmOFSP1 := TfrmOFSP1.Create(Self);
  frmOFSP1.ShowModal;
  frmOFSP1.Free;
  // ==========================================
  frmWorkSheet.Show;
  ActionManager1.State := asNormal;
  If Not bFTQuitUsed Then Begin
    Scrno := 2;
    fFilno := Fwksheet_no;
    pGetWorkSheetData;
  End;
End;

Procedure TfrmWorkSheet.pOpenOffFinP2; // Open frmOffFinP2
Begin
  ActionManager1.State := asSuspended;
  frmWorkSheet.Hide;
  // ==========================================
  frmOFSP2 := TfrmOFSP2.Create(Self);
  frmOFSP2.ShowModal;
  frmOFSP2.Free;
  // ==========================================
  frmWorkSheet.Show;
  ActionManager1.State := asNormal;
  If Not bFTQuitUsed Then Begin
    Scrno := 2;
    fFilno := Fwksheet_no;
    pGetWorkSheetData;
  End;
End;

Procedure TfrmWorkSheet.pGetInvoiceKey;
Begin
  If trim(ksFTInfoKey) <> '' Then Begin
    ksITempKey := ksFTInfoKey;
    { Go to the first record }
    ClearKey(fIdxKey[FInvoice_no, 1]^);
    SearchKey(fIdxKey[FInvoice_no, 1]^, fRecno[FInvoice_no], ksITempKey);
    If OK And CompareKey(ksITempKey, ksFTInfoKey) Then Begin
      { Store the table fields in the record variable }
      fGetARec(FInvoice_no);
      liRecnoINV := fDbase.fRecno[FInvoice_no];
      { Store the FHASH key (2) from the Invoice record }
      ksFInvoiceKey := fGetKey(FInvoice_no, 2);
    End
    Else Begin
      ksFInvoiceKey := '';
      pclear_rec(FInvoice_no);
      liRecnoINV := 0;
    End;
  End
  Else Begin
    ksFInvoiceKey := '';
    pclear_rec(FInvoice_no);
  End;
End;

Procedure TfrmWorkSheet.pSaveChanges;
Begin
  { Data Mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  Case iDataMode Of
    1, 3: Begin // Add or Copy Modes
        pCopyRecData;
        fdbase.fAdd_Record;
        liRecnoWS := fDbase.fRecno[fFilno];
        pButtOn(True);
        pUpdateStatusBar;
        FixTotals(iDataMode);
      End;
    2: Begin // Edit Mode
        pCheckEditFields;
        bKeyIsUnique := fPostEditFixKeys;
        If Not bKeyIsUnique Then Begin
          If (MessageDlg('This Sanction Number already exists in this database.', mtError,
            [mbOK], 0) = mrOK) Then Begin
            OSFEventCode.SetFocus;
            Exit;
          End
          Else pAbortChanges;
        End
        Else If fValidateFields Then Begin
          fdbase.fPutARec(fFilNo);
          FixTotals(iDataMode);
        End;
      End;
  End;
  pFillFields;
  iDataMode := 0;
  sDataMode := null;
  pResetTabStops;
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(False);
End;

Procedure TfrmWorkSheet.pCopyRecData;
Begin
  { Used when Adding or Copying a Record }
  With FWKSHEET Do Begin
    SANCTION_NO := Pad(OSFSanctionNo.Text,1);
    EVENT_CODE := Pad(OSFEventCode.Text,4);
    EVENT_DATE := Pad(fStripDateField(ODEEventDate.Text),8);
    TIME := Pad(OPFTime.GetStrippedEditString,4);
    EVENT_NAME := Pad(OSFEventName.Text,25);
    SESSIONS := LeftPad(Trim(ONFSessions.Text),2);
    TABLES := LeftPad(Trim(OPFTables.Text),6);
    ENTRIES := LeftPad(Trim(ONFEntries.Text),4);
    ENTRY_FEE := LeftPad(Trim(OPFEntryFee.Text),6);
    ENTRY_TOTAL := LeftPad(Trim(OPFEntryTotal.Text),8);
    NON_MEMB := LeftPad(Trim(OPFNonMember.Text),6);
    FILL_IN := LeftPad(Trim(OPFFillIn.Text),6);
    COMMENT := Pad(OSFComment.Text,25);
    new_page := Pad(OSFNewPage.Text,1);
  End;
End;

Procedure TfrmWorkSheet.pCheckEditFields;
Begin
  { Used when Editing a Record }
  With FWKSHEET Do Begin
    // ---------------------------------------------------------------------
    { SANCTION_NO comes from FTINFO.SANCTION_NO }
    // ---------------------------------------------------------------------
    EVENT_CODE := Pad(OSFEventCode.Text,4);
    EVENT_DATE := Pad(fStripDateField(ODEEventDate.Text),8);
    TIME := Pad(OPFTime.GetStrippedEditString,4);
    EVENT_NAME := Pad(OSFEventName.Text,25);
    SESSIONS := LeftPad(Trim(ONFSessions.Text),2);
    TABLES := LeftPad(Trim(OPFTables.Text),6);
    ENTRIES := LeftPad(Trim(ONFEntries.Text),4);
    ENTRY_FEE := LeftPad(Trim(OPFEntryFee.Text),6);
    ENTRY_TOTAL := LeftPad(Trim(OPFEntryTotal.Text),8);
    NON_MEMB := LeftPad(Trim(OPFNonMember.Text),6);
    FILL_IN := LeftPad(Trim(OPFFillIn.Text),6);
    COMMENT := Pad(OSFComment.Text,25);
    new_page := Pad(OSFNewPage.Text,1);
  End;
End;

Procedure TfrmWorkSheet.pCountWorkSheets;
Begin
  { Count the WorkSheet records for this Sanction Number }
  iWorkSheetsCount := 0;
  { Go to the first record }
  ClearKey(fIdxKey[fFilno, 1]^);
  SearchKey(fIdxKey[fFilno, 1]^, fRecno[fFilno], ksTTempKey);
  While OK And CompareKey(ksTTempKey, ksTRecNoKey) Do Begin
    Inc(iWorkSheetsCount);
    NextKey(fIdxKey[fFilno, 1]^, frecno[fFilno], ksTTempKey);
  End;
  pUpdateStatusBar;
  ksTTempKey := ksTRecNoKey;
End;

Procedure TfrmWorkSheet.pClearFields;
Begin
  // --------------------------------------------
  With FTINFO Do Begin
    OSFSanctionNo.Text := SANCTION_NO;
    OSFTournament.Text := TOURN_NAME;
    OPFTotalTables.Text := TOTAL_TABLES;
    OPFTotalOfEntry.Text := ENTRY_TOTAL;
    OPFPlusNonMembers.Text := NON_MEM;
    OPFGrossEntries.Text := GROSS;
    OPFLessFillins.Text := FILL_IN;
    OPFNetReceipts.Text := NET;
  End;
  // --------------------------------------------
  OSFEventCode.Text := '';
  ODEEventDate.Text := '';
  LabelDOWText.Caption := '';
  OPFTime.Text := '';
  OSFEventName.Text := '';
  ONFSessions.Text := '0';
  OPFTables.Text := '0';
  ONFEntries.Text := '0';
  OPFEntryFee.Text := '0';
  OPFEntryTotal.Text := '0';
  OPFNonMember.Text := '0';
  OPFFillIn.Text := '0';
  OSFComment.Text := '';
  OSFNewPage.Text := '0';
End;

Function TfrmWorkSheet.fValidateFields: Boolean;
Var
  sDateText: String;
  sTimeText: String;
Begin
  { Must take out the slashes before passing the date to ChkDate Function }
  sDateText := fStripDateField(ODEEventDate.Text);
  sTimeText := OPFTime.GetStrippedEditString;
  // --------------------------------------------------------------------------
  Result := True;
  If Not ((OSFEventCode.Text = NonEvent) Or (OSFEventCode.Text = OtherEvent) Or
    Found(TournEv_no, 1, OSFSanctionNo.Text + OSFEventCode.Text)) Then
    Result := False
  Else Begin
    If Not (ChkDate(sDateText)) Then
      Result := False
    Else Begin
      If Not (fCheckTime(OPFTime.Text)) Then Begin
        Result := False
      End
      Else Begin
        If Not (Pos(OSFNewPage.Text, '0123456789P') > 0) Then Begin
          Result := False
        End;
      End;
    End;
  End;
  If Not result Then
    MessageDlg('Record failed edit checks. Cannot save edits...', mtError, [mbOK], 0);
End;

Procedure TfrmWorkSheet.pDateFieldKeyPress(Sender: TObject; Var Key: Char);
Begin
  pEditFieldKeyPress(Sender, Key);
End;

Procedure TfrmWorkSheet.pButtOn(bTurnOn: Boolean);
Begin
  // ------------------------------------------------------------
  { Buttons }
  If Not bTournDir Then Begin
    ButAdd.Enabled := bTurnOn;
    ButAddAction.Enabled := bTurnOn;
  End;
  ButCopy.Enabled := bTurnOn;
  ButCopyAction.Enabled := bTurnOn;
  ButDelete.Enabled := bTurnOn;
  ButDeleteAction.Enabled := bTurnOn;
  ButEdit.Enabled := bTurnOn;
  ButEditAction.Enabled := bTurnOn;
  ButFind.Enabled := bTurnOn;
  ButFindAction.Enabled := bTurnOn;
  ButLast.Enabled := bTurnOn;
  ButLastAction.Enabled := bTurnOn;
  ButNext.Enabled := bTurnOn;
  ButNextAction.Enabled := bTurnOn;
  ButPrev.Enabled := bTurnOn;
  ButPrevAction.Enabled := bTurnOn;
  ButTop.Enabled := bTurnOn;
  ButTopAction.Enabled := bTurnOn;
  // ------------------------------------------------------------
  { Function Keys }
  F3Key.Enabled := bTurnOn;
  F4Key.Enabled := bTurnOn;
  F5Key.Enabled := bTurnOn;
  ButtonF6Key.Enabled := bTurnOn;
  F6Key.Enabled := bTurnOn;
  ButtonF7Key.Enabled := bTurnOn;
  F7Key.Enabled := bTurnOn;
  ButtonF8Key.Enabled := bTurnOn;
  F8Key.Enabled := bTurnOn;
  // ------------------------------------------------------------
End;

Procedure TfrmWorkSheet.pClear_Rec(Const Fno: Integer);
Var
  i: Integer;
Begin
  For i := 1 To fMaxFilno Do
    If fDBFileOpen[i] And (fGetLinkBackFile(i) = Fno) Then pClear_Rec(i);
End;

Procedure TfrmWorkSheet.pFillDescript;
Var
  FndKey: KeyStr;
  iFor, iVoucherRecs, iIndex: Integer;
Begin
  slVoucherDescript := tStringList.Create;
  // -----------------------------------------------------------------
  iVoucherRecs := db33.UsedRecs(fDatF[FINVITEM_NO]^);
  // -----------------------------------------------------------------
  ClearKey(fIdxKey[FINVITEM_NO, 1]^);
  SearchKey(fIdxKey[FINVITEM_NO, 1]^, fRecno[FINVITEM_NO], FndKey);
  If OK Then Begin
    For iFor := 1 To iVoucherRecs Do Begin
      { Store the table fields in the record variable }
      fGetARec(FINVITEM_NO);
      If trim(FInvItem.DESCRIPTION) <> '' Then Begin
        slVoucherDescript.Sort;
        If Not slVoucherDescript.Find(Trim(FInvItem.DESCRIPTION), iIndex) Then
          slVoucherDescript.Add(Trim(FInvItem.DESCRIPTION));
      End;
      LibData.SkipNextf(FINVITEM_NO, FndKey);
    End;
  End;
  slVoucherDescript.SaveToFile('Descript.txt');
End;

Procedure TfrmWorkSheet.FullScreenExecute(Sender: TObject);
Begin
  pSetWinState;
End;

Procedure TfrmWorkSheet.Action1Execute(Sender: TObject);
Begin
  pSetWinState;
End;

Procedure TfrmWorkSheet.Action2Execute(Sender: TObject);
Begin
  pSetWinState;
End;

Procedure TfrmWorkSheet.ResizeKit1ExitResize(Sender: TObject; XScale,
  YScale: Double);
Begin
  pSetPanelWidth(frmWorkSheet, StatusBar1);
End;

Procedure TfrmWorkSheet.pSetWinState;
Begin
  If WindowState = wsMaximized Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  bWinStNorm := (WindowState = wsNormal);
  pSetPanelWidth(frmWorkSheet, StatusBar1);
End;

//Procedure TfrmWorkSheet.pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
//Begin
//  If UpperCase(sType) = 'S' Then pOSFieldAvail(bDoedit, (oSender As TOvcSimpleField))
//  Else If UpperCase(sType) = 'P' Then pOPFieldAvail(bDoedit, (oSender As TOvcPictureField))
//  Else If UpperCase(sType) = 'D' Then pODEFieldAvail(bDoedit, (oSender As TOvcDateEdit))
//  Else If UpperCase(sType) = 'N' Then pONFieldAvail(bDoedit, (oSender As TOvcNumericField))
//  Else MessageDlg('No Procedure created for that type!', mtError, [mbOK], 0);
//  // -------------------------------------------------------------------------------------------------------
//  pFillFields;
//  // -------------------------------------------------------------------------------------------------------
//  If bWasOnEnter Then Begin
//    If UpperCase(sType) = 'S' Then Begin
//      If Not (oSender As TOvcSimpleField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'P') Then Begin
//      If Not (oSender As TOvcPictureField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'D') Then Begin
//      If Not (oSender As TOvcDateEdit).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'N') Then Begin
//      If Not (oSender As TOvcNumericField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End;
//  End;
//End;
//
//Procedure TfrmWorkSheet.pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoCaretToEnd, efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [efoCaretToEnd];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmWorkSheet.pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoCaretToEnd, efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [efoCaretToEnd];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmWorkSheet.pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.ReadOnly := True;
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.ReadOnly := False;
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmWorkSheet.pONFieldAvail(bAvail: Boolean; oOSField: TOvcNumericField);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;

Procedure TfrmWorkSheet.OSFEventCodeEnter(Sender: TObject);
Begin
  pResetTabStops;
  bDoEdit := True;
  FWKSHEET.EVENT_CODE := Pad(OSFEventCode.Text,4);
  bDoEdit := CustomFinanceEdit(Scrno, 2, FWKSHEET.EVENT_CODE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmWorkSheet.OSFEventCodeExit(Sender: TObject);
Var
  sLink: String;
Begin
  //  { If in Add or Copy Mode or in Edit Mode and the Event Code Has Been Changed }
  //  If (iDataMode In [1, 3]) Or ((iDataMode = 2) And (OSFEventCode.Text <> FWKSHEET.EVENT_CODE)) Then Begin
  //    { If the ESCape Key was not the last key used, then run the code of field validation }
  //    If Not bEscKeyUsed Then Begin
  //      If length(Trim(OSFEventCode.Text)) < 4 Then Pad(OSFEventCode.Text, 4);
  //      sLink := OSFSanctionNo.Text + Pad(OSFEventCode.Text, 4);
  //      OK := True;
  //      { NonEvent = 'ZZZZ', OtherEvent = 'YYYY' const created in fVariables.Inc }
  //      { If the event code is not for an Actual Event or Non-Event or Other_Event
  //        then reject the event code }
  //      If Not ((OSFEventCode.Text = NonEvent) Or (OSFEventCode.Text = OtherEvent)
  //        Or fFound(TournEv_no, 1, sLink)) Then Begin
  //        If (MessageDlg('This event code not found in tournament data base.' + #13 + #10 +
  //          '               Correct this error?', mtError, [mbYes, mbNo], 0) = mrYes) Then Begin
  //          OSFEventCode.Text := '';
  //          OSFEventCode.SetFocus;
  //        End
  //        Else pAbortChanges; { If the user does not want to correct the error, abort the changes }
  //      End
  //      Else Begin
  //        If fFound(TournEv_no, 1, sLink) Then Begin
  //          OSFEventName.Text := TournEv.Event_Name;
  //          If TournEv.Event_Type = Continuous Then Begin
  //            ODEEventDate.Text := fCheckDate(SysDate);
  //            ONFSessions.Text := '1';
  //          End
  //          Else Begin
  //            ODEEventDate.Text := fCheckDate(TournEv.Event_Date);
  //            ONFSessions.Text := TournEv.Sessions;
  //          End;
  //        End
  //        Else Begin
  //          ODEEventDate.Text := fCheckDate(SysDate);
  //          ONFSessions.Text := '0';
  //          OPFTables.Text := Fstr(0, 6, 1);
  //          ONFEntries.Text := '0';
  //          OPFEntryFee.Text := Fstr(0, 6, 2);
  //          OPFEntryTotal.Text := Fstr(0, 8, 2);
  //          OPFFillIn.Text := Fstr(0, 6, 2);
  //        End;
  //        ONFSessions.Enabled := Not (OSFEventCode.Text = NonEvent);
  //        OPFTables.Enabled := Not (OSFEventCode.Text = NonEvent);
  //        ONFEntries.Enabled := Not (OSFEventCode.Text = NonEvent);
  //        OPFEntryFee.Enabled := Not (OSFEventCode.Text = NonEvent);
  //        OPFNonMember.Enabled := Not (OSFEventCode.Text = NonEvent);
  //        OPFFillIn.Enabled := Not (OSFEventCode.Text = NonEvent);
  //      End;
  //    End;
  //  End;

  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      If length(Trim(OSFEventCode.Text)) < 4 Then Pad(OSFEventCode.Text, 4);
      sLink := OSFSanctionNo.Text + Pad(OSFEventCode.Text, 4);
      OK := True;
      { NonEvent = 'ZZZZ', OtherEvent = 'YYYY' const created in fVariables.Inc }
      { If the event code is not for an Actual Event or Non-Event or Other_Event
        then reject the event code }
      Found(TournEv_no, 1, sLink);
      If Not ((OSFEventCode.Text = NonEvent) Or (OSFEventCode.Text = OtherEvent)
        Or Found(TournEv_no, 1, sLink)) Then Begin
        If (MessageDlg('This event code not found in tournament data base.' + #13 + #10 +
          '               Correct this error?', mtError, [mbYes, mbNo], 0) = mrYes) Then Begin
          OSFEventCode.Text := '';
          OSFEventCode.SetFocus;
        End
        Else pAbortChanges; { If the user does not want to correct the error, abort the changes }
      End
      Else Begin
        FWKSHEET.EVENT_CODE := Pad(OSFEventCode.Text,4);
        bDoEdit := CustomFinanceEdit(Scrno, 2, FWKSHEET.EVENT_CODE, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmWorkSheet.ODEEventDateEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FWKSHEET.EVENT_DATE := Pad(fStripDateField(ODEEventDate.Text),8);
  bDoEdit := CustomFinanceEdit(Scrno, 3, FWKSHEET.EVENT_DATE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'D');
End;

Procedure TfrmWorkSheet.pDateExit(Sender: TObject);
Var
  sDateText: String;
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      { Replace all blanks with zeros in the date }
      (Sender As TOvcDateEdit).Text := fFillDateBlanks((Sender As TOvcDateEdit).Text);
      { Must take out the slashes before passing the date to ChkDate Function }
      sDateText := fStripDateField((Sender As TOvcDateEdit).Text);
      If Not (ChkDate(sDateText)) Then Begin
        MessageDlg('Event date is invalid.', mtError, [mbOK], 0);
        LabelDOWText.Caption := '';
        (Sender As TOvcDateEdit).SetFocus;
      End
      Else Begin
        FWKSHEET.EVENT_DATE := Pad(fStripDateField(ODEEventDate.Text),8);
        bDoEdit := CustomFinanceEdit(Scrno, 3, FWKSHEET.EVENT_DATE, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'D');
        If Length(ODEEventDate.Text) > 9 Then
          LabelDOWText.Caption := FormatDateTime('DDDD', StrToDate(ODEEventDate.Text))
        Else LabelDOWText.Caption := '';
      End;
    End;
  End;
End;

Procedure TfrmWorkSheet.OPFTimeEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FWKSHEET.TIME := Pad(OPFTime.GetStrippedEditString,4);
  bDoEdit := CustomFinanceEdit(Scrno, 5, FWKSHEET.TIME, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmWorkSheet.OPFTimeExit(Sender: TObject);
Var
  sTimeText: String;
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      sTimeText := OPFTime.GetStrippedEditString;
      If Not (fCheckTime(OPFTime.Text)) Then Begin
        ErrBox('Invalid time.', mc, 0);
        OPFTime.SetFocus;
      End
      Else Begin
        FWKSHEET.TIME := Pad(OPFTime.GetStrippedEditString,4);
        bDoEdit := CustomFinanceEdit(Scrno, 5, FWKSHEET.TIME, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'P');
      End;
    End;
  End;
End;

Procedure TfrmWorkSheet.OSFEventNameEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FWKSHEET.EVENT_NAME := Pad(OSFEventName.Text,25);
  bDoEdit := CustomFinanceEdit(Scrno, 6, FWKSHEET.EVENT_NAME, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmWorkSheet.OSFEventNameExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FWKSHEET.EVENT_NAME := Pad(OSFEventName.Text,25);
      bDoEdit := CustomFinanceEdit(Scrno, 6, FWKSHEET.EVENT_NAME, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmWorkSheet.ONFSessionsEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FWKSHEET.SESSIONS := LeftPad(Trim(ONFSessions.Text),2);
  bDoEdit := CustomFinanceEdit(Scrno, 7, FWKSHEET.SESSIONS, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'N');
End;

Procedure TfrmWorkSheet.ONFSessionsExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FWKSHEET.SESSIONS := LeftPad(Trim(ONFSessions.Text),2);
      bDoEdit := CustomFinanceEdit(Scrno, 7, FWKSHEET.SESSIONS, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'N');
    End;
  End;
End;

Procedure TfrmWorkSheet.OPFTablesEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FWKSHEET.TABLES := LeftPad(Trim(OPFTables.Text),6);
  bDoEdit := CustomFinanceEdit(Scrno, 8, FWKSHEET.TABLES, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmWorkSheet.OPFTablesExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FWKSHEET.TABLES := LeftPad(Trim(OPFTables.Text),6);
      bDoEdit := CustomFinanceEdit(Scrno, 8, FWKSHEET.TABLES, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmWorkSheet.ONFEntriesEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FWKSHEET.ENTRIES := LeftPad(Trim(ONFEntries.Text),4);
  bDoEdit := CustomFinanceEdit(Scrno, 9, FWKSHEET.ENTRIES, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'N');
End;

Procedure TfrmWorkSheet.ONFEntriesExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FWKSHEET.ENTRIES := LeftPad(Trim(ONFEntries.Text),4);
      bDoEdit := CustomFinanceEdit(Scrno, 9, FWKSHEET.ENTRIES, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'N');
    End;
  End;
End;

Procedure TfrmWorkSheet.OPFEntryFeeEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FWKSHEET.ENTRY_FEE := LeftPad(Trim(OPFEntryFee.Text),6);
  bDoEdit := CustomFinanceEdit(Scrno, 10, FWKSHEET.ENTRY_FEE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmWorkSheet.OPFEntryFeeExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FWKSHEET.ENTRY_FEE := LeftPad(Trim(OPFEntryFee.Text),6);
      bDoEdit := CustomFinanceEdit(Scrno, 10, FWKSHEET.ENTRY_FEE, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmWorkSheet.OPFEntryTotalEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FWKSHEET.ENTRY_TOTAL := LeftPad(Trim(OPFEntryTotal.Text),8);
  bDoEdit := CustomFinanceEdit(Scrno, 11, FWKSHEET.ENTRY_TOTAL, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmWorkSheet.OPFEntryTotalExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FWKSHEET.ENTRY_TOTAL := LeftPad(Trim(OPFEntryTotal.Text),8);
      bDoEdit := CustomFinanceEdit(Scrno, 11, FWKSHEET.ENTRY_TOTAL, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmWorkSheet.OPFNonMemberEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FWKSHEET.NON_MEMB := LeftPad(Trim(OPFNonMember.Text),6);
  bDoEdit := CustomFinanceEdit(Scrno, 12, FWKSHEET.NON_MEMB, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmWorkSheet.OPFNonMemberExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FWKSHEET.NON_MEMB := LeftPad(Trim(OPFNonMember.Text),6);
      bDoEdit := CustomFinanceEdit(Scrno, 12, FWKSHEET.NON_MEMB, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmWorkSheet.OPFFillInEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FWKSHEET.FILL_IN := LeftPad(Trim(OPFFillIn.Text),6);
  bDoEdit := CustomFinanceEdit(Scrno, 13, FWKSHEET.FILL_IN, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmWorkSheet.OPFFillInExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FWKSHEET.FILL_IN := LeftPad(Trim(OPFFillIn.Text),6);
      bDoEdit := CustomFinanceEdit(Scrno, 13, FWKSHEET.FILL_IN, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmWorkSheet.OSFCommentEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FWKSHEET.COMMENT := PaD(OSFComment.Text,25);
  bDoEdit := CustomFinanceEdit(Scrno, 14, FWKSHEET.COMMENT, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmWorkSheet.OSFCommentExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FWKSHEET.COMMENT := Pad(OSFComment.Text,25);
      bDoEdit := CustomFinanceEdit(Scrno, 14, FWKSHEET.COMMENT, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmWorkSheet.OSFNewPageEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FWKSHEET.new_page := Pad(OSFNewPage.Text,1);
  bDoEdit := CustomFinanceEdit(Scrno, 15, FWKSHEET.new_page, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmWorkSheet.OSFNewPageExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      If Not (Pos(OSFNewPage.Text, '0123456789P') > 0) Then Begin
        ErrBox('Must be P for new page or the number of blank lines.', mc, 0);
        OSFNewPage.Text := '0';
        LabelLinePageControl.Caption := OSFNewPage.Text + ' blank lines';
        OSFNewPage.SetFocus;
      End
      Else Begin
        FWKSHEET.new_page := Pad(OSFNewPage.Text,1);
        bDoEdit := CustomFinanceEdit(Scrno, 15, FWKSHEET.new_page, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'S');
        If OSFNewPage.Text = 'P' Then LabelLinePageControl.Caption := 'New Page'
        Else If Pos(OSFNewPage.Text, '0123456789') > 0 Then
          LabelLinePageControl.Caption := OSFNewPage.Text + ' blank lines';
      End;
    End;
  End;
End;

Procedure TfrmWorkSheet.Action3Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmWorkSheet.Action4Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Function TfrmWorkSheet.FormHelp(Command: Word; Data: Integer; Var CallHelp: Boolean): Boolean;
Begin
//  If data < 500000 Then WinHelp(Application.Handle, 'ACBLSCORE.HLP', Help_Context, Data);
  If data < 500000 Then
    HtmlHelp(GetDesktopWindow,DefChmFile,hh_help_context,Data);
End;

Procedure TfrmWorkSheet.pResetTabStops;
Begin
  { Reset ALL editable fields tabstop to true so fields will be available for next action: Add/Edit/Copy }
  OSFEventCode.TabStop := True;
  ODEEventDate.TabStop := True;
  OPFTime.TabStop := True;
  OSFSanctionNumberWS.TabStop := True;
  OSFEventName.TabStop := True;
  ONFSessions.TabStop := True;
  OPFTables.TabStop := True;
  ONFEntries.TabStop := True;
  OPFEntryFee.TabStop := True;
  OPFEntryTotal.TabStop := True;
  OPFNonMember.TabStop := True;
  OPFFillIn.TabStop := True;
  OSFComment.TabStop := True;
  OSFNewPage.TabStop := True;
End;

procedure TfrmWorkSheet.FormCreate(Sender: TObject);
begin
  mHHelp := THookHelpSystem.Create(DefChmFile,'',htHHAPI);

end;

procedure TfrmWorkSheet.FormDestroy(Sender: TObject);
begin
  mHHelp.Free;
  HHCloseAll;
end;

End.

