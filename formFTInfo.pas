Unit formFTInfo;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ovcbase, ovcdlg, ovcclkdg, StdCtrls, Mask, Buttons,
  ToolWin, ExtCtrls, ActnMan, ActnCtrls, Menus, ActnList, ovcef, ovcsf,
  ovcrvcbx, ovcpb, ovcpf, ovcnf, ovceditf, ovcedpop, ovcedcal, ovcbordr,
  ovcbcalc, ovcbcldr, XPStyleActnCtrls, Grids, ImgList, ResizeKit, LMDCustomComponent,
  LMDWndProcComponent, LMDFormShape, LMDShapeHint,
  LMDHint, LMDCustomScrollBox, LMDListBox, LMDCustomControl, LMDCustomPanel,
  LMDCustomBevelPanel, LMDBaseEdit, LMDCustomEdit, LMDCustomMaskEdit, LMDCustomExtCombo,
  LMDCustomListComboBox, LMDListComboBox, ovccmbx, LMDHeaderListComboBox,
  LMDCustomComboBox, LMDComboBox, STSTRS, LMDIniCtrl, LMDFormA,
  LMDStorBase, LMDStorXMLVault, LMDStorBinVault, LMDCustomHint,
  LMDCustomShapeHint, LMDMessageHint, LibData, LMDCalendarEdit,
  LMDCustomButton, LMDButton, fDbase, Dbase, DB33, LibUtil, Clipbrd, CSDef,
  Util1, Util2, YesNoBoxU, PVIO, VListBox, History, LMDButtonBarPopupMenu, Groups, LibFinance,
  LMDCustomParentPanel, LMDCustomGroupBox, LMDGroupBox, I_FCommon,hh;

Type
  { Required for Util2.Player_Check Function }
  OpenString = String[10];
  _DateString = String[8];
  _str2 = String[2];
  _str1 = String[1];
  _str4 = String[4];
  _str10 = String[10];
  _str14 = String[14];
  _str25 = String[25];
  _str40 = String[40];
  _str60 = String[60];
  _str80 = String[80];

Type
  TfrmFTInfo = Class(TForm)
    GBMenu: TGroupBox;
    Shape23: TShape;
    ButNext: TButton;
    ButPrev: TButton;
    ButFind: TButton;
    ButTop: TButton;
    ButLast: TButton;
    ButEdit: TButton;
    ButAdd: TButton;
    ButCopy: TButton;
    ButDelete: TButton;
    ButQuit: TButton;
    ButtonF7Key: TButton;
    ButtonF4Key: TButton;
    ButtonF8Key: TButton;
    ButtonF2Key: TButton;
    StaticTextF3Key: TStaticText;
    StaticTextF4Key: TStaticText;
    StaticTextF5Key: TStaticText;
    StaticTextF2Key: TStaticText;
    ButtonF1Key: TButton;
    StaticTextF1Key: TStaticText;
    ActionManager1: TActionManager;
    ButNextAction: TAction;
    ButPrevAction: TAction;
    ButFindAction: TAction;
    ButTopAction: TAction;
    ButLastAction: TAction;
    ButEditAction: TAction;
    ButAddAction: TAction;
    ButCopyAction: TAction;
    ButDeleteAction: TAction;
    ButQuitAction: TAction;
    F4Key: TAction;
    F3Key: TAction;
    F5Key: TAction;
    F2Key: TAction;
    F7Key: TAction;
    F1Key: TAction;
    F8Key: TAction;
    F6Key: TAction;
    CtrlCKey: TAction;
    ButPlus: TButton;
    StaticText1: TStaticText;
    PlusKey: TAction;
    ButMinus: TButton;
    StaticText2: TStaticText;
    MinusKey: TAction;
    StatusBar1: TStatusBar;
    OvcController1: TOvcController;
    LMDHint1: TLMDHint;
    ActionManager2: TActionManager;
    ButFTInfoDone: TAction;
    StaticText3: TStaticText;
    GBTournWorkSheet: TGroupBox;
    Label3: TLabel;
    OSFSanctionNo: TOvcSimpleField;
    SpeedButton1: TSpeedButton;
    ListTables: TAction;
    Label1: TLabel;
    OSFTournName: TOvcSimpleField;
    Label2: TLabel;
    OSFTournCity: TOvcSimpleField;
    Label4: TLabel;
    OSFTournSite: TOvcSimpleField;
    Label5: TLabel;
    OSFTournDates: TOvcSimpleField;
    Label6: TLabel;
    OSFTDNo: TOvcSimpleField;
    Label7: TLabel;
    OSFDicName: TOvcSimpleField;
    Label8: TLabel;
    OSFTournType: TOvcSimpleField;
    LabelTournTypeText: TLabel;
    Label20: TLabel;
    OSFUnitNo: TOvcSimpleField;
    Label21: TLabel;
    OSFDistrictNo: TOvcSimpleField;
    PopupMenu1: TPopupMenu;
    Worksheet1: TMenuItem;
    Invoice1: TMenuItem;
    Voucher1: TMenuItem;
    Ballancesheet1: TMenuItem;
    Officefinancialpart11: TMenuItem;
    Officefinancialpart2: TMenuItem;
    Quit1: TMenuItem;
    TournamentInformation1: TMenuItem;
    LMDGroupBox1: TLMDGroupBox;
    Label9: TLabel;
    OPFSanctionFee: TOvcPictureField;
    Label10: TLabel;
    OPFSurcharge: TOvcPictureField;
    Label11: TLabel;
    OPFPerDiemRate: TOvcPictureField;
    Label12: TLabel;
    OPFMileageRate: TOvcPictureField;
    Label13: TLabel;
    OPFSupplyRate: TOvcPictureField;
    Label14: TLabel;
    OSFIsSponCheck: TOvcSimpleField;
    Label15: TLabel;
    OSFUseExRate: TOvcSimpleField;
    Label16: TLabel;
    Label17: TLabel;
    OPFExchangeRate: TOvcPictureField;
    Label18: TLabel;
    OPFExchangeRateTD: TOvcPictureField;
    Label19: TLabel;
    OPFGSTRate: TOvcPictureField;
    LMDFTInfoDone: TLMDButton;
    FullScreen: TAction;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Procedure ButQuitActionExecute(Sender: TObject);
    Procedure FormClose(Sender: TObject; Var Action: TCloseAction);
    Procedure FormActivate(Sender: TObject);
    Procedure ListTablesExecute(Sender: TObject);
    Procedure pFillFields;
    Procedure pNextRecordf;
    Procedure pPrevRecordf;
    Procedure ButNextActionExecute(Sender: TObject);
    Procedure ButPrevActionExecute(Sender: TObject);
    Procedure ButLastActionExecute(Sender: TObject);
    Procedure ButTopActionExecute(Sender: TObject);
    Procedure ButFindActionExecute(Sender: TObject);
    Procedure pFTInfo(bEnable: Boolean);
    Procedure ButEditActionExecute(Sender: TObject);
    Procedure ButFTInfoDoneExecute(Sender: TObject);
    Procedure ButAddActionExecute(Sender: TObject);
    Procedure ButCopyActionExecute(Sender: TObject);
    Procedure ButDeleteActionExecute(Sender: TObject);
    Procedure pUpdateStatusBar;
    Procedure pDateExit(Sender: TObject);
    Function fshow_tourn_type(Const tt: String): _str25;
    Function fGetTTInd(Const tt: String): byte;
    Procedure MinusKeyExecute(Sender: TObject);
    Procedure F1KeyExecute(Sender: TObject);
    Procedure F2KeyExecute(Sender: TObject);
    Procedure pPuMenuItemSelected(Sender: TObject);
    Procedure pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure PlusKeyExecute(Sender: TObject);
    Procedure OSFSanctionNoExit(Sender: TObject);
    Procedure OSFTournNameExit(Sender: TObject);
    Procedure OSFTournDatesExit(Sender: TObject);
    Procedure OSFTDNoExit(Sender: TObject);
    Procedure OSFUnitNoExit(Sender: TObject);
    Function fFValidUnit(Var UNum: Openstring): boolean;
    Procedure pAbortChanges;
    Procedure OSFDistrictNoExit(Sender: TObject);
    Procedure OSFIsSponCheckExit(Sender: TObject);
    Procedure OSFUseExRateExit(Sender: TObject);
    Procedure F7KeyExecute(Sender: TObject);
    Procedure F8KeyExecute(Sender: TObject);
    Procedure F6KeyExecute(Sender: TObject);
    Procedure pTypeTourHintFill;
    Procedure pEditFees;
    Procedure F4KeyExecute(Sender: TObject);
    Procedure pClearFields;
    Procedure pSaveChanges;
    Procedure pCopyRecData;
    Procedure pCheckEditFields;
    Function fValidateFields: Boolean;
    Procedure OSFTournTypeExit(Sender: TObject);
    //    Function fFound(Const Fno, Kno: Integer; ChkKey: KeyStr): Boolean;
    //Procedure pChooseTourn;
    Procedure pButtOn(bTurnOn: Boolean);
    Procedure OSFDicNameEnter(Sender: TObject);
    Procedure OSFTDNoChange(Sender: TObject);
    Procedure pBlankFTInfoRecord;
    Procedure FullScreenExecute(Sender: TObject);
    Procedure Action1Execute(Sender: TObject);
    Procedure ResizeKit1ExitResize(Sender: TObject; XScale, YScale: Double);
    Procedure pSetWinState;
    //    Procedure pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
    //    Procedure pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
    //    Procedure pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
    //    Procedure pONFieldAvail(bAvail: Boolean; oOSField: TOvcNumericField);
    Procedure OSFSanctionNoEnter(Sender: TObject);
    Procedure OSFTournNameEnter(Sender: TObject);
    Procedure OSFTournCityEnter(Sender: TObject);
    Procedure OSFTournCityExit(Sender: TObject);
    Procedure OSFTournSiteEnter(Sender: TObject);
    Procedure OSFTournSiteExit(Sender: TObject);
    Procedure OSFTournDatesEnter(Sender: TObject);
    Procedure OSFTDNoEnter(Sender: TObject);
    Procedure OSFDicNameExit(Sender: TObject);
    Procedure OSFTournTypeEnter(Sender: TObject);
    Procedure OSFUnitNoEnter(Sender: TObject);
    Procedure OSFDistrictNoEnter(Sender: TObject);
    Procedure OPFSanctionFeeEnter(Sender: TObject);
    Procedure OPFSanctionFeeExit(Sender: TObject);
    Procedure OPFSurchargeEnter(Sender: TObject);
    Procedure OPFSurchargeExit(Sender: TObject);
    Procedure OPFPerDiemRateEnter(Sender: TObject);
    Procedure OPFPerDiemRateExit(Sender: TObject);
    Procedure OPFMileageRateEnter(Sender: TObject);
    Procedure OPFMileageRateExit(Sender: TObject);
    Procedure OPFSupplyRateEnter(Sender: TObject);
    Procedure OPFSupplyRateExit(Sender: TObject);
    Procedure OSFIsSponCheckEnter(Sender: TObject);
    Procedure OSFUseExRateEnter(Sender: TObject);
    Procedure OPFExchangeRateEnter(Sender: TObject);
    Procedure OPFExchangeRateExit(Sender: TObject);
    Procedure OPFExchangeRateTDEnter(Sender: TObject);
    Procedure OPFExchangeRateTDExit(Sender: TObject);
    Procedure OPFGSTRateEnter(Sender: TObject);
    Procedure OPFGSTRateExit(Sender: TObject);
    Procedure Action2Execute(Sender: TObject);
    Procedure Action3Execute(Sender: TObject);
    Function FormHelp(Command: Word; Data: Integer; Var CallHelp: Boolean): Boolean;
    Procedure pResetTabStops;
  private
    { Private declarations }
    Procedure SysCommand(Var Message: TWMSYSCOMMAND); message WM_SYSCOMMAND;
  public
    { Public declarations }
    bKeyIsUnique: Boolean;
  End;

Var
  frmFTInfo: TfrmFTInfo;
  bEscKeyUsed: Boolean;
  iLenTournTypes: Integer;
  { Integer variable used dynamically to store the tables record count }
  iFTInfoRecs, iTournRecs: Integer;
  sSanctionNo: String;
  sClassName, sSenderName: String;
  sClassType: TClass;

Const
  iLenArray: Integer = MaxFilno;

Implementation

Uses formTblsCurrOpen, formWorkSheet, tfprint, FormEdit;

{$I STSTRS.dcl}

{$R *.dfm}

Procedure TfrmFTInfo.ButQuitActionExecute(Sender: TObject);
Begin
  { The user wants to Quit, change the Public Variable:  bFTQuitUsed to True }
  frmWorkSheet.bFTQuitUsed := True;
  Close;
End;

Procedure TfrmFTInfo.FormClose(Sender: TObject; Var Action: TCloseAction);
Begin
  If frmWorkSheet.iFormTag = (Scrno - 1) Then frmWorkSheet.iFormTag := 1;
  If frmWorkSheet.bFTQuitUsed Then Begin
    { Close any open tables }
    fdBase.fCloseFiles;
    dBase.CloseFiles;
    Application.Terminate;
  End;
End;

Procedure TfrmFTInfo.FormActivate(Sender: TObject);
Begin
  frmWorkSheet.bWinStNorm := (frmWorkSheet.WindowState = wsNormal);
  If frmWorkSheet.bWinStNorm Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  frmWorkSheet.bWinStNorm := (WindowState = wsNormal);
  pSetPanelWidth(frmFTInfo, StatusBar1);
  { TopFilNo: What is the topmost database we are using in this application. }
  TopFilNo := Ftinfo_no;
  fFilno := Ftinfo_no;
  bEscKeyUsed := False;
  Application.HintHidePause := 200000;
  pTypeTourHintFill;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  { Flush_DF & Flush_IF: Must be set to True for so that everytime a change is
    made to a "Record", dbase re-writes the table header and in Playern.dat,
    updates dbLastImpDate entry. Set them to False when you Delete a record,
    then immediately set them back to True }
  { This must be done as a standard whenever a table is opened for modifications }
  Flush_DF := True;
  Flush_IF := True;
  { Find the first available record and load it's values to the fields on the screen
    if frmWorkSheet.ksFTInfoKey is blank. If it is not, load the record in memory. }
  If ksFTInfoKey = '' Then Begin
    fTop_Record;
  End;
  // ------------------------------------------------------
  pFillFields;
  { This variable Scrno, is used in some of the older code procedures to determine which
    calculations need to be performed based on the screen we are in.
    ----------------------------------------------------------
    Screen 1 = Tournament Information    - FTInfo.dat
    Screen 2 = Tournament Work Sheet     - FWkSheet.dat
    Screen 3 = Tournament Invoice        - FInvoice.dat
    Screen 4 = Voucher Items             - FInvItem.dat
    Screen 5 = Tournament Balance Sheet  - FBalance.dat
    Screen 6 = Office financial part 1   - FOFs.dat
    Screen 7 = Office financial part 2   - FOFs.dat  }
  Scrno := 1;
  //==========================================================
  If CSDef.CFG.Tournament Then StatusBar1.Panels[0].Text := 'Tournament'
  Else StatusBar1.Panels[0].Text := 'Club';
  // ----------------------------------------------------------------------------------------------
  { Show the path to the table in use and the table name in StatusBar1. }
  StatusBar1.Panels[1].Text := UpperCase(Prepend + 'FTINFO.DAT');
  { Store the number of records to iFTInfoRecs }
  iFTInfoRecs := db33.UsedRecs(fDatF[FTInfo_no]^);
  iTournRecs := db33.UsedRecs(DatF[Tourn_no]^);
  { Add the record count to StatusBar1's display }
  StatusBar1.Panels[1].Text := StatusBar1.Panels[1].Text + ' - Contains ' +
    Trim(IntToStr(iFTInfoRecs)) + ' Records.';
  // ----------------------------------------------------------------------------------------------
  If iFTInfoRecs = 0 Then Begin
    pButtOn(False);
    ButAdd.SetFocus;
  End;
End;

Procedure TfrmFTInfo.ListTablesExecute(Sender: TObject);
Begin
  frmTblsCurrOpen := TfrmTblsCurrOpen.Create(Self);
  frmTblsCurrOpen.ShowModal;
  frmTblsCurrOpen.Free;
End;

Procedure TfrmFTInfo.pFillFields;
Begin
  With FTINFO Do Begin
    OSFSanctionNo.Text := Trim(SANCTION_NO);
    OSFTournName.Text := Trim(TOURN_NAME);
    OSFTournCity.Text := Trim(TOURN_CITY);
    OSFTournSite.Text := Trim(TOURN_SITE);
    OSFTournDates.Text := Trim(TOURN_DATES);
    OSFTDNo.Text := TD_NO;
    OSFDicName.Text := Trim(DIC_NAME);
    OSFTournType.Text := Trim(TOURN_TYPE);
    LabelTournTypeText.Caption := fshow_tourn_type(OSFTournType.Text);
    OSFUnitNo.Text := UNIT_NO;
    OSFDistrictNo.Text := DISTRICT_NO;
    OPFSanctionFee.Text := SANCTION_FEE;
    OPFSurcharge.Text := SURCHARGE;
    OPFPerDiemRate.Text := PER_DIEM_Rate;
    OPFMileageRate.Text := MILEAGE_RATE;
    OPFSupplyRate.Text := SUPPLY_RATE;
    OSFIsSponCheck.Text := Is_spon_check;
    OSFUseExRate.Text := USE_EX_RATE;
    OPFExchangeRate.Text := EXCHANGE_RATE;
    OPFExchangeRateTD.Text := EXCHANGE_RATE_TD;
    OPFGSTRate.Text := Gst;
  End;
End;

Procedure TfrmFTInfo.pNextRecordf;
Begin
  { Move the record pointer in the table to the next available record and load the data in the screen fields }
  fNext_Record;
  pFillFields;
End;

Procedure TfrmFTInfo.ButNextActionExecute(Sender: TObject);
Begin
  pNextRecordf;
End;

Procedure TfrmFTInfo.pPrevRecordf;
Var
  FndKey: KeyStr;
Begin
  { Move the record pointer in the table to the previous record and load the data in the screen fields }
  fPrev_Record;
  pFillFields;
End;

Procedure TfrmFTInfo.ButPrevActionExecute(Sender: TObject);
Begin
  pPrevRecordf;
End;

Procedure TfrmFTInfo.ButLastActionExecute(Sender: TObject);
Begin
  fLast_Record;
  pFillFields;
End;

Procedure TfrmFTInfo.ButTopActionExecute(Sender: TObject);
Begin
  fTop_Record;
  pFillFields;
End;

Procedure TfrmFTInfo.ButFindActionExecute(Sender: TObject);
Begin
  fFind_Record(' ', fGetKey(fFilno, 1), '', MC, 0, True);
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmFTInfo.pFTInfo(bEnable: Boolean);
Begin
  FreeHintWin;
  { Set the state of ActionManager1 for the players GBMenu }
  If bEnable Then ActionManager1.State := asSuspended Else
    ActionManager1.State := asNormal;
  { Set the state of ActionManager2 for the form players GBMenu }
  If bEnable Then ActionManager2.State := asNormal Else
    ActionManager2.State := asSuspended;
  // ----------------------------------------------------------------------
  If bEnable Then GBMenu.Hide Else GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := Not bEnable;
  // ----------------------------------------------------------------------
  GBTournWorkSheet.Enabled := bEnable;
  // ----------------------------------------------------------------------
  LMDFTInfoDone.Enabled := bEnable;
  LMDFTInfoDone.Visible := bEnable;
  // ----------------------------------------------------------------------
End;

Procedure TfrmFTInfo.ButEditActionExecute(Sender: TObject);
Begin
  { In module Dbase call the procedure fPreEditSaveKeys to save the KEYS in all tables we are
    editing so the application will know if a KEY Field has been changed and Indexes need to be
    regenerated }
  fPreEditSaveKeys;
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 2;
  sDataMode := 'E';
  { Turn the Info Data Area ON }
  pFTInfo(True);
  { Store the original Sanction Number for validation purposes }
  sSanctionNo := OSFSanctionNo.Text;
  { Put the cursor in the first field }
  OSFSanctionNo.SetFocus;
End;

Procedure TfrmFTInfo.ButFTInfoDoneExecute(Sender: TObject);
Begin
  pSaveChanges;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  { Turn the Player Info Data Area OFF }
  pFTInfo(False);
  pUpdateStatusBar;
End;

Procedure TfrmFTInfo.ButAddActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 1;
  sDataMode := 'A';
  fInitRecord(fFilno);
  { Clear the Edit Fields of any data }
  pClearFields;
  { Turn the Player Info Data Area ON }
  pFTInfo(True);
  OSFSanctionNo.SetFocus;
End;

Procedure TfrmFTInfo.pClearFields;
Begin
  OSFSanctionNo.Text := '';
  OSFTournName.Text := '';
  OSFTournCity.Text := '';
  OSFTournSite.Text := '';
  OSFTournDates.Text := '';
  OSFTDNo.Text := '';
  OSFDicName.Text := '';
  OSFTournType.Text := '';
  LabelTournTypeText.Caption := '';
  OSFUnitNo.Text := '';
  OSFDistrictNo.Text := '';
  OPFSanctionFee.Text := '0';
  OPFSurcharge.Text := '0';
  OPFPerDiemRate.Text := '0';
  OPFMileageRate.Text := '0';
  OPFSupplyRate.Text := '0';
  OSFIsSponCheck.Text := 'N';
  OSFUseExRate.Text := 'N';
  OPFExchangeRate.Text := '0';
  OPFExchangeRateTD.Text := '0';
  OPFGSTRate.Text := '0';
End;

Procedure TfrmFTInfo.ButCopyActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 3;
  sDataMode := 'C';
  { Turn the Player Info Data Area ON }
  pFTInfo(True);
  OSFSanctionNo.SetFocus;
End;

Procedure TfrmFTInfo.ButDeleteActionExecute(Sender: TObject);
Begin
  fDelete_Record(True, Self.HelpContext);
  // ------------------------------------------------------------
  pUpdateStatusBar;
  // ------------------------------------------------------------
  iFTInfoRecs := db33.UsedRecs(fDatF[FTInfo_no]^);
  If iFTInfoRecs > 0 Then Begin
    { Move the record pointer in the table to the next available record and load the
    data in the screen fields }
    pNextRecordf;
    If ButNext.Enabled Then ButNext.SetFocus Else ButQuit.SetFocus;
  End
  Else Begin
    pClearFields;
    pButtOn(False);
    ButAdd.SetFocus;
  End;
End;

Procedure TfrmFTInfo.pUpdateStatusBar;
Begin
  iFTInfoRecs := db33.UsedRecs(fDatF[FTInfo_no]^);
  StatusBar1.Panels[1].Text := UpperCase(dbase.Prepend + 'FTINFO.DAT') + ' - Contains ' +
    Trim(IntToStr(iFTInfoRecs)) + ' Records.';
End;

Procedure TfrmFTInfo.pDateExit(Sender: TObject);
Begin
  { Replace all blanks with zeros in the date }
  (Sender As TOvcDateEdit).Text := fFillDateBlanks((Sender As TOvcDateEdit).Text);
End;

Function TfrmFTInfo.fshow_tourn_type(Const tt: String): _str25;
Var
  j: byte;
Begin
  j := fGetTTInd(Trim(tt));
  If j > 0 Then Result := Pad(Copy(TournTypes[j], 4, 25), 25)
  Else Result := Pad('Unknown', 25);
End;

Function TfrmFTInfo.fGetTTInd(Const tt: String): byte;
Var
  j: byte;
Begin
  For j := 1 To Ttypes Do
    If tt = Trim(Copy(TournTypes[j], 1, 2)) Then Begin
      Result := j;
      exit;
    End;
  Result := 0;
End;

Procedure TfrmFTInfo.MinusKeyExecute(Sender: TObject);
Begin
  { Store this records key to the Public Variable:  frmWorkSheet.ksFTInfoKey }
  ksFTInfoKey := fGetKey(FTINFO_no, 1);
  { Variable possible values and their meanings:
    0 = Tournament Information, frmTournInfo
    1 = Tournament Worksheet, frmWorkSheet
    2 = Invoice, frmInvoice
    3 = Voucher, frmVoucher
    4 = Balance Sheet, frmBalanceSheet
    5 = Office Financial Part 1, frmOffFinP1
    6 = Office Financial Part 2, frmOffFinP2 }
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := 6; { 6 = Office Financial Part 2, frmOffFinP2 }
  frmWorkSheet.bFTQuitUsed := False;
  Close;
End;

Procedure TfrmFTInfo.F1KeyExecute(Sender: TObject);
Begin
  //pRunExtProg(frmFTInfo, CFG.LoadPath + 'WUtility', 'MANUAL\CONTENTS');
  keybd_event(VK_F1, MapVirtualKey(VK_F1, 0), 0, 0);
End;

Procedure TfrmFTInfo.F2KeyExecute(Sender: TObject);
Var
  iForLoop: Integer;
Begin
  GBMenu.Enabled := False;
  For iForLoop := (PopupMenu1.Items.Count - 1) Downto 0 Do
    PopupMenu1.Items.Items[iForLoop].Enabled := True;
  { 0 = Tournament Information, frmTournInfo }
  PopupMenu1.Items.Items[0].Enabled := False;
  PopupMenu1.Popup(350, 500);
  GBMenu.Enabled := True;
End;

Procedure TfrmFTInfo.pPuMenuItemSelected(Sender: TObject);
Begin
  { Variable iFormTag possible values and their meanings:
    0 = Tournament Information, frmTournInfo
    1 = Tournament Worksheet, frmWorkSheet
    2 = Invoice, frmInvoice
    3 = Voucher, frmVoucher
    4 = Balance Sheet, frmBalanceSheet
    5 = Office Financial Part 1, frmOffFinP1
    6 = Office Financial Part 2, frmOffFinP2 }
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := (Sender As TMenuItem).Tag;
  If ((frmWorkSheet.iFormTag >= 1) And (frmWorkSheet.iFormTag <= 6)) Then Begin
    { Store this records key to the Public Variable:  frmWorkSheet.ksFTInfoKey }
    ksFTInfoKey := fGetKey(FTINFO_no, 1);
    { Store the Invoice Record FHash Key to the Public Variable:  frmWorkSheet.ksFInvoiceKey
      If the Voucher Screen was selected }
    If frmWorkSheet.iFormTag = 3 Then frmWorkSheet.pGetInvoiceKey;
    Close;
  End;
End;

Procedure TfrmFTInfo.pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Var
  wKey: Word;
Begin
  sClasstype := TOvcSimpleField;
  wKey := Key;
  Case key Of
    { 107 is the Plus Key was pressed on the numeric keypad }
    107: If GBMenu.Enabled Then PlusKeyExecute(Sender); // pDiffScreen(1)
    { 109 is the Minus Key was pressed on the numeric keypad }
    109: If GBMenu.Enabled Then MinusKeyExecute(Sender);
    { Return key was pressed }
    VK_RETURN: If Not GBMenu.Enabled Then Begin
        key := 0;
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
      End;
    { ESCape key was pressed, Abort the record or record changes... }
    VK_ESCAPE: If Not GBMenu.Enabled Then Begin
        bEscKeyUsed := True;
        pAbortChanges;
        sClassName := Sender.ClassName;
        sClassType := Sender.ClassType;
        If Sender.ClassType = TOvcSimpleField Then
          sSenderName := (Sender As TOvcSimpleField).Name
        Else If Sender.ClassType = TOvcPictureField Then
          sSenderName := (Sender As TOvcPictureField).Name;
        If iFTInfoRecs = 0 Then close;
      End
      Else close;
    { F9 key was pressed, save the record or the changes... }
    VK_F9: If Not GBMenu.Enabled Then pSaveChanges;
  Else
    If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
      If (Key = VK_UP) Then Begin
        ButPrev.Click;
        ButPrev.SetFocus;
      End
      Else Begin
        ButNext.Click;
        ButNext.SetFocus;
      End;
      Key := 0;
    End;
  End;
End;

Procedure TfrmFTInfo.pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
Var
  wKey: Word;
Begin
  { Do not trap for the Return Key here, it causes the cursor to move two fields at a time }
  wKey := Key;
  Case key Of
    { ESCape key was pressed, Abort the record or record changes... }
    VK_ESCAPE: If Not GBMenu.Enabled Then Begin
        bEscKeyUsed := True;
        pAbortChanges;
        If iFTInfoRecs = 0 Then close;
      End;
    { F9 key was pressed, save the record or the changes... }
    VK_F9: If Not GBMenu.Enabled Then pSaveChanges;
  Else
    If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
      If (Key = VK_UP) Then Begin
        ButPrev.Click;
        ButPrev.SetFocus;
      End
      Else Begin
        ButNext.Click;
        ButNext.SetFocus;
      End;
      Key := 0;
    End;
  End;
End;

Procedure TfrmFTInfo.PlusKeyExecute(Sender: TObject);
Begin
  { Open the WorkSheet Form }
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := 1;
  { Store this records key to the Public Variable:  frmWorkSheet.ksFTInfoKey }
  ksFTInfoKey := fGetKey(FTINFO_no, 1);
  Close;
End;

Function TfrmFTInfo.fFValidUnit(Var UNum: Openstring): boolean;
Var
  Dnum: String[2];
Begin
  Result := ValidUnit(UNum, Dnum);
  If Ival(Dnum) > 0 Then FTinfo.District_no := FixNumField(Dnum,2);
End;

Procedure TfrmFTInfo.SysCommand(Var Message: TWMSYSCOMMAND);
Begin
  { Check to see if "X" was used to close the form }
  If Message.CmdType And $FFF0 = SC_CLOSE Then Begin
    { The user wants to Quit, change the Public Variable:  bFTQuitUsed to True }
    frmWorkSheet.bFTQuitUsed := True;
    { Store this records key to the Public Variable:  frmWorkSheet.ksFTInfoKey }
    ksFTInfoKey := '';
    Close;
  End
  Else Inherited;
End;

Procedure TfrmFTInfo.pAbortChanges;
Begin
  FreeHintWin;
  pResetTabStops;
  { Turn the Groupbox area off }
  pFTInfo(False);
  // ------------------------------------------------------
  If iDataMode = 1 Then fTop_Record
  Else fGetARec(fFilNo);
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  pFillFields;
End;

Procedure TfrmFTInfo.F7KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (2) to Print Reports }
  PrintReport(2);
End;

Procedure TfrmFTInfo.F8KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (1) to Print to Screen }
  PrintReport(1);
End;

Procedure TfrmFTInfo.F6KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (3) to Print Reports to an ASCII file }
  PrintReport(3);
End;

Procedure TfrmFTInfo.pTypeTourHintFill;
Var
  iLoop: Integer;
  sHint: String;
Begin
  // TournTypes: Array[1..Ttypes] Of string =
  sHint := '';
  iLenTournTypes := Length(TournTypes);
  If iLenTournTypes >= 1 Then Begin
    For iLoop := 1 To iLenTournTypes Do Begin
      sHint := sHint + TournTypes[iLoop] + #13 + #10;
    End;
  End;
  OSFTournType.Hint := sHint;
End;

Const
  EditRec: PEdit = Nil;

Procedure TfrmFTInfo.pEditFees;
Var
  j: word;
  RFees: Array[1..Ttypes] Of real48;
  SCharge: Array[1..Ttypes] Of SmallInt;
  Col: byte;
  Row: byte;
Begin
  eUseFixedFont := True;
  EditInit(EditRec, 6, 3, ' Edit ACBL fees ', ' Press F9 when done ');
  EditAddText(EditRec, 1, 'Year that fees are effective', RateYear, 4, 1, 4, Nil, Nil,
    2, 2, 0);
  EditAddLine(EditRec, 0, 'Tournament director sponsor fees:', false, 0, 2, 2);
  EditAddLine(EditRec, 0, CharStr(' ', 70), false, 0, 1, 2);
  For j := 1 To MaxTDrank Do EditAddFloat(EditRec, j + 1, Tdrates[j], 50, 300, 6, 2,
      Pad(TdrankName[j] + ' Director', 30), Nil, Nil, 1, 1, 0);
  {$IFDEF tsup}
  EditAddFloat(EditRec, 8, RegSup, 0, 100, 6, 2, Pad('DIC Regional supplement', 29),
    Nil, Nil, 2, 2, 0);
  EditAddFloat(EditRec, 9, SecSup, 0, 100, 6, 2, Pad('DIC Sectional supplement', 29),
    Nil, Nil, 1, 2, 0);
  {$ENDIF}
  EditAddLine(EditRec, 0, 'Press F4 to edit sanction fees', true, 0, 2, 2);
  EditAddLine(EditRec, 0, ' ', false, 0, 1, 2);
  EditShow(EditRec, Escaped, ifunc, [4, 9], 13);
  EditDone(EditRec);
  If ifunc = 4 Then Begin
    eUseFixedFont := True;
    EditInit(EditRec, 2, 1, ' Edit ACBL sanction fees ', ' Press F9 when done ');
    EditAddFloat(EditRec, 60, SecSurcharge, 50, 300, 6, 2,
      Pad('Sectional Surcharge', 25), Nil, Nil, 1, 2, 0);
    EditAddLine(EditRec, 0, CharStr(' ', 29) + 'Sanc' + CharStr(' ', 36) + 'Sanc',
      false, 0, 2, 2);
    EditAddLine(EditRec, 0, 'Tournament type              Fee Code', false, 0, 1, 2);
    EditAddLine(EditRec, 0, 'Tournament type              Fee Code', false, 0, 0, 42);
    EditAddLine(EditRec, 0, ' ', false, 0, 1, 2);
    For j := 1 To Ttypes Do Begin
      row := j Mod 2;
      col := ((1 - row) * 40) + 2;
      RFees[j] := SancFees[j] / 100;
      SCharge[j] := Surcharges[j];
      EditAddFloat(EditRec, j, RFees[j], 0, 30, 6, 2, Pad(TournTypes[j], 25),
        Nil, Nil, row, col, 0);
      EditAddInt(EditRec, j + 30, SCharge[j], 0, 3, 2, '', Nil, Nil, 0, Col + 33, 0);
    End;
    EditAddLine(EditRec,0,
      'Code:  0: No surcharge;  1: Apply surcharge;  2: Calculate surcharge',
      false,0,2,2);
    EditAddLine(EditRec,0,'       3: Calculate Sanction fee (STAC)',false,0,1,2);
    EditAddLine(EditRec, 0, ' ', false, 0, 1, 2);
    EditShow(EditRec, Escaped, ifunc, [9], 13);
    EditDone(EditRec);
    For j := 1 To Ttypes Do Begin
      SancFees[j] := Round(RFees[j] * 100);
      Surcharges[j] := SCharge[j];
    End;
  End;
  escaped := false;
  pWriteFees;
End;

Procedure TfrmFTInfo.F4KeyExecute(Sender: TObject);
Begin
  { Disable the form and turn off the actionmanager }
  frmFTInfo.Enabled := False;
  ActionManager1.State := asSuspended;
  { Show the fees form }
  pEditFees;
  { Enable the original form and turn on the actionmanager }
  frmFTInfo.Enabled := True;
  ActionManager1.State := asNormal;
End;

Procedure TfrmFTInfo.pSaveChanges;
Begin
  { Data Mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  Case iDataMode Of
    1, 3: Begin // Add or Copy Modes
        { Do not Add or Copy to a new record if the Sanction Number is blank or already exists in a
          Tournament Information Record }
        If ((Trim(OSFSanctionNo.Text) <> '') And Not fFDuplicateKey(FTInfo_no, 1, iDataMode)) Then Begin
          pCopyRecData;
          fdbase.fAdd_Record;
          CalcAll;
          pUpdateStatusBar;
          pButtOn(True);
          FinancePostEditProc(FTInfo_no);
          FixTotals(iDataMode);
        End;
      End;
    2: Begin // Edit Mode
        pCheckEditFields;
        bKeyIsUnique := fPostEditFixKeys;
        If Not bKeyIsUnique Then Begin
          If (MessageDlg('This Sanction Number already exists in this database.', mtError,
            [mbOK], 0) = mrOK) Then Begin
            OSFSanctionNo.SetFocus;
            Exit;
          End
          Else pAbortChanges;
        End
        Else If fValidateFields Then Begin
          CalcAll;
          fdbase.fPutARec(fFilNo);
          FinancePostEditProc(FTInfo_no);
          FixTotals(iDataMode);
        End;
      End;
  End;
  pFillFields;
  iDataMode := 0;
  sDataMode := null;
  pResetTabStops;
  { Turn the Player Info Data Area ON }
  pFTInfo(False);
End;

Procedure TfrmFTInfo.pCopyRecData;
Begin
  { Used when Adding or Copying a Record }
  With FTINFO Do Begin
    SANCTION_NO := Pad(OSFSanctionNo.Text,10);
    TOURN_NAME := Pad(OSFTournName.Text,25);
    TOURN_CITY := Pad(OSFTournCity.Text,16);
    TOURN_SITE := Pad(OSFTournSite.Text,25);
    TOURN_DATES := Pad(OSFTournDates.Text,20);
    TD_NO := FixNumField(OSFTDNo.Text,3);
    DIC_NAME := Pad(OSFDicName.Text,25);
    TOURN_TYPE := Pad(OSFTournType.Text,2);
    UNIT_NO := FixNumField(OSFUnitNo.Text,3);
    DISTRICT_NO := FixNumField(OSFDistrictNo.Text,2);
    SANCTION_FEE := LeftPad(Trim(OPFSanctionFee.GetStrippedEditString),6);
    SURCHARGE := LeftPad(Trim(OPFSurcharge.GetStrippedEditString),6);
    PER_DIEM_Rate := LeftPad(Trim(OPFPerDiemRate.GetStrippedEditString),6);
    MILEAGE_RATE := LeftPad(Trim(OPFMileageRate.AsString),5);
    SUPPLY_RATE := LeftPad(Trim(OPFSupplyRate.GetStrippedEditString),5);
    Is_spon_check := Pad(OSFIsSponCheck.Text,1);
    USE_EX_RATE := Pad(OSFUseExRate.Text,1);
    EXCHANGE_RATE := LeftPad(Trim(OPFExchangeRate.GetStrippedEditString),6);
    EXCHANGE_RATE_TD := LeftPad(Trim(OPFExchangeRateTD.GetStrippedEditString),6);
    Gst := LeftPad(Trim(OPFGSTRate.GetStrippedEditString),5);
    If UpperCase(Trim(EXCHANGE_RATE)) = 'Y' Then OfsItemtypes := (MaxOfsItemtypes - 2)
    Else OfsItemtypes := MaxOfsItemtypes;
  End;
End;

Procedure TfrmFTInfo.pCheckEditFields;
Begin
  { Used when Editing a Record }
  // ---------------------------------------------------------------------
  With FTINFO Do Begin
    SANCTION_NO := Pad(OSFSanctionNo.Text,10);
    TOURN_NAME := Pad(OSFTournName.Text,25);
    TOURN_CITY := Pad(OSFTournCity.Text,16);
    TOURN_SITE := Pad(OSFTournSite.Text,25);
    TOURN_DATES := Pad(OSFTournDates.Text,20);
    TD_NO := FixNumField(OSFTDNo.Text,3);
    DIC_NAME := Pad(OSFDicName.Text,25);
    TOURN_TYPE := Pad(OSFTournType.Text,2);
    UNIT_NO := FixNumField(OSFUnitNo.Text,3);
    DISTRICT_NO := FixNumField(OSFDistrictNo.Text,2);
    SANCTION_FEE := LeftPad(Trim(OPFSanctionFee.GetStrippedEditString),6);
    SURCHARGE := LeftPad(Trim(OPFSurcharge.GetStrippedEditString),6);
    PER_DIEM_Rate := LeftPad(Trim(OPFPerDiemRate.GetStrippedEditString),6);
    MILEAGE_RATE := LeftPad(Trim(OPFMileageRate.AsString),5);
    SUPPLY_RATE := LeftPad(Trim(OPFSupplyRate.GetStrippedEditString),5);
    Is_spon_check := Pad(OSFIsSponCheck.Text,1);
    USE_EX_RATE := Pad(OSFUseExRate.Text,1);
    EXCHANGE_RATE := LeftPad(Trim(OPFExchangeRate.GetStrippedEditString),6);
    EXCHANGE_RATE_TD := LeftPad(Trim(OPFExchangeRateTD.GetStrippedEditString),6);
    Gst := LeftPad(Trim(OPFGSTRate.GetStrippedEditString),5);
  End;
End;

Function TfrmFTInfo.fValidateFields: Boolean;
Var
  sUnitNo: Openstring;
Begin
  Result := True;
  If Trim(OSFTournName.Text) = '' Then Result := False
  Else Begin
    If Trim(OSFTournDates.Text) = '' Then Result := False
    Else Begin
      If Not (fFound(TDINFO_No, 3, OSFTDNo.Text)) Then Result := False
      Else Begin
        sUnitNo := Copy(OSFUnitNo.Text, 1, 10);
        If Not (fFValidUnit(sUnitNo)) Then Result := False
        Else Begin
          If Trim(OSFDistrictNo.Text) = '' Then Result := False
        End;
      End;
    End;
  End;
  If Not result Then MessageDlg('Record failed edit checks. Cannot save edits...', mtError, [mbOK], 0);
End;

//Function TfrmFTInfo.fFound(Const Fno, Kno: Integer; ChkKey: KeyStr): Boolean;
//Begin
//  If (Fno <> fFilno) And (fKeyLen[Fno, Kno] > 0) Then Begin
//    FindKey(fIdxKey[Fno, Kno]^, fRecno[Fno], ChkKey);
//    If OK Then Begin
//      Case Fno Of
//        1: GetRec(fDatF[1]^, fRecno[1], FTINFO);
//        2: GetRec(fDatF[2]^, fRecno[2], FWKSHEET);
//        3: GetRec(fDatF[3]^, fRecno[3], FINVOICE);
//        4: GetRec(fDatF[4]^, fRecno[4], FINVITEM);
//        5: GetRec(fDatF[5]^, fRecno[5], FBALANCE);
//        6: GetRec(fDatF[6]^, fRecno[6], Fofs);
//        7: GetRec(fDatF[7]^, fRecno[7], TDINFO);
//      End; {case}
//    End;
//    fFound := OK;
//  End
//  Else fFound := False;
//  OK := True; (* RESET OK for SUBSEQUENT File ALIGNMENT *)
//End;

//Procedure TfrmFTInfo.pChooseTourn;
//Var
//  sTType: String;
//Begin
//  { If the number of Tournament Records is GREATER than the number of Tournament Information
//    Records, allow the user to Add or Copy a new Tournament Information Record }
//  If iTournRecs > iFTInfoRecs Then Begin
//    Filno := Tourn_no;
//    Find_Record(' ', '', '', MC, 0, True);
//    If Not bEscKeyUsed Then Begin
//      { Clear the Edit Fields of any data }
//      pClearFields;
//      { Turn the Player Info Data Area ON }
//      pFTInfo(True);
//      // -----------------------------------------------------------------------------------------
//      { Fill fields with the selected Tournament Record Data }
//      With tourn Do Begin
//        OSFSanctionNo.Text := Sanction_no;
//        OSFTournName.Text := Tourn_Name;
//        OSFTournCity.Text := tourn_City;
//        OSFTournDates.Text := tourn_Dates;
//        OSFDistrictNo.Text := District_no;
//        OSFUnitNo.Text := Unit_no;
//      End;
//      OSFTournType.Text := Copy(OSFSanctionNo.Text, 1, 2);
//      sTType := copy(OSFTournType.Text, 2, 1);
//      { sTType (Tournament Type) comes from the first two caracters  is the If the second character of the Sanction Number is not an Alpha Character, }
//      If sTType < 'A' Then
//        OSFTournType.Text := copy(OSFTournType.Text, 1, 1) + ' ' + copy(OSFTournType.Text, 3,
//          Length(OSFTournType.Text) - 2);
//      LabelTournTypeText.Caption := fshow_tourn_type(OSFTournType.Text);
//      OPFPerDiemRate.Text := FTInfo.PER_DIEM_Rate;
//      // -----------------------------------------------------------------------------------------
//      { Store the original Sanction Number for validation purposes }
//      sSanctionNo := OSFSanctionNo.Text;
//      { Put the cursor in the first field }
//      OSFSanctionNo.SetFocus;
//    End
//    Else pAbortChanges;
//  End
//  Else Begin
//    MessageDlg('There are no unassigned Sanction Numbers in the Tournament Database!' + #13 + #10 +
//      '' + #13 + #10 + '         You cannot Add or Copy a new Tournament Information record!',
//      mtError, [mbOK], 0);
//    pAbortChanges;
//  End;
//End;

Procedure TfrmFTInfo.pButtOn(bTurnOn: Boolean);
Begin
  // ------------------------------------------------------------
  { Buttons }
  //ButAddAction.Enabled := bTurnOn;
  ButCopy.Enabled := bTurnOn;
  ButCopyAction.Enabled := bTurnOn;
  ButDelete.Enabled := bTurnOn;
  ButDeleteAction.Enabled := bTurnOn;
  ButEdit.Enabled := bTurnOn;
  ButEditAction.Enabled := bTurnOn;
  ButFind.Enabled := bTurnOn;
  ButFindAction.Enabled := bTurnOn;
  ButLast.Enabled := bTurnOn;
  ButLastAction.Enabled := bTurnOn;
  ButNext.Enabled := bTurnOn;
  ButNextAction.Enabled := bTurnOn;
  ButPrev.Enabled := bTurnOn;
  ButPrevAction.Enabled := bTurnOn;
  ButTop.Enabled := bTurnOn;
  ButTopAction.Enabled := bTurnOn;
  //ButQuitAction.Enabled := bTurnOn;
  // ------------------------------------------------------------
  { Function Keys }
  //F1Key.Enabled := bTurnOn;
  ButtonF2Key.Enabled := bTurnOn;
  F2Key.Enabled := bTurnOn;
  F3Key.Enabled := bTurnOn;
  ButtonF4Key.Enabled := bTurnOn;
  F4Key.Enabled := bTurnOn;
  F5Key.Enabled := bTurnOn;
  F6Key.Enabled := bTurnOn;
  ButtonF7Key.Enabled := bTurnOn;
  F7Key.Enabled := bTurnOn;
  ButtonF8Key.Enabled := bTurnOn;
  F8Key.Enabled := bTurnOn;
  ButMinus.Enabled := bTurnOn;
  MinusKey.Enabled := bTurnOn;
  ButPlus.Enabled := bTurnOn;
  PlusKey.Enabled := bTurnOn;
  // ------------------------------------------------------------
End;

Procedure TfrmFTInfo.OSFTDNoChange(Sender: TObject);
Begin
  OSFDicName.Text := '';
End;

Procedure TfrmFTInfo.pBlankFTInfoRecord;
Begin
  With FTInfo Do Begin
    ACBL_COLLECTED := '      0.00'; // STRING[10]; {N  2}{Total collected for ACBL - ofs}
    ACBL_DUE := '      0.00'; // STRING[10]; {N  2}{Total due to ACBL - ofs}
    ACBL_OTHER_PAID := '    0.00'; // STRING[8]; {N  2}{Other paid by ACBL - ofs}
    ACBL_OTHER := '    0.00'; // STRING[8]; {N  2}{Other due to ACBL - ofs}
    ACBL_PAID_OUT := '     0.00'; // STRING[9]; {N  2}{Total paid by ACBL -ofs}
    ADDITIONS_TOTAL := '     0.00'; // STRING[9]; {N  2}{Total additions - balance}
    CAN_CHECK := '      0.00'; // STRING[10]; {N  2}{Amount of Can check - ofs}
    CASH_TOTAL := '     0.00'; // STRING[9]; {N  2}{total cash - balance}
    CHECKS_TOTAL := '     0.00'; // STRING[9]; {N  2}{total checks - balance}
    COMP_RENTAL := '  0.00'; // STRING[6]; {N  2}{Total comp rental - ofs}
    DEDUCTIONS_TOTAL := '     0.00'; // STRING[9]; {N  2}{total deductions - balance}
    DIC_NAME := '                         '; // STRING[25]; {C  0}{Dic name - info}
    DIC_SUPP := '  0.00'; // STRING[6]; {N  2}{Dic supplement - info}
    DIRECTOR_FEES := '     0.00'; // STRING[9]; {N  2}{total director fees - invoice}
    DISTRICT_NO := '  '; // STRING[2]; {C  0}{district # - info}
    ENTRY_TOTAL := '      0.00'; // STRING[10]; {N  2}{Total entry fees - worksheet}
    EXCHANGE_ACBL_TOTAL := '     0.00'; // STRING[9]; {N  2}{exchange for ACBL expenses - balance}
    EXCHANGE_RATE := '  0.00'; // STRING[6]; {N  2}{us exchange rate for ACBL exp - info}
    EXCHANGE_TD_TOTAL := '     0.00'; // STRING[9]; {N  2}{exchange for TD expenses - balance}
    EXPENSES_TOTAL := '      0.00'; // STRING[10]; {N  2}{total expenses - invoice, balance}
    FILL_IN := '     0.00'; // STRING[9]; {N  2}{Total fill-in entries - worksheet}
    FULL_TIME_FEES := '     0.00'; // STRING[9]; {N  2}{full time td fees - ofs}
    GROSS := '      0.00'; // STRING[10]; {N  2}{Total entry fees+non-members worksheet}
    HAND_RECORD_TOTAL := '    0.00'; // STRING[8]; {N  2}{total hand record cost - invoice, ofs}
    HOTEL_DAYS_TOTAL := '  0'; // STRING[3]; {N  0}{total hotel days - invoice}
    HOTEL_TOTAL := '    0.00'; // STRING[8]; {N  2}{total hotel cost - invoice}
    MILEAGE_RATE := '0.000'; // STRING[5]; {N  3}{mileage rate - info}
    MONEY_TOTAL := '      0.00'; // STRING[10]; {N  2}{total money - balance}
    NET := '      0.00'; // STRING[10]; {N  2}{Net receipts - worksheet}
    NON_MEMBERS_ACBL := '   0'; // STRING[4]; {N  0}{# of non members - invoice, ofs}
    NON_MEMB_ACBL_RATE := '  0.00'; // STRING[6]; {N  2}{non mem ACBL sanc fee - info}
    NON_MEM_TOTAL_ACBL := '     0.00'; // STRING[9]; {N  2}{total non-mem sanc fee - invoice, ofs}
    NON_MEM := '     0.00'; // STRING[9]; {N  2}{Total non member extra collected worksheet}
    OTHER_TOTAL := '     0.00'; // STRING[9]; {N  2}{Total other money - balance}
    PART_TIME_FEES := '     0.00'; // STRING[9]; {N  2}{part time td fees - ofs}
    PER_DIEM_CIx := '  0.00'; // STRING[6]; {N  2}{per diem city rate - info}
    PER_DIEM_PAID_TO_TDx := '     0.00'; // STRING[9]; {N  2}{per diem paid to td - ofs}
    PER_DIEM_Rate := '  0.00'; // STRING[6]; {N  2}{per diem sponsor rate - info}
    PER_DIEM_TOTAL_DAYS := '   0.0'; // STRING[6]; {N  1}{total per diem days - invoice, ofs}
    PER_DIEM_TOTAL := '     0.00'; // STRING[9]; {N  2}{per diem charged to sponsor - invoice, ofs}
    SALARIED_FEES := '     0.00'; // STRING[9]; {N  2}{salaried td fees - ofs}
    SANCTION_FEE_TOTAL := '     0.00'; // STRING[9]; {N  2}{total sanction fee - invoice, ofs}
    SANCTION_FEE := '  0.00'; // STRING[6]; {N  2}{sanction fee rate - info}
    SANCTION_NO := '          '; // STRING[10]; {C  0}{sanction # - info} {K1}
    SANCTION_TABLES := '     0.0'; // STRING[8]; {N  1}{tables to base sanc. fee - invoice, ofs}
    SANC_FREE_TABLES := '    0.0'; // STRING[7]; {N  1}{sanction free tables - ofs}
    SCRIP_TO_ACBL := '    0.00'; // STRING[8]; {N  2}{Total scrip to ACBL - ofs}
    SPONSOR_CHECK := '      0.00'; // STRING[10]; {N  2}{Check from sponsor - balance}
    SPONSOR_NET := '      0.00'; // STRING[10]; {N  2}{Net due to sponsor - balance}
    SUPPLY_RATE := ' 0.00'; // STRING[5]; {N  2}{supply rate - info}
    SUPPLY_TABLES := '     0.0'; // STRING[8]; {N  1}{tables to base supply fee - invoice, ofs}
    SUPPLY_TOTAL := '     0.00'; // STRING[9]; {N  2}{total supply fee - invoice, ofs}
    SURCHARGE := '  0.00'; // STRING[6]; {N  2}{sectional surcharge - info}
    TD_NO := '   '; // STRING[3]; {C  0}{DIC td # - info}
    TOTAL_TABLES := '     0.0'; // STRING[8]; {N  1}{Total tables - worksheet, ofs}
    TOURN_CITY := '                '; // STRING[16]; {C  0}{tournament city - info}
    TOURN_DATES := '                    '; // STRING[20]; {C  0}{tournament dates - info}
    TOURN_NAME := '                         '; // STRING[25]; {C  0}{tournament name - info}
    TOURN_SITE := '                         '; // STRING[25]; {C  0}{tournament site - info}
    TOURN_TYPE := '  '; // STRING[2]; {C  0}{type of tournament - info}
    TRANS_DIC := '    0.00'; // STRING[8]; {N  2}{DIC trans. paid by ACBL - ofs}
    TRANS_PREPAID := '    0.00'; // STRING[8]; {N  2}{prepaid transportation - ofs}
    TRANS_TOTAL_INV := '    0.00'; // STRING[8]; {N  2}{trans. charged to sponsor - invoice}
    UNIT_NO := '   '; // STRING[3]; {C  0}{Unit # - info}
    USE_EX_RATE := ' '; // STRING[1]; {C  0}{Canadian tournament? - info}
    US_CHECK := '      0.00'; // STRING[10]; {N  2}{Amount of US check - ofs}
    US_DUE_ACBL := '      0.00'; // STRING[10]; {N  2}{money due ACBL - ofs}
    ZCANADIAN_DUE_ACBL := '      0.00'; // STRING[10]; {N  2}{Can $ due ACBL - ofs}
    EXCHANGE_RATE_TD := '  0.00'; // string[6]; {n 2}{exchange rate for td expenses - info}
    COMP_RENTAL_I := '  0.00'; // STRING[6]; {N 2}{total comp rental from vouchers}
    COMP_RENTAL_O := '  0.00'; // STRING[6]; {N 2}{total comp rental from ofs}
    TD_SESSIONSx := '  0'; // STRING[3]; {N 0}
    Memb_fees := '   0.00'; // String[7]; {n 2}{total memb fees collected - ofs}
    Dir_sessionsx := '  0.0'; // string[5]; {n 1}{total td sessions - invoice - old}
    Flags := '  '; // string[2];
    Gst := ' 0.00'; // string[5]; {n 2}{Canadian GST rate}
    Can_Memb_fees := '   0.00'; // string[7]; {n 2}{Canadian membership fees - ofs}
    Is_spon_check := ' '; // String[1]; {c 0}{Y/N sponsor check to acbl}
    Acbl_other_paid_i := '   0.00'; // String[7]; {n 2}{Paid by ACBL from vouchers}
    Extended_fees := '     0.00'; // STRING[9]; {n 2}{PT extended fees}
    Dir_sessions := '  0.00'; // string[6]; {n 2}{total td sessions - invoice}
  End;
End;

Procedure TfrmFTInfo.FullScreenExecute(Sender: TObject);
Begin
  pSetWinState;
End;

Procedure TfrmFTInfo.Action1Execute(Sender: TObject);
Begin
  pSetWinState;
End;

Procedure TfrmFTInfo.ResizeKit1ExitResize(Sender: TObject; XScale,
  YScale: Double);
Begin
  //  frmWorkSheet.bWinStNorm := (WindowState = wsNormal);
  pSetPanelWidth(frmFTInfo, StatusBar1);
End;

Procedure TfrmFTInfo.pSetWinState;
Begin
  If WindowState = wsMaximized Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  frmWorkSheet.bWinStNorm := (WindowState = wsNormal);
  If frmWorkSheet.bWinStNorm Then frmWorkSheet.WindowState := wsNormal
  Else frmWorkSheet.WindowState := wsMaximized;
  pSetPanelWidth(frmFTInfo, StatusBar1);
End;

//Procedure TfrmFTInfo.pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
//Begin
//  If UpperCase(sType) = 'S' Then pOSFieldAvail(bDoedit, (oSender As TOvcSimpleField))
//  Else If UpperCase(sType) = 'P' Then pOPFieldAvail(bDoedit, (oSender As TOvcPictureField))
//  Else MessageDlg('No Procedure created for that type!', mtError, [mbOK], 0);
//  // -------------------------------------------------------------------------------------------------------
//  If bDisplay_Rec Then pFillFields;
//  // -------------------------------------------------------------------------------------------------------
//  If bWasOnEnter Then Begin
//    If UpperCase(sType) = 'S' Then Begin
//      If Not (oSender As TOvcSimpleField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'P') Then Begin
//      If Not (oSender As TOvcPictureField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End;
//  End;
//End;
//
//Procedure TfrmFTInfo.pONFieldAvail(bAvail: Boolean; oOSField: TOvcNumericField);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmFTInfo.pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoCaretToEnd, efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [efoCaretToEnd];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmFTInfo.pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoCaretToEnd, efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [efoCaretToEnd];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;

Procedure TfrmFTInfo.OSFSanctionNoEnter(Sender: TObject);
Begin
  pResetTabStops;
  bDoEdit := True;
  FTINFO.SANCTION_NO := Pad(OSFSanctionNo.Text,10);
  bDoEdit := CustomFinanceEdit(Scrno, 1, FTINFO.SANCTION_NO, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmFTInfo.OSFSanctionNoExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      If (Trim(OSFSanctionNo.Text) = '') Or (fFDuplicateKey(FTInfo_No, 1, iDataMode)) Then Begin
        If (MessageDlg('Sanction number is empty or already exists.' + #13 + #10 +
          '               Correct this error?', mtError, [mbYes, mbNo], 0) = mrYes) Then Begin
          OSFSanctionNo.Text := '';
          OSFSanctionNo.SetFocus;
        End
        Else pAbortChanges;
      End
      Else Begin
        FTINFO.SANCTION_NO := Pad(OSFSanctionNo.Text,10);
        bDoEdit := CustomFinanceEdit(Scrno, 1, FTINFO.SANCTION_NO, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmFTInfo.OSFTournNameEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.TOURN_NAME := Pad(OSFTournName.Text,25);
  bDoEdit := CustomFinanceEdit(Scrno, 2, FTINFO.TOURN_NAME, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmFTInfo.OSFTournNameExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      If Trim(OSFTournName.Text) = '' Then Begin
        ErrBox('Tournament Name must not be blank!', mc, 0);
        OSFTournName.SetFocus;
      End
      Else Begin
        FTINFO.TOURN_NAME := Pad(OSFTournName.Text,25);
        bDoEdit := CustomFinanceEdit(Scrno, 2, FTINFO.TOURN_NAME, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmFTInfo.OSFTournCityEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.TOURN_CITY := Pad(OSFTournCity.Text,16);
  bDoEdit := CustomFinanceEdit(Scrno, 3, FTINFO.TOURN_CITY, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmFTInfo.OSFTournCityExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FTINFO.TOURN_CITY := Pad(OSFTournCity.Text,16);
      bDoEdit := CustomFinanceEdit(Scrno, 3, FTINFO.TOURN_CITY, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmFTInfo.OSFTournSiteEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.TOURN_SITE := Pad(OSFTournSite.Text,25);
  bDoEdit := CustomFinanceEdit(Scrno, 4, FTINFO.TOURN_SITE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmFTInfo.OSFTournSiteExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FTINFO.TOURN_SITE := Pad(OSFTournSite.Text,25);
      bDoEdit := CustomFinanceEdit(Scrno, 4, FTINFO.TOURN_SITE, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmFTInfo.OSFTournDatesEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.TOURN_DATES := Pad(OSFTournDates.Text,20);
  bDoEdit := CustomFinanceEdit(Scrno, 5, FTINFO.TOURN_DATES, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmFTInfo.OSFTournDatesExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      If Trim(OSFTournDates.Text) = '' Then Begin
        ErrBox('Tournament Dates must not be blank!', mc, 0);
        OSFTournDates.SetFocus;
      End
      Else Begin
        FTINFO.TOURN_DATES := Pad(OSFTournDates.Text,20);
        bDoEdit := CustomFinanceEdit(Scrno, 5, FTINFO.TOURN_DATES, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmFTInfo.OSFTDNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.TD_NO := FixNumField(OSFTDNo.Text,3);
  bDoEdit := CustomFinanceEdit(Scrno, 6, FTINFO.TD_NO, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmFTInfo.OSFTDNoExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      If Not (fFound(TDINFO_no, 3, OSFTDNo.Text)) Then Begin
        ErrBox('This TD number not found.', mc, 0);
        OSFDicName.Text := '';
        OSFTDNo.Text := '';
        OSFTDNo.SetFocus;
      End
      Else Begin
        FTINFO.TD_NO := FixNumField(OSFTDNo.Text,3);
        bDoEdit := CustomFinanceEdit(Scrno, 6, FTINFO.TD_NO, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmFTInfo.OSFDicNameEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.DIC_NAME := Pad(OSFDicName.Text,25);
  bDoEdit := CustomFinanceEdit(Scrno, 7, FTINFO.DIC_NAME, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmFTInfo.OSFDicNameExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FTINFO.DIC_NAME := Pad(OSFDicName.Text,25);
      bDoEdit := CustomFinanceEdit(Scrno, 7, FTINFO.DIC_NAME, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmFTInfo.OSFTournTypeEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.TOURN_TYPE := Pad(OSFTournType.Text,2);
  bDoEdit := CustomFinanceEdit(Scrno, 8, FTINFO.TOURN_TYPE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmFTInfo.OSFTournTypeExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FTINFO.TOURN_TYPE := Pad(OSFTournType.Text,2);
      bDoEdit := CustomFinanceEdit(Scrno, 8, FTINFO.TOURN_TYPE, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmFTInfo.OSFUnitNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.UNIT_NO := FixNumField(OSFUnitNo.Text,3);
  bDoEdit := CustomFinanceEdit(Scrno, 10, FTINFO.UNIT_NO, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmFTInfo.OSFUnitNoExit(Sender: TObject);
Var
  sUnitNo: Openstring;
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      sUnitNo := Copy(OSFUnitNo.Text, 1, 10);
      If Not (fFValidUnit(sUnitNo)) Then Begin
        ErrBox('Invalid Unit Number.', mc, 0);
        OSFTDNo.Text := '';
        OSFTDNo.SetFocus;
      End
      Else Begin
        FTINFO.UNIT_NO := FixNumField(OSFUnitNo.Text,3);
        bDoEdit := CustomFinanceEdit(Scrno, 10, FTINFO.UNIT_NO, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmFTInfo.OSFDistrictNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.DISTRICT_NO := FixNumField(OSFDistrictNo.Text,2);
  bDoEdit := CustomFinanceEdit(Scrno, 11, FTINFO.DISTRICT_NO, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmFTInfo.OSFDistrictNoExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      If Trim(OSFDistrictNo.Text) = '' Then Begin
        ErrBox('District must be filled in.', mc, 0);
        OSFDistrictNo.SetFocus;
      End
      Else Begin
        FTINFO.DISTRICT_NO := FixNumField(OSFDistrictNo.Text,2);
        bDoEdit := CustomFinanceEdit(Scrno, 11, FTINFO.DISTRICT_NO, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmFTInfo.OPFSanctionFeeEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.SANCTION_FEE := LeftPad(Trim(OPFSanctionFee.Text),6);
  bDoEdit := CustomFinanceEdit(Scrno, 12, FTINFO.SANCTION_FEE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmFTInfo.OPFSanctionFeeExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FTINFO.SANCTION_FEE := LeftPad(Trim(OPFSanctionFee.Text),6);
      bDoEdit := CustomFinanceEdit(Scrno, 12, FTINFO.SANCTION_FEE, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmFTInfo.OPFSurchargeEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.SURCHARGE := LeftPad(Trim(OPFSurcharge.Text),6);
  bDoEdit := CustomFinanceEdit(Scrno, 13, FTINFO.SURCHARGE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmFTInfo.OPFSurchargeExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FTINFO.SURCHARGE := LeftPad(Trim(OPFSurcharge.Text),6);
      bDoEdit := CustomFinanceEdit(Scrno, 13, FTINFO.SURCHARGE, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmFTInfo.OPFPerDiemRateEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.PER_DIEM_Rate := LeftPad(Trim(OPFPerDiemRate.Text),6);
  bDoEdit := CustomFinanceEdit(Scrno, 15, FTINFO.PER_DIEM_Rate, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmFTInfo.OPFPerDiemRateExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FTINFO.PER_DIEM_Rate := LeftPad(Trim(OPFPerDiemRate.Text),6);
      bDoEdit := CustomFinanceEdit(Scrno, 15, FTINFO.PER_DIEM_Rate, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmFTInfo.OPFMileageRateEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.MILEAGE_RATE := LeftPad(Trim(OPFMileageRate.Text),5);
  bDoEdit := CustomFinanceEdit(Scrno, 16, FTINFO.MILEAGE_RATE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmFTInfo.OPFMileageRateExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FTINFO.MILEAGE_RATE := LeftPad(Trim(OPFMileageRate.Text),5);
      bDoEdit := CustomFinanceEdit(Scrno, 16, FTINFO.MILEAGE_RATE, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmFTInfo.OPFSupplyRateEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.SUPPLY_RATE := LeftPad(Trim(OPFSupplyRate.Text),5);
  bDoEdit := CustomFinanceEdit(Scrno, 18, FTINFO.SUPPLY_RATE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmFTInfo.OPFSupplyRateExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FTINFO.SUPPLY_RATE := LeftPad(Trim(OPFSupplyRate.Text),5);
      bDoEdit := CustomFinanceEdit(Scrno, 18, FTINFO.SUPPLY_RATE, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmFTInfo.OSFIsSponCheckEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.is_spon_check := Pad(OSFIsSponCheck.Text,1);
  bDoEdit := CustomFinanceEdit(Scrno, 20, FTINFO.is_spon_CHECK, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmFTInfo.OSFIsSponCheckExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      If Not ((OSFIsSponCheck.Text = 'Y') Or (OSFIsSponCheck.Text = 'N')) Then Begin
        ErrBox('Sponsor will issue a check to ACBL, must be Y(es) or N(o).', mc, 0);
        OSFIsSponCheck.Text := 'N';
        OSFIsSponCheck.SetFocus;
      End
      Else Begin
        FTINFO.is_SPON_CHECK := Pad(OSFIsSponCheck.Text,1);
        bDoEdit := CustomFinanceEdit(Scrno, 20, FTINFO.is_SPON_CHECK, False, bDisplay_Rec, sDataMode,
          Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmFTInfo.OSFUseExRateEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.USE_EX_RATE := Pad(OSFUseExRate.Text,1);
  bDoEdit := CustomFinanceEdit(Scrno, 21, FTINFO.USE_EX_RATE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmFTInfo.OSFUseExRateExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      If Not ((OSFUseExRate.Text = 'Y') Or (OSFUseExRate.Text = 'N')) Then Begin
        ErrBox('Apply exchange rate, must be Y(es) or N(o).', mc, 0);
        OSFUseExRate.Text := 'N';
        OSFUseExRate.SetFocus;
      End
      Else Begin
        FTINFO.USE_EX_RATE := Pad(OSFUseExRate.Text,1);
        bDoEdit := CustomFinanceEdit(Scrno, 21, FTINFO.USE_EX_RATE, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmFTInfo.OPFExchangeRateEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.EXCHANGE_RATE := LeftPad(Trim(OPFExchangeRate.Text),6);
  bDoEdit := CustomFinanceEdit(Scrno, 22, FTINFO.EXCHANGE_RATE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmFTInfo.OPFExchangeRateExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FTINFO.EXCHANGE_RATE := LeftPad(Trim(OPFExchangeRate.Text),6);
      bDoEdit := CustomFinanceEdit(Scrno, 22, FTINFO.EXCHANGE_RATE, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmFTInfo.OPFExchangeRateTDEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.EXCHANGE_RATE_TD := LeftPad(Trim(OPFExchangeRateTD.Text),6);
  bDoEdit := CustomFinanceEdit(Scrno, 23, FTINFO.EXCHANGE_RATE_TD, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmFTInfo.OPFExchangeRateTDExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FTINFO.EXCHANGE_RATE_TD := LeftPad(Trim(OPFExchangeRateTD.Text),6);
      bDoEdit := CustomFinanceEdit(Scrno, 23, FTINFO.EXCHANGE_RATE_TD, False, bDisplay_Rec, sDataMode,
        Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmFTInfo.OPFGSTRateEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FTINFO.Gst := LeftPad(Trim(OPFGSTRate.Text),5);
  bDoEdit := CustomFinanceEdit(Scrno, 24, FTINFO.Gst, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmFTInfo.OPFGSTRateExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FTINFO.Gst := LeftPad(Trim(OPFGSTRate.Text),5);
      bDoEdit := CustomFinanceEdit(Scrno, 24, FTINFO.Gst, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmFTInfo.Action2Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmFTInfo.Action3Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Function TfrmFTInfo.FormHelp(Command: Word; Data: Integer; Var CallHelp: Boolean): Boolean;
Begin
  if data < 500000 then
    HtmlHelp(GetDesktopWindow,DefChmFile,HH_HELP_CONTEXT,Data);
//  If data < 500000 Then WinHelp(Application.Handle, 'ACBLSCORE.HLP', Help_Context, Data);
End;

Procedure TfrmFTInfo.pResetTabStops;
Begin
  { Reset ALL editable fields tabstop to true so fields will be available for next action: Add/Edit/Copy }
  OSFSanctionNo.TabStop := True;
  OSFTournName.TabStop := True;
  OSFTournCity.TabStop := True;
  OSFTournSite.TabStop := True;
  OSFTournDates.TabStop := True;
  OSFTDNo.TabStop := True;
  OSFDicName.TabStop := True;
  OSFTournType.TabStop := True;
  OSFUnitNo.TabStop := True;
  OSFDistrictNo.TabStop := True;
  OPFSanctionFee.TabStop := True;
  OPFSurcharge.TabStop := True;
  OPFPerDiemRate.TabStop := True;
  OPFMileageRate.TabStop := True;
  OPFSupplyRate.TabStop := True;
  OSFIsSponCheck.TabStop := True;
  OSFUseExRate.TabStop := True;
  OPFExchangeRate.TabStop := True;
  OPFExchangeRateTD.TabStop := True;
  OPFGSTRate.TabStop := True;
End;

End.

