Unit formInvoice;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ovcbase, ovcdlg, ovcclkdg, StdCtrls, Mask, Buttons,
  ToolWin, ExtCtrls, ActnMan, ActnCtrls, Menus, ActnList, ovcef, ovcsf,
  ovcrvcbx, ovcpb, ovcpf, ovcnf, ovceditf, ovcedpop, ovcedcal, ovcbordr,
  ovcbcalc, ovcbcldr, XPStyleActnCtrls, Grids, ImgList, ResizeKit, LMDCustomComponent,
  LMDWndProcComponent, LMDFormShape, LMDShapeHint,
  LMDHint, LMDCustomScrollBox, LMDListBox, LMDCustomControl, LMDCustomPanel,
  LMDCustomBevelPanel, LMDBaseEdit, LMDCustomEdit, LMDCustomMaskEdit, LMDCustomExtCombo,
  LMDCustomListComboBox, LMDListComboBox, ovccmbx, LMDHeaderListComboBox,
  LMDCustomComboBox, LMDComboBox, STSTRS, LMDIniCtrl, LMDFormA,
  LMDStorBase, LMDStorXMLVault, LMDStorBinVault, LMDCustomHint,
  LMDCustomShapeHint, LMDMessageHint, LibData, LMDCalendarEdit,
  LMDCustomButton, LMDButton, fDbase, Dbase, DB33, LibUtil, Clipbrd, CSDef,
  Util1, Util2, YesNoBoxU, PVIO, VListBox, History, LMDCustomParentPanel,
  LMDCustomGroupBox, LMDGroupBox, LibFinance, I_FCommon,hh;

Type
  TfrmInvoice = Class(TForm)
    GBMenu: TGroupBox;
    Shape23: TShape;
    ButNext: TButton;
    ButPrev: TButton;
    ButFind: TButton;
    ButTop: TButton;
    ButLast: TButton;
    ButEdit: TButton;
    ButAdd: TButton;
    ButCopy: TButton;
    ButDelete: TButton;
    ButQuit: TButton;
    ButtonF7Key: TButton;
    ButtonF6Key: TButton;
    ButtonF8Key: TButton;
    ButtonF2Key: TButton;
    StaticTextF3Key: TStaticText;
    StaticTextF4Key: TStaticText;
    StaticTextF5Key: TStaticText;
    StaticTextF2Key: TStaticText;
    ButtonF1Key: TButton;
    StaticTextF1Key: TStaticText;
    ActionManager1: TActionManager;
    ButNextAction: TAction;
    ButPrevAction: TAction;
    ButFindAction: TAction;
    ButTopAction: TAction;
    ButLastAction: TAction;
    ButEditAction: TAction;
    ButAddAction: TAction;
    ButCopyAction: TAction;
    ButDeleteAction: TAction;
    ButQuitAction: TAction;
    F4Key: TAction;
    F3Key: TAction;
    F5Key: TAction;
    F2Key: TAction;
    F7Key: TAction;
    F1Key: TAction;
    F8Key: TAction;
    F6Key: TAction;
    CtrlCKey: TAction;
    ButPlus: TButton;
    StaticText1: TStaticText;
    PlusKey: TAction;
    ButMinus: TButton;
    StaticText2: TStaticText;
    MinusKey: TAction;
    StatusBar1: TStatusBar;
    ResizeKit1: TResizeKit;
    OvcController1: TOvcController;
    LMDHint1: TLMDHint;
    ActionManager2: TActionManager;
    ButPlayerInfoDone: TAction;
    StaticText3: TStaticText;
    GBTournWorkSheet: TGroupBox;
    Shape1: TShape;
    Label3: TLabel;
    Label4: TLabel;
    Label10: TLabel;
    OSFSanctionNo: TOvcSimpleField;
    OSFTDNumber: TOvcSimpleField;
    OSFName: TOvcSimpleField;
    LMDTournWSDone: TLMDButton;
    SpeedButton1: TSpeedButton;
    ListTables: TAction;
    Label1: TLabel;
    OSFTournament: TOvcSimpleField;
    PopupMenu1: TPopupMenu;
    TournamentInformation1: TMenuItem;
    Worksheet1: TMenuItem;
    Invoice1: TMenuItem;
    Voucher1: TMenuItem;
    Ballancesheet1: TMenuItem;
    Officefinancialpart11: TMenuItem;
    Officefinancialpart2: TMenuItem;
    Quit1: TMenuItem;
    Label23: TLabel;
    OSFTDRank: TOvcSimpleField;
    Label2: TLabel;
    OSFEmpStatus: TOvcSimpleField;
    Label5: TLabel;
    OSFDates: TOvcSimpleField;
    Label6: TLabel;
    OSFPlayerNo: TOvcSimpleField;
    OSFSalaried: TOvcSimpleField;
    GBTDSessions: TGroupBox;
    Label12: TLabel;
    OPFDICSessions: TOvcPictureField;
    Label13: TLabel;
    OPFStaffSessions: TOvcPictureField;
    GBTransAmount: TGroupBox;
    OPFTransAmount: TOvcPictureField;
    GBHotel: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    OPFHotelAmount: TOvcPictureField;
    GBPerDiem: TGroupBox;
    Label9: TLabel;
    OPFPerDiemDays: TOvcPictureField;
    GBAdvanceAmount: TGroupBox;
    Label14: TLabel;
    OPFAdvanceAmount: TOvcPictureField;
    GBCPTRAmount: TGroupBox;
    Label11: TLabel;
    OPFCPTRAmount: TOvcPictureField;
    GBOther: TGroupBox;
    Label17: TLabel;
    OPFOtherAmount: TOvcPictureField;
    LMDGroupBox1: TLMDGroupBox;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    LMDGroupBox2: TLMDGroupBox;
    Label15: TLabel;
    OPFDirectorsFees: TOvcPictureField;
    Label19: TLabel;
    OPFTransportation: TOvcPictureField;
    Label24: TLabel;
    OPFHotel: TOvcPictureField;
    Label25: TLabel;
    OPFSectionalSurcharge: TOvcPictureField;
    Label26: TLabel;
    OPFACBLHandRecords: TOvcPictureField;
    OPFPerDiemTotalDays: TOvcPictureField;
    Label27: TLabel;
    OPFPerDiemRate: TOvcPictureField;
    Label28: TLabel;
    OPFPerDiemTotal: TOvcPictureField;
    OPFSanctionTables: TOvcPictureField;
    Label16: TLabel;
    OPFSanctionFee: TOvcPictureField;
    Label18: TLabel;
    OPFSanctionFeeTotal: TOvcPictureField;
    OPFSupplyTables: TOvcPictureField;
    Label20: TLabel;
    OPFSupplyRate: TOvcPictureField;
    Label21: TLabel;
    OPFSupplyTotal: TOvcPictureField;
    OPFNonMembersACBL: TOvcPictureField;
    Label22: TLabel;
    OPFNonMembersACBLRate: TOvcPictureField;
    Label29: TLabel;
    OPFNonMembersACBLTotal: TOvcPictureField;
    Label34: TLabel;
    OPFExpensesTotal: TOvcPictureField;
    Label35: TLabel;
    OPFACBLOther: TOvcPictureField;
    ONFHotelDays: TOvcNumericField;
    FullScreen: TAction;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Procedure ButQuitActionExecute(Sender: TObject);
    Procedure FormActivate(Sender: TObject);
    Procedure ListTablesExecute(Sender: TObject);
    Procedure pFillFields;
    Procedure pNextRecordf;
    Procedure pPrevRecordf;
    Procedure ButNextActionExecute(Sender: TObject);
    Procedure ButPrevActionExecute(Sender: TObject);
    Procedure ButLastActionExecute(Sender: TObject);
    Procedure ButTopActionExecute(Sender: TObject);
    Procedure ButFindActionExecute(Sender: TObject);
    Procedure PInvoiceInfo(bEnable: Boolean);
    Procedure ButEditActionExecute(Sender: TObject);
    Procedure ButPlayerInfoDoneExecute(Sender: TObject);
    Procedure ButAddActionExecute(Sender: TObject);
    Procedure ButCopyActionExecute(Sender: TObject);
    Procedure ButDeleteActionExecute(Sender: TObject);
    Procedure pUpdateStatusBar;
    Procedure OSFTDNumberExit(Sender: TObject);
    //    Function fFound(Const Fno, Kno: Integer; ChkKey: KeyStr): Boolean;
    Procedure pDateExit(Sender: TObject);
    Procedure MinusKeyExecute(Sender: TObject);
    Procedure F1KeyExecute(Sender: TObject);
    Procedure pGetInvoiceData;
    Procedure pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure PlusKeyExecute(Sender: TObject);
    Procedure F2KeyExecute(Sender: TObject);
    Procedure pPuMenuItemSelected(Sender: TObject);
    Procedure pAbortChanges;
    Procedure F8KeyExecute(Sender: TObject);
    Procedure F7KeyExecute(Sender: TObject);
    Procedure F6KeyExecute(Sender: TObject);
    Procedure pWhichScreen(oSender: TObject);
    Procedure OSFDatesExit(Sender: TObject);
    Procedure FormClose(Sender: TObject; Var Action: TCloseAction); // Open frmOffFinP2
    Procedure pClearFields;
    Procedure pCheckEditFields;
    Procedure pCopyRecData;
    Procedure pSaveChanges;
    Procedure OSFTDRankExit(Sender: TObject);
    Function fcheck_tdnumber(Const TDNo, SanctionNo: String): boolean;
    Function fKeyFound(Const Keytofind: String; Const Fno, Kno: byte; Const CheckRecno: boolean;
      Const iDMode: Integer): boolean;
    Procedure pFillTDInfoFields;
    Procedure OSFEmpStatusExit(Sender: TObject);
    Function fValidEmployeeStatus(Const Field: String): boolean;
    Procedure pButtOn(bTurnOn: Boolean);
    Procedure pBlankInvoiceRecord;
    Procedure FullScreenExecute(Sender: TObject);
    Procedure Action1Execute(Sender: TObject);
    Procedure pSetWinState;
    Procedure ResizeKit1ExitResize(Sender: TObject; XScale, YScale: Double);
    //    Procedure pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
    //    Procedure pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
    //    Procedure pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
    Procedure OSFTDNumberEnter(Sender: TObject);
    Procedure OSFTDRankEnter(Sender: TObject);
    Procedure OSFEmpStatusEnter(Sender: TObject);
    Procedure OSFDatesEnter(Sender: TObject);
    Procedure OSFNameEnter(Sender: TObject);
    Procedure OSFNameExit(Sender: TObject);
    Procedure OSFPlayerNoEnter(Sender: TObject);
    Procedure OSFPlayerNoExit(Sender: TObject);
    Procedure Action2Execute(Sender: TObject);
    Procedure Action3Execute(Sender: TObject);
    Function FormHelp(Command: Word; Data: Integer;
      Var CallHelp: Boolean): Boolean;
    Procedure pResetTabStops;
  private
    { Private declarations }
  public
    { Public declarations }
    iMinusKey, iPlusKey: Integer;
    bKeyIsUnique: Boolean;
  End;

Var
  frmInvoice: TfrmInvoice;
  bEscKeyUsed: Boolean;
  { Integer variable used dynamically to store the tables record count }
  iInvoiceRecs: Integer;
  { Integer variable used to store the records for this sanction number }
  iInvoicesCount: Integer;
  bFound, bTDUsed: Boolean;
  ksThisTempKey: keystr;

Const
  iLenArray: Integer = MaxFilno;

Implementation

Uses formTblsCurrOpen, formFTInfo, tfprint, formWorkSheet;

{$I STSTRS.dcl}

{$R *.dfm}

Procedure TfrmInvoice.ButQuitActionExecute(Sender: TObject);
Begin
  { The user wants to Quit, change the Public Variable:  bFTQuitUsed to True }
  frmWorkSheet.bFTQuitUsed := True;
  Close;
End;

Procedure TfrmInvoice.FormActivate(Sender: TObject);
Begin
  frmWorkSheet.bWinStNorm := (frmWorkSheet.WindowState = wsNormal);
  If frmWorkSheet.bWinStNorm Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  frmWorkSheet.bWinStNorm := (WindowState = wsNormal);
  pSetPanelWidth(frmInvoice, StatusBar1);
  { TopFilNo: What is the topmost database we are using in this application. }
  TopFilNo := Ftinfo_no;
  fFilno := Finvoice_no;
  bEscKeyUsed := False;
  // -------------------------------------------------------------------------
  Application.HintHidePause := 200000;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  { Flush_DF & Flush_IF: Must be set to True for so that everytime a change is
    made to a "Record", dbase re-writes the table header and in Playern.dat,
    updates dbLastImpDate entry. Set them to False when you Delete a record,
    then immediately set them back to True }
  { This must be done as a standard whenever a table is opened for modifications }
  Flush_DF := True;
  Flush_IF := True;
  { Store the FTInfo Key variable to This Temp Key }
  ksThisTempKey := ksFTInfoKey;
  { Load the appropriate data }
  pGetInvoiceData;
  { This variable Scrno, is used in some of the older code procedures to determine which
    calculations need to be performed based on the screen we are in.
    ----------------------------------------------------------
    Screen 1 = Tournament Information    - FTInfo.dat
    Screen 2 = Tournament Work Sheet     - FWkSheet.dat
    Screen 3 = Tournament Invoice        - FInvoice.dat
    Screen 4 = Voucher Items             - FInvItem.dat
    Screen 5 = Tournament Balance Sheet  - FBalance.dat
    Screen 6 = Office financial part 1   - FOFs.dat
    Screen 7 = Office financial part 2   - FOFs.dat  }
  Scrno := 3;
  //==========================================================
  If CSDef.CFG.Tournament Then StatusBar1.Panels[0].Text := 'Tournament'
  Else StatusBar1.Panels[0].Text := 'Club';
  // ----------------------------------------------------------------------------------------------------------
  { Show the path to the table in use and the table name in StatusBar1. }
  StatusBar1.Panels[1].Text := UpperCase(Prepend + 'FINVOICE.DAT');
  { Store the number of records to iInvoiceRecs }
  iInvoiceRecs := db33.UsedRecs(fDatF[FInvoice_no]^);
  { Add the record count to StatusBar1's display }
  StatusBar1.Panels[1].Text := StatusBar1.Panels[1].Text + ' - Contains ' +
    Trim(IntToStr(iInvoiceRecs)) + ' Records.';
End;

Procedure TfrmInvoice.ListTablesExecute(Sender: TObject);
Begin
  frmTblsCurrOpen := TfrmTblsCurrOpen.Create(Self);
  frmTblsCurrOpen.ShowModal;
  frmTblsCurrOpen.Free;
End;

Procedure TfrmInvoice.pFillFields;
Begin
  With FTINFO Do Begin
    OSFSanctionNo.Text := SANCTION_NO;
    OSFTournament.Text := TOURN_NAME;
    OSFTDNumber.Text := FINVOICE.TD_NO;
    OSFTDRank.Text := FINVOICE.TD_RANK;
    OSFEmpStatus.Text := FINVOICE.EMPL_STAT;
  End;
  With FINVOICE Do Begin
    OSFSalaried.Text := SALARIED;
    OSFDates.Text := Trim(DATES);
    OSFName.Text := Trim(NAME);
    OSFPlayerNo.Text := PLAYER_NO;
    OPFDicSessions.Text := Dic_Sessions;
    OPFStaffSessions.Text := Staff_Sessions;
    OPFTransAmount.Text := TRANS_AMT;
    ONFHotelDays.Text := HOTEL_DAYS;
    OPFHotelAmount.Text := HOTEL_AMT;
    OPFPerDiemDays.Text := PER_DIEM_DAYS;
    OPFAdvanceAmount.Text := Advance_amt;
    OPFCPTRAmount.Text := Cptr_amt;
    OPFOtherAmount.Text := Other_amt;
  End;
  // -------------------------------------------------
  With FTINFO Do Begin
    OPFDirectorsFees.Text := DIRECTOR_FEES;
    OPFTransportation.Text := TRANS_TOTAL_INV;
    OPFHotel.Text := HOTEL_TOTAL;
    OPFSectionalSurcharge.Text := SURCHARGE;
    OPFACBLHandRecords.Text := HAND_RECORD_TOTAL;
    OPFPerDiemTotalDays.Text := PER_DIEM_TOTAL_DAYS;
    OPFPerDiemRate.Text := PER_DIEM_Rate;
    OPFPerDiemTotal.Text := PER_DIEM_TOTAL;
    OPFSanctionTables.Text := SANCTION_TABLES;
    OPFSanctionFee.Text := SANCTION_FEE;
    OPFSanctionFeeTotal.Text := SANCTION_FEE_TOTAL;
    OPFSupplyTables.Text := SUPPLY_TABLES;
    OPFSupplyRate.Text := SUPPLY_RATE;
    OPFSupplyTotal.Text := SUPPLY_TOTAL;
    OPFNonMembersACBL.Text := NON_MEMBERS_ACBL;
    OPFNonMembersACBLRate.Text := NON_MEMB_ACBL_RATE;
    OPFNonMembersACBLTotal.Text := NON_MEM_TOTAL_ACBL;
    OPFACBLOther.Text := ACBL_OTHER;
    OPFExpensesTotal.Text := EXPENSES_TOTAL;
    If UpperCase(Trim(EXCHANGE_RATE)) = 'Y' Then OfsItemtypes := (MaxOfsItemtypes - 2)
    Else OfsItemtypes := MaxOfsItemtypes;
  End;
End;

Procedure TfrmInvoice.pNextRecordf;
Begin
  { Move the record pointer in the table to the next available record and load the data in the screen fields }
  fNext_Record;
  liRecnoINV := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmInvoice.ButNextActionExecute(Sender: TObject);
Begin
  pNextRecordf;
End;

Procedure TfrmInvoice.pPrevRecordf;
Begin
  { Move the record pointer in the table to the previous record and load the data in the screen fields }
  fPrev_Record;
  liRecnoINV := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmInvoice.ButPrevActionExecute(Sender: TObject);
Begin
  pPrevRecordf;
End;

Procedure TfrmInvoice.ButLastActionExecute(Sender: TObject);
Begin
  fLast_Record;
  liRecnoINV := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmInvoice.ButTopActionExecute(Sender: TObject);
Begin
  fTop_Record;
  liRecnoINV := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmInvoice.ButFindActionExecute(Sender: TObject);
Begin
  fFind_Record(ksFTInfoKey, fGetKey(Filno, 1), '', MC, 0, True);
  liRecnoINV := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmInvoice.PInvoiceInfo(bEnable: Boolean);
Begin
  FreeHintWin;
  { Set the state of ActionManager1 for the GBMenu }
  If bEnable Then ActionManager1.State := asSuspended Else
    ActionManager1.State := asNormal;
  { Set the state of ActionManager2 for the form GBMenu }
  If bEnable Then ActionManager2.State := asNormal Else
    ActionManager2.State := asSuspended;
  // ----------------------------------------------------------------------
  If bEnable Then GBMenu.Hide Else GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := Not bEnable;
  // ----------------------------------------------------------------------
  GBTournWorkSheet.Enabled := bEnable;
  // ----------------------------------------------------------------------
  LMDTournWSDone.Enabled := bEnable;
  LMDTournWSDone.Visible := bEnable;
  // ----------------------------------------------------------------------
End;

Procedure TfrmInvoice.ButEditActionExecute(Sender: TObject);
Begin
  { In module Dbase call the procedure fPreEditSaveKeys to save the KEYS in all tables we are
    editing so the application will know if a KEY Field has been changed and Indexes need to be
    regenerated }
  fPreEditSaveKeys;
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 2;
  sDataMode := 'E';
  { Turn the Info Data Area ON }
  PInvoiceInfo(True);
  { Put the cursor in the first field }
  OSFTDNumber.SetFocus;
End;

Procedure TfrmInvoice.ButPlayerInfoDoneExecute(Sender: TObject);
Begin
  pSaveChanges;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  { Turn the Info Data Area OFF }
  PInvoiceInfo(False);
  pUpdateStatusBar;
End;

Procedure TfrmInvoice.ButAddActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 1;
  sDataMode := 'A';
  //  pBlankInvoiceRecord;
  //  { Clear the Edit Fields of any data }
  //  pClearFields;
  fInitRecord(fFilno);
  pFillFields;
  { Turn the Info Data Area ON }
  PInvoiceInfo(True);
  { Put the cursor in the first field }
  OSFTDNumber.SetFocus;
End;

Procedure TfrmInvoice.ButCopyActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 3;
  sDataMode := 'C';
  fAdd_Init;
  pFillFields;
  { Turn the Info Data Area ON }
  PInvoiceInfo(True);
  { Put the cursor in the first field }
  OSFTDNumber.SetFocus;
End;

Procedure TfrmInvoice.ButDeleteActionExecute(Sender: TObject);
Begin
  fDelete_Record(True, self.HelpContext);
  pUpdateStatusBar;
  If iInvoiceRecs > 0 Then Begin
    pNextRecordf; { Move the record pointer in the table to the next available record and load the
    data in the screen fields }
    If ButNext.Enabled Then ButNext.SetFocus Else ButQuit.SetFocus;
  End
  Else Begin
    liRecnoINV := 0;
    pClearFields;
    pButtOn(False);
    ButAdd.SetFocus;
  End;
End;

Procedure TfrmInvoice.pUpdateStatusBar;
Begin
  iInvoiceRecs := db33.UsedRecs(fDatF[FInvoice_no]^);
  StatusBar1.Panels[1].Text := UpperCase(dbase.Prepend + 'FINVOICE.DAT') + ' - Contains ' +
    Trim(IntToStr(iInvoiceRecs)) + ' Records.';
  StatusBar1.Panels[2].Text := IntToStr(iInvoicesCount) + ' invoices';
End;

//Function TfrmInvoice.fFound(Const Fno, Kno: Integer; ChkKey: KeyStr): Boolean;
//Begin
//  If (Fno <> fFilno) And (fKeyLen[Fno, Kno] > 0) Then Begin
//    FindKey(fIdxKey[Fno, Kno]^, fRecno[Fno], ChkKey);
//    If OK Then Begin
//      Case Fno Of
//        1: GetRec(fDatF[1]^, fRecno[1], FTINFO);
//        2: GetRec(fDatF[2]^, fRecno[2], FWKSHEET);
//        3: GetRec(fDatF[3]^, fRecno[3], FINVOICE);
//        4: GetRec(fDatF[4]^, fRecno[4], FINVITEM);
//        5: GetRec(fDatF[5]^, fRecno[5], FBALANCE);
//        6: GetRec(fDatF[6]^, fRecno[6], Fofs);
//        7: GetRec(fDatF[7]^, fRecno[7], TDINFO);
//      End; {case}
//    End;
//    fFound := OK;
//  End
//  Else fFound := False;
//  OK := True; (* RESET OK for SUBSEQUENT File ALIGNMENT *)
//End;

Procedure TfrmInvoice.pDateExit(Sender: TObject);
Var
  sDateText: String;
Begin
  { Replace all blanks with zeros in the date }
  (Sender As TOvcDateEdit).Text := fFillDateBlanks((Sender As TOvcDateEdit).Text);
  { Must take out the slashes before passing the date to ChkDate Function }
  sDateText := fStripDateField((Sender As TOvcDateEdit).Text);
  If Not (ChkDate(sDateText)) Then Begin
    MessageDlg('Event date is invalid.', mtError, [mbOK], 0);
    (Sender As TOvcDateEdit).SetFocus;
  End;
End;

Procedure TfrmInvoice.MinusKeyExecute(Sender: TObject);
Begin
  ksFInvoiceKey := '';
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := 1;
  frmWorkSheet.bFTQuitUsed := False;
  Close;
End;

Procedure TfrmInvoice.F1KeyExecute(Sender: TObject);
Begin
  //pRunExtProg(frmInvoice, CFG.LoadPath + 'WUtility', 'MANUAL\CONTENTS');
  keybd_event(VK_F1, MapVirtualKey(VK_F1, 0), 0, 0);
End;

Procedure TfrmInvoice.pGetInvoiceData;
Begin
  If liRecnoINV > 0 Then
    { Fill the form fields with the Data }
    pFillFields
  Else Begin
    fTop_Record;
    If fStatus_OK(fFilno) Then Begin
      liRecnoINV := fDbase.fRecno[fFilno];
      { Fill the form fields with the Data }
      pFillFields;
    End
    Else Begin
      liRecnoINV := 0;
      pButtOn(False);
      ButAdd.SetFocus;
    End;
  End;
End;

Procedure TfrmInvoice.pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Var
  wKey: Word;
Begin
  wKey := Key;
  Case key Of
    { 107 is the Plus Key was pressed on the numeric keypad }
    107: If GBMenu.Enabled Then PlusKeyExecute(Sender);
    { 109 is the Minus Key was pressed on the numeric keypad }
    109: If GBMenu.Enabled Then MinusKeyExecute(Sender);
    { Return key was pressed }
    VK_RETURN: If Not GBMenu.Enabled Then Begin
        key := 0;
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
      End;
    { ESCape key was pressed, Abort the record or record changes... }
    VK_ESCAPE: If Not GBMenu.Enabled Then Begin
        bEscKeyUsed := True;
        pAbortChanges;
        If Not ok Then Begin
          { frmWorkSheet.iFormTag must be set to the forms number that user selected }
          frmWorkSheet.iFormTag := 0;
          frmWorkSheet.bFTQuitUsed := False;
          Close;
        End;
        //Else Close;
      End;
    { F9 key was pressed, save the record or the changes... }
    VK_F9: If Not GBMenu.Enabled Then pSaveChanges;
  Else
    If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
      If (Key = VK_UP) Then Begin
        ButPrev.Click;
        ButPrev.SetFocus;
      End
      Else Begin
        ButNext.Click;
        ButNext.SetFocus;
      End;
      Key := 0;
    End;
  End;
End;

Procedure TfrmInvoice.pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
Var
  wKey: Word;
Begin
  { Do not trap for the Return Key here, it causes the cursor to move two fields at a time }
  wKey := Key;
  Case key Of
    { ESCape key was pressed, Abort the record or record changes... }
    VK_ESCAPE: If Not GBMenu.Enabled Then Begin
        bEscKeyUsed := True;
        pAbortChanges;
      End;
    { F9 key was pressed, save the record or the changes... }
    VK_F9: If Not GBMenu.Enabled Then pSaveChanges;
  Else
    If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
      If (Key = VK_UP) Then Begin
        ButPrev.Click;
        ButPrev.SetFocus;
      End
      Else Begin
        ButNext.Click;
        ButNext.SetFocus;
      End;
      Key := 0;
    End;
  End;
End;

Procedure TfrmInvoice.PlusKeyExecute(Sender: TObject);
Begin
  ksFInvoiceKey := fGetKey(fFilno, 2);
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := 3; { 3 = Voucher, frmVoucher }
  frmWorkSheet.bFTQuitUsed := False;
  Close;
End;

Procedure TfrmInvoice.F2KeyExecute(Sender: TObject);
Var
  iForLoop: Integer;
Begin
  GBMenu.Enabled := False;
  For iForLoop := (PopupMenu1.Items.Count - 1) Downto 0 Do
    PopupMenu1.Items.Items[iForLoop].Enabled := True;
  PopupMenu1.Items.Items[(Scrno - 1)].Enabled := False;
  PopupMenu1.Popup(350, 500);
  GBMenu.Enabled := True;
End;

Procedure TfrmInvoice.pPuMenuItemSelected(Sender: TObject);
Begin
  { Variable iFormTag possible values and their meanings:
    0 = Tournament Information, frmTournInfo
    1 = Tournament Worksheet, frmWorkSheet
    2 = Invoice, frmInvoice
    3 = Voucher, frmVoucher
    4 = Balance Sheet, frmBalanceSheet
    5 = Office Financial Part 1, frmOffFinP1
    6 = Office Financial Part 2, frmOffFinP2 }
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := (Sender As TMenuItem).Tag;
  If ((frmWorkSheet.iFormTag >= 0) And (frmWorkSheet.iFormTag <= 6)) Then Begin
    { Store the Invoice Record FHash Key to the Public Variable:  frmWorkSheet.ksFInvoiceKey
      If the Voucher Screen was selected }
    If frmWorkSheet.iFormTag = 3 Then ksFInvoiceKey := fGetKey(fFilno, 2)
    Else ksFInvoiceKey := '';
    { Store this records key to the Public Variable:  ksFTInfoKey }
    If ksFTInfoKey = '' Then ksFTInfoKey := GetKey(FTINFO_no, 1);
    Close;
  End;
End;

Procedure TfrmInvoice.pWhichScreen(oSender: TObject);
Begin
  frmWorkSheet.sCaption := (oSender As TMenuItem).Caption;
  frmWorkSheet.sPopupItemName := frmWorkSheet.sCaption;
  MessageDlg('You selected item ' + IntToStr(frmWorkSheet.iformTag) +
    ', which is ' + frmWorkSheet.sPopupItemName, mtInformation, [mbOK], 0);
End;

Procedure TfrmInvoice.pAbortChanges;
Begin
  FreeHintWin;
  pResetTabStops;
  { Turn the Info Data Area OFF }
  PInvoiceInfo(False);
  { Fill the form fields with the Data }
  If iDataMode = 1 Then fTop_Record
  Else fGetARec(fFilNo);
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  pFillFields;
End;

Procedure TfrmInvoice.F8KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (1) to Print to Screen }
  PrintReport(1);
End;

Procedure TfrmInvoice.F7KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (2) to Print Reports }
  PrintReport(2);
End;

Procedure TfrmInvoice.F6KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (3) to Print Reports to an ASCII file }
  PrintReport(3);
End;

Procedure TfrmInvoice.FormClose(Sender: TObject; Var Action: TCloseAction);
Begin
  If frmWorkSheet.iFormTag = (Scrno - 1) Then frmWorkSheet.iFormTag := 1;
  If frmWorkSheet.bFTQuitUsed Then Begin
    { Close any open tables }
    fdBase.fCloseFiles;
    dBase.CloseFiles;
    Application.Terminate;
  End;
End;

Procedure TfrmInvoice.pClearFields;
Begin
  OSFSanctionNo.Text := FTInfo.SANCTION_NO;
  OSFTDNumber.Text := '';
  { Data Mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  If iDataMode = 1 Then OSFTDRank.Text := 'LTD'
  Else OSFTDRank.Text := '';
  OSFEmpStatus.Text := '';
  OSFDates.Text := FTInfo.TOURN_DATES; // was '';
  OSFName.Text := '';
  OSFPlayerNo.Text := '';
  OPFDicSessions.Text := '0';
  OPFStaffSessions.Text := '0';
  OPFTransAmount.Text := '0';
  ONFHotelDays.Text := '0';
  OPFHotelAmount.Text := '0';
  OPFPerDiemDays.Text := '0';
  OPFAdvanceAmount.Text := '0';
  OPFCPTRAmount.Text := '0';
  OPFOtherAmount.Text := '0';
End;

Procedure TfrmInvoice.pCopyRecData;
Begin
  { Used when Adding or Copying a Record }
  With FINVOICE Do Begin
    SANCTION_NO := pad(OSFSanctionNo.Text, 10);
    TD_NO := pad(OSFTDNumber.Text, 3);
    TD_RANK := pad(OSFTDRank.Text, 3);
    EMPL_STAT := Pad(OSFEmpStatus.Text,7);
    SALARIED := Pad(OSFSalaried.Text,1);
    DATES := Pad(OSFDates.Text,20);
    NAME := Pad(OSFName.Text,25);
    PLAYER_NO := Pad(OSFPlayerNo.Text,7);
    Dic_Sessions := LeftPad(Trim(OPFDicSessions.Text),5);
    Staff_Sessions := LeftPad(Trim(OPFStaffSessions.Text),5);
    TRANS_AMT := LeftPad(trim(OPFTransAmount.Text),8);
    HOTEL_DAYS := LeftPad(Trim(ONFHotelDays.Text),2);
    HOTEL_AMT := LeftPad(Trim(OPFHotelAmount.Text),8);
    PER_DIEM_DAYS := LeftPad(Trim(OPFPerDiemDays.Text),4);
    Advance_amt := LeftPad(Trim(OPFAdvanceAmount.Text),7);
    Cptr_amt := LeftPad(Trim(OPFCPTRAmount.Text),6);
    Other_amt := LeftPad(Trim(OPFOtherAmount.Text),7);
  End;
End;

Procedure TfrmInvoice.pCheckEditFields;
Begin
  { Used when Editing a Record }
  // ---------------------------------------------------------------------
  With FINVOICE Do Begin
    TD_NO := pad(OSFTDNumber.Text, 3);
    TD_RANK := pad(OSFTDRank.Text, 3);
    EMPL_STAT := Pad(OSFEmpStatus.Text,7);
    SALARIED := Pad(OSFSalaried.Text,1);
    DATES := Pad(OSFDates.Text,20);
    NAME := Pad(OSFName.Text,25);
    PLAYER_NO := Pad(OSFPlayerNo.Text,7);
    Dic_Sessions := LeftPad(Trim(OPFDicSessions.Text),5);
    Staff_Sessions := LeftPad(Trim(OPFStaffSessions.Text),5);
    TRANS_AMT := LeftPad(trim(OPFTransAmount.Text),8);
    HOTEL_DAYS := LeftPad(Trim(ONFHotelDays.Text),2);
    HOTEL_AMT := LeftPad(Trim(OPFHotelAmount.Text),8);
    PER_DIEM_DAYS := LeftPad(Trim(OPFPerDiemDays.Text),4);
    Advance_amt := LeftPad(Trim(OPFAdvanceAmount.Text),7);
    Cptr_amt := LeftPad(Trim(OPFCPTRAmount.Text),6);
    Other_amt := LeftPad(Trim(OPFOtherAmount.Text),7);
  End;
End;

Procedure TfrmInvoice.pSaveChanges;
Begin
  { Data Mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  Case iDataMode Of
    1, 3: Begin // Add or Copy Modes
        pCopyRecData;
        fdbase.fAdd_Record;
        //liRecnoINV := fDbase.fRecno[fFilno];
        pButtOn(True);
        Inc(iInvoicesCount);
        pUpdateStatusBar;
        FixTotals(iDataMode);
      End;
    2: Begin // Edit Mode
        pCheckEditFields;
        bKeyIsUnique := fPostEditFixKeys;
        If Not bKeyIsUnique Then Begin
          If (MessageDlg('This TD Number already exists in this database.', mtError,
            [mbOK], 0) = mrOK) Then Begin
            OSFTDNumber.SetFocus;
            Exit;
          End
          Else pAbortChanges;
        End
        Else Begin
          {If fValidateFields Then}
          fdbase.fPutARec(fFilNo);
          FixTotals(iDataMode);
        End;
      End;
  End;
  pFillFields;
  iDataMode := 0;
  sDataMode := null;
  pResetTabStops;
  { Turn the Player Info Data Area ON }
  pInvoiceInfo(False);
End;

Function TfrmInvoice.fcheck_tdnumber(Const TDNo, SanctionNo: String): boolean;
Begin
  fcheck_tdnumber := fKeyFound(SanctionNo + TDNo, Finvoice_no, 1, true, iDataMode);
End;

Function TfrmInvoice.fKeyFound(Const Keytofind: String; Const Fno, Kno: byte; Const CheckRecno:
  boolean; Const iDMode: Integer): boolean;
Var
  tempkey: keystr;
  tRecno: longint;
  found: boolean;
Begin
  { iDMode Integer Variable for the current data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  tempkey := KeytoFind;
  findkey(fIdxKey[Fno, Kno]^, tRecno, tempkey);
  fKeyFound := ok;
  If CheckRecno Then fKeyFound := ok And ((tRecno <> fRecno[Fno])
      Or ((iDMode = 1) Or (iDMode = 3)));
  ok := true;
End;

Procedure TfrmInvoice.pFillTDInfoFields;
Begin
  With tdinfo Do Begin
    OSFTDRank.Text := TD_RANK;
    OSFEmpStatus.Text := EMPL_STAT;
    //OSFSalaried.Text := SALARIED;
    OSFName.Text := Trim(FIRST_NAME) + ' ' + Trim(LAST_NAME);
    OSFPlayerNo.Text := PLAYER_NO;
    OSFDates.Text := FTINFO.TOURN_DATES;
  End;
End;

Function TfrmInvoice.fValidEmployeeStatus(Const Field: String): boolean;
Var
  EmpStat: String[7];
Begin
  EmpStat := Trim(Field);
  Result := false;
  Case Length(EmpStat) Of
    1: Result := Pos(EmpStat[1], 'PFS') > 0;
    2: Result := (Pos(EmpStat, 'EPPMFM') In [1, 3, 5]);
  End;
End;

Procedure TfrmInvoice.pButtOn(bTurnOn: Boolean);
Begin
  // ------------------------------------------------------------
  { Buttons }
  ButCopy.Enabled := bTurnOn;
  ButCopyAction.Enabled := bTurnOn;
  ButDelete.Enabled := bTurnOn;
  ButDeleteAction.Enabled := bTurnOn;
  ButEdit.Enabled := bTurnOn;
  ButEditAction.Enabled := bTurnOn;
  ButFind.Enabled := bTurnOn;
  ButFindAction.Enabled := bTurnOn;
  ButLast.Enabled := bTurnOn;
  ButLastAction.Enabled := bTurnOn;
  ButNext.Enabled := bTurnOn;
  ButNextAction.Enabled := bTurnOn;
  ButPrev.Enabled := bTurnOn;
  ButPrevAction.Enabled := bTurnOn;
  ButTop.Enabled := bTurnOn;
  ButTopAction.Enabled := bTurnOn;
  // ------------------------------------------------------------
  { Function Keys }
  F3Key.Enabled := bTurnOn;
  F4Key.Enabled := bTurnOn;
  F5Key.Enabled := bTurnOn;
  ButtonF6Key.Enabled := bTurnOn;
  F6Key.Enabled := bTurnOn;
  ButtonF7Key.Enabled := bTurnOn;
  F7Key.Enabled := bTurnOn;
  ButtonF8Key.Enabled := bTurnOn;
  F8Key.Enabled := bTurnOn;
  // ------------------------------------------------------------
End;

Procedure TfrmInvoice.pBlankInvoiceRecord;
Begin
  With FInvoice Do Begin
    DATES := '                    '; // STRING[20]; {C  0}
    DIC_TD_SESSIONSx := ' 0'; // STRING[2]; {N  0}
    EMPL_STAT := '       '; // STRING[7]; {C  0}
    EXCHANGE_TOTAL := '    0.00'; // STRING[8]; {N  2}
    HOTEL_AMT := '    0.00'; // STRING[8]; {N  2}
    HOTEL_DAYS := ' 0'; // STRING[2]; {N  0}
    NAME := '                         '; // STRING[25]; {C  0}
    PER_DIEM_DAYS := ' 0.0'; // STRING[4]; {N  1}
    PLAYER_NO := '       '; // STRING[7]; {C  0}
    SALARIED := ' '; // STRING[1]; {C  0}
    SANCTION_NO := FTInfo.SANCTION_NO; // STRING[10]; {C  0}
    STAFF_TD_SESSIONSx := ' 0'; // STRING[2]; {N  0}
    STAFF_TD_TOTAL := '    0.00'; // STRING[8]; {N  2}
    TD_NO := '   '; // STRING[3]; {C  0}
    TD_RANK := '   '; // STRING[3]; {C  0}
    TRANS_AMTD := '    0.00'; // STRING[8]; {N  2}
    TRANS_AMT := '    0.00'; // STRING[8]; {N  2}
    Dic_Sessionsx := ' 0.0'; // string[4]; {n 1}
    Staff_Sessionsx := ' 0.0'; // string[4]; {n 1}
    Advance_amt := '   0.00'; // string[7]; {n 2}
    Cptr_amt := '  0.00'; // string[6]; {n 2}
    Other_amt := '   0.00'; // string[7]; {n 2}
    Other_ACBL_amt := '   0.00'; // string[7]; {n 2}
    Dic_Sessions := ' 0.00'; // string[5]; {n 2}
    Staff_Sessions := ' 0.00'; // string[5]; {n 2}
  End;
End;

Procedure TfrmInvoice.FullScreenExecute(Sender: TObject);
Begin
  pSetWinState;
End;

Procedure TfrmInvoice.Action1Execute(Sender: TObject);
Begin
  pSetWinState;
End;

Procedure TfrmInvoice.pSetWinState;
Begin
  If WindowState = wsMaximized Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  frmWorkSheet.bWinStNorm := (WindowState = wsNormal);
  If frmWorkSheet.bWinStNorm Then frmWorkSheet.WindowState := wsNormal
  Else frmWorkSheet.WindowState := wsMaximized;
  pSetPanelWidth(frmInvoice, StatusBar1);
End;

Procedure TfrmInvoice.ResizeKit1ExitResize(Sender: TObject; XScale, YScale: Double);
Begin
  pSetPanelWidth(frmInvoice, StatusBar1);
End;

//Procedure TfrmInvoice.pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
//Begin
//  If UpperCase(sType) = 'S' Then pOSFieldAvail(bDoedit, (oSender As TOvcSimpleField))
//  Else If UpperCase(sType) = 'P' Then pOPFieldAvail(bDoedit, (oSender As TOvcPictureField))
//  Else MessageDlg('No Procedure created for that type!', mtError, [mbOK], 0);
//  // -------------------------------------------------------------------------------------------------------
//  pFillFields;
//  // -------------------------------------------------------------------------------------------------------
//  If bWasOnEnter Then Begin
//    If UpperCase(sType) = 'S' Then Begin
//      If Not (oSender As TOvcSimpleField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'P') Then Begin
//      If Not (oSender As TOvcPictureField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End;
//  End;
//End;
//
//Procedure TfrmInvoice.pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoCaretToEnd, efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [efoCaretToEnd];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmInvoice.pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoCaretToEnd, efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [efoCaretToEnd];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;

Procedure TfrmInvoice.OSFTDNumberEnter(Sender: TObject);
Begin
  pResetTabStops;
  bDoEdit := True;
  FINVOICE.TD_NO := Pad(OSFTDNumber.Text,3);
  bDoEdit := CustomFinanceEdit(Scrno, 2, FINVOICE.TD_NO, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmInvoice.OSFTDNumberExit(Sender: TObject);
Begin
  //  { If in Add or Copy Mode or in Edit Mode and the Event Code Has Been Changed }
  //  If (iDataMode In [1, 3]) Or ((iDataMode = 2) And (OSFTDNumber.Text <> FWKSHEET.EVENT_CODE)) Then Begin
  //    { If the ESCape Key was not the last key used, then run the code of field validation }
  //    If Not bEscKeyUsed Then Begin
  //      OK := True;
  //      { Need to check to see if we are in edit mode and if the OSFTDNumber is different than the
  //        FInvoice.TD_NO.  Data Mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  //      bFound := fFound(TDINFO_No, 3, OSFTDNumber.Text);
  //      bTDUsed := fcheck_tdnumber(OSFTDNumber.Text, OSFSanctionNo.Text);
  //      Case iDataMode Of
  //        1, 3: Begin // Add or Copy Modes
  //            If ((Not bFound) Or (bTDUsed)) Then Begin
  //              MessageDlg('This TD # not found or already Added.', mtError, [mbOK], 0);
  //              OSFTDNumber.Text := '';
  //              OSFTDNumber.SetFocus;
  //            End
  //            Else Begin
  //              If bFound Then Begin
  //                pFillTDInfoFields;
  //              End;
  //            End;
  //          End;
  //        2: Begin // Edit Mode
  //            If (OSFTDNumber.Text <> FInvoice.TD_NO) Then Begin
  //              If ((Not bFound) Or (bTDUsed)) Then Begin
  //                MessageDlg('This TD # not found or already Added.', mtError, [mbOK], 0);
  //                OSFTDNumber.Text := '';
  //                OSFTDNumber.SetFocus;
  //              End
  //              Else Begin
  //                If bFound Then Begin
  //                  pFillTDInfoFields;
  //                End;
  //              End;
  //            End;
  //          End;
  //      End;
  //    End;
  //  End;

  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      { If in Add or Copy Mode or in Edit Mode and the Event Code Has Been Changed }
      If (iDataMode In [1, 3]) Or ((iDataMode = 2) And (OSFTDNumber.Text <> FWKSHEET.EVENT_CODE)) Then Begin
        OK := True;
        { Need to check to see if we are in edit mode and if the OSFTDNumber is different than the
          FInvoice.TD_NO.  Data Mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
        bFound := fFound(TDINFO_No, 3, OSFTDNumber.Text);
        bTDUsed := fcheck_tdnumber(OSFTDNumber.Text, OSFSanctionNo.Text);
        Case iDataMode Of
          1, 3: Begin // Add or Copy Modes
              If ((Not bFound) Or (bTDUsed)) Then Begin
                MessageDlg('This TD # not found or already Added.', mtError, [mbOK], 0);
                OSFTDNumber.Text := '';
                OSFTDNumber.SetFocus;
              End
              Else Begin
                FINVOICE.TD_NO := Pad(OSFTDNumber.Text,3);
                bDoEdit := CustomFinanceEdit(Scrno, 2, FINVOICE.TD_NO, False, bDisplay_Rec, sDataMode,
                  Sender);
                If bDisplay_Rec Then pFillFields;
                plOnFieldLastStep(False, Sender, 'S');
              End;
            End;
          2: Begin // Edit Mode
              If (OSFTDNumber.Text <> FInvoice.TD_NO) Then Begin
                If ((Not bFound) Or (bTDUsed)) Then Begin
                  ErrBox('This TD # not found or already Added.', mc, 0);
                  OSFTDNumber.Text := '';
                  OSFTDNumber.SetFocus;
                End
                Else Begin
                  FINVOICE.TD_NO := Pad(OSFTDNumber.Text,3);
                  bDoEdit := CustomFinanceEdit(Scrno, 2, FINVOICE.TD_NO, False, bDisplay_Rec, sDataMode,
                    Sender);
                  If bDisplay_Rec Then pFillFields;
                  plOnFieldLastStep(False, Sender, 'S');
                End;
              End;
            End;
        End;
      End;
    End;
  End;
End;

Procedure TfrmInvoice.OSFTDRankEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FINVOICE.TD_RANK := Pad(OSFTDRank.Text,3);
  bDoEdit := CustomFinanceEdit(Scrno, 3, FINVOICE.TD_RANK, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmInvoice.OSFTDRankExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      If Not ((OSFTDRank.Text = 'LTD') Or (OSFTDRank.Text = 'ATD') Or (OSFTDRank.Text = 'TD ') Or
        (OSFTDRank.Text = 'AN ') Or (OSFTDRank.Text = 'N  ')) Then Begin
        ErrBox('Invalid TD rank', mc, 0);
        OSFTDRank.Text := '';
        OSFTDRank.SetFocus;
      End
      Else Begin
        FINVOICE.TD_RANK := Pad(OSFTDRank.Text,3);
        bDoEdit := CustomFinanceEdit(Scrno, 3, FINVOICE.TD_RANK, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmInvoice.OSFEmpStatusEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FINVOICE.EMPL_STAT := Pad(OSFEmpStatus.Text,7);
  bDoEdit := CustomFinanceEdit(Scrno, 4, FINVOICE.EMPL_STAT, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmInvoice.OSFEmpStatusExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      If Not (fValidEmployeeStatus(OSFEmpStatus.Text)) Then Begin
        ErrBox('Invalid TD Employee status', mc, 0);
        OSFEmpStatus.Text := '';
        OSFEmpStatus.SetFocus;
      End
      Else Begin
        FINVOICE.EMPL_STAT := Pad(OSFEmpStatus.Text,7);
        bDoEdit := CustomFinanceEdit(Scrno, 4, FINVOICE.EMPL_STAT, False, bDisplay_Rec, sDataMode, Sender);
        OSFSalaried.Text := FINVOICE.SALARIED;
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmInvoice.OSFDatesEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FINVOICE.DATES := Pad(OSFDates.Text,20);
  bDoEdit := CustomFinanceEdit(Scrno, 5, FINVOICE.DATES, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmInvoice.OSFDatesExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      If Trim(OSFDates.Text) = '' Then Begin
        ErrBox('Date must not be blank!', mc, 0);
        OSFDates.SetFocus;
      End
      Else Begin
        FINVOICE.DATES := Pad(OSFDates.Text,20);
        bDoEdit := CustomFinanceEdit(Scrno, 5, FINVOICE.DATES, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmInvoice.OSFNameEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FINVOICE.NAME := Pad(OSFName.Text,25);
  bDoEdit := CustomFinanceEdit(Scrno, 6, FINVOICE.NAME, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmInvoice.OSFNameExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      FINVOICE.NAME := Pad(OSFName.Text,25);
      bDoEdit := CustomFinanceEdit(Scrno, 6, FINVOICE.NAME, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmInvoice.OSFPlayerNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FINVOICE.PLAYER_NO := Pad(OSFPlayerNo.Text,7);
  bDoEdit := CustomFinanceEdit(Scrno, 7, FINVOICE.PLAYER_NO, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmInvoice.OSFPlayerNoExit(Sender: TObject);
Var
  osPlyerNo: OpenString;
Begin
  If bDoEdit Then Begin
    { If the ESCape Key was not the last key used, then run the code of field validation }
    If Not bEscKeyUsed Then Begin
      osPlyerNo := OSFPlayerNo.Text;
      If Not (Player_check(osPlyerNo, pnACBL)) Then Begin
        ErrBox('Invalid player number', mc, 0);
        OSFPlayerNo.SetFocus;
      End
      Else Begin
        FINVOICE.PLAYER_NO := Pad(OSFPlayerNo.Text,7);
        bDoEdit := CustomFinanceEdit(Scrno, 7, FINVOICE.PLAYER_NO, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmInvoice.Action2Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmInvoice.Action3Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Function TfrmInvoice.FormHelp(Command: Word; Data: Integer; Var CallHelp: Boolean): Boolean;
Begin
  if data < 500000 then
    HtmlHelp(GetDesktopWindow,DefChmFile,HH_HELP_CONTEXT,Data);
//  If data < 500000 Then WinHelp(Application.Handle, 'ACBLSCORE.HLP', Help_Context, Data);
End;

Procedure TfrmInvoice.pResetTabStops;
Begin
  { Reset ALL editable fields tabstop to true so fields will be available for next action: Add/Edit/Copy }
  OSFDates.TabStop := True;
  OSFName.TabStop := True;
  OSFPlayerNo.TabStop := True;
  OPFDicSessions.TabStop := True;
  OPFStaffSessions.TabStop := True;
  OPFTransAmount.TabStop := True;
  ONFHotelDays.TabStop := True;
  OPFHotelAmount.TabStop := True;
  OPFPerDiemDays.TabStop := True;
  OPFAdvanceAmount.TabStop := True;
  OPFCPTRAmount.TabStop := True;
  OPFOtherAmount.TabStop := True;
End;

End.

