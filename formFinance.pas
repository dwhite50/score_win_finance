Unit formFinance;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ovcbase, ovcdlg, ovcclkdg, StdCtrls, Mask, Buttons,
  ToolWin, ExtCtrls, ActnMan, ActnCtrls, Menus, ActnList, ovcef, ovcsf,
  ovcrvcbx, ovcpb, ovcpf, ovcnf, ovceditf, ovcedpop, ovcedcal, ovcbordr,
  ovcbcalc, ovcbcldr, XPStyleActnCtrls, Grids, ImgList, ResizeKit, LMDCustomComponent,
  LMDWndProcComponent, LMDFormShape, LMDShapeHint,
  LMDHint, LMDCustomScrollBox, LMDListBox, LMDCustomControl, LMDCustomPanel,
  LMDCustomBevelPanel, LMDBaseEdit, LMDCustomEdit, LMDCustomMaskEdit, LMDCustomExtCombo,
  LMDCustomListComboBox, LMDListComboBox, ovccmbx, LMDHeaderListComboBox,
  LMDCustomComboBox, LMDComboBox, STSTRS, LMDIniCtrl, LMDFormA,
  LMDStorBase, LMDStorXMLVault, LMDStorBinVault, LMDCustomHint,
  LMDCustomShapeHint, LMDMessageHint, LibData, LMDCalendarEdit,
  LMDCustomButton, LMDButton, fDbase, Dbase, DB33, LibUtil, Clipbrd, CSDef,
  Util1, Util2, YesNoBoxU, PVIO, VListBox, History;

{ Initialize all variables for finances located in the file fVariables.inc}
{$I fVariables.inc}

Type
  TfrmFinance = Class(TForm)
    GBMenu: TGroupBox;
    Shape23: TShape;
    ButNext: TButton;
    ButPrev: TButton;
    ButFind: TButton;
    ButTop: TButton;
    ButLast: TButton;
    ButEdit: TButton;
    ButAdd: TButton;
    ButCopy: TButton;
    ButDelete: TButton;
    ButQuit: TButton;
    ButtonF4Key: TButton;
    ButtonF3Key: TButton;
    ButtonF5Key: TButton;
    ButtonF2Key: TButton;
    StaticTextF3Key: TStaticText;
    StaticTextF4Key: TStaticText;
    StaticTextF5Key: TStaticText;
    StaticTextF2Key: TStaticText;
    ButtonF1Key: TButton;
    StaticTextF1Key: TStaticText;
    ActionManager1: TActionManager;
    ButNextAction: TAction;
    ButPrevAction: TAction;
    ButFindAction: TAction;
    ButTopAction: TAction;
    ButLastAction: TAction;
    ButEditAction: TAction;
    ButAddAction: TAction;
    ButCopyAction: TAction;
    ButDeleteAction: TAction;
    ButQuitAction: TAction;
    F4Key: TAction;
    F3Key: TAction;
    F5Key: TAction;
    F2Key: TAction;
    F7Key: TAction;
    F1Key: TAction;
    F8Key: TAction;
    F6Key: TAction;
    CtrlCKey: TAction;
    Button1: TButton;
    StaticText1: TStaticText;
    PlusKey: TAction;
    Button2: TButton;
    StaticText2: TStaticText;
    MinusKey: TAction;
    StatusBar1: TStatusBar;
    OvcController1: TOvcController;
    LMDHint1: TLMDHint;
    ActionManager2: TActionManager;
    ButPlayerInfoDone: TAction;
    StaticText3: TStaticText;
    GBTournWorkSheet: TGroupBox;
    Shape1: TShape;
    Label3: TLabel;
    Label4: TLabel;
    Label10: TLabel;
    Shape3: TShape;
    OSFSanctionNo: TOvcSimpleField;
    OSFEventCode: TOvcSimpleField;
    OSFEventName: TOvcSimpleField;
    LMDTournWSDone: TLMDButton;
    SpeedButton1: TSpeedButton;
    ListTables: TAction;
    Label1: TLabel;
    OSFTournament: TOvcSimpleField;
    Label2: TLabel;
    ODEEventDate: TOvcDateEdit;
    LabelMonthText: TLabel;
    Label5: TLabel;
    OPFTime: TOvcPictureField;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    OSFSessions: TOvcSimpleField;
    OSFEntries: TOvcSimpleField;
    OPFTables: TOvcPictureField;
    OPFEntryFee: TOvcPictureField;
    OPFEntryTotal: TOvcPictureField;
    OPFNonMember: TOvcPictureField;
    OPFFillIn: TOvcPictureField;
    Label15: TLabel;
    OPFTotalTables: TOvcPictureField;
    Label16: TLabel;
    OPFTotalOfEntry: TOvcPictureField;
    Label17: TLabel;
    OSFComment: TOvcSimpleField;
    Label18: TLabel;
    OPFPlusNonMembers: TOvcPictureField;
    Label19: TLabel;
    OSFNewPage: TOvcSimpleField;
    Label20: TLabel;
    OPFGrossEntries: TOvcPictureField;
    OPFLessFillins: TOvcPictureField;
    Label21: TLabel;
    Label22: TLabel;
    OPFNetReceipts: TOvcPictureField;
    PopupMenu1: TPopupMenu;
    TournamentInformation1: TMenuItem;
    Worksheet1: TMenuItem;
    Invoice1: TMenuItem;
    Voucher1: TMenuItem;
    Ballancesheet1: TMenuItem;
    Officefinancialpart11: TMenuItem;
    Officefinancialpart2: TMenuItem;
    Quit1: TMenuItem;
    Procedure ButQuitActionExecute(Sender: TObject);
    Procedure FormClose(Sender: TObject; Var Action: TCloseAction);
    Procedure FormActivate(Sender: TObject);
    Procedure pCheckNeededFiles;
    Function fFinancePrepend(Const Fno: Integer): String;
    Procedure pOpenNeededFiles;
    Procedure ListTablesExecute(Sender: TObject);
    Procedure pFillFields;
    Procedure pNextRecordf;
    Procedure pPrevRecordf;
    Procedure ButNextActionExecute(Sender: TObject);
    Procedure ButPrevActionExecute(Sender: TObject);
    Procedure ButLastActionExecute(Sender: TObject);
    Procedure ButTopActionExecute(Sender: TObject);
    Procedure ButFindActionExecute(Sender: TObject);
    Procedure pWorkSheetInfo(bEnable: Boolean);
    Procedure ButEditActionExecute(Sender: TObject);
    Procedure ButPlayerInfoDoneExecute(Sender: TObject);
    Procedure ButAddActionExecute(Sender: TObject);
    Procedure ButCopyActionExecute(Sender: TObject);
    Procedure ButDeleteActionExecute(Sender: TObject);
    Procedure pUpdateStatusBar;
    Procedure OSFEventCodeExit(Sender: TObject);
    Function fFound(Const Fno, Kno: Integer; ChkKey: KeyStr): Boolean;
    Procedure pDateExit(Sender: TObject);
    Procedure MinusKeyExecute(Sender: TObject);
    Procedure pOpenFTInfo;
    Procedure F1KeyExecute(Sender: TObject);
    Procedure pDiffScreen;
    Procedure pGetWorkSheetData;
    Procedure pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure PlusKeyExecute(Sender: TObject);
    Procedure OPFTimeExit(Sender: TObject);
    Procedure F2KeyExecute(Sender: TObject);
    Procedure pPuMenuItemSelected(Sender: TObject);
    Procedure OSFNewPageExit(Sender: TObject);
    Procedure pAbortChanges;
    Procedure F8KeyExecute(Sender: TObject);
    Procedure F7KeyExecute(Sender: TObject);
    Procedure F6KeyExecute(Sender: TObject);
    Procedure pWhichScreen(oSender: TObject);
    Procedure pOpenInvoice; // Open frmInvoice
    Procedure pOpenVoucher; // Open frmVoucher
    Procedure pOpenBalanceSheet; // Open frmBalanceSheet
    Procedure pOpenOffFinP1; // Open frmOffFinP1
    Procedure pOpenOffFinP2; // Open frmOffFinP2
  private
    { Private declarations }
  public
    { Public declarations }
    { Variable to store the length of the table array }
    { Integer variable used dynamically to store the tables record count }
    iReccount: Integer;
    iFTIReccount: Integer;
    { iDataMode Integer Variable
      Set the current data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
    iDataMode: Integer;
    ksFTInfoKey: KeyStr;
    iMinusKey, iPlusKey, iformToOpen: Integer;
    sPopupItemName: String;
    bFTQuitUsed: Boolean;
    iFormTag: Integer;
    sCaption: String;
  End;

Var
  frmFinance: TfrmFinance;
  GetPrePend: String;
  ksFirstKey: KeyStr;
  ksTRecNoKey, ksTTempKey: keystr;
  ksERecNoKey, ksETempKey: keystr;
  ksARecNoKey, ksATempKey: keystr;
  bEscKeyUsed: Boolean;

Const
  iLenArray: Integer = MaxFilno;

Function CalcUSamountForOfs(Const showit: boolean): real;

Implementation

Uses formTblsCurrOpen, formFTInfo, fGeneric, tfprint, formInvoice;

{$DEFINE indatabase}
{$DEFINE isnoattend}
{$I STSTRS.dcl}
{$I showpts.inc}
{$I acblrank.dcl}
{$I Fkeys.inc}
{$I Cftinfo.inc}
{$I tfixdb.inc}

{$R *.dfm}

Procedure TfrmFinance.ButQuitActionExecute(Sender: TObject);
Begin
  Close;
End;

Procedure TfrmFinance.FormClose(Sender: TObject; Var Action: TCloseAction);
Begin
  { Close any open tables }
  fdBase.fCloseFiles;
  dBase.CloseFiles;
End;

Procedure TfrmFinance.FormActivate(Sender: TObject);
Begin
  { TopFilNo: What is the topmost database we are using in this application. }
  TopFilNo := Ftinfo_no;
  bEscKeyUsed := False;
  // -------------------------------------------------------------------------
  { Variable iFormToOpen possible values and their meanings:
    0 = Tournament Information, frmTournInfo
    1 = Tournament Worksheet, frmFinance
    2 = Invoice, frmInvoice
    3 = Voucher, frmVoucher
    4 = Balance Sheet, frmBalanceSheet
    5 = Office Financial Part 1, frmOffFinP1
    6 = Office Financial Part 2, frmOffFinP2 }
  iFormToOpen := 0;
  iFormTag := 0;
  // -------------------------------------------------------------------------
  Application.HintHidePause := 200000;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  { Flush_DF & Flush_IF: Must be set to True for so that everytime a change is
    made to a "Record", dbase re-writes the table header and in Playern.dat,
    updates dbLastImpDate entry. Set them to False when you Delete a record,
    then immediately set them back to True }
  { This must be done as a standard whenever a table is opened for modifications }
  Flush_DF := True;
  Flush_IF := True;
  //==========================================================
  GetPrePend := fFinancePrePend(TDInfo_no);
  Prepend := FixPath(CFG.DBPath);
  pCheckNeededFiles;
  pOpenNeededFiles;
  //==========================================================
  fFilno := Ftinfo_no;
  iFTIReccount := db33.UsedRecs(fDatF[Ftinfo_no]^);
  bFTQuitUsed := False;
  { Open FTInfo if there is more than record in FTInfo.Dat }
  If ((iFTIReccount > 1) And (ksFTInfoKey = '')) Then
    pOpenFTInfo { Open FTInfo if there is more than record in FTInfo.Dat }
  Else
    iFormTag := 1; { Set iFormTag to 1 so we know that frmFinance is what should be open }
  { Load the appropriate worksheet data }
  pGetWorkSheetData;
  Scrno := 2;
  //==========================================================
  { These variables are declared in CSDef.pas }
  IniName := GetCurrentDir + '\' + DefIniName;
  AppName := 'Finance';
  //==========================================================
  CSDef.ReadCFG;
  If CSDef.CFG.Tournament Then StatusBar1.Panels[0].Text := 'Tournament'
  Else StatusBar1.Panels[0].Text := 'Club';
  // ----------------------------------------------------------------------------------------------------------
  { Show the path to the table in use and the table name in StatusBar1. }
  StatusBar1.Panels[1].Text := UpperCase(fdbase.fPrepend + 'FWKSHEET.DAT');
  { Store the number of records to iReccount }
  iReccount := db33.UsedRecs(fDatF[Fwksheet_no]^);
  { Add the record count to StatusBar1's display }
  StatusBar1.Panels[1].Text := StatusBar1.Panels[1].Text + ' - Contains ' +
    Trim(IntToStr(iReccount)) + ' Records.';
End;

Procedure TfrmFinance.pCheckNeededFiles;
Begin
  If Not FileExists(Prepend + DBNames[Tourn_no, 0])
    Or Not FileExists(Prepend + DBNames[Tournev_no, 0]) Then Begin
    ErrBox('Tournament needs to be set up before financial reports can be used.', MC, 0);
    Close;
  End
  Else Begin
    // MessageDlg('Tournament files exist...', mtInformation, [mbOK], 0);
  End;
End;

Function TfrmFinance.fFinancePrepend(Const Fno: Integer): String;
Begin
  Case Fno Of
    TDInfo_no: Result := FixPath(CFG.PLPath);
  Else Result := FixPath(CFG.DBPath);
  End;
End;

Procedure TfrmFinance.pOpenNeededFiles;
Begin
  { Ftinfo_no = 1;
    Fwksheet_no = 2;
    Finvoice_no = 3;
    Tourn_no = 4;
    TournEv_no = 5;
    TDInfo_no = 6;
    Finvitem_no = 7;
    FBalance_no = 8;
    FOfs_no = 9; }
  fopenfiles;
  OpenDBFile(3); // Tourn_no
  OpenDBFile(4); // Tournev_no
End;

Procedure TfrmFinance.ListTablesExecute(Sender: TObject);
Begin
  frmTblsCurrOpen := TfrmTblsCurrOpen.Create(Self);
  frmTblsCurrOpen.ShowModal;
  frmTblsCurrOpen.Release;
End;

Procedure TfrmFinance.pFillFields;
Begin
  OSFSanctionNo.Text := FTINFO.SANCTION_NO;
  OSFTournament.Text := FTINFO.TOURN_NAME;
  OSFEventCode.Text := FWKSHEET.EVENT_CODE;
  ODEEventDate.Text := LibData.fFormatDateString(FWKSHEET.EVENT_DATE);
  If Length(ODEEventDate.Text) > 9 Then
    LabelMonthText.Caption := FormatDateTime('mmmm', StrToDate(ODEEventDate.Text))
  Else LabelMonthText.Caption := '';
  OPFTime.Text := FWKSHEET.TIME;
  OSFEventName.Text := FWKSHEET.EVENT_NAME;
  OSFSessions.Text := FWKSHEET.SESSIONS;
  OPFTables.Text := FWKSHEET.TABLES;
  OSFEntries.Text := FWKSHEET.ENTRIES;
  OPFEntryFee.Text := FWKSHEET.ENTRY_FEE;
  OPFEntryTotal.Text := FWKSHEET.ENTRY_TOTAL;
  OPFNonMember.Text := FWKSHEET.NON_MEMB;
  OPFFillIn.Text := FWKSHEET.FILL_IN;
  OSFComment.Text := FWKSHEET.COMMENT;
  OSFNewPage.Text := FWKSHEET.new_page;
  // -------------------------------------------------
  OPFTotalTables.Text := FTINFO.TOTAL_TABLES;
  OPFTotalOfEntry.Text := FTINFO.ENTRY_TOTAL;
  OPFPlusNonMembers.Text := FTINFO.NON_MEM;
  OPFGrossEntries.Text := FTINFO.GROSS;
  OPFLessFillins.Text := FTINFO.FILL_IN;
  OPFNetReceipts.Text := FTINFO.NET;
End;

Procedure TfrmFinance.pNextRecordf;
Var
  FndKey: KeyStr;
Begin
  { Move the record pointer in the table to the next available record and load the data in the screen fields }
  { Set the fDbase.pas variable fFilno to Table Number we are working with, Filno is used Globally }
  fdBase.fFilno := Fwksheet_no;
  LibData.SkipNextF(fdBase.fFilno, FndKey);
  { Read data base file }
  fGetARec(Fwksheet_no);
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmFinance.ButNextActionExecute(Sender: TObject);
Begin
  pNextRecordf;
End;

Procedure TfrmFinance.pPrevRecordf;
Var
  FndKey: KeyStr;
Begin
  fdBase.fFilno := Fwksheet_no;
  { Move the record pointer in the table to the previous record and load the data in the screen fields }
  PrevKey(fIdxKey[Fwksheet_no, 1]^, fRecno[Fwksheet_no], FndKey);
  { Read data base file }
  fGetARec(Fwksheet_no);
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmFinance.ButPrevActionExecute(Sender: TObject);
Begin
  pPrevRecordf;
End;

Procedure TfrmFinance.ButLastActionExecute(Sender: TObject);
Begin
  ClearKey(fIdxKey[Fwksheet_no, 1]^);
  pPrevRecordf; // Move the record pointer in the table to the previous record and load the data in the screen fields
End;

Procedure TfrmFinance.ButTopActionExecute(Sender: TObject);
Begin
  ClearKey(fIdxKey[Fwksheet_no, 1]^);
  pNextRecordf; // Move the record pointer in the table to the previous record and load the data in the screen fields
End;

Procedure TfrmFinance.ButFindActionExecute(Sender: TObject);
Begin
  //If fStatus_OK(fFilno) Then fFind_Record(fGetKey(Fwksheet_no, 1), fGetKey(Fwksheet_no, 1), '', MC, 1);
  // ----------------------------------------------------------------------------
  fFind_Record('', '', '', MC, 0);
  // ----------------------------------------------------------------------------
  fGetARec(Fwksheet_no);
  // ----------------------------------------------------------------------------
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmFinance.pWorkSheetInfo(bEnable: Boolean);
Begin
  { Set the state of ActionManager1 for the players GBMenu }
  If bEnable Then ActionManager1.State := asSuspended Else
    ActionManager1.State := asNormal;
  { Set the state of ActionManager2 for the form players GBMenu }
  If bEnable Then ActionManager2.State := asNormal Else
    ActionManager2.State := asSuspended;
  // ----------------------------------------------------------------------
  If bEnable Then GBMenu.Hide Else GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := Not bEnable;
  //GBMenu.Align := alNone;
  // ----------------------------------------------------------------------
  GBTournWorkSheet.Enabled := bEnable;
  // ----------------------------------------------------------------------
  LMDTournWSDone.Enabled := bEnable;
  LMDTournWSDone.Visible := bEnable;
  // ----------------------------------------------------------------------
End;

Procedure TfrmFinance.ButEditActionExecute(Sender: TObject);
Begin
  { In module Dbase call the procedure PreEditSaveKeys to save the KEYS in all tables we are
    editing so the application will know if a KEY Field has been changed and Indexes need to be
    regenerated }
  PreEditSaveKeys;
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 2;
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(True);
  { Put the cursor in the first field }
  OSFEventCode.SetFocus;
End;

Procedure TfrmFinance.ButPlayerInfoDoneExecute(Sender: TObject);
Begin
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  { Turn the Player Info Data Area OFF }
  pWorkSheetInfo(False);
End;

Procedure TfrmFinance.ButAddActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 1;
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(True);
  { Put the cursor in the first field }
  OSFEventCode.SetFocus;
End;

Procedure TfrmFinance.ButCopyActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 3;
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(True);
  { Put the cursor in the first field }
  OSFEventCode.SetFocus;
End;

Procedure TfrmFinance.ButDeleteActionExecute(Sender: TObject);
Begin
  If MessageDlg('Confirm deletion of this worksheet item.', mtWarning, [mbYes, mbNo], 0) = mrYes
    Then Begin
    fdbase.fFilno := Fwksheet_no;
    Flush_DF := False;
    Flush_IF := False;
    fdbase.fDelARec(Fwksheet_no);
    Flush_DF := True;
    Flush_IF := True;
    pUpdateStatusBar;
    pNextRecordf; { Move the record pointer in the table to the next available record and load the
    data in the screen fields }
    If ButNext.Enabled Then ButNext.SetFocus Else ButQuit.SetFocus;
  End;
End;

Procedure TfrmFinance.pUpdateStatusBar;
Begin
  iReccount := db33.UsedRecs(fDatF[Fwksheet_no]^);
  StatusBar1.Panels[1].Text := UpperCase(dbase.Prepend + 'FWKSHEET.DAT') + ' - Contains ' +
    Trim(IntToStr(iReccount)) + ' Records.';
End;

Procedure TfrmFinance.OSFEventCodeExit(Sender: TObject);
Var
  sLink: String;
Begin
  { If the ESCape Key was not the last key used, then run the code of field validation }
  If Not bEscKeyUsed Then Begin
    sLink := OSFSanctionNo.Text + OSFEventCode.Text;
    OK := True;
    If Not ((OSFEventCode.Text = NonEvent) Or (OSFEventCode.Text = OtherEvent) Or
      fFound(5, 1, OSFSanctionNo.Text + OSFEventCode.Text)) Then Begin
      If (MessageDlg('This event code not found in tournament data base.' + #13 + #10 +
        '               Correct this error?', mtError, [mbYes, mbNo], 0) = mrYes) Then Begin
        MessageDlg('This event code not found in tournament data base.', mtError, [mbOK], 0);
        OSFEventCode.SetFocus;
      End
      Else pAbortChanges;
    End;
  End;
End;

Function TfrmFinance.fFound(Const Fno, Kno: Integer; ChkKey: KeyStr): Boolean;
Begin
  If (Fno <> fFilno) And (fKeyLen[Fno, Kno] > 0) Then Begin
    FindKey(fIdxKey[Fno, Kno]^, fRecno[Fno], ChkKey);
    If OK Then GetRec(fDatF[2]^, fRecno[2], FWKSHEET);
    fFound := OK;
  End
  Else fFound := False;
  OK := True; (* RESET OK for SUBSEQUENT File ALIGNMENT *)
End;

Procedure TfrmFinance.pDateExit(Sender: TObject);
Var
  sDateText: String;
Begin
  { Replace all blanks with zeros in the date }
  (Sender As TOvcDateEdit).Text := fFillDateBlanks((Sender As TOvcDateEdit).Text);
  { Must take out the slashes before passing the date to ChkDate Function }
  sDateText := fStripDateField((Sender As TOvcDateEdit).Text);
  If Not (ChkDate(sDateText)) Then Begin
    MessageDlg('Event date is invalid.', mtError, [mbOK], 0);
    (Sender As TOvcDateEdit).SetFocus;
  End;
End;

Procedure TfrmFinance.MinusKeyExecute(Sender: TObject);
Begin
  { Reset the KeyStr variable ksFTInfoKey to blank }
  ksFTInfoKey := '';
  { Open frmFTInfo to view FTInfo.Dat (Tournament Information) }
  pOpenFTInfo;
  { Load the appropriate worksheet data }
  pGetWorkSheetData;
End;

Procedure TfrmFinance.pOpenFTInfo;
Begin
  frmFTInfo := TfrmFTInfo.Create(Self);
  frmFTInfo.ShowModal;
  frmFTInfo.Release;
  If bFTQuitUsed Then Begin
    Close;
  End;
  // ==========================================
  { frmFTInfo closes all tables when it is closed, tables need to be reopened }
  pCheckNeededFiles;
  pOpenNeededFiles;
End;

Procedure TfrmFinance.F1KeyExecute(Sender: TObject);
Begin
  pRunExtProg(frmFinance, CFG.LoadPath + 'WUtility', 'MANUAL\CONTENTS');
End;

Procedure TfrmFinance.pDiffScreen;
Begin
  //
End;

Procedure TfrmFinance.pGetWorkSheetData;
Begin
  { ksFTInfoKey contains the KeyStr if a FTInfo record has been selected, it is blank if not }
  If Trim(ksFTInfoKey) = '' Then Begin
    { Go to the first record in FTInfo }
    ClearKey(fIdxKey[FTINFO_no, 1]^);
    { Find the first available record }
    LibData.SkipNextf(FTINFO_no, ksFirstKey);
    { Store the records KeyStr }
    ksTRecNoKey := fGetKey(FTINFO_no, 1)
  End
  Else Begin
    { Store the selected FTInfo KeyStr to the Temp KeyStr Variable }
    ksTRecNoKey := ksFTInfoKey;
    { Go to the record in FTInfo selected in form frmFTInfo }
    FindKey(fIdxKey[FTINFO_no, 1]^, fRecno[FTINFO_no], ksTRecNoKey);
  End;
  { Store the table fields in the record variable }
  fGetARec(FTINFO_no);
  { Store the selected FTInfo KeyStr to the Temp KeyStr Variable }
  ksTTempKey := ksTRecNoKey;
  { Align the child tables of FTInfo }
  fAlign_Rec(fRecNo[fFilno]);
  LibData.SkipNextf(Fwksheet_no, ksTTempKey);
  { Store the table fields in the record variable }
  fGetARec(Fwksheet_no);
  { Fill the form fields with the WorkSheet Data }
  pFillFields;
  { The user has selected another form, so it needs to opened }
  If (iFormTag <> 1) Then Begin
    Case iFormTag Of
      0: pOpenFTInfo; // 0 = Tournament Information, frmTournInfo
      2: pOpenInvoice; // 2 = Invoice, frmInvoice
      3: pOpenVoucher; // 3 = Voucher, frmVoucher
      4: pOpenBalanceSheet; // 4 = Balance Sheet, frmBalanceSheet
      5: pOpenOffFinP1; // 5 = Office Financial Part 1, frmOffFinP1
      6: pOpenOffFinP2; // 6 = Office Financial Part 2, frmOffFinP2
    Else
      iFormTag := 1;
    End;
  End;
End;

Procedure TfrmFinance.pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Var
  wKey: Word;
Begin
  wKey := Key;
  Case key Of
    { 107 is the Plus Key was pressed on the numeric keypad }
    107: If GBMenu.Enabled Then PlusKeyExecute(Sender); // pDiffScreen(1)
    { 109 is the Minus Key was pressed on the numeric keypad }
    109: If GBMenu.Enabled Then MinusKeyExecute(Sender); // pDiffScreen(6);
    { Return key was pressed }
    VK_RETURN: If Not GBMenu.Enabled Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    { ESCape key was pressed, Abort the record or record changes... }
    VK_ESCAPE: If Not GBMenu.Enabled Then Begin
        bEscKeyUsed := True;
        pAbortChanges;
      End;
    { F9 key was pressed, save the record or the changes... }
    VK_F9: If Not GBMenu.Enabled Then {pSaveChanges};
  End;
End;

Procedure TfrmFinance.pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
Var
  wKey: Word;
Begin
  { Do not trap for the Return Key here, it causes the cursor to move two fields at a time }
  wKey := Key;
  Case key Of
    { 107 is the Plus Key was pressed on the numeric keypad }
    107: If GBMenu.Enabled Then PlusKeyExecute(Sender);
    { 109 is the Minus Key was pressed on the numeric keypad }
    109: If GBMenu.Enabled Then MinusKeyExecute(Sender); // pDiffScreen(6);
    { ESCape key was pressed, Abort the record or record changes... }
    VK_ESCAPE: If Not GBMenu.Enabled Then Begin
        bEscKeyUsed := True;
        pAbortChanges;
      End;
    { F9 key was pressed, save the record or the changes... }
    VK_F9: If Not GBMenu.Enabled Then {pSaveChanges};
  End;
End;

Procedure TfrmFinance.PlusKeyExecute(Sender: TObject);
Begin
  { Open the Invoice Form }
  //
End;

Procedure TfrmFinance.OPFTimeExit(Sender: TObject);
Var
  sTimeText: String;
Begin
  sTimeText := OPFTime.Text;
  If Not (fCheckTime(OPFTime.Text)) Then Begin
    MessageDlg('Invalid time.', mtError, [mbOK], 0);
    OPFTime.SetFocus;
  End;
End;

Procedure TfrmFinance.F2KeyExecute(Sender: TObject);
Begin
  GBMenu.Hide;
  GBMenu.Enabled := False;
  PopupMenu1.Items.Items[1].Enabled := False;
  PopupMenu1.Popup(350, 500);
  GBMenu.Enabled := True;
  GBMenu.Show;
End;

Procedure TfrmFinance.pPuMenuItemSelected(Sender: TObject);
Begin
  { Variable iFormToOpen possible values and their meanings:
    0 = Tournament Information, frmTournInfo
    1 = Tournament Worksheet, frmFinance
    2 = Invoice, frmInvoice
    3 = Voucher, frmVoucher
    4 = Balance Sheet, frmBalanceSheet
    5 = Office Financial Part 1, frmOffFinP1
    6 = Office Financial Part 2, frmOffFinP2 }
  iFormTag := (Sender As TMenuItem).Tag;
  Case iFormTag Of
    0: pOpenFTInfo; // 0 = Tournament Information, frmTournInfo
    //1: pWhichScreen(Sender); // 1 = Tournament Worksheet, frmFinance
    2: pWhichScreen(Sender); // 2 = Invoice, frmInvoice
    3: pWhichScreen(Sender); // 3 = Voucher, frmVoucher
    4: pWhichScreen(Sender); // 4 = Balance Sheet, frmBalanceSheet
    5: pWhichScreen(Sender); // 5 = Office Financial Part 1, frmOffFinP1
    6: pWhichScreen(Sender); // 6 = Office Financial Part 2, frmOffFinP2
  Else
    iFormTag := 1;
  End;
End;

Procedure TfrmFinance.pWhichScreen(oSender: TObject);
Begin
  sCaption := (oSender As TMenuItem).Caption;
  frmFinance.iformToOpen := iFormTag;
  frmFinance.sPopupItemName := sCaption;
  MessageDlg('You selected item ' + IntToStr(frmFinance.iformToOpen) +
    ', which is ' + frmFinance.sPopupItemName, mtInformation, [mbOK], 0);
End;

Procedure TfrmFinance.OSFNewPageExit(Sender: TObject);
Begin
  If Not (Pos(OSFNewPage.Text, '0123456789P') > 0) Then Begin
    MessageDlg('Must be P for new page or a number of blank lines.', mtError, [mbOK], 0);
    OSFNewPage.Text := '';
    OSFNewPage.SetFocus;
  End;
End;

Procedure TfrmFinance.pAbortChanges;
Begin
  { Turn the Player Info Data Area OFF }
  pWorkSheetInfo(False);
  { Fill the form fields with the WorkSheet Data }
  pFillFields;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
End;

Procedure TfrmFinance.F8KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (1) to Print to Screen }
  PrintReport(1);
End;

Procedure TfrmFinance.F7KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (2) to Print Reports }
  PrintReport(2);
End;

Function VEditStat(Const vtype, subtype: byte; Const mask: word): boolean;
Begin
  VEditStat := (VoucherEditStat[vtype, subtype] And mask) <> 0;
End;

Function CalcUSamountForOfs(Const showit: boolean): real;
Var
  tempreal: real;
Begin
  With FInvItem Do Begin
    If (Ftinfo.Use_ex_rate = 'Y') And (Use_ex_rate = 'N') And (paid_by = 'A')
      And VEditStat(Ival(Item_type), Ival(Sub_Item_type), vesCalcOfsEx) Then Begin
      tempreal := Valu(Total) / (1 + Valu(Ftinfo.Exchange_rate) / 100);
      CalcUsAmountForOfs := tempreal;
      If showit Then MsgBox('     US$: ' + Fstr(tempreal, 7, 2) + ' (for OFS)'#13
          + 'Exchange: ' + Fstr(Valu(Total) - tempreal, 7, 2) + #13 +
          '    CDN$:' + Total, 0, MC);
    End
    Else CalcUsAmountForOfs := Valu(Total);
  End;
End;

Procedure TfrmFinance.F6KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (3) to Print Reports to an ASCII file }
  PrintReport(3);
End;

Procedure TfrmFinance.pOpenInvoice; // Open frmInvoice
Begin
  //MessageDlg('Opening Form Invoice', mtInformation, [mbOK], 0);
  frmInvoice := TfrmInvoice.Create(Self);
  frmInvoice.ShowModal;
  frmInvoice.Release;
  If bFTQuitUsed Then Begin
    Close;
  End;
  // ==========================================
  { frmFTInfo closes all tables when it is closed, tables need to be reopened }
  pCheckNeededFiles;
  pOpenNeededFiles;
End;

Procedure TfrmFinance.pOpenVoucher; // Open frmVoucher
Begin
  MessageDlg('Opening Form Voucher', mtInformation, [mbOK], 0);
End;

Procedure TfrmFinance.pOpenBalanceSheet; // Open frmBalanceSheet
Begin
  MessageDlg('Opening Form Balance Sheet', mtInformation, [mbOK], 0);
End;

Procedure TfrmFinance.pOpenOffFinP1; // Open frmOffFinP1
Begin
  MessageDlg('Opening Form Office Financial Part 1', mtInformation, [mbOK], 0);
End;

Procedure TfrmFinance.pOpenOffFinP2; // Open frmOffFinP2
Begin
  MessageDlg('Opening Form Office Financial Part 2', mtInformation, [mbOK], 0);
End;

End.

