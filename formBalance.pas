Unit formBalance;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ovcbase, ovcdlg, ovcclkdg, StdCtrls, Mask, Buttons,
  ToolWin, ExtCtrls, ActnMan, ActnCtrls, Menus, ActnList, ovcef, ovcsf,
  ovcrvcbx, ovcpb, ovcpf, ovcnf, ovceditf, ovcedpop, ovcedcal, ovcbordr,
  ovcbcalc, ovcbcldr, XPStyleActnCtrls, Grids, ImgList, ResizeKit, LMDCustomComponent,
  LMDWndProcComponent, LMDFormShape, LMDShapeHint,
  LMDHint, LMDCustomScrollBox, LMDListBox, LMDCustomControl, LMDCustomPanel,
  LMDCustomBevelPanel, LMDBaseEdit, LMDCustomEdit, LMDCustomMaskEdit, LMDCustomExtCombo,
  LMDCustomListComboBox, LMDListComboBox, ovccmbx, LMDHeaderListComboBox,
  LMDCustomComboBox, LMDComboBox, STSTRS, LMDIniCtrl, LMDFormA,
  LMDStorBase, LMDStorXMLVault, LMDStorBinVault, LMDCustomHint,
  LMDCustomShapeHint, LMDMessageHint, LibData, LMDCalendarEdit,
  LMDCustomButton, LMDButton, fDbase, Dbase, DB33, LibUtil, Clipbrd, CSDef,
  Util1, Util2, YesNoBoxU, PVIO, VListBox, History, LMDCustomParentPanel,
  LMDCustomGroupBox, LMDGroupBox, LibFinance, LMDWaveComp, I_FCommon,hh;

Type
  TfrmBalance = Class(TForm)
    GBMenu: TGroupBox;
    Shape23: TShape;
    ButNext: TButton;
    ButPrev: TButton;
    ButFind: TButton;
    ButTop: TButton;
    ButLast: TButton;
    ButEdit: TButton;
    ButAdd: TButton;
    ButCopy: TButton;
    ButDelete: TButton;
    ButQuit: TButton;
    ButtonF7Key: TButton;
    ButtonF6Key: TButton;
    ButtonF8Key: TButton;
    ButtonF2Key: TButton;
    StaticTextF3Key: TStaticText;
    StaticTextF4Key: TStaticText;
    StaticTextF5Key: TStaticText;
    StaticTextF2Key: TStaticText;
    ButtonF1Key: TButton;
    StaticTextF1Key: TStaticText;
    ActionManager1: TActionManager;
    ButNextAction: TAction;
    ButPrevAction: TAction;
    ButFindAction: TAction;
    ButTopAction: TAction;
    ButLastAction: TAction;
    ButEditAction: TAction;
    ButAddAction: TAction;
    ButCopyAction: TAction;
    ButDeleteAction: TAction;
    ButQuitAction: TAction;
    F4Key: TAction;
    F3Key: TAction;
    F5Key: TAction;
    F2Key: TAction;
    F7Key: TAction;
    F1Key: TAction;
    F8Key: TAction;
    F6Key: TAction;
    CtrlCKey: TAction;
    ButPlus: TButton;
    StaticText1: TStaticText;
    PlusKey: TAction;
    ButMinus: TButton;
    StaticText2: TStaticText;
    MinusKey: TAction;
    StatusBar1: TStatusBar;
    OvcController1: TOvcController;
    LMDHint1: TLMDHint;
    ActionManager2: TActionManager;
    ButPlayerInfoDone: TAction;
    StaticText3: TStaticText;
    GBTournWorkSheet: TGroupBox;
    LMDTournWSDone: TLMDButton;
    SpeedButton1: TSpeedButton;
    ListTables: TAction;
    PopupMenu1: TPopupMenu;
    TournamentInformation1: TMenuItem;
    Worksheet1: TMenuItem;
    Invoice1: TMenuItem;
    Voucher1: TMenuItem;
    Ballancesheet1: TMenuItem;
    Officefinancialpart11: TMenuItem;
    Officefinancialpart2: TMenuItem;
    Quit1: TMenuItem;
    LMDGroupBox1: TLMDGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    OSFDescription: TOvcSimpleField;
    LabelItemType: TLabel;
    LMDGroupBox2: TLMDGroupBox;
    Label2: TLabel;
    Label27: TLabel;
    OPFNetReceipts: TOvcPictureField;
    OPFItems: TOvcPictureField;
    Label4: TLabel;
    Label5: TLabel;
    OPFItemRate: TOvcPictureField;
    Label6: TLabel;
    OPFAmount: TOvcPictureField;
    Label9: TLabel;
    Label10: TLabel;
    OPFAdditions: TOvcPictureField;
    Label11: TLabel;
    Label12: TLabel;
    OPFSubTotal: TOvcPictureField;
    Label13: TLabel;
    Label14: TLabel;
    OPFLessSponsorInvoice: TOvcPictureField;
    Label15: TLabel;
    Label16: TLabel;
    OPFLessDeductions: TOvcPictureField;
    Label17: TLabel;
    Label18: TLabel;
    OPFExchangeForTDExpenses: TOvcPictureField;
    Label19: TLabel;
    Label20: TLabel;
    OPFExchangeForACBLExpenses: TOvcPictureField;
    Label21: TLabel;
    Label22: TLabel;
    OPFNetToSponsor: TOvcPictureField;
    Label23: TLabel;
    Label24: TLabel;
    OPFAdjustedNetToSponsor: TOvcPictureField;
    Label25: TLabel;
    Label26: TLabel;
    OPFCheckToAcblFromSponsor: TOvcPictureField;
    LMDGroupBox3: TLMDGroupBox;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    OPFChecks: TOvcPictureField;
    OPFCash: TOvcPictureField;
    OPFTotalToSponsor: TOvcPictureField;
    OPFOther: TOvcPictureField;
    LMDGroupBox4: TLMDGroupBox;
    Label32: TLabel;
    Label33: TLabel;
    OPFTotalNeededToBalance: TOvcPictureField;
    LMDGroupBox5: TLMDGroupBox;
    Label3: TLabel;
    OSFSanctionNo: TOvcSimpleField;
    Label1: TLabel;
    OSFTournament: TOvcSimpleField;
    OSFSanctionNumberB: TOvcSimpleField;
    ONFItemType: TOvcNumericField;
    Action1: TAction;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    Procedure ButQuitActionExecute(Sender: TObject);
    Procedure FormActivate(Sender: TObject);
    Procedure ListTablesExecute(Sender: TObject);
    Procedure pFillFields;
    Procedure pNextRecordf;
    Procedure pPrevRecordf;
    Procedure ButNextActionExecute(Sender: TObject);
    Procedure ButPrevActionExecute(Sender: TObject);
    Procedure ButLastActionExecute(Sender: TObject);
    Procedure ButTopActionExecute(Sender: TObject);
    Procedure ButFindActionExecute(Sender: TObject);
    Procedure pWorkSheetInfo(bEnable: Boolean);
    Procedure ButEditActionExecute(Sender: TObject);
    Procedure ButPlayerInfoDoneExecute(Sender: TObject);
    Procedure ButAddActionExecute(Sender: TObject);
    Procedure ButCopyActionExecute(Sender: TObject);
    Procedure ButDeleteActionExecute(Sender: TObject);
    Procedure pUpdateStatusBar;
    Procedure pDateExit(Sender: TObject);
    Procedure MinusKeyExecute(Sender: TObject);
    Procedure F1KeyExecute(Sender: TObject);
    Procedure pGetBalanceData;
    Procedure pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure PlusKeyExecute(Sender: TObject);
    Procedure F2KeyExecute(Sender: TObject);
    Procedure pPuMenuItemSelected(Sender: TObject);
    Procedure pAbortChanges;
    Procedure F8KeyExecute(Sender: TObject);
    Procedure F7KeyExecute(Sender: TObject);
    Procedure F6KeyExecute(Sender: TObject);
    Procedure pWhichScreen(oSender: TObject); // Open frmOffFinP2
    Function fShowItemType(Const Item: byte): String;
    Function fShowSubItemType(Const Item, SubItem: byte): String;
    Function fValidVoucherSubItem(Const Item, SubItem: byte): boolean;
    Function fValidVoucherItem(Const Item: byte): boolean;
    Procedure FormClose(Sender: TObject; Var Action: TCloseAction);
    Function fShowBalanceItem(Const Item: String): String;
    Function fCalcSponsorPreNet: real;
    Procedure pSaveChanges;
    Procedure pCheckEditFields;
    Procedure pCopyRecData;
    Procedure pClearFields;
    Procedure pButtOn(bTurnOn: Boolean);
    Procedure ONFItemTypeExit(Sender: TObject);
    Function fBalStat(Const vtype, mask: byte): boolean;
    Procedure OPFItemsExit(Sender: TObject);
    Procedure OSFDescriptionExit(Sender: TObject);
    Procedure OPFItemRateExit(Sender: TObject);
    Procedure OPFAmountExit(Sender: TObject);
    Procedure ONFItemTypeEnter(Sender: TObject);
    Procedure OSFDescriptionEnter(Sender: TObject);
    Procedure OPFItemsEnter(Sender: TObject);
    Procedure OPFItemRateEnter(Sender: TObject);
    Procedure OPFAmountEnter(Sender: TObject);
    Procedure pFillStaticFields;
    Procedure pBlankBalanceRecord;
    Procedure Action1Execute(Sender: TObject);
    Procedure Action2Execute(Sender: TObject);
    Procedure pSetWinState;
    Procedure ResizeKit1ExitResize(Sender: TObject; XScale, YScale: Double);
    Procedure Action3Execute(Sender: TObject);
    Procedure Action4Execute(Sender: TObject);
    Function FormHelp(Command: Word; Data: Integer; Var CallHelp: Boolean): Boolean;
    //    Procedure pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
    //    Procedure pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
    //    Procedure pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
    //    Procedure pOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
    //    Procedure pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
    //    Procedure pLMDHCBFieldAvail(bAvail: Boolean; oOSField: TLMDHeaderListComboBox);
    //    Procedure pONFieldAvail(bAvail: Boolean; oOSField: TOvcNumericField);
//    Procedure pResetTabStops;
  private
    { Private declarations }
  public
    { Public declarations }
    { Variable to store the length of the table array }
    bKeyIsUnique: Boolean;
  End;

Var
  frmBalance: TfrmBalance;
  bEscKeyUsed: Boolean;
  bIs: Boolean;
  { Integer variable used dynamically to store the tables record count }
  iBalanceRecs: Integer;
  { Integer variable used to store the records for this sanction number }
  iBalancesCount: Integer;
  ksThisTempKey: keystr;

Const
  iLenArray: Integer = MaxFilno;

Implementation

Uses formTblsCurrOpen, formFTInfo, tfprint, formWorkSheet;

{$I STSTRS.dcl}

{$R *.dfm}


Procedure TfrmBalance.ButQuitActionExecute(Sender: TObject);
Begin
  { The user wants to Quit, change the Public Variable:  bFTQuitUsed to True }
  frmWorkSheet.bFTQuitUsed := True;
  Close;
End;

Procedure TfrmBalance.FormActivate(Sender: TObject);
Begin
  frmWorkSheet.bWinStNorm := (frmWorkSheet.WindowState = wsNormal);
  If frmWorkSheet.bWinStNorm Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  frmWorkSheet.bWinStNorm := (WindowState = wsNormal);
  pSetPanelWidth(frmBalance, StatusBar1);
  { TopFilNo: What is the topmost database we are using in this application. }
  TopFilNo := Ftinfo_no;
  fFilno := FBalance_no;
  bEscKeyUsed := False;
  // -------------------------------------------------------------------------
  Application.HintHidePause := 200000;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := Null;
  { Flush_DF & Flush_IF: Must be set to True for so that everytime a change is
    made to a "Record", dbase re-writes the table header and in Playern.dat,
    updates dbLastImpDate entry. Set them to False when you Delete a record,
    then immediately set them back to True }
  { This must be done as a standard whenever a table is opened for modifications }
  Flush_DF := True;
  Flush_IF := True;
  //==========================================================
  { Store the FTInfo Key variable to This Temp Key }
  ksThisTempKey := ksFTInfoKey;
  { Load the appropriate Data }
  pGetBalanceData;
  { This variable Scrno, is used in some of the older code procedures to determine which
    calculations need to be performed based on the screen we are in.
    ----------------------------------------------------------
    Screen 1 = Tournament Information    - FTInfo.dat
    Screen 2 = Tournament Work Sheet     - FWkSheet.dat
    Screen 3 = Tournament Invoice        - FInvoice.dat
    Screen 4 = Voucher Items             - FInvItem.dat
    Screen 5 = Tournament Balance Sheet  - FBalance.dat
    Screen 6 = Office financial part 1   - FOFs.dat
    Screen 7 = Office financial part 2   - FOFs.dat }
  Scrno := 5;
  //==========================================================
  If CSDef.CFG.Tournament Then StatusBar1.Panels[0].Text := 'Tournament'
  Else StatusBar1.Panels[0].Text := 'Club';
  // ----------------------------------------------------------------------------------------------------------
  { Show the path to the table in use and the table name in StatusBar1. }
  StatusBar1.Panels[1].Text := UpperCase(Prepend + 'FBalance.DAT');
  { Store the number of records to IBalanceRecs }
  IBalanceRecs := db33.UsedRecs(fDatF[FBalance_no]^);
  { Add the record count to StatusBar1's display }
  StatusBar1.Panels[1].Text := StatusBar1.Panels[1].Text + ' - Contains ' +
    Trim(IntToStr(IBalanceRecs)) + ' Records.';
End;

Procedure TfrmBalance.ListTablesExecute(Sender: TObject);
Begin
  frmTblsCurrOpen := TfrmTblsCurrOpen.Create(Self);
  frmTblsCurrOpen.ShowModal;
  frmTblsCurrOpen.Free;
End;

Procedure TfrmBalance.pFillFields;
Var
  rVal: Real;
  sValue: String;
Begin
  With ftinfo Do Begin
    OSFSanctionNo.Text := SANCTION_NO;
    OSFTournament.Text := TOURN_NAME;
  End;
  // -------------------------------------------------
  With FBALANCE Do Begin
    ONFItemType.Text := ITEM_TYPE;
    LabelItemType.Caption := fShowBalanceItem(item_type);
    OSFDescription.Text := Trim(DESCRIPTION);
    OPFItems.Text := ITEMS;
    OPFItemRate.Text := ITEM_RATE;
    OPFAmount.Text := Fstr(Valu(AMOUNT), 10, 2);
    OSFSanctionNumberB.Text := SANCTION_NO;
  End;
  // -------------------------------------------------
  pFillStaticFields;
End;

Procedure TfrmBalance.pNextRecordf;
Begin
  { Move the record pointer in the table to the next available record and load the data in the screen fields }
  fNext_Record;
  liRecnoBAL := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmBalance.ButNextActionExecute(Sender: TObject);
Begin
  pNextRecordf;
End;

Procedure TfrmBalance.pPrevRecordf;
Begin
  { Move the record pointer in the table to the previous record and load the data in the screen fields }
  fPrev_Record;
  liRecnoBAL := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmBalance.ButPrevActionExecute(Sender: TObject);
Begin
  pPrevRecordf;
End;

Procedure TfrmBalance.ButLastActionExecute(Sender: TObject);
Begin
  fLast_Record;
  liRecnoBAL := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmBalance.ButTopActionExecute(Sender: TObject);
Begin
  fTop_Record;
  liRecnoBAL := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmBalance.ButFindActionExecute(Sender: TObject);
Begin
  fFind_Record(fGetKey(Ftinfo_no, 1), fGetKey(fFilno, 1), '', MC, 0, True);
  liRecnoBAL := fDbase.fRecno[fFilno];
  { Load all information to the appropriate MaskEdit field from the current record }
  pFillFields;
End;

Procedure TfrmBalance.pWorkSheetInfo(bEnable: Boolean);
Begin
  FreeHintWin;
  { Set the state of ActionManager1 for the players GBMenu }
  If bEnable Then ActionManager1.State := asSuspended Else
    ActionManager1.State := asNormal;
  { Set the state of ActionManager2 for the form players GBMenu }
  If bEnable Then ActionManager2.State := asNormal Else
    ActionManager2.State := asSuspended;
  // ----------------------------------------------------------------------
  If bEnable Then GBMenu.Hide Else GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := Not bEnable;
  //GBMenu.Align := alNone;
  // ----------------------------------------------------------------------
  GBTournWorkSheet.Enabled := bEnable;
  // ----------------------------------------------------------------------
  LMDTournWSDone.Enabled := bEnable;
  LMDTournWSDone.Visible := bEnable;
  // ----------------------------------------------------------------------
End;

Procedure TfrmBalance.ButEditActionExecute(Sender: TObject);
Begin
  { In module Dbase call the procedure fPreEditSaveKeys to save the KEYS in all tables we are
    editing so the application will know if a KEY Field has been changed and Indexes need to be
    regenerated }
  fPreEditSaveKeys;
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 2;
  sDataMode := 'E';
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(True);
  { Put the cursor in the first field }
  ONFItemType.SetFocus;
End;

Procedure TfrmBalance.ButPlayerInfoDoneExecute(Sender: TObject);
Begin
  pSaveChanges;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := null;
  pFillStaticFields;
  { Turn the Player Info Data Area OFF }
  pWorkSheetInfo(False);
  pUpdateStatusBar;
End;

Procedure TfrmBalance.ButAddActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 1;
  sDataMode := 'A';
  fInitRecord(fFilno);
  { Clear the Edit Fields of any data }
  pFillFields;
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(True);
  { Put the cursor in the first field }
  ONFItemType.SetFocus;
End;

Procedure TfrmBalance.ButCopyActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 3;
  sDataMode := 'C';
  fAdd_Init;
  pFillFields;
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(True);
  { Put the cursor in the first field }
  ONFItemType.SetFocus;
End;

Procedure TfrmBalance.ButDeleteActionExecute(Sender: TObject);
Begin
  fDelete_Record(True, self.HelpContext);
  pUpdateStatusBar;
  If IBalanceRecs > 0 Then Begin
    { Move the record pointer in the table to the next available record and load the
      data in the screen fields }
    pNextRecordf;
    If ButNext.Enabled Then ButNext.SetFocus Else ButQuit.SetFocus;
  End
  Else Begin
    liRecnoBAL := 0;
    pClearFields;
    pFillFields;
    pButtOn(False);
    ButAdd.SetFocus;
  End;
End;

Procedure TfrmBalance.pUpdateStatusBar;
Begin
  IBalanceRecs := db33.UsedRecs(fDatF[FBalance_no]^);
  StatusBar1.Panels[1].Text := UpperCase(dbase.Prepend + 'FBALANCE.DAT') + ' - Contains ' +
    Trim(IntToStr(IBalanceRecs)) + ' Records.';
  StatusBar1.Panels[2].Text := IntToStr(iBalancesCount) + ' balances';
End;

Procedure TfrmBalance.pDateExit(Sender: TObject);
Var
  sDateText: String;
Begin
  { Replace all blanks with zeros in the date }
  (Sender As TOvcDateEdit).Text := fFillDateBlanks((Sender As TOvcDateEdit).Text);
  { Must take out the slashes before passing the date to ChkDate Function }
  sDateText := fStripDateField((Sender As TOvcDateEdit).Text);
  If Not (ChkDate(sDateText)) Then Begin
    MessageDlg('Event date is invalid.', mtError, [mbOK], 0);
    (Sender As TOvcDateEdit).SetFocus;
  End;
End;

Procedure TfrmBalance.MinusKeyExecute(Sender: TObject);
Begin
  { Store the Invoice Record FHash Key to the Public Variable:  frmWorkSheet.ksFInvoiceKey
    If the Voucher Screen was selected }
  If frmWorkSheet.iFormTag = 3 Then frmWorkSheet.pGetInvoiceKey;
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := 3; { 3 = Voucher, frmVoucher }
  frmWorkSheet.bFTQuitUsed := False;
  Close;
End;

Procedure TfrmBalance.F1KeyExecute(Sender: TObject);
Begin
  //pRunExtProg(frmBalance, CFG.LoadPath + 'WUtility', 'MANUAL\CONTENTS');
  keybd_event(VK_F1, MapVirtualKey(VK_F1, 0), 0, 0);
End;

Procedure TfrmBalance.pGetBalanceData;
Begin
  pFillStaticFields;
  If liRecnoBAL > 0 Then
    { Fill the form fields with the Data }
    pFillFields
  Else Begin
    { Go to the first record }
    fTop_Record;
    If fStatus_OK(fFilno) Then Begin
      liRecnoBAL := fDbase.fRecno[fFilno];
    End
    Else Begin
      liRecnoBAL := 0;
      fInitRecord(fFilno);
      pButtOn(False);
      ButAdd.SetFocus;
    End;
    { Fill the form fields with the Data }
    pFillFields;
  End;
End;

Procedure TfrmBalance.pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Var
  wKey: Word;
Begin
  wKey := Key;
  Case key Of
    { 107 is the Plus Key was pressed on the numeric keypad }
    107: If GBMenu.Enabled Then PlusKeyExecute(Sender);
    { 109 is the Minus Key was pressed on the numeric keypad }
    109: If GBMenu.Enabled Then MinusKeyExecute(Sender);
    { Return key was pressed }
    VK_RETURN: If Not GBMenu.Enabled Then Begin
        key := 0;
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
      End;
    { ESCape key was pressed, Abort the record or record changes... }
    VK_ESCAPE: If Not GBMenu.Enabled Then Begin
        bEscKeyUsed := True;
        pAbortChanges;
        If Not ok Then Begin
          { frmWorkSheet.iFormTag must be set to the forms number that user selected }
          frmWorkSheet.iFormTag := 0;
          frmWorkSheet.bFTQuitUsed := False;
          Close;
        End;
        //Else Close;
      End;
    { F9 key was pressed, save the record or the changes... }
    VK_F9: If Not GBMenu.Enabled Then pSaveChanges;
  Else
    If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
      If (Key = VK_UP) Then Begin
        ButPrev.Click;
        ButPrev.SetFocus;
      End
      Else Begin
        ButNext.Click;
        ButNext.SetFocus;
      End;
      Key := 0;
    End;
  End;
End;

Procedure TfrmBalance.pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
Var
  wKey: Word;
Begin
  { Do not trap for the Return Key here, it causes the cursor to move two fields at a time }
  wKey := Key;
  Case key Of
    { ESCape key was pressed, Abort the record or record changes... }
    VK_ESCAPE: If Not GBMenu.Enabled Then Begin
        bEscKeyUsed := True;
        pAbortChanges;
        If Not ok Then Begin
          { frmWorkSheet.iFormTag must be set to the forms number that user selected }
          frmWorkSheet.iFormTag := 0;
          frmWorkSheet.bFTQuitUsed := False;
          Close;
        End;
        //Else Close;
      End;
    { F9 key was pressed, save the record or the changes... }
    VK_F9: If Not GBMenu.Enabled Then pSaveChanges;
  Else
    If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
      If (Key = VK_UP) Then Begin
        ButPrev.Click;
        ButPrev.SetFocus;
      End
      Else Begin
        ButNext.Click;
        ButNext.SetFocus;
      End;
      Key := 0;
    End;
  End;
End;

Procedure TfrmBalance.PlusKeyExecute(Sender: TObject);
Begin
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := 5; { 5 = Office Financial Part 1, frmOffFinP1 }
  frmWorkSheet.bFTQuitUsed := False;
  Close;
End;

Procedure TfrmBalance.F2KeyExecute(Sender: TObject);
Var
  iForLoop: Integer;
Begin
  GBMenu.Enabled := False;
  For iForLoop := (PopupMenu1.Items.Count - 1) Downto 0 Do
    PopupMenu1.Items.Items[iForLoop].Enabled := True;
  PopupMenu1.Items.Items[(Scrno - 1)].Enabled := False;
  PopupMenu1.Popup(350, 500);
  GBMenu.Enabled := True;
End;

Procedure TfrmBalance.pPuMenuItemSelected(Sender: TObject);
Begin
  { Variable iFormTag possible values and their meanings:
    0 = Tournament Information, frmTournInfo
    1 = Tournament Worksheet, frmWorkSheet
    2 = Invoice, frmInvoice
    3 = Voucher, frmVoucher
    4 = Balance Sheet, frmBalanceSheet
    5 = Office Financial Part 1, frmOffFinP1
    6 = Office Financial Part 2, frmOffFinP2 }
  { frmWorkSheet.iFormTag must be set to the forms number that user selected }
  frmWorkSheet.iFormTag := (Sender As TMenuItem).Tag;
  If ((frmWorkSheet.iFormTag >= 0) And (frmWorkSheet.iFormTag <= 6)) Then Begin
    { Store this records key to the Public Variable:  frmWorkSheet.ksFTInfoKey }
    If ksFTInfoKey = '' Then ksFTInfoKey := GetKey(FTINFO_no, 1);
    { Store the Invoice Record FHash Key to the Public Variable:  frmWorkSheet.ksFInvoiceKey
      If the Voucher Screen was selected }
    If frmWorkSheet.iFormTag = 3 Then frmWorkSheet.pGetInvoiceKey;
    Close;
  End;
End;

Procedure TfrmBalance.pWhichScreen(oSender: TObject);
Begin
  frmWorkSheet.sCaption := (oSender As TMenuItem).Caption;
  frmWorkSheet.sPopupItemName := frmWorkSheet.sCaption;
  MessageDlg('You selected item ' + IntToStr(frmWorkSheet.iformTag) +
    ', which is ' + frmWorkSheet.sPopupItemName, mtInformation, [mbOK], 0);
End;

Procedure TfrmBalance.pAbortChanges;
Begin
  FreeHintWin;
//  pResetTabStops;
  { Turn the Player Info Data Area OFF }
  pWorkSheetInfo(False);
  { Fill the form fields with the Data }
  If iDataMode = 1 Then fTop_Record
  Else fGetARec(fFilNo);
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := Null;
  pFillFields;
End;

Procedure TfrmBalance.F8KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (1) to Print to Screen }
  PrintReport(1);
End;

Procedure TfrmBalance.F7KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (2) to Print Reports }
  PrintReport(2);
End;

Procedure TfrmBalance.F6KeyExecute(Sender: TObject);
Begin
  { Call PrintReport in TFPrint.Pas, with a parameter of (3) to Print Reports to an ASCII file }
  PrintReport(3);
End;

Function TfrmBalance.fShowItemType(Const Item: byte): String;
Begin
  If ((Item > 0) And (Item <= 7)) Then
    fShowItemType := Pad(Copy(VoucherTypes[Item], 3, 14), 14)
  Else fShowItemType := '';
End;

Function TfrmBalance.fShowSubItemType(Const Item, SubItem: byte): String;
Begin
  If fValidVoucherSubItem(Item, SubItem) Then
    fShowSubItemType := Pad(Copy(VoucherSubTypes[Item, SubItem], 3, 20), 20)
  Else fShowSubItemType := '';
End;

Function TfrmBalance.fValidVoucherSubItem(Const Item, SubItem: byte): boolean;
Begin
  fValidVoucherSubItem := fValidVoucherItem(Item) And (SubItem > 0) And (SubItem <=
    Vsubtypes[Item]);
End;

Function TfrmBalance.fValidVoucherItem(Const Item: byte): boolean;
Begin
  fValidVoucherItem := (Item > 0) And (Item <= 7);
End;

Procedure TfrmBalance.FormClose(Sender: TObject; Var Action: TCloseAction);
Begin
  If frmWorkSheet.iFormTag = (Scrno - 1) Then frmWorkSheet.iFormTag := 1;
  liRecnoBAL := 0;
  If frmWorkSheet.bFTQuitUsed Then Begin
    { Close any open tables }
    fdBase.fCloseFiles;
    dBase.CloseFiles;
    Application.Terminate;
  End;
End;

Function TfrmBalance.fShowBalanceItem(Const Item: String): String;
Var
  i: byte;
Begin
  i := Ival(Item);
  If (i > 0) And (i <= BalItemtypes) Then
    fShowBalanceItem := Pad(Copy(BalItemHelp[Ival(Item)], 3, 20), 20)
  Else fShowBalanceItem := '';
End;

Function TfrmBalance.fCalcSponsorPreNet: real;
Begin
  With Ftinfo Do fCalcSponsorPreNet := Valu(Net) + Valu(Additions_total)
    - Valu(Expenses_total) - Valu(Deductions_total) - Valu(Exchange_acbl_total)
      - Valu(Exchange_td_total);
End;

Procedure TfrmBalance.pClearFields;
Begin
  OSFSanctionNo.Text := FTINFO.SANCTION_NO;
  // -------------------------------------------------
  ONFItemType.Text := '';
  LabelItemType.Caption := '';
  OSFDescription.Text := '';
  OPFItems.Text := '0';
  OPFItemRate.Text := '0';
  OPFAmount.Text := '0';
End;

Procedure TfrmBalance.pCopyRecData;
Begin
  { Used when Adding or Copying a Record }
  With FBALANCE Do Begin
    SANCTION_NO := Pad(OSFSanctionNo.Text,10);
    // -------------------------------------------------
//    ITEM_TYPE := ONFItemType.Text;
//    DESCRIPTION := OSFDescription.Text;
//    ITEMS := OPFItems.Text;
//    ITEM_RATE := OPFItemRate.Text;
//    AMOUNT := OPFAmount.Text;
    ITEM_TYPE := LeftPad(Trim(ONFItemType.Text),2);
    DESCRIPTION := Pad(OSFDescription.Text,25);
    ITEMS := LeftPad(Trim(OPFItems.GetStrippedEditString),6);
    ITEM_RATE := LeftPad(Trim(OPFItemRate.GetStrippedEditString),8);
    AMOUNT := LeftPad(Trim(OPFAmount.GetStrippedEditString),9);
  End;
End;

Procedure TfrmBalance.pCheckEditFields;
Begin
  { Used when Editing a Record }
  // ---------------------------------------------------------------------
  With FBALANCE Do Begin
    ITEM_TYPE := LeftPad(Trim(ONFItemType.Text),2);
    DESCRIPTION := Pad(OSFDescription.Text,25);
    ITEMS := LeftPad(Trim(OPFItems.GetStrippedEditString),6);
    ITEM_RATE := LeftPad(Trim(OPFItemRate.GetStrippedEditString),8);
    AMOUNT := LeftPad(Trim(OPFAmount.GetStrippedEditString),9);
  End;
End;

Procedure TfrmBalance.pSaveChanges;
Begin
  { Data Mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  Case iDataMode Of
    1, 3: Begin // Add or Copy Modes
        pCopyRecData;
        fdbase.fAdd_Record;
        liRecnoBAL := fDbase.fRecno[fFilno];
        pButtOn(True);
        Inc(iBalancesCount);
        pUpdateStatusBar;
        FixTotals(iDataMode);
      End;
    2: Begin // Edit Mode
        pCheckEditFields;
        bKeyIsUnique := fPostEditFixKeys;
        If Not bKeyIsUnique Then Begin
          If (MessageDlg('This Balance Item already exists in this database.', mtError,
            [mbOK], 0) = mrOK) Then Begin
            ONFItemType.SetFocus;
            Exit;
          End
          Else pAbortChanges;
        End
        Else Begin
          fdbase.fPutARec(fFilNo);
          FixTotals(iDataMode);
        End;
      End;
  End;
  pFillFields;
  iDataMode := 0;
  sDataMode := null;
//  pResetTabStops;
  { Turn the Player Info Data Area ON }
  pWorkSheetInfo(False);
End;

Procedure TfrmBalance.pButtOn(bTurnOn: Boolean);
Begin
  // ------------------------------------------------------------
  { Buttons }
  ButCopy.Enabled := bTurnOn;
  ButCopyAction.Enabled := bTurnOn;
  ButDelete.Enabled := bTurnOn;
  ButDeleteAction.Enabled := bTurnOn;
  ButEdit.Enabled := bTurnOn;
  ButEditAction.Enabled := bTurnOn;
  ButFind.Enabled := bTurnOn;
  ButFindAction.Enabled := bTurnOn;
  ButLast.Enabled := bTurnOn;
  ButLastAction.Enabled := bTurnOn;
  ButNext.Enabled := bTurnOn;
  ButNextAction.Enabled := bTurnOn;
  ButPrev.Enabled := bTurnOn;
  ButPrevAction.Enabled := bTurnOn;
  ButTop.Enabled := bTurnOn;
  ButTopAction.Enabled := bTurnOn;
  // ------------------------------------------------------------
  { Function Keys }
  F3Key.Enabled := bTurnOn;
  F4Key.Enabled := bTurnOn;
  F5Key.Enabled := bTurnOn;
  ButtonF6Key.Enabled := bTurnOn;
  F6Key.Enabled := bTurnOn;
  ButtonF7Key.Enabled := bTurnOn;
  F7Key.Enabled := bTurnOn;
  ButtonF8Key.Enabled := bTurnOn;
  F8Key.Enabled := bTurnOn;
  // ------------------------------------------------------------
End;

Procedure TfrmBalance.ONFItemTypeExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (ValidBalItem(Ival(ONFItemType.Text))) Then Begin
        MessageDlg('Invalid item type', mtError, [mbOK], 0);
        ONFItemType.Text := '';
        ONFItemType.SetFocus;
      End
      Else Begin
        FBALANCE.ITEM_TYPE := LeftPad(Trim(ONFItemType.Text),2);
        bDoEdit := CustomFinanceEdit(Scrno, 1, FBALANCE.ITEM_TYPE, False, bDisplay_Rec, sDataMode, Sender);
        If bDisplay_Rec Then pFillFields;
        plOnFieldLastStep(False, Sender, 'N');
      End;
    End;
  End;
End;

Procedure TfrmBalance.OPFItemsExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      FBALANCE.ITEMS := LeftPad(Trim(OPFItems.Text),6);
      bDoEdit := CustomFinanceEdit(Scrno, 4, FBALANCE.ITEMS, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Function TfrmBalance.fBalStat(Const vtype, mask: byte): boolean;
Begin
  fBalStat := (BalEditStat[vtype] And mask) <> 0;
End;

Procedure TfrmBalance.OSFDescriptionExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      FBALANCE.DESCRIPTION := Pad(OSFDescription.Text,25);
      bDoEdit := CustomFinanceEdit(Scrno, 3, FBALANCE.DESCRIPTION, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmBalance.OPFItemRateExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      FBALANCE.ITEM_RATE := LeftPad(Trim(OPFItemRate.Text),8);
      bDoEdit := CustomFinanceEdit(Scrno, 5, FBALANCE.ITEM_RATE, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmBalance.OPFAmountExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      FBALANCE.AMOUNT := LeftPad(Trim(OPFAmount.Text),9);
      bDoEdit := CustomFinanceEdit(Scrno, 6, FBALANCE.AMOUNT, False, bDisplay_Rec, sDataMode, Sender);
      If bDisplay_Rec Then pFillFields;
      plOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmBalance.ONFItemTypeEnter(Sender: TObject);
Begin
//  pResetTabStops;
  bDoEdit := True;
  FBALANCE.ITEM_TYPE := LeftPad(Trim(ONFItemType.Text),2);
  bDoEdit := CustomFinanceEdit(Scrno, 1, FBALANCE.ITEM_TYPE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'N');
End;

Procedure TfrmBalance.OSFDescriptionEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FBALANCE.DESCRIPTION := Pad(OSFDescription.Text,25);
  bDoEdit := CustomFinanceEdit(Scrno, 3, FBALANCE.DESCRIPTION, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmBalance.OPFItemsEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FBALANCE.ITEMS := LeftPad(Trim(OPFItems.Text),6);
  bDoEdit := CustomFinanceEdit(Scrno, 4, FBALANCE.ITEMS, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmBalance.OPFItemRateEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FBALANCE.ITEM_RATE := LeftPad(Trim(OPFItemRate.Text),8);
  bDoEdit := CustomFinanceEdit(Scrno, 5, FBALANCE.ITEM_RATE, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmBalance.OPFAmountEnter(Sender: TObject);
Begin
  bDoEdit := True;
  FBALANCE.AMOUNT := LeftPad(Trim(OPFAmount.Text),9);
  bDoEdit := CustomFinanceEdit(Scrno, 6, FBALANCE.AMOUNT, True, bDisplay_Rec, sDataMode, Sender);
  If bDisplay_Rec Then pFillFields;
  plOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmBalance.pFillStaticFields;
Var
  rVal: Real;
  sValue: String;
Begin
  OPFNetReceipts.Text := FTINFO.NET;
  OPFAdditions.Text := FTINFO.ADDITIONS_TOTAL;
  OPFSubTotal.Text := Fstr(Valu(FTINFO.net) + Valu(FTINFO.additions_total), 10, 2);
  OPFLessSponsorInvoice.Text := Fstr(-Valu(FTINFO.expenses_total), 10, 2);
  OPFLessDeductions.Text := Fstr(-Valu(FTINFO.deductions_total), 10, 2);
  // ----------------------------------------------------------------------------------------------
  { Only Visible if this is a Canadian Tournament, denoted by FTInfo.USE_EX_RATE being Yes }
  If UpperCase(FTInfo.USE_EX_RATE) = 'Y' Then Begin
    // -----------------------------------------------------------------------------------------
    OPFExchangeForTDExpenses.Text := Fstr(-Valu(FTINFO.exchange_td_total), 9, 2);
    Label17.Visible := True;
    Label18.Visible := True;
    OPFExchangeForTDExpenses.Visible := True;
    // -----------------------------------------------------------------------------------------
    OPFExchangeForACBLExpenses.Text := Fstr(-Valu(FTINFO.exchange_acbl_total), 9, 2);
    Label19.Visible := True;
    Label20.Visible := True;
    OPFExchangeForACBLExpenses.Visible := True;
  End
  Else Begin
    // -----------------------------------------------------------------------------------------
    OPFExchangeForTDExpenses.Text := '0';
    Label17.Visible := False;
    Label18.Visible := False;
    OPFExchangeForTDExpenses.Visible := False;
    // -----------------------------------------------------------------------------------------
    OPFExchangeForACBLExpenses.Text := '0';
    Label19.Visible := False;
    Label20.Visible := False;
    OPFExchangeForACBLExpenses.Visible := False;
  End;
  If UpperCase(Trim(FTINFO.EXCHANGE_RATE)) = 'Y' Then OfsItemtypes := (MaxOfsItemtypes - 2)
  Else OfsItemtypes := MaxOfsItemtypes;
  // ----------------------------------------------------------------------------------------------
  OPFNetToSponsor.Text := Fstr(fCalcSponsorPreNet, 10, 2);
  OPFCheckToAcblFromSponsor.Text := FTINFO.SPONSOR_CHECK;
  OPFAdjustedNetToSponsor.Text := FTINFO.SPONSOR_NET;
  // ----------------------------------------------------------------------------------------------
  OPFChecks.Text := FTINFO.CHECKS_TOTAL;
  OPFCash.Text := FTINFO.CASH_TOTAL;
  OPFOther.Text := FTINFO.OTHER_TOTAL;
  OPFTotalToSponsor.Text := FTINFO.MONEY_TOTAL;
  // ----------------------------------------------------------------------------------------------
  OPFTotalNeededToBalance.Text := Fstr(Valu(FTINFO.sponsor_net) - Valu(FTINFO.money_total), 10, 2);
  sValue := OPFTotalNeededToBalance.GetStrippedEditString;
  rVal := StrToFloat(sValue);
  { If there is an Amount needed to balance, show it in red }
  If (rVal > 0.00) Then OPFTotalNeededToBalance.Font.Color := clRed
  Else OPFTotalNeededToBalance.Font.Color := clBtnText;
End;

Procedure TfrmBalance.pBlankBalanceRecord;
Begin
  With FBALANCE Do Begin
    AMOUNT := '     0.00'; // STRING[9]; {N  2}
    BAL_OFF := ' '; // STRING[1]; {C  0}
    DATE_TIME := '    '; // STRING[4]; {C  0}
    DESCRIPTION := '                         '; // STRING[25]; {C  0}
    ITEMS := '   0.0'; // STRING[6]; {N  1}
    ITEM_RATE := '    0.00'; // STRING[8]; {N  2}
    ITEM_TYPE := ' 0'; // STRING[2]; {N  0}
    SANCTION_NO := '          '; // STRING[10]; {C  0}
  End;
End;

Procedure TfrmBalance.Action1Execute(Sender: TObject);
Begin
  pSetWinState;
End;

Procedure TfrmBalance.Action2Execute(Sender: TObject);
Begin
  pSetWinState;
End;

Procedure TfrmBalance.pSetWinState;
Begin
  If WindowState = wsMaximized Then WindowState := wsNormal
  Else WindowState := wsMaximized;
  frmWorkSheet.bWinStNorm := (WindowState = wsNormal);
  If frmWorkSheet.bWinStNorm Then frmWorkSheet.WindowState := wsNormal
  Else frmWorkSheet.WindowState := wsMaximized;
  pSetPanelWidth(frmBalance, StatusBar1);
End;

Procedure TfrmBalance.ResizeKit1ExitResize(Sender: TObject; XScale, YScale: Double);
Begin
  //  frmWorkSheet.bWinStNorm := (WindowState = wsNormal);
  pSetPanelWidth(frmBalance, StatusBar1);
End;

Procedure TfrmBalance.Action3Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmBalance.Action4Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Function TfrmBalance.FormHelp(Command: Word; Data: Integer; Var CallHelp: Boolean): Boolean;
Begin
  if data < 500000 then
    HtmlHelp(GetDesktopWindow,DefChmFile,HH_HELP_CONTEXT,Data);
//  If data < 500000 Then WinHelp(Application.Handle, 'ACBLSCORE.HLP', Help_Context, Data);
End;

//Procedure TfrmBalance.pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
//Begin
//  If UpperCase(sType) = 'S' Then pOSFieldAvail(bDoedit, (oSender As TOvcSimpleField))
//  Else If UpperCase(sType) = 'P' Then pOPFieldAvail(bDoedit, (oSender As TOvcPictureField))
//  Else If UpperCase(sType) = 'C' Then pOCBFieldAvail(bDoedit, (oSender As TOvcComboBox))
//  Else If UpperCase(sType) = 'D' Then pODEFieldAvail(bDoedit, (oSender As TOvcDateEdit))
//  Else If UpperCase(sType) = 'H' Then pLMDHCBFieldAvail(bDoedit, (oSender As TLMDHeaderListComboBox))
//  Else If UpperCase(sType) = 'N' Then pONFieldAvail(bDoedit, (oSender As TOvcNumericField))
//  Else MessageDlg('No Procedure created for that type!', mtError, [mbOK], 0);
//  // -------------------------------------------------------------------------------------------------------
//  If bDisplay_Rec Then pFillFields;
//  // -------------------------------------------------------------------------------------------------------
//  If bWasOnEnter Then Begin
//    If UpperCase(sType) = 'S' Then Begin
//      If Not (oSender As TOvcSimpleField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'P') Then Begin
//      If Not (oSender As TOvcPictureField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'D') Then Begin
//      If Not (oSender As TOvcDateEdit).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'H') Then Begin
//      If Not (oSender As TLMDHeaderListComboBox).TabStop Then
//        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'C') Then Begin
//      If Not (oSender As TOvcComboBox).TabStop Then
//        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End
//    Else If (UpperCase(sType) = 'N') Then Begin
//      If Not (oSender As TOvcNumericField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
//    End;
//  End;
//End;
//
//Procedure TfrmBalance.pONFieldAvail(bAvail: Boolean; oOSField: TOvcNumericField);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmBalance.pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoCaretToEnd, efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [efoCaretToEnd];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmBalance.pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.Options := [efoCaretToEnd, efoReadOnly];
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.Options := [efoCaretToEnd];
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmBalance.pOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmBalance.pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.ReadOnly := True;
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.ReadOnly := False;
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;
//
//Procedure TfrmBalance.pLMDHCBFieldAvail(bAvail: Boolean; oOSField: TLMDHeaderListComboBox);
//Begin
//  If Not bAvail Then Begin
//    { Disallow editing of the Field }
//    oOSField.ReadOnly := True;
//    oOSField.TabStop := False;
//  End
//  Else Begin
//    { Allow editing of the Field }
//    oOSField.ReadOnly := False;
//    oOSField.TabStop := True;
//    oOSField.Color := clWindow;
//  End;
//  Refresh;
//End;

{Procedure TfrmBalance.pResetTabStops;
Begin
  { Reset ALL editable fields tabstop to true so fields will be available for next action: Add/Edit/Copy }
{  ONFItemType.TabStop := True;
  OSFDescription.TabStop := True;
  OPFItems.TabStop := True;
  OPFItemRate.TabStop := True;
  OPFAmount.TabStop := True;
  OSFSanctionNumberB.TabStop := True;
End;}

End.

